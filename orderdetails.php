<?php 
include 'src/config.php';
if(!isset($_SESSION["uid"])){
	header("Location: login.php"); 
}
else{
$orderid=$_GET['orderid'];
?>
<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie ie9">
   <![endif]-->
   <html lang="en-US">
      
      <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
         
         <meta http-equiv="X-UA-Compatible" content="IE=edge">
         <meta name="viewport" content="width=device-width, initial-scale=1">
         <!-- Favicons-->
         <title>Grill guru</title>
         <link rel="apple-touch-icon" sizes="57x57" href="wp-content/themes/quickfood/img/apple-icon-57x57.png">
		 <link rel="apple-touch-icon" sizes="60x60" href="wp-content/themes/quickfood/img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="wp-content/themes/quickfood/img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="wp-content/themes/quickfood/img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="wp-content/themes/quickfood/img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="wp-content/themes/quickfood/img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="wp-content/themes/quickfood/img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="wp-content/themes/quickfood/img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="wp-content/themes/quickfood/img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="wp-content/themes/quickfood/img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="wp-content/themes/quickfood/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="wp-content/themes/quickfood/img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="wp-content/themes/quickfood/img/favicon-16x16.png">
<link rel="manifest" href="wp-content/themes/quickfood/img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="wp-content/themes/quickfood/img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<meta name="description" content="We are so honoured to introduce Grill Guru&#039;s updated website to deliver the best meals prepared daily fresh with the freshest ingredients..."/>
<link rel="canonical" href="https://grill-guru.co.uk/" />
<meta property="og:locale" content="en_GB" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Grill Guru Gril Store | Based In Glasgow" />
<meta property="og:description" content="We are so honoured to introduce Grill Guru&#039;s updated website to deliver the best meals prepared daily fresh with the freshest ingredients..." />
<meta property="og:url" content="https://grill-guru.co.uk/" />
<meta property="og:site_name" content="Grill Guru" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="We are so honoured to introduce Grill Guru&#039;s updated website to deliver the best meals prepared daily fresh with the freshest ingredients..." />
<meta name="twitter:title" content="Grill Guru Gril Store | Based In Glasgow" />
         <link rel='stylesheet' id='layerslider-css'  href='wp-content/plugins/LayerSlider/static/css/layerslider76f9.css?ver=5.6.8' type='text/css' media='all' />
         <link rel='stylesheet' id='ls-google-fonts-css'  href='http://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900%7COpen+Sans:300%7CIndie+Flower:regular%7COswald:300,regular,700&amp;subset=latin%2Clatin-ext' type='text/css' media='all' />
         <link rel='stylesheet' id='family-fonts-css'  href='https://fonts.googleapis.com/css?family=Lato%3A400%2C700%2C900%2C400italic%2C700italic%2C300%2C300italic&amp;ver=1.0.0' type='text/css' media='all' />
         <link rel='stylesheet' id='animate-css'  href='wp-content/themes/quickfood/css/animate.min66f2.css?ver=4.7.5' type='text/css' media='all' />
         <link rel='stylesheet' id='bootstrap-css'  href='wp-content/themes/quickfood/css/bootstrap.min66f2.css?ver=4.7.5' type='text/css' media='all' />
         <link rel='stylesheet' id='menu-css'  href='wp-content/themes/quickfood/css/menu66f2.css?ver=4.7.5' type='text/css' media='all' />
         <link rel='stylesheet' id='style-css'  href='wp-content/themes/quickfood/css/stylee199.css?ver=2016-08-04' type='text/css' media='all' />
         <link rel='stylesheet' id='responsive-css'  href='wp-content/themes/quickfood/css/responsive66f2.css?ver=4.7.5' type='text/css' media='all' />
         <link rel='stylesheet' id='elegant_font-css'  href='wp-content/themes/quickfood/css/elegant_font/elegant_font.min66f2.css?ver=4.7.5' type='text/css' media='all' />
         <link rel='stylesheet' id='fontello-css'  href='wp-content/themes/quickfood/css/fontello/css/fontello.min66f2.css?ver=4.7.5' type='text/css' media='all' />
         <link rel='stylesheet' id='magnific-popup-css'  href='wp-content/themes/quickfood/css/magnific-popup66f2.css?ver=4.7.5' type='text/css' media='all' />
         <link rel='stylesheet' id='pop_up-css'  href='wp-content/themes/quickfood/css/pop_up66f2.css?ver=4.7.5' type='text/css' media='all' />
         <link rel='stylesheet' id='base-css'  href='wp-content/themes/quickfood/css/base66f2.css?ver=4.7.5' type='text/css' media='all' />
         <link rel='stylesheet' id='blog-css'  href='wp-content/themes/quickfood/css/blog66f2.css?ver=4.7.5' type='text/css' media='all' />
         <link rel='stylesheet' id='cookiebar-css'  href='wp-content/themes/quickfood/css/jquery.cookiebar66f2.css?ver=4.7.5' type='text/css' media='all' />
         <link rel='stylesheet' id='grey-css'  href='wp-content/themes/quickfood/css/skins/square/grey66f2.css?ver=4.7.5' type='text/css' media='all' />
         <link rel='stylesheet' id='rangeSlider-css'  href='wp-content/themes/quickfood/css/ion.rangeSlider66f2.css?ver=4.7.5' type='text/css' media='all' />
         <link rel='stylesheet' id='skinFlat-css'  href='wp-content/themes/quickfood/css/ion.rangeSlider.skinFlat66f2.css?ver=4.7.5' type='text/css' media='all' />
         <link rel='stylesheet' id='slider-pro-css'  href='wp-content/themes/quickfood/css/slider-pro.min66f2.css?ver=4.7.5' type='text/css' media='all' />
         <link rel='stylesheet' id='style-theme-css'  href='wp-content/themes/quickfood/style4a68.css?ver=2016-08-05' type='text/css' media='all' />
         <link rel='stylesheet' id=''  href='css/acdn.css' type='text/css' media='all' />
         <script type='text/javascript' src='wp-content/plugins/LayerSlider/static/js/greensockcd11.js?ver=1.11.8'></script>
         <script type='text/javascript' src='wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
         <script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1'></script>
         <script type='text/javascript' src='wp-content/plugins/LayerSlider/static/js/layerslider.kreaturamedia.jquery76f9.js?ver=5.6.8'></script>
         <script type='text/javascript' src='wp-content/plugins/LayerSlider/static/js/layerslider.transitions76f9.js?ver=5.6.8'></script>
         <script type='text/javascript' src='wp-content/themes/quickfood/js/ajax2e67.js?ver=2016-08-06'></script>
		 
<body class="page-template page-template-template_part page-template-payment-success page-template-template_partpayment-success-php page page-id-279 wpb-js-composer js-comp-ver-4.11.2 vc_responsive">

    <!--[if lte IE 8]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a>.</p>
    <![endif]-->
    
    <div id="preloader">
        <div class="sk-spinner sk-spinner-wave" id="status">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
    </div><!-- End Preload -->
    
    <!-- Header ================================================== -->
   <?php 
   include("header.php");
   ?>
	<!-- End Header =============================================== -->
<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="" data-natural-width="1400" data-natural-height="350" style="background: #000000fa;height: 51px;">
  
</section><!-- End section -->
<!-- End SubHeader ============================================ -->

<div id="position">
    <div class="container">
        <ul class="ec-breadcrumb"><li><a rel="v:url" property="v:title" href="index.php">Home</a></li><li class="active">Order Details</li></ul>    </div>
</div><!-- Position -->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<!-- Content ================================================== -->

<div class="container">
<div class="row">
<div class="admin-content-wrapper">
									<h2 class="inner">Order Status </h2>
					  <a href="orderhistory.php" class="pull-right btn btn-primary">Back to Home</a>
					  <table class="table table-striped jambo_table bulk_action">                      
                      <tbody>		
                      <?php 
	                    $msql="select * from orders where id='$orderid'";
	                    $msql1 = mysqli_query($connection,$msql);
						$ss1="select * from orders where id='$orderid'";
						$ssql1 = mysqli_query($connection,$ss1);
						$ss=$ssql1->fetch_array();
						$sql1=$msql1->num_rows;
						$oooid=$ss['orderno'];
						$userid=$ss['person_id'];
						$userql="select * from user_details where user_id='$userid'";
						$usersql1=mysqli_query($connection,$userql);
						$usersql2=$usersql1->fetch_array();
						$deliveryadd="select * from delivery_add where orderno='$oooid'";
						$deliveryadd1=mysqli_query($connection,$deliveryadd);
						$deliveryadd2=$deliveryadd1->fetch_array();
						$address = $deliveryadd2['address'];
						
						$usql="select * from user_location where orderno='$oooid'";
						$usqlq=mysqli_query($connection,$usql);
						$usql1=$usqlq->fetch_array();
						$did=$ss['driver_id'];
						$dsql="select * from driverlocation where driver_id='$did'";
						$dsqlq=mysqli_query($connection,$dsql);
						$dsql1=$dsqlq->fetch_array();
        
						if($sql1>0){
						$msql2=$msql1->fetch_array();
                        ?>
					 		<tr><td>Order ID #: </td><td><?php echo $msql2['orderno']; ?></td></tr>
							<tr><td>Name: </td><td><?php echo $usersql2['name'];?></td></tr>
							<tr><td>Email: </td><td><?php echo $usersql2['email'];?></td></tr>
							<tr><td>Phone: </td><td><?php echo $usersql2['mobile'];?></td></tr>
							<tr><td>Address: </td><td><?php echo $address;?></td></tr>							
							<tr><td>Total Amount: </td><td>&pound; <?php echo $msql2['total_amount']; ?></td></tr>
							<tr><td>Order Created: </td><td><?php echo $msql2['created']; ?></td></tr>
							<tr><td>Driver Status: </td><td>
							    <?php if($msql2['driver_id']=='0')
							    {
							        echo "Driver Not Assigned";
							    }
							    else{
							         echo "Driver Assigned";
							    }
							    ?>
							</td>
							<tr><td>Order Status: </td><td>
							    <?php if($msql2['status']=='2')
							    {
							        echo "Order Accepted";
							    }
							    else if($msql2['status']=='7'){
							         echo "Order in kitchen";
							    }
							    else if($msql2['status']=='1'){
							         echo "Order Accepted and Driver Assigned";
							    }
							     else if($msql2['status']=='5'){
							         echo "order Prepared Ready to Deliver";
							    }
							     else if($msql2['status']=='4'){
							         echo "Order With Driver";
							    }
							       else if($msql2['status']=='3'){
							         echo "Order Completed";
							    }
							    else if($msql2['status']=='6'){
							         echo "Order Rejected By Restaurent";
							    }
							    else if($msql2['status']=='0'){
							         echo "Order Canceled By Customer";
							    }
							    ?>
							</td></tr>
							<?php } else {?>
						<tr><td>No orders Available !!! </td><td></td></tr>
						<?php }?>
												</tbody>
					</table>
					
<br>

</div>
<!-- End container -->
<!-- End Content =============================================== -->
  <?php 
		 include("footer.php");
		 ?>

    <!-- End Footer =============================================== -->
<div class="layer"></div><!-- Mobile menu overlay mask -->
<!-- COMMON SCRIPTS -->

<script type='text/javascript' src='wp-content/themes/quickfood/js/common_scripts_min66f2.js?ver=4.7.5'></script>
<script type='text/javascript' src='wp-content/themes/quickfood/js/functions66f2.js?ver=4.7.5'></script>
<script type='text/javascript' src='wp-content/themes/quickfood/assets/validate66f2.js?ver=4.7.5'></script>

<script type='text/javascript' src='wp-content/themes/quickfood/js/infobox66f2.js?ver=4.7.5'></script>
<script type='text/javascript' src='wp-content/themes/quickfood/js/jquery.sliderPro.min66f2.js?ver=4.7.5'></script>
<script type='text/javascript' src='wp-content/themes/quickfood/js/cat_nav_mobile66f2.js?ver=4.7.5'></script>
<script type='text/javascript' src='wp-content/themes/quickfood/js/ion.rangeSlider66f2.js?ver=4.7.5'></script>
<script type='text/javascript' src='wp-content/themes/quickfood/js/theia-sticky-sidebar66f2.js?ver=4.7.5'></script>
<script type='text/javascript' src='wp-content/themes/quickfood/js/custom-main66f2.js?ver=4.7.5'></script>
<script type='text/javascript' src='wp-includes/js/wp-embed.min66f2.js?ver=4.7.5'></script>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});
</script>

</body>
</html>
<?php } ?>