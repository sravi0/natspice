<?php 
include_once 'src/config.php'; 
$uid=$_SESSION['uid'];
$_SESSION = array();
unset($_SESSION['uid']);
unset($_SESSION['userid']);
unset($_SESSION['orderno']);
unset($_SESSION["carttoken"]);
unset($_SESSION);
session_destroy();

session_start();
//$_SESSION['success'] = "Successfully Logged Out!";
header("location: menu.php");
exit;

?>