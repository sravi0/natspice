<?php
session_start();
//$_SESSION['cartItems'];




      if(isset($_GET['unset'])){
      unset($_SESSION['cartItems']);
      }

      function d($d){
         echo '<pre>';
         print_r($d);
         echo '</pre>';
      }
      //d($_SESSION);
      $addItems = array();
      if(isset($_POST['Items']) && $_POST['Items']!=''){
         $mainItems = json_decode($_POST['Items']);
         if(isset($_POST['addOns']) && count($_POST['addOns'])>0) { $addItems = json_decode($_POST['addOns']); } 

         $addItems = array(0=>array($mainItems, $addItems));


         if(!isset($_SESSION['cartItems'])){
            $_SESSION['cartItems'] = $addItems;
         } else {
            $_SESSION['cartItems'] = array_merge($_SESSION['cartItems'], $addItems);
         }
      }

      if(isset($_POST['action'])) {
         if($_POST['action']=='update') {
            if($_POST['qty']<=0){
               unset($_SESSION['cartItems'][$_POST['id']]);
            } else {
               $_SESSION['cartItems'][$_POST['id']][0][3] = $_POST['qty'];
            }
         }
         if($_POST['action']=='note') {
            $_SESSION['cartItems'][$_POST['id']][0][4] = $_POST['qty'];
         }
         if($_POST['action']=='del_type') {
            $_SESSION['del_type'] = $_POST['qty'];
         }
      } 
$totPrice = 0;
if(isset($_SESSION['cartItems']) && count($_SESSION['cartItems'])>0)
{
      //d(array_filter($_SESSION['cartItems']));
      $allItems = array_filter($_SESSION['cartItems']); 
      //d($allItems);
      ?>
      
      <div class="card-body p-2">
      <div class="mb-1 bg-white p-1 clearfix">
         <ul class="list-group list-group-flush">
         <?php
         foreach($allItems as $k => $val) { 
            
            $itemPrice = $val[0][2] * $val[0][3];
            $totPrice = $totPrice + $itemPrice;
            ?>
         
            <li class="list-group-item" style="padding:2px 0px;">
                  <div class="row mb-1">
                     <span class="col-10"><?php echo $val[0][3]; ?>x <b><?php echo $val[0][1]; ?></b></span>
                     <span class="badge badge-primary badge-pill col-2">£ <?php echo number_format($itemPrice, 2, '.', ''); ?></span>
                  </div>  
                     <?php 
                     $listAddons = array();
                     foreach($val[1] as $k2 => $val2) {
                        
                        $addOn = explode(',',$val2);  
                        $totPrice = $totPrice + $addOn[3];
                        $listAddons[] = $addOn[2]; 
                     }
                     $addonList = implode(' &#9679; ', $listAddons);
                     if($addonList!='') { ?>     
                        <div class="row">
                        <div class="col-12">
                           <i class="icofont-clip"> </i> <i class="itemNotes"> <?php echo $addonList; ?></i>
                        </div>
                        </div>
                     <?php   } 
                     if($val[0][4]!='') { ?>  
                        <div class="col-12">
                           <i class="icofont-ui-edit"> </i> <i class="itemNotes"> <?php echo $val[0][4]; ?></i>
                        </div>
                     <?php   }   ?>
                     <div class="row text-red text-right"><a onClick="updateCart(<?php echo $k; ?>, 1, 'remove');"><u><i style="font-style: initial;
    font-weight: 800;">Remove</i></u></a></div>
                     <hr style="color: #acacac;height: .5px;">
            </li>
           
            <?php
            foreach($val[1] as $k2 => $val2) {
                        
               $addOn = explode(',',$val2);  
               $totPrice = $totPrice + $addOn[3];
            }
         }  
         $ser_fee = 0.55;
         $del_fee = 2.00;
         $disc_fee = 0;
         $subTot = $totPrice;
         $grandTot = $subTot + $del_fee - $disc_fee;
         ?>
         
         </ul>
         
            
                       
                        <h6 class="font-weight-bold mb-0"> Total  <span class="float-right">£ <?php echo number_format($subTot, 2, '.', ''); ?></span></h6>
                     <br>

                     <a href="checkout.php" class="btn btn-success btn-block btn-lg">Checkout <i class="icofont-long-arrow-right"></i></a>
                     
                     </div>

                     
         
      </div>
      <?php
}
else {
   ?>
   <center style="margin-top:12%;"><img src="https://grill-guru.co.uk/newdev/img/empty-cart.png" width="200" /><br><p>Your cart is empty<br>Please add items</p></center>
   <?
}
?>