<?php session_start();
//$_SESSION['cartItems'];
$linkID = '';
if(!isset($_SESSION['del_type'])) { $_SESSION['del_type']='delivery'; }
?>

<?php
function getAPI() {
  $graph_url= 'https://deliveryguru.co.uk/dg_api/menu_submenu/47';
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $graph_url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  $output = curl_exec($ch); 
  curl_close($ch);
  return json_decode($output, true); 
}
$getItems = getAPI();


function getHotelAPI() {
  $graph_url= 'https://deliveryguru.co.uk/dg_api/getRestaurantsDetails/47';
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $graph_url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  $output = curl_exec($ch); 
  curl_close($ch);
  return json_decode($output, true); 
}
$getHotelDetails = getHotelAPI();
$_SESSION['discountper']=$getHotelDetails[0]['discount'];

?>  
<!DOCTYPE html>
<html>
  <head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
           <title>Grill Guru Menu | Online Food Delivery Services In Glasgow</title>
        <meta name="description" content="Order food online in Grill Guru and get it delivered to your doorstep. Here is the best food delivery service get an offer, discount and deals for the weekend."/>
<meta name="keywords" content="grill guru glasgow menu, food delivery services glasgow, bbq food near me, barbecue restaurants near me, hamburger restaurant, grill guru glasgow, online food delivery, bbq delivery, takeaway food near me"/>

    <meta property="og:title" content="Food Delivery Services|Online Food Delivery|Grill Guru"/>
	<meta property="og:type" content="website"/>
	<meta property="og:url" content="https://www.grill-guru.co.uk/menu.php">
	<meta property="og:image" content="https://www.grill-guru.co.uk/img/grill-guru-full-landingLogo.png"/>
	<meta property="og:site_name" content="grill-guru Menu"/>
	<meta property="og:description" content="Order a food online in Grill Guru and Get it delivered to your door step .Here is a best food delivery services Get a offer ,discount for the weekend and occasions"/>
       
         <link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
     <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
     
       <!--  <link href="vendor/select2/css/select2.min.css" rel="stylesheet">-->
         <!-- Custom styles for this template
         <link href="css/osahan.css" rel="stylesheet">
         <link href="vendor/icofont/icofont.min.css" rel="stylesheet">-->
         <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
         <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.1/font/bootstrap-icons.css">
         <!-- CSS only -->
       <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    <!--<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap" rel="stylesheet">
      <link href="https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b598ebab05857545f242caf2b4b48c/dist/semantic.min.css" rel="stylesheet" type="text/css" />-->
<!--<script src="https://code.jquery.com/jquery-2.1.4.js"></script>
<script src="https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b598ebab05857545f242caf2b4b48c/dist/semantic.min.js"></script>-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="menu-src/menu.min.css" rel="stylesheet">
  <style>
    .card-header { padding: 0.75rem 1.25rem 0 1.25rem; }
    .count-number .btn { border: none; }
    .count-number-input { border: none; margin-left: 10px; }
    .count-number .bi { font-size: 30px;}
    .btn-number { border: none !important; }
    .inc_btn {
    font-size: 20px; color: #757070;}
.btn-outline-secondary:hover { background: none !important }
.form-control:disabled, .form-control[readonly] {  margin-top: 2px; font-size: 15px; }
.btn.focus, .btn:focus { box-shadow: none; }
.txtDiv { padding: 1rem 1rem 0 1rem; width: 75%; }
.restaurant-detailed-header  { position: absolute; top: 60px; left: 50%;
    transform: translate(-50%, -50%);     top: 22%; }
  .restaurant-detailed-header img { height: 80px;  }
  .redbg,.redbg:hover { background: red !important; color: white; border: none !important; }
  .navbar-light .navbar-nav .nav-link { color: #000; } 
  .osahan-nav .nav-link {
    font-weight: 600;
    font-size: 12px;
}
@media (min-width: 1400px) {
.container, .container-lg, .container-md, .container-sm, .container-xl, .container-xxl {
    max-width: 1320px;
}}
h1, h2, h3, h4, h5 {font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif; line-height: 1.2857em;font-weight: 700; padding: 0; }
h5 { font-size: 1rem; }
.h2, h2 { font-size: 1.714rem;}
  </style>
<script>
<?php
$ids = array_column($getItems, 'id');
//print_r($ids);

?>
//const catIDs = [174, 147, 1997, 182];
const catIDs = [<?php echo implode(',', $ids); ?>];
//const catIDs = [];
let fLen = catIDs.length;

var allItems = [];

window.onload = function(){
  
  var f = (function(){
      var xhr = [], i;
      for (let i = 0; i < fLen; i++) {
          (function(i){
              var j = catIDs[i];
            
              const xhr = new XMLHttpRequest();
              xhr.open('GET', 'api-test.php?catId='+j);
              xhr.responseType = 'json';
              xhr.onload = function(e) {
                console.log(this.status);
                if (this.status == 200) {
                  //console.log('response', this.response); // JSON response  
                  allItems[j] = this.response;
                }
              };
              xhr.send();
          })(i);
      }
  })();

};

// PHP [closure.php]
//echo "Hello Kitty -> " . $_GET["data"];

</script>
</head>


  <body class="unselectable">
  <div id="snackbar"></div>

<!-- Menu addon Modal -->
<div class="modal fade" id="addonModal" tabindex="-1" aria-labelledby="addonModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addonModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="font-size: 36px;font-weight: 400;">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="itemDataModal" id="itemDataModal"></div>
        <div class="linear-background"></div>
          <div class="linear-background"></div>
          <div class="linear-background"></div>
        <div class="Addons accordionMenu" id="AddOns">
        </div>
      </div>
      <div class="alert alert-danger" role="alert"></div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<!-- Closed hotel Modal -->
<div class="modal fade" id="closedModal" tabindex="-1" role="dialog" aria-labelledby="closedModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
      <div class="modal-header">
              <h5 class="modal-title" style="text-align:center;">CLOSED NOW!</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closemodal()">
                <span aria-hidden="true">&times;</span>
              </button>
      </div>
      <div class="row no-gutters">
        <div class="col-md-6 ">
          <div class="modal-body1 p-0 text-center d-flex1">
              <img src="img/closepopimg.jpg" style="width:100%;">
          </div>
        </div>
        <div class="col-md-6 ">
          <div class="modal-body1 p-2 d-flex align-items-center">
            <div class="text w-100 text-center">
                
              <h2 class="mb-0">Pre Order</h2>  <br>
              <form action="#" class="code-form">
                <div class="form-group p2">
             
                <span class="col-4 p-1">
                Date:<br>
                                    <div class="ui calendar" id="preorderdateformat1" style="">
                            
    <div class="ui input left icon" style="width: 100%;">
      <i class="calendar icon"></i>
      <input type="text" id="pre_date" class="form-control"   placeholder="Schedule Date and Time" style="width: 100% !important; background:white; " readonly/>
    </div>
  </div>
               
                Time:<br> <select id="pre_time" class="form-control">     
                <option value="">Please Select Time</option>
                <option value="17:00">17:00</option>
                <option value="18:00">18:00</option>
                <option value="19:00">19:00</option>
                <option value="20:00">20:00</option>
                <option value="21:00">21:00</option>
                <option value="22:00">22:00</option>
                <option value="23:00">23:00</option>
                <option value="24:00">24:00</option>
                <option value="01:00">01:00</option>
                <option value="02:00">02:00</option>
                <option value="03:00">03:00</option>
                <option value="04:00">04:00</option>
                <option value="05:00">05:00</option></select>
                </div>
                <div class="alert alert-danger" role="alert"></div>
              </form>
              
              <a class="btn btn-primary d-block py-3 redbg" onClick="PreOrder();" style="color:#fff;">Pre Order</a>
            </div>
          </div>
         </div>
        </div>
      </div>
    </div>
</div>
<?php include "header3.php"; ?>
<section class="restaurant-detailed-banner mobileonly" style="margin-top: -80px;">
         <div class="text-center">
            <img class="img-fluid cover" src="img/Banner.jpg">
         </div>
         <div class="restaurant-detailed-header">
            <div class="container" style="max-width:1600px;">
               <div class="row d-flex align-items-end">
                <img class="img-fluid mr-3 float-left" alt="osahan" src="https://grill-guru.co.uk/img/grill-guru-full-landingLogo.png">
               </div>
            </div>
         </div>
      </section>  
      
    <main>
    <!--<span class="float">
      <i class="cart icon"></i>
      <span class="badge rounded-pill badge-notification bg-danger">0</span>
    </span>  -->
   
        <article class="article">
          <div class="banner-new desktoponly" style="margin-top: 55px; padding: 10px;">
            <img src="img/Banner.jpg" style="width:100%; border-radius:15px;"  />
              <div class="row d-flex" style="position: relative;">
                      <div class="col-md-12" style="margin-top: -140px; ">
                        <div class="">
                            <img class="img-fluid mr-3 pl-2" id="bannerLogo" alt="osahan" src="https://grill-guru.co.uk/img/grill-guru-full-landingLogo.png">
                        </div>
                      </div>
                  </div>
          </div>
         <div class="sticky-div"> 
                  <div class="scroller scroller-left"><i class="arrowPrev"></i></div>
               <div class="scroller scroller-right"><i class="arrowNext"></i></div>
               <div class="wrapper">
               <ul class="nav nav-tabs list cart-menu" id="myTab">
                <?php
                foreach($getItems as $k => $val) {
                    echo '<li><a data-id="'.$val['id'].'" id="anchorTag'.$val['id'].'" class="nav-item">'.$val['name'].'</a></li>';
                }
                ?>
                </ul>
               </div>
               </div>
      
      <div id="panel_groups" class="">
        
      <div class="discount-banner">
          <!--<img src="img/banner.jpeg" style="width:100%" />-->
         
          </div>

                           <?php
                           foreach($getItems as $k => $val) {
                              //echo '<li><a href="#'.$val['id'].'" id="anchorTag'.$val['id'].'" class="nav-item">'.$val['name'].'</a></li>';
                              //print_r($val); exit;
                           ?>
                           <div class="item-section" id="<?php echo $val['id']; ?>">
                                <h3 class="mb-4 mt-3 col-md-12" style="text-transform: capitalize;"><?php echo $val['name']; ?> <small class="h6 text-black-50"></small></h3>
                                 <div class="menusWrapper">
                                 <?php foreach($val['value'] as $k1 => $v1) { ?>
                                    <div class="menuInner "  data-role="recipe">
                                    <div class="card" style="cursor:pointer;" onclick="getHotelStatus(<?php echo $k; ?>, <?php echo $k1; ?>, <?php echo $v1['id']; ?>, <?php echo $val['id']; ?>);">
                                    <?php 
                                          if($v1['image'] == 'dummy.jpg') { 
                                              $itemImg ="menu-src/noimage.jpg"; 
                                              $divClass = 'w-100';
                                          } else { 
                                              $itemImg ="https://deliveryguru.co.uk/admin/images/itemimages/" .  $v1['image']; 
                                              $divClass = ''; $imgClass = 'w-50';
                                          }
                                          if(isset($_GET['img'])) {
                                            $itemImg ="menu-src/noimage.jpg"; 
                                          } 
                                         // $divClass =  $imgClass = 'w-50';
                                          ?>
                                        <div class="card-body d-flex p-0">
                                            <div class="txtDiv <?php echo $divClass; ?>">
                                            <h6 class="mb-1 item-tit"><a class="text-black" style="text-transform: capitalize;"><?php echo $v1['item_name']; ?></a></h6>
                                            <p class="text-gray mb-2 item-desc" style="text-transform: capitalize;"><?php echo $v1['item_desc']; ?></p>
                                            <p class="text-gray time">
                                                 <?php 
                                                 if($v1['price']!=0){
                                                 ?>
                                                <span style="font-weight: 700; font-size: 16px;" class="text-dark rounded-sm pl-0 pb-1 pt-1 pr-2">£<?php echo $v1['price']; ?></span> 
                                            <?php } else {?>
                                            <span style="font-weight: 900;" class="text-dark rounded-sm pl-0 pb-1 pt-1 pr-2"></span> 
                                            <?php }?>
                                             </p>
                                              <?php 
                                          if($v1['discount'] != '0' || $v1['discount'] != 0 ) {  ?>
                                              <div class="list-card-badge">
                                                <span class="badge badge-info">OFFER</span> 
                                                <small><?php echo $v1['discount']; ?> off </small> 
                                             </div> 
                                             <?php  }
                                          ?>
                                             
                                            </div>
                                            <?php if($v1['image'] != 'dummy.jpg') { ?>
                                            <div class="text-right">
                                              <img class="" src="<?php echo $itemImg; ?>" alt="sans" width="150px" height="120">
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                       <!---->
                                 </div>
                              <?php } ?>
                              </div>
                              </div>
                           <?php } ?>
                           
                        </div>

      </article>
     
      <aside class="sidebar desktoponly">
        
        <div class="cartItems shadow component mt-1">
          <div class="card-header">
                <h5 class="text-center">Your Order</h5>
          </div>
              <div class="btn-group btn-toggle">
                <button class="btn btn-lg btn-default <?php if(!isset($_SESSION['del_type']) || (isset($_SESSION['del_type']) && $_SESSION['del_type']=='delivery')) { ?> active<?php } ?>" id="delivery" data-id="delivery" >Delivery <small id="del_sm">In 40 min</small></button>
                <button class="btn btn-lg btn-default <?php if(isset($_SESSION['del_type']) && $_SESSION['del_type']=='collection') { ?>active<?php } ?>" id="collection" data-id="collection" >Collection <small  id="col_sm">In 15 min</small></button>
              </div>

           <div class="footer2 p-2">
              <div id="cartItems_section"></div>
          </div>
        </div>
      </aside>


    </main>
   <footer>
          
          <div class="container">
              <div class="footerWrap col-md-12 col-md-offset-1">
                  <div class="copyright col-md-12">
                      <p style="color: #fff;font-size: 14px;">Copyright © 2020 <a href="https://deliveryguru.co.uk/" target="_blank" style="color:#e81818;">DeliveryGuru</a> All rights reserved.</p>
                  </div>
              </div>
          </div>
      </footer>
                          
  </body>
 
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/pikaday/pikaday.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/pikaday/css/pikaday.css">
<script src="menu.min.js"></script>
<script>
$('input[type=search]').on('search', function () {
  $('#searchinput, .search-mob').val('').change();
});
window.onscroll = function() { fixedLogo() };
  
var header = document.getElementById("bannerLogo");
var sticky = header.offsetTop;
function fixedLogo() {
    if (window.pageYOffset > 100) {
      header.classList.add("stickyLogo");
      document.getElementById("headerText").style.marginLeft = '20px';
    } else {
      header.classList.remove("stickyLogo");
      document.getElementById("headerText").style.marginLeft = '-30px';
    }
}
$(document).on('click', "input:radio", function(){
    var parent = $(this).closest(".boxes").nextAll(".minY").eq(0).attr('id');
    if(typeof(parent) !== "undefined") {
      window.location.hash = "";
      window.location.hash = '#'+parent;
    }
});
var today = new Date();

const picker = new Pikaday({
    field: document.getElementById('pre_date'),
    firstDay: 1,
    minDate: new Date(),
    maxDate: new Date(today.getFullYear(), today.getMonth(), today.getDate() + 5),
    yearRange: [2021,2021],
    onSelect: date => {
      const year = date.getFullYear()  ,month = date.getMonth() + 1 ,day = date.getDate(),formattedDate = [ day < 10 ? '0' + day : day, month < 10 ? '0' + month : month, year].join('/');
      document.getElementById('pre_date').value = formattedDate
  }
});

var getItems = <?php echo json_encode($getItems); ?>;
var pre_order_status = 0;
var date = new Date();
date.setDate(date.getDate());
</script>
<script>

function getHotelStatus(mId, arId, iId, cId) {

   if(pre_order_status!==0) {
      showAddOns(mId, arId, iId, cId);
   } else {
      var d = new Date();
      var n = d.getDay();
      var openhours='17:00';
      var closehour='05:00';
      var midtime="00:00";
      var event = new Date().toLocaleTimeString('en-GB', { timeZone: 'Europe/London' ,hour: 'numeric',minute: 'numeric', hour12: false});
   //alert(event);
  if(event<openhours && event>closehour){
        <?php if(!isset($_SESSION['pre_order'])) { ?>closedMsg();  <?php } else { ?>  pre_order_status=1; showAddOns(mId, arId, iId, cId); <?php }  ?>
     }
    else{
        showAddOns(mId, arId, iId, cId);
    }
    }

}

$(".dropdown-menu, .mobCartBody").click(function(e){
   e.stopPropagation();
})
//getHotelStatus();
    $(document).ready(function() {
      
        var openhours='17:00';
        var closehour='05:00';
        var midtime="00:00";
        var event = new Date().toLocaleTimeString('en-GB', { timeZone: 'Europe/London' ,hour: 'numeric',minute: 'numeric', hour12: false});
        if(event<openhours && event>closehour){
          $('#del_sm').html('Open at 17:00');
          $('#col_sm').html('Open at 17:00');
        }
          $('body').bind('cut copy', function(e) {
              e.preventDefault();
            });
        $("body").on("contextmenu", function(e) {
              return false;
            });
            window.ondragstart = function() { return false; } 
        });
  refreshCart();    
  var hidWidth;
var scrollBarWidths = 40;

var widthOfList = function(){
 var itemsWidth = 0;
 $('.list li').each(function(){
   var itemWidth = $(this).outerWidth();
   itemsWidth+=itemWidth;
 });
 return itemsWidth;
};

var widthOfHidden = function(){
 var t = (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
 console.log('widthOfHidden: '+(($('.wrapper').outerWidth())+','+widthOfList()+','+getLeftPosi())+','+scrollBarWidths+','+t);
 return (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
};

var getLeftPosi = function(){
 return $('.list').position().left;
};

var reAdjust = function(){
 if (($('.wrapper').outerWidth()) < widthOfList()) {
   $('.scroller-right').show();
 }
 else {
   $('.scroller-right').show();
 }
 
 if (getLeftPosi()<0) {
   $('.scroller-left').show();
 }
 else {
   $('.scroller-left').hide();
 }
 if(getLeftPosi()==0){
  $('.scroller-left').fadeOut('slow');
 }
}

reAdjust();

$(window).on('resize',function(e){  
   reAdjust();
});

$('.scroller-right').click(function() {
    $('.scroller-left, .scroller-right').hide();
    $('.scroller-left').fadeIn('slow');
    //var t = (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
    var t1 = -1 * (getLeftPosi()-($('.wrapper').outerWidth())) + ($('.wrapper').outerWidth());
    if(t1>Math.round(widthOfList())) {
      $('.list').animate({left: "-"+((Math.round(widthOfList())+20)-$('.wrapper').outerWidth())+"px"}, 'slow'); 
      $('.scroller-right').fadeOut('slow');
    } else {
      $('.list').animate({left:"-="+($('.wrapper').outerWidth()   )+"px"},'slow',function() {
            if(($('.wrapper').outerWidth()) >= (widthOfList()+getLeftPosi())){
                $('.scroller-right').fadeOut('slow');
            } else {
              $('.scroller-right').fadeIn('slow');
            }
      });
    }
});

$('.scroller-left').click(function() {
  $('.scroller-left, .scroller-right').hide();
  $('.scroller-right').fadeIn('slow');
  var t1 =  getLeftPosi() + $('.wrapper').outerWidth() - 40;
  //alert(t1);
  if(t1==0 || t1>0) {
    $('.list').animate({left: "+0px"}, 'slow'); 
    $('.scroller-left').fadeOut('slow');
  } else {
    $('.list').animate({left:"+="+($('.wrapper').outerWidth())+"px"},'slow',function(){
      var t = (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
      //alert(getLeftPosi());
      if(getLeftPosi()==0){
          $('.scroller-left').fadeOut('slow');
      } else {
          $('.scroller-left').fadeIn('slow');
      }
    });
  }
});   


$('.cart-menu a').on('click', function() {
   var scrollAnchor = $(this).attr('data-id');
   scrollPoint = $('#panel_groups #' + scrollAnchor).offset().top - 70 - 50;
   $('body,html').animate({
       scrollTop: scrollPoint
   }, 500);
   return false;
})

$(window).scroll(function() {
   var windscroll = $(window).scrollTop();
   if (windscroll >= 100) {
       $('#panel_groups .item-section').each(function(i) {
           if ($(this).position().top <= windscroll + 150) {
               $('.cart-menu a.active').removeClass('active');
               $('.cart-menu a').eq(i).addClass('active');
           }
       });

   } else {
       $('.cart-menu a.active').removeClass('active');
       $('.cart-menu a:first').addClass('active');
   }

   var leftOffset = $('.cart-menu a.active').offset().left - $('.cart-menu').offset().left +   $('.cart-menu').scrollLeft();
   var scrollPos = leftOffset / (($('.wrapper').outerWidth())-40);
   var calWid = Math.round(widthOfList()) - $('.wrapper').outerWidth();

  console.log(''+$('.wrapper').outerWidth()+'_'+Math.round(widthOfList())+'_'+Math.round(getLeftPosi())+'_'+Math.round(scrollBarWidths)+'_'+parseInt(scrollPos)+'_'+leftOffset+'_'+parseInt(calWid));

  
    if(parseInt(scrollPos)>0) 
    {
      if((parseInt(leftOffset)+parseInt(calWid)+40)>Math.round(widthOfList())) 
      {
        $('.list').animate({left: "-"+((Math.round(widthOfList())+20)-$('.wrapper').outerWidth())+"px"}, 0); 
        $('.scroller-left').fadeIn('slow');
        $('.scroller-right').fadeOut('slow');
      } else {
        setPos = (parseInt(scrollPos) * ($('.wrapper').outerWidth()));
        $('.list').animate({left: "-"+(setPos)+"px"}, 0); 
        if(($('.wrapper').outerWidth()) >= (widthOfList()+getLeftPosi())) {
            $('.scroller-left').fadeIn('slow');
            $('.scroller-right').fadeOut('slow');
        }
      }
    } else {
        setPos = parseInt(scrollPos) * ($('.wrapper').outerWidth());
        $('.list').animate({left: "+"+setPos+"px"}, 0); 
        if(($('.wrapper').outerWidth()) <= (widthOfList()+getLeftPosi())) {
            $('.scroller-left').fadeOut('slow');
            $('.scroller-right').fadeIn('slow');
        }
    }
  


}).scroll();
</script>


</html>