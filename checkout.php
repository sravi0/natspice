<?php session_start();
include 'src/config.php';
$uid=$_SESSION['uid'];
if(!isset($_SESSION['uid'])){
    header("Location:login.php?pg=checkout");
}
else{
    $uid=$_SESSION['uid'];
    //echo $_SESSION['del_type']; exit; 
?>
<!doctype html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- Favicons-->
  <title>NATURAL SPICE TANDOORI</title>
   
     <link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
      <link href="vendor1/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome-->
      <link href="vendor1/fontawesome/css/all.min.css" rel="stylesheet">
      <!-- Font Awesome-->
      <link href="vendor1/icofont/icofont.min.css" rel="stylesheet">
      <!-- Select2 CSS-->
      <link href="vendor1/select2/css/select2.min.css" rel="stylesheet">
      <!-- Custom styles for this template-->
      <link href="css1/osahan.css" rel="stylesheet">
      <script src="js/jquery-3.5.1.js"></script>
      <link rel="preconnect" href="https://fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap" rel="stylesheet">
      <link href="checkout.css" rel="stylesheet">
      <style>
         @media only screen and (min-width: 576px){
  .modal-dialog {
      max-width: 80% !important;
  }
  }
    @media only screen and (min-width: 576px){
  .addressMDiv {
      max-width: 70% !important;
  }
  .addressCDiv{
      width: 50%;left: 21%;
  }
  .gold-members {
    padding: 1.5rem!important;
}
.card{
    width:100%;
  height: 200px;
}
.text-black {
    color: #000000;
    word-break: break-all;
}
.deletemodal{
    width: 25% !important;
    left: 37% !important;
}
  }
    @media only screen and (max-width: 576px){
        .div1 {
  display:table-footer-group;  
}
.div2 {
  
  display:table-header-group;
}
.sectionRowM{
    display: -webkit-box !important;
}
        .loader .loader_inner {
    background-image: url(../img/DG-loader.gif);
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center center;
    height: 200px;
    width: 200px;
    margin-top: -30px;
    margin-left: -30px;
    left: 30%;
    top: 34%;
    position: absolute;
}
  .addressMDiv {
      max-width: 100% !important;
  }
  .addressCDiv{
      width: 100%;left: 0px;
  }
  .gold-members {
    padding: 0.5rem!important;
}
.card{
    width:100%;
    height:auto;
}
.text-black {
    color: #000000;
    word-break: break-all;
}
.deletemodal{
    width: 100% !important;
    left: 0px !important;
}
  }
        </style>
  <style>
  #snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color:red;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 1;
    left: 50%;
    bottom: 30px;
    font-size: 17px;
  }
  
  #snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
  }
  
  @-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;} 
    to {bottom: 30px; opacity: 1;}
  }
  
  @keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
  }
  
  @-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;} 
    to {bottom: 0; opacity: 0;}
  }
  
  @keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
  }
  
  .nav-tabs {
          padding: 10px 0;
      }
      .btn-toggle .active {
        background: #379ed0 !important;
        font-weight: 400 !important;
        color: white !important;
      }
      
      .btn-toggle  {  border: 1px solid #e2e0e0;
          border-radius: 25px; margin: 10px; }
  
        .restaurant-detailed-banner .img-fluid.cover {
          height: 300px;
          object-fit: cover;
          width: 100%;
      }
      .nav-tabs {
          border-bottom: 0px solid #dee2e6 !important;
      }
      .alert-danger { display: none; }
      .progressbar {
        counter-reset: step;
      }
      
      .progressbar li {
        position: relative;
        list-style: none;
        float: left;
        width: 47.33%;
        text-align: center;
      }
      
      /* Circles */
      .progressbar li:before {
        content: counter(step);
        counter-increment: step;
        width: 40px;
        height: 40px;
        border: 1px solid #2979FF;
        display: block;
        text-align: center;
        margin: 0 auto 10px auto;
        border-radius: 50%;
        background-color: #FF9100;
         
        /* Center # in circle */
        line-height: 39px;
      }
      
      .progressbar li:after {
        content: "";
          position: absolute;
        width: 90%;
          height: 1px;
          background: orange;
          top: 20px;
          left: -45%;
          z-index: 0;
      }
      
      .progressbar li:first-child:after {
        content: none;
      }
      
      .progressbar li.active:before {
        background: #00E676;
        content: "✔";  
      }
      
      .progressbar li.active + li:after {
        background: #00E676;
      }
      .itemNotes {width: 99%;
          text-align: left;
          font-size: 10px;
          color: #7a7a7a;
          line-height: 18px;
          font-style: initial;}
          .cart_item_pr {float: right; }
          .cart_thumb { object-fit: cover; height: 50px; width: 50px; -moz-border-radius:7px; border-radius:7px; border: 1px solid #e3e3e4; }
          #addonModalLabel { font-size: 27px; font-weight: 700; }
          #searchclear {
          position: absolute;
          right: 5px;
          top: 0;
          bottom: 0;
          height: 14px;
          margin: auto;
          font-size: 14px;
          cursor: pointer;
          color: #ccc;
        }
      .itemTit { font-size:14px; }
      .addOn { border-top: none; padding: 5px 2px;
              padding-left: 20px; }
          .list-group-item {
              border: none;
          }
          
          .list-card.bg-white.rounded.overflow-hidden.position-relative.shadow-sm {
            margin: 0 10px;
              border-radius: 50px;
              border: 1px solid #f2f1f1;
              border-radius: 10px !important;
              box-shadow: 20;
              box-shadow: 1px 1px 1px 1px #f2f1f1 !important;
          }
          .row {  --bs-gutter-x: 0; }
          .card { border:none; }
          .btn small { display:block; font-size: 12px;}
          .p-3 {
              padding: 0 1rem !important;
          }
          
          .bg_white { background: #fff; }
          .text-red { color:#ea5f5f; font-size:10px; cursor:default; float:right !important;  }
          .count-number .btn { border-radius: 20px; margin: 0 30px; }
          .count-number-input { color:black;background: #f8f8f8;
              border: .5px solid #bbbbbb1c;   
         }
      
         .btn-group .btn-lg {
            padding: 5px !important;
            border-radius: 25px;
            width: 100%;
          background: #fff;
         }
         .btn-group .btn-lg .active {
            border-radius: 25px;
         }
         .disable-click{
            pointer-events:none;
         }
         .pay_loading { display: none;}
          .unselectable {
        -webkit-user-select: none;
        -webkit-touch-callout: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        /*color: #cc0000;*/
      }
      .form-group {
    margin-bottom: 0.5rem;
}
.btn-toggle {
    border: 1px solid #e2e0e0;
    border-radius: 10px;
    margin: 5px 10px 0px 10px;
    padding: 5px;
    background: #e2e0e0;
}
.btn-toggle .active {
    background: #379ed0;
    font-weight: 400;
    color: #fff;
    width: 100%;
    border-radius: 10px !important;
}
.btn-toggle .active {
        background: #ED732A !important;
        font-weight: 400 !important;
        color: white !important;
      }
      
      .btn-toggle  {  border: 3px solid #e2e0e0;
          border-radius: 25px; margin: 10px;  }
          .btn-toggle .btn-lg{
              background: #e2e0e0;
          }
         </style>
   </head>
   <body class="unselectable">
       <div class="loader" style="display:none" id="onloader"><div class="loader_inner"></div></div>
       <input type="hidden" id="del_statuuu" value="<?php echo $_SESSION['del_type_status'];?>">
          
   <div id="modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body" id="iframe-container">
          <h5>Please wait while your transaction is processed</h5>
        </div>
      </div>
    </div>
  </div>
  
       <div id="snackbar"></div>
          <input type="hidden" id="del_id" value="0">
          <div class="modal fade" id="deliveryFeeModel" tabindex="-1" role="dialog" aria-labelledby="edit-profile" aria-hidden="true">
         <div class="modal-dialog modal-sm modal-dialog-centered" role="document" style="width: 100%;">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="edit-profile">Delivery Locations</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body" id="deliveryLocations" style="overflow: auto;height: 400px;">
               
               </div>
               <div class="modal-footer">
                 
               </div>
            </div>
         </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="add-address-modal" tabindex="-1" role="dialog" aria-labelledby="add-address" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered addressMDiv" role="document" style="">
            <div class="modal-content addressCDiv" style="">
               <div class="modal-header">
                  <h5 class="modal-title" id="add-address">Add Delivery Address</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <form id="addaddressform">
                     <div class="form-row">
                     
                    <div class="col-md-12">
                    <input type="hidden" id="lat" name="lat" val="40.713956" disabled/>
							<input type="hidden" id="long" name="long" val="74.006653" disabled/>
                     <input type="hidden" class="maddress" id="dnum" value="" style="display:none;" placeholder="Door No!">
                        <div class="form-group col-md-12">
                           <label for="inputPassword4">Door & Flat No</label>
                           <div class="input-group">
                           <input type="text" required id="street_number" class="form-control col-md-6" maxlength="10" placeholder="Door" autocomplete="off" />
                           <input type="text" class="form-control col-md-6" id="street_number1" maxlength="10" placeholder="Flat No"></br>
                           
                           </div>
                            <p style="color:red;display:none" id="doorandflaterror">Please Enter Door and Flat No</p>
                        </div>
                    
                        <div class="form-group col-md-12">
                           <label for="inputPassword4">Complete Address
                           </label>
                           <input type="text" required id="addrr" class="form-control" placeholder="Building number, street name" maxlength="45">
                           <p style="color:red;display:none" id="caddresserror">Please Enter Address</p>
                        </div>
                      
                        <div class="form-group col-md-12">
                           <label for="inputPassword4">City
                           </label>
                           <input type="text" required id="locality" class="form-control" placeholder="" onkeypress="allowAlphaNumericSpace(event)" maxlength="20">
                           <p style="color:red;display:none" id="cityerror">Please Enter City</p>
                        </div>
                         
                        <input type="hidden" required id="administrative_area_level_1" class="form-control" placeholder="">
                        <div class="form-group col-md-12">
                           <label for="inputPassword4">Postcode
                           </label>
                           <input type="text" required id="postal_code" class="form-control" placeholder="" onfocusout="validPin(this.value)" onkeypress="allowAlphaNumericSpace(event)" maxlength="10">
                           <p style="color:red;display:none" id="postcodeerror">Please Enter Post Code</p>
                        </div>
                        <div class="form-group col-md-12">
                           <label for="inputPassword4">Delivery Instruction
                           </label>
                           <input type="text" required id="landmark" class="form-control" placeholder="e.g Leave at The Door!" onkeypress="allowAlphaNumericSpace(event)" maxlength="30">
                           <p id="landmarkerr" style="color:red;display:none;">* Please Fill Landmark!!!</p>
                        </div>
                        </div>
                        <div id="myaddress" style="display: none;"></div>
                        <!-- <div class="form-group col-md-12">
                           <label for="inputPassword4">Delivery Instructions
                           </label>
                           <input type="text" class="form-control" placeholder="Delivery Instructions e.g. Opposite Gold Souk Mall">
                        </div> -->
                        <!-- <div class="form-group mb-0 col-md-12">
                           <label for="inputPassword4">Nickname
                           </label>
                           <div class="btn-group btn-group-toggle d-flex justify-content-center" data-toggle="buttons">
                              <label class="btn btn-info active">
                              <input type="radio" name="options" id="option1" autocomplete="off" checked> Home
                              </label>
                              <label class="btn btn-info">
                              <input type="radio" name="options" id="option2" autocomplete="off"> Work
                              </label>
                              <label class="btn btn-info">
                              <input type="radio" name="options" id="option3" autocomplete="off"> Other
                              </label>
                           </div>
                        </div> -->
                     </div>
                  </form>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn d-flex w-50 text-center justify-content-center btn-outline-primary" data-dismiss="modal">CANCEL
                  </button><button type="button" class="btn d-flex w-50 text-center justify-content-center btn-primary" id="addaddressButton" onclick="addaddress()">SUBMIT</button>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="delete-address-modal" tabindex="-1" role="dialog" aria-labelledby="delete-address" aria-hidden="true">
         <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content deletemodal">
               <div class="modal-header">
               <input type="hidden" id="adddeleteid">
                  <h5 class="modal-title" id="delete-address">Delete</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <p class="mb-0 text-black">Are you sure you want to delete?</p>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn d-flex w-50 text-center justify-content-center btn-outline-primary" data-dismiss="modal">CANCEL
                  </button><button type="button" class="btn d-flex w-50 text-center justify-content-center btn-primary" onclick="deleteaddress()">DELETE</button>
               </div>
            </div>
         </div>
      </div>
       <?php include("headerot.php");?>
       <input type="hidden" id="couponid" value="0">
    <section class="offer-dedicated-body mt-4 mb-4 pt-2 pb-2">
         <div class="container-fluid" style="max-width:1400px;">
           <div class="row sectionRowM">
               <div class="col-md-8 div1">
                  <div class="offer-dedicated-body-left">
					 <div id="choose_address" class="bg-white rounded shadow-sm p-4 mb-4">
                        <h4 class="mb-1">Choose a delivery address</h4>
                        <h6 class="mb-3 text-black-50">Deliver With only in 7 Miles from Restaurant <span style="cursor:pointer;color:#ED732A;" onclick="getDeliverylocation()">(View Delivery Locations)</span></h6></h6>
                          <div id="selectedaddresslist" style="margin-top: 12px;">
                           <div class="col-md-12">
                              <!--<input type="radio" checked> <label>Door No. 12,Walmer Cres ,Glasgow ,G51 1AQ</label>-->
                              <ul class="progressbar">
                                 <li class="active">From</li>
                                 <li class="active">To</li>
                                 
                              </ul>
                           </div>                   
                        <div class="col-md-12" style="margin-left: -2%;">
                            <div class="row">
                                <div class="col-md-6">
                              <div class="bg-white card addresses-item" style="border: none;height: auto;
    text-align: justify;">
                                  <div class="gold-members p-4"><div class="media">
                                      <div class="mr-3"><i class="icofont-5-star-hotel icofont-3x"></i></div>
                                       <div class="media-body"> <h6 class="mb-1 text-secondary">NATURAL SPICE TANDOORI</h6><p class="text-black"> 1791, Paisley Road West, Cardonald, Glasgow, G52 3SS</p>
                                      <p class="mb-0 text-black font-weight-bold">
                                          <!--<a data-toggle="modal" data-target="#add-address-modal" class="btn btn-sm btn-primary mr-2" href="#"> ADD NEW ADDRESS</a>-->
                                          </p>
                                          </div></div></div></div></div>
                          <div class="col-md-6">
                              <div class="bg-white card addresses-item" style="border: none;height: auto;
    text-align: justify;line-break:anywhere;"><div class="gold-members p-4">
                                  <div class="media"><div class="mr-3"><i class="icofont-ui-home icofont-3x"></i></div>
                                  <div class="media-body"> <h6 class="mb-1 text-secondary"></h6><p class="text-black delivery_address"></p>
                                  <p class="mb-0 text-black font-weight-bold">
                                      <a style="text-decoration: underline !important;cursor:pointer" onclick="changeAddress()">Change Address</a></p>
                                      </div></div></div></div></div>
                            </div>
                        </div>
                          
                         
                        </div>
                        <div class="col-md-12" id="addresslist">
                          
                         
                        </div>
                    
                       
                       
                     </div>
                     <div class="bg-white rounded shadow-sm p-4 mb-2 personal_detail">
                        <h4 class="mb-1">Personal Detail</h4>
                        <div class="form-row pt-2">
                           <div class="form-group col-md-4">
                                             <input type="type" name="p_full_name" id="p_full_name" ondrop="return false;" onpaste="return false;" class="form-control" size="4" placeholder="Full Name" required>
                           </div>
                           <div class="form-group col-md-4">
                                             <input type="email" name="p_email" id="p_email" ondrop="return false;" onpaste="return false;" class="form-control" size="4" placeholder="Email" required>
                           </div>
                           <div class="form-group col-md-4">
                                            <?php 
                               $str_to_replace = '0';
                                $input_str = $_SESSION['user_login_mobile'];
                                $output_str = $str_to_replace . substr($input_str, 2);
                               ?>
                                             <input type="text" name="p_phone" id="p_phone" ondrop="return false;" onpaste="return false;" class="form-control" size="4" placeholder="Phone" value="<?php echo $output_str;?>" maxlength='11' required >
                           </div>
                        </div>
                     </div>
					      <div class="pt-2"></div>
                     <div class="bg-white rounded shadow-sm p-4 osahan-payment">
                        <h4 class="mb-1">Choose Payment Method</h4>
                        <h6 class="mb-3 text-black-50">Credit/Debit Cards</h6>
                        <div class="row">
                           <div class="col-sm-4 pr-0">
                              <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                 <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><i class="icofont-credit-card"></i> Credit/Debit Cards</a>
                                 <!--<a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"><i class="icofont-id-card"></i> Food Cards</a>-->
                                 <!--<a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false"><i class="icofont-card"></i> Credit</a>-->
                                 <!--<a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><i class="icofont-bank-alt"></i> Netbanking</a>-->
                                 <a class="nav-link" id="v-pills-cash-tab" data-toggle="pill" href="#v-pills-cash" role="tab" aria-controls="v-pills-cash" aria-selected="false"><i class="icofont-money"></i> Cash on Delivery</a>
                              </div>
                           </div>
                           <div class="col-sm-8 pl-0">
                              <div class="tab-content h-100" id="v-pills-tabContent">
                                 <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                    <!--<h6 class="mb-3 mt-0 mb-3">Add new card</h6>-->
                                    <p>WE ACCEPT <span class="osahan-card">
                                       <i class="icofont-visa-alt"></i> <i class="icofont-mastercard-alt"></i> <i class="icofont-american-express-alt"></i> <i class="icofont-payoneer-alt"></i> <i class="icofont-apple-pay-alt"></i> <i class="icofont-bank-transfer-alt"></i> <i class="icofont-discover-alt"></i> <i class="icofont-jcb-alt"></i>
                                       </span>
                                    </p>
                                    <form method="POST" id="frmStripePayment" class="form" style="padding-top: 0px;">
                                       <div class="form-row">
                                          <div class="form-group col-md-12">
                                             <label for="inputPassword4">Card number</label>
                                             <div class="input-group">
                                                <input type="text" class="form-control"  name="cardNumber" autocomplete="off" ondrop="return false;" onpaste="return false;" id="cardNumber" onkeyup="GetCardType()" maxlength='16' value="" placeholder="Please Enter Card Number">
                                                
                                                <div class="input-group-append">
                                                   <button class="btn btn-outline-secondary" type="button" id="button-addon2"><i class="icofont-card"></i></button>
                                                </div>
                                             </div>
                                          </div>
                                           <!--<input type='text' class="form-control"  name="cardNumber" autocomplete="off" id="cardNumber" onkeypress='return formats(this,event)' onkeyup="return numberValidation(event)" placeholder='Enter Credit Card No' >-->
                                          <span id="cardIdname" style="width: 100%;"></span></br>
                                          <div class="form-group col-md-4">
                                                <label>Valid Through(MM)
                                                </label>
                                                   <select name="month" id="month"
                                                   class="demoSelectBox form-control">
                                                   <option value="01">01</option>
                                                   <option value="02">02</option>
                                                   <option value="03">03</option>
                                                   <option value="04">04</option>
                                                   <option value="05">05</option>
                                                   <option value="06">06</option>
                                                   <option value="07">07</option>
                                                   <option value="08">08</option>
                                                   <option value="09">09</option>
                                                   <option value="10">10</option>
                                                   <option value="11">11</option>
                                                   <option value="12">12</option>
                                                </select>
                                             </div>
                                             <div class="form-group col-md-4">
                                                <label>Valid Through(YY)
                                                </label>
                                             <select name="year" id="year"
                                                   class="demoSelectBox form-control">
                                                   <option value="21">2021</option>
                                                   <option value="22" selected>2022</option>
                                                   <option value="23">2023</option>
                                                   <option value="24">2024</option>
                                                   <option value="25">2025</option>
                                                   <option value="26">2026</option>
                                                   <option value="27">2027</option>
                                                   <option value="28">2028</option>
                                                   <option value="29">2029</option>
                                                   <option value="30">2030</option>
                                                </select>
                                             </div>
                                          <div class="form-group col-md-4">
                                             <label>CVV
                                             </label>
                                             <input type="password" name="cardCVC" size="4" autocomplete="off" value="" id="cardCVC" maxlength="3"  class="form-control" placeholder="Enter CVC Number">
                                          </div>
                                          <div class="form-group col-md-12">
                                             <label>Name on card
                                             </label>
                                             <input type="text" class="form-control" name="cardname" id="cardname" ondrop="return false;" onpaste="return false;"  placeholder="Enter Name">
                                          </div>
                                          <div class="form-group col-md-12">
                                             <div class="custom-control custom-checkbox">
                                               <p style="color:red;"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Please do not close this window or click the Back button on your browser while payment processing!....</p>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-12">
                                             <div class="alert alert-danger m-0 text-center" role="alert"></div>
                                          </div>
                                          <div class="form-group col-md-12 mb-0">
                                             <a  id="makePayment" onClick="stripePay(event,1);" class="btn btn-success btn-block btn-lg makePayment" style="color: #fff;">Pay Now
                                             <i class="icofont-long-arrow-right"></i></a>

                                             <button class="btn btn-success btn-block btn-lg pay_loading" type="button" disabled>
                                             <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                             Payment Processing...
                                             </button>
                                          </div>
                                       </div>
                                    </form>
                                 </div>
                                
                                 <div class="tab-pane fade" id="v-pills-cash" role="tabpanel" aria-labelledby="v-pills-cash-tab">
                                    <h6 class="mb-3 mt-0 mb-3">Cash</h6>
                                    <p>Please keep exact change handy to help us serve you better</p>
                                    <hr>
                                    <form>
                                    <a id="makePayment" onClick="stripePay(event,2);" class="btn btn-success btn-block btn-lg makePayment" style="color: #fff;">Pay Now
                                             <i class="icofont-long-arrow-right"></i></a>
                                    
                                    <button class="btn btn-success btn-block btn-lg pay_loading" type="button" disabled>
                                       <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                       Order Processing...
                                    </button>
                                 </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
             <div class="col-md-4 div2">
               <div class="bg-white rounded shadow-sm p-4 mb-4" style="padding: 0.5rem!important;">
               <div class="btn-group btn-toggle d-flex justify-content-center" style="margin-bottom: -4px;"> 
                  <?php if($_SESSION['del_type_status']==0 || $_SESSION['del_type_status']==2){?>     <button class="btn btn-lg btn-default <?php if(isset($_SESSION['del_type']) && $_SESSION['del_type']=='delivery') { ?>active<?php } ?>" data-id="delivery">Delivery <small>In 40 min</small></button><?php } ?>
                 <?php if($_SESSION['del_type_status']==0 || $_SESSION['del_type_status']==1){?>  <button class="btn btn-lg btn-default <?php if(isset($_SESSION['del_type']) && $_SESSION['del_type']=='collection') { ?>active<?php } ?>" data-id="collection">Collection <small>In 15 min</small></button><?php } ?>
               </div>
               <div class="postoder">
                 
                    <div class="modal-body1 p-2 d-flex align-items-center">
            <div class="text w-100 text-center">
                 <?php if($_SESSION['pre_order']==null || $_SESSION['pre_order']==''){?>
                <div id="postoder">
              <h6 class="mb-0">Schedule Order</h6>  <br>
                <select  id="postordert" onchange="getScheduleirder(this)" class="form-control">     
                <option value="0">Order Now</option>
                <option value="1">Schedule Order</option>
                </select>
                </div>
                <?php } ?>
              <form action="#" class="code-form" id="postorderform" style="display:none;">
                <div class="form-group p2">
             
                <span class="col-4 p-1">
                Date:<br>
                                    <div class="ui calendar" id="preorderdateformat1" style="">
                            
    <div class="ui input left icon" style="width: 100%;">
      <i class="calendar icon"></i>
      <input type="text" id="pre_date" class="form-control"   placeholder="Schedule Date" style="width: 100% !important; background:white; " readonly/>
    </div>
  </div>
               
                Time:<br> <select id="pre_time" class="form-control">     
                <option value="">Please Select Time</option>
                 <option value="17:00">17:00</option>
                <option value="18:00">18:00</option>
                <option value="19:00">19:00</option>
                <option value="20:00">20:00</option>
                <option value="21:00">21:00</option>
                <option value="22:00">22:00</option>
                <option value="23:00">23:00</option>               
                <!--<option value="23:00">23:00</option>
                <option value="24:00">24:00</option>
                <option value="01:00">01:00</option>
                <option value="02:00">02:00</option>
                <option value="03:00">03:00</option>
                <option value="04:00">04:00</option>
                <option value="05:00">05:00</option>--></select>
                </div>
                <div class="alert alert-danger" role="alert"></div>
                  <a class="btn btn-primary d-block py-3" onClick="PreOrder();" style="color:#fff;padding: 5px !important;">Confirm</a>
              </form>
              
           
            </div>
          </div>
               </div>
               <div id="cartItems_section">
               </div>
               </div>
            </div>
         </div>
      </section>
      <footer class="pt-4 pb-4 text-center">
         <div class="container">
            <p class="mt-0 mb-0">© Copyright 2021 DeliveyGuru. All Rights Reserved</p>
          
         </div>
      </footer>
      <!-- jQuery -->
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script> 
$('.personal_detail, .osahan-payment').hide();
<?php if(isset($_SESSION['del_type']) && $_SESSION['del_type']=='collection') { ?>$('#choose_address').hide(); $('.personal_detail, .osahan-payment').show();<?php } ?>
<?php $_SESSION['del_charge'] = 0; ?>
dellivery_addr_id = 0;
dellivery_addr_text = '';


var items = [];
var extra_items = [];
var ser_fee = del_fee = disc_fee = subTot = grandTot = grandTot2 = 0;

var userID = <?php echo $uid;?>;
var userID2 = <?php if (isset($_SESSION['uid'])) { echo $_SESSION['uid']; } else { echo "0"; } ?>;

var userOrderId = "NSP" + "<?php echo date("YmdHis") ?>" + "<?php if (isset($_SESSION['uid'])){ echo $_SESSION['uid']; } else { echo "0"; }?>";

var preOrd_dt = "null";
var preOrd_ti = "null";
<?php if(isset($_SESSION['pre_order']) && $_SESSION['pre_order']!='') { ?> var preDt='<?php echo $_SESSION['pre_order']; ?>'; preDt = preDt.split(','); preOrd_dt=preDt[0].trim(); preOrd_ti=preDt[1].trim(); <?php } ?>

</script>

<script type="text/javascript" src="checkout.js"></script>

      <script data-cfasync="false" src="js/email-decode.min.js"></script>
      <!-- <script src="vendor1/jquery/jquery-3.3.1.slim.min.js" type="762714e34f17a0f5b9715d6b-text/javascript"></script> -->
      <!-- Bootstrap core JavaScript-->
      <script src="vendor1/bootstrap/js/bootstrap.bundle.min.js" type="762714e34f17a0f5b9715d6b-text/javascript"></script>
      <!-- Select2 JavaScript-->
      <script src="vendor1/select2/js/select2.min.js" type="762714e34f17a0f5b9715d6b-text/javascript"></script>
      <!-- Custom scripts for all pages-->
      <script src="js/custom.js" type="762714e34f17a0f5b9715d6b-text/javascript"></script>
   <script src="js/rocket-loader.min.js" data-cf-settings="762714e34f17a0f5b9715d6b-|49" defer=""></script></body>
   <script src="https://cdn.jsdelivr.net/npm/pikaday/pikaday.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/pikaday/css/pikaday.css">

  <script>
       $(document).ready(function() {
          $('body').bind('cut copy', function(e) {
              e.preventDefault();
            });
        $("body").on("contextmenu", function(e) {
              return false;
            });
        });
        
function allowAlphaNumericSpace(e) {
  var code = ('charCode' in e) ? e.charCode : e.keyCode;
  if (!(code == 32) && // space
    !(code > 47 && code < 58) && // numeric (0-9)
    !(code > 64 && code < 91) && // upper alpha (A-Z)
    !(code > 96 && code < 123)) { // lower alpha (a-z)
    e.preventDefault();
  }
}
function myKeyPress(e){
    var keynum;
    if(window.event) { // IE                    
      keynum = e.keyCode;
    } else if(e.which){ // Netscape/Firefox/Opera                   
      keynum = e.which;
    }
    if(keynum==62 || keynum==60)
        e.preventDefault();
  }
   function PreOrder()
 {

     var pre_date= $('#pre_date').val();
     var pre_time= $('#pre_time').val();
     if(pre_date=='' || pre_time=='') {
    
                 var x = document.getElementById("snackbar"); 
        x.innerHTML='Choose Schedule order Date & Time!';
        // document.getElementById('onloader').style.display='none';
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
             return false;
             
     } else {
    
       updateCart(0, pre_date+', '+pre_time, 'pre_order');
        document.getElementById('postorderform').style="display:none";
        // document.getElementById('postoder').style="display:none";
        //  document.getElementById('changedate').style="display:block";
        
   
     }
 }
 
 
  function changeDate(){
      document.getElementById('postorderform').style="display:block";
       document.getElementById('changedate').style="display:none";
 }
 
 function deleteDate(){
      var keyStatus= sessionStorage.getItem('preorder_time');
      if(keyStatus==0){
     document.getElementById('preorder_value').style="display:none";
          document. getElementById("preorder_status"). value = "";
          document. getElementById("preorder_value"). value = "";
          document.getElementById('deletedate').style="display:block";
          updateCart(0,'', 'pre_order');
           document.getElementById('postorderform').style="display:none";
         var dropDown = document.getElementById("postordert");  
        dropDown.selectedIndex = 0;  
      }
           else{
               var x = document.getElementById("snackbar"); 
        x.innerHTML='Pre-Order Time required when restaurant is closed!';
        // document.getElementById('onloader').style.display='none';
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
           }
 }
 function getScheduleirder(data){
      var type=document.getElementById('postordert').value;
      if(type==0){
          document.getElementById('postorderform').style="display:none";
        //   document.getElementById('preorder_value').style="display:none";
        //   document. getElementById("preorder_status"). value = " ";
        //   updateCart(0,' ', 'pre_order');
      }
      else{
          document.getElementById('postorderform').style="display:block";
      }
  }
       $(document).ready(function() {
          $('body').bind('cut copy', function(e) {
              e.preventDefault();
            });
        $("body").on("contextmenu", function(e) {
              return false;
            });
        });
        function getDeliverylocation(){
    $("#deliveryFeeModel").modal("show");
   var obj='';
     $.ajax({
type: "GET",
url: 'https://deliveryguru.co.uk/dg_api/getRestaurantsDetails/200',
dataType: "json",
crossDomain: true,
cache: "false",
success: function(json) {
   var fresult=JSON.parse(json[0].deliverydetails);
   var fresult1=fresult.deliverypostcode;
   console.log(fresult1);
     obj+='<table style="width:100%;text-align: center;">';
  obj+='<tr>';
    obj+='<th style="border: 1px solid #ccc;">Post Code</th>';
    obj+='<th style="border: 1px solid #ccc;">Delivery Fee</th>';
  obj+='</tr>';
for(i=0;i<fresult1.length;i++){
 
  obj+='<tr>';
    obj+='<td style="border: 1px solid #ccc;">'+fresult1[i].postcode+'</td>';
   obj+=' <td style="border: 1px solid #ccc;">&#163;'+fresult1[i].price+'</td>';
  obj+='</tr>';
  
}
obj+='</table>';

$('#deliveryLocations').html(obj);
}
});
 }
 var today = new Date();

const picker = new Pikaday({
    field: document.getElementById('pre_date'),
    firstDay: 1,
    minDate: new Date(),
    maxDate: new Date(today.getFullYear(), today.getMonth(), today.getDate() + 5),
    yearRange: [2021,2021],
    onSelect: date => {
      const year = date.getFullYear()  ,month = date.getMonth() + 1 ,day = date.getDate(),formattedDate = [ day < 10 ? '0' + day : day, month < 10 ? '0' + month : month, year].join('/');
      document.getElementById('pre_date').value = formattedDate
  }
});
// function getrestatus(){
//      var keyStatus= sessionStorage.getItem('preorder_time');
//      console.log(keyStatus);
//      if(keyStatus==0){
//          document.getElementById('deletedate').style="display:block";
//      }
//      else{
//          document.getElementById('deletedate').style="display:none";
//      }
//  }getrestatus();
  </script>

</html>
<?php } ?>