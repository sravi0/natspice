
function getCartPrice(){
  $.ajax({
         async: false,
         type: "POST",
         url: "ajaxAddToCart3.php",
         data: {action : 1},
         dataType: "json",
         success: function(res) 
         {
             totQty = res.totQty;
             $('.badge-notification').html(totQty);
         }
  });
 }
 
 function getordertype(data){
     if(data.id=="collection"){
         document.getElementById('collectiondiv').style="display:block";
     }
     else{
         document.getElementById('collectiondiv').style="display:none";
     }
 }
 
 function showError(errMsg) {
   var x = document.getElementById("snackbar");
   $('#snackbar').css('background-color', 'green');
   x.innerHTML=errMsg;
   x.className = "show";
   setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
 }
 
 
 function gridSearch(value) {
   $('div[data-role="recipe"]').filter(function() {
               $(this).toggle($(this).find('h6 a').text().toLowerCase().indexOf(value) > -1);
    });
    $('.item-section').each( function() { 
               var len = $('div[data-role="recipe"]', this).children(':visible').length;
               if(len==0)
               {
                 $('h3',this).hide();
               } else {
                 $('h3',this).show();
               }
     });
 }
 
 $("#searchclear").click(function(){
   $("#searchinput").val('');
 });


 $(document).ready(function() {
     $("#searchinput, .search-mob").on("keyup change", function() {
          $("#searchinput, .search-mob").val($(this).val());
          var value = $(this).val().toLowerCase();
          $('div[data-role="recipe"]').filter(function() {
                       $(this).toggle($(this).find('h6 a').text().toLowerCase().indexOf(value) > -1);
           });
           $('.item-section').each( function() { 
                       var len = $('div[data-role="recipe"]', this).children(':visible').length;
                       if(len==0)
                       {
                         $('h3',this).hide();
                       } else {
                         $('h3',this).show();
                       }
             });
     });
 });
 
 localStorage.clear();
 function refreshCart() {
 var del_type = $('.btn-toggle .active').data("id");
 $.ajax({
           type: "POST",
           url: "ajaxAddToCart.php",
           data: {action : 'del_type', qty: del_type},
           success: function(res) 
           {
               $('#cartItems_section, .mobCartBody').html(res);
               getCartPrice();
           }
       });
 }
 refreshCart();
 
 $('.close').on('click', function () {
             $('#addonModal').modal('hide'); 
 })
 
 
 
 var itemMinMax = {};
 function btnAddToCart() {
        $('.alert-danger').hide();
        var errorMsg='';
        
        var MainSel = $('input[name="subMenuList"]:checked').val();
 
        if($('input[name="subMenuList"]:checked').length == 0) {
          errorMsg = 'Please select at least one item!';
        }  else 
        {
           
           var temp = selItem = new Array();
           selItem = MainSel.split(",");
           selId = selItem[0];
           if(itemMinMax[selId]){
             temp = itemMinMax[selId].split("},{");
             var addOnItems = [];
             for(i=0;i<temp.length;i++){
                 var adOnQty   = 0;
                 var splitTxt = new Array();
                 splitTxt = temp[i].replace('{','');
                 splitTxt = splitTxt.replace('}','');
                 splitTxt =  splitTxt.split(',');
                 var chkId = $('input[name="addOnCheck'+selId+'_'+splitTxt[0]+'[]"]:checked').length;
                 if(splitTxt[1]==1 && splitTxt[2]==1) {
                     adOnQty = $('input[name="addOnCheck'+selId+'_'+splitTxt[0]+'[]"]:checked').length;
                 } else {
                   $('input[name="addOnSel'+selId+'_'+splitTxt[0]+'[]"]').each( function() { 
                     if($(this).is(":visible")) {
                       adOnQty += parseInt($(this).val());
                     }
                   });
                 }
 
                 if((adOnQty < splitTxt[1] || adOnQty > splitTxt[2]) && errorMsg==''){
                   errorMsg = 'Please select Min '+splitTxt[1]+' and Max '+splitTxt[2]+' Add on from '+splitTxt[3];
                   window.location.hash = "";
                   window.location.hash = '#box_'+selId+'_'+splitTxt[0];
                   $('#box_'+selId+'_'+splitTxt[0]+' .list-card-body .mb-1 a').attr('style', 'color: red !important');
                   $('#box_'+selId+'_'+splitTxt[0]+' .list-card-body .mb-1 a').fadeIn(100).fadeOut(300).fadeIn(300).fadeOut(300).fadeIn(300);
                 }
                 $('input[name="addOnCheck'+selId+'_'+splitTxt[0]+'[]"]:checked').map(function(){
                     if(splitTxt[1]==1 && splitTxt[2]==1) { 
                         var addOnQty = 1; 
                     } else {
                         var addOnQty = $("~ span.addOnSel input", this).val();
                     }
                     addOnItems.push($(this).val()+','+addOnQty);
                 });
                 console.log('Values: '+selId+','+splitTxt[1]+','+splitTxt[2]+', '+chkId);
             }
           }
        }
 
        if(errorMsg!=''){
         $('.alert-danger').html(errorMsg);
         $('.alert-danger').show();
         setTimeout(
             function() {
               $('.alert-danger').hide();
               $('.list-card-body .mb-1 a').attr('style', 'color: #000000 !important');
             }, 5000);
         return false;
        } else {
          var totItem = parseInt($('input[name="totItem"]').val());
          selItem.push(totItem);
          selItem.push($('#addInst').val());
          var ItemsStr = JSON.stringify(selItem);
          var addonItemsStr = JSON.stringify(addOnItems);
          AajxUpdateCart(ItemsStr, addonItemsStr, 'add');
        }
 }
 function AajxUpdateCart(items, addONitem, add){
       $('#addonModal').modal('hide'); 
       $.ajax({
           type: "POST",
           url: "ajaxAddToCart.php",
           data: {Items : items, addOns: addONitem},
           success: function(res) 
           {
               getCartPrice();
               $('#cartItems_section, .mobCartBody').html(res);
               showError('Successfully added!');
           }
       });
 }
 
 function updateCart(id, qty, act){
      
       if(act=='add') {
         qty = qty + 1; action = 'update';
       } else if(act=='remove') {
         qty = qty - 1; action = 'update';
       } else if(act=='note') {
          action = 'note';
       } else if(act=='del_type') {
          action = 'del_type';
       } else if(act=='pre_order') {
          action = 'pre_order';
       }
       $.ajax({
           type: "POST",
           url: "ajaxAddToCart.php",
           data: {action : action, qty: qty, id: id},
           success: function(res) 
           {   
               $('#cartItems_section, .mobCartBody').html(res);
               getCartPrice();
           },
           error: function(json) {
                   alert(json);
           }
       });
      
 }
 
 function updateCartItems(items,add){
   var getItem = localStorage.getItem("cartItems");
   alert(getItem);
   if (localStorage.getItem("cartItems") === null) {
     localStorage.setItem('cartItems', items);
   } else {
     getItem = localStorage.getItem("cartItems");
     getItemArr = JSON.parse(getItem);
     getItemArr.push(items);
     localStorage.setItem('cartItems', JSON.stringify(getItemArr));
   }
   alert(JSON.stringify(JSON.parse(localStorage.getItem("cartItems"))));
 }
 
 function addInst() {
   $('.addInst').show();
 }
 
 function updateNote(id) {
   $('.editInst_'+id+'').hide();
   $('.viewInst_'+id+'').css("display", "flex");
   var notes = $('#editNote_'+id+'').val();
   updateCart(id, notes, 'note');
 }
 
 function editNote(id) {
   $('.editInst_'+id+'').css("display", "flex");
   $('.viewInst_'+id+'').hide();
 }
 
 
 $(document).on('click', '.btn-number', function(e){
     e.preventDefault();
     fieldName = $(this).attr('data-field');
     type      = $(this).attr('data-type');
     cls      = $(this).attr('data-id');
     var input = $('.'+cls);
     var currentVal = parseInt(input.val());
     if (!isNaN(currentVal)) {
         if(type == 'minus') {
             
             if(currentVal > input.attr('min')) {
                 input.val(currentVal - 1).change();
             } 
             if(parseInt(input.val()) == input.attr('min')) {
                 $(this).attr('disabled', true);
             }
 
         } else if(type == 'plus') {
 
             if(currentVal < input.attr('max')) {
                 input.val(currentVal + 1).change();
             }
             if(parseInt(input.val()) == input.attr('max')) {
                 $(this).attr('disabled', true);
             }
 
         }
     } else {
         input.val(1);
     }
 });
 
 
 $(document).ready(function() {
 
   
   $(document).on('click', 'details', function(){
      var id = $(this).attr('class');
     $(this).siblings('details').removeAttr('open');
     $(this).attr('open','');
     alert(id);
     
   });
   
   $(document).on('click', '.summary', function(){
       $('.addonDiv').hide();
       $('input[name="subMenuList"]:checked').attr('checked', false);
       var id = $(this).data('id');
       $('.summary #'+id+'').attr('checked', true); 
       $('.addOn_'+id+'').show();
     });
 });
 var hidWidth;
 var scrollBarWidths = 40;
 
 var widthOfList = function(){
   var itemsWidth = 0;
   $('.list li').each(function(){
     var itemWidth = $(this).outerWidth();
     itemsWidth+=itemWidth;
   });
   return itemsWidth;
 };
 
 var widthOfHidden = function(){
   var t = (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
   console.log('widthOfHidden: '+(($('.wrapper').outerWidth())+','+widthOfList()+','+getLeftPosi())+','+scrollBarWidths+','+t);
   return (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
 };
 
 var getLeftPosi = function(){
   return $('.list').position().left;
 };
 
 var reAdjust = function(){
   if (($('.wrapper').outerWidth()) < widthOfList()) {
     $('.scroller-right').show();
   }
   else {
     $('.scroller-right').show();
   }
   
   if (getLeftPosi()<0) {
     $('.scroller-left').show();
   }
   else {
     $('.scroller-left').hide();
   }
   if(getLeftPosi()==0){
    $('.scroller-left').fadeOut('slow');
   }
 }
 
 reAdjust();
 
 $(window).on('resize',function(e){  
     reAdjust();
 });
 
 $('.scroller-right').click(function() {
   
   $('.scroller-left, .scroller-right').hide();
   $('.scroller-left').fadeIn('slow');
    $('.list').animate({left:"-="+($('.wrapper').outerWidth()   )+"px"},'slow',function() {
       var t = (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
       if(($('.wrapper').outerWidth()) >= (widthOfList()+getLeftPosi())){
          $('.scroller-right').fadeOut('slow');
       } else {
         $('.scroller-right').fadeIn('slow');
       }
    });
 });
 
 $('.scroller-left').click(function() {
    $('.scroller-left, .scroller-right').hide();
    $('.scroller-right').fadeIn('slow');
    $('.list').animate({left:"+="+($('.wrapper').outerWidth())+"px"},'slow',function(){
       var t = (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
       if(getLeftPosi()==0){
          $('.scroller-left').fadeOut('slow');
       } else {
          $('.scroller-left').fadeIn('slow');
       }
     });
 });   
 
 
 
 
 $('.cart-menu a').on('click', function() {
     var scrollAnchor = $(this).attr('data-id');
     scrollPoint = $('#panel_groups #' + scrollAnchor).offset().top - 70 - 50;
     $('body,html').animate({
         scrollTop: scrollPoint
     }, 500);
     return false;
 })
 
 $(window).scroll(function() {
     var windscroll = $(window).scrollTop();
     if (windscroll >= 100) {
         $('#panel_groups .item-section').each(function(i) {
             if ($(this).position().top <= windscroll + 150) {
                 $('.cart-menu a.active').removeClass('active');
                 $('.cart-menu a').eq(i).addClass('active');
             }
         });
 
     } else {
         $('.cart-menu a.active').removeClass('active');
         $('.cart-menu a:first').addClass('active');
     }
 
     var leftOffset = $('.cart-menu a.active').offset().left - $('.cart-menu').offset().left +   $('.cart-menu').scrollLeft();
     var scrollPos = leftOffset / (($('.wrapper').outerWidth())-40);
 
     console.log('abcd: '+$('.wrapper').outerWidth()+','+Math.round(widthOfList())+','+Math.round(getLeftPosi())+','+Math.round(scrollBarWidths)+','+parseInt(scrollPos)+','+leftOffset);
     if(parseInt(scrollPos)>0) 
     {
         setPos = parseInt(scrollPos) * ($('.wrapper').outerWidth());
         $('.list').animate({left: "-"+setPos+"px"}, 0); 
         if(($('.wrapper').outerWidth()) >= (widthOfList()+getLeftPosi())) {
             $('.scroller-left').fadeIn('slow');
             $('.scroller-right').fadeOut('slow');
         }
     } else {
         setPos = parseInt(scrollPos) * ($('.wrapper').outerWidth());
         $('.list').animate({left: "+"+setPos+"px"}, 0); 
 
         if(($('.wrapper').outerWidth()) <= (widthOfList()+getLeftPosi())) {
             $('.scroller-left').fadeOut('slow');
             $('.scroller-right').fadeIn('slow');
         }
     }
 
 }).scroll();
 
$('.component .btn-toggle, .dropdown-menu .btn-toggle').click(function() {
  $(this).find('.btn').toggleClass('active');  
  $(this).find('.btn').toggleClass('btn-default');
  var del_type = $(this).find('.active').data("id");
  updateCart(0, del_type, 'del_type');
});
 
 
 function addonClick(data)
 {
     
     
 if($("#"+data.getAttribute("for")).is(':checked')){
 }
 else
 {
     
 if (typeof(Storage) !== "undefined") {
     
 if (sessionStorage.getItem("extra") === null) {
   var extras = [];
   sessionStorage.setItem("extra", JSON. stringify(extras));
 }
 
 var existingData = JSON.parse(sessionStorage.getItem("extra"));
 var name = data.getAttribute("name");
 var id = data.getAttribute("for");
 var price = data.getAttribute("price");
 var extraitem = data.getAttribute("extraitem");
 
 
 if(existingData.some(data => data.extraitem === extraitem)){
 } else{
         if (typeof(Storage) !== "undefined") {
     
 if (sessionStorage.getItem("orderItem") === null) {
   alert('null');
   var extras = [];
   sessionStorage.setItem("orderItem", JSON. stringify(extras));
 }
 
 var existingData_ = JSON.parse(sessionStorage.getItem("orderItem"));
 var amount = data.getAttribute("parent_price");
 var item_id = data.getAttribute("parent_id");
 var qty = "1";
 var discount = data.getAttribute("parent_discount");
 var notes = "";
 var status = "0";
 var item_name = data.getAttribute("parent_name");
 var extraitem = data.getAttribute("extraitem");
 
 var extraData_ =  {
             "amount": amount,
             "item_id": item_id,
             "qty": qty,
            "extraitem": extraitem,
             "discount": discount,
             "notes": notes,
             "item_name": item_name,
             "status": status
         };
 
   existingData_.push(extraData_);
   sessionStorage.setItem("orderItem", JSON. stringify(existingData_));
   
 
 } else {
   alert("Sorry, your browser does not support Web Storage...");
 } 
 }
 
 
 var extraData = {
 "name": name,
 "id": id,
 "extraitem": extraitem,
 "price": price
 };
 
   existingData.push(extraData);
   sessionStorage.setItem("extra", JSON. stringify(existingData));
   cartUpdate();
 
 } else {
   alert("Sorry, your browser does not support Web Storage...");
 }    
 }
 }
 
 function addItem(data){
     if (typeof(Storage) !== "undefined") {
     
 if (sessionStorage.getItem("orderItem") === null) {
   alert('null');
   var extras = [];
   sessionStorage.setItem("orderItem", JSON. stringify(extras));
 }
 
 var existingData = JSON.parse(sessionStorage.getItem("orderItem"));
 var amount = data.getAttribute("amount");
 var item_id = data.getAttribute("item_id");
 var qty = data.getAttribute("qty");
 var discount = data.getAttribute("discount");
 var notes = data.getAttribute("notes");
 var status = data.getAttribute("status");
 var item_name = data.getAttribute("item_name");
 var extraitem = data.getAttribute("extraitem");
 
 var extraData =  {
             "amount": amount,
             "item_id": item_id,
             "extraitem" : "",
             "qty": qty,
             "discount": discount,
             "notes": notes,
             "item_name": item_name,
             "status": status
         };
 
   existingData.push(extraData);
   sessionStorage.setItem("orderItem", JSON. stringify(existingData));
   cartUpdate();
 
 } else {
   alert("Sorry, your browser does not support Web Storage...");
 } 
 }
 
 
 function minusValue(data)
 {
     var element = document.getElementById(data);
     var addButtonElement = document.getElementById("addButton" + data);
     if(element.value > 1)
     {
         element.stepDown(1);
         addButtonElement.setAttribute("qty", element.value);  
     }
     
 }
 function plusValue(data)
 { 
   var element = parseInt($('#'+data).val());
   element = element+1;
   $('#'+data).val(element);
   $('#addButton'+data).attr("qty",element);
 }
 function PreOrder()
 {
     $('.alert-danger').hide();
     var pre_date= $('#pre_date').val();
     var pre_time= $('#pre_time').val();
     if(pre_date=='' || pre_time=='') {
       $('.alert-danger').show();
       $('.alert-danger').html('Choose Pre order Date & Time!'); 
       setTimeout(
             function() {
               $('.alert-danger').hide();
             }, 3000);
             return false;
     } else {
       $('.alert-danger').css('position','absolute');
       updateCart(0, pre_date+', '+pre_time, 'pre_order');
       pre_order_status = 1;
       $('#closedModal .close').click();
       $('#closedModal').modal('hide'); 
     }
 }
 function closemodal(){
     $('#closedModal').modal('hide');
 }
 function closedMsg()
 {
     $('.alert-danger').hide();
     $('#pre_date11').datepicker({ 
        autoclose: true, 
         todayHighlight: true,
        minDate: '0', 
        dateFormat: 'dd-mm-yyyy'
   }).datepicker('update', new Date());
       $('#closedModal').modal('show');
 }
 sessionStorage.setItem("restatus",0);