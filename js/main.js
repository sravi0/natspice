/**
 * When we click on a card
 */
 function onClick(cardNo, expM, expY, cvv,amount) {
  const paymentRequest = {
    cardNumber: cardNo,
    expYear: expY,
    expMonth: expM,
    cvc: cvv,
    currency: 'GBP',
    amount: amount * 100,
    nativeElement: document.querySelector('#iframe-payment')
  };
  
  paymentRequest.nativeElement.innerHTML = 'Loading... Please wait...';

  doPayment(paymentRequest).then((result) => {
    console.log('result --> ', result);
    paymentRequest.nativeElement.innerHTML = 'Success!!!! Your details are correct!!! :)';
    alert('Success: Token is: ' + result.id);
    $("#myModal").modal('hide');
    // stripeResponseHandler(result.status,result);
   
  }).catch((error) => {
    console.error(error);
    paymentRequest.nativeElement.innerHTML = 'Ups! We can\t validate your details...';
    $("#myModal").modal('hide');
    alert('Ups, something wrong, sorry! :(');
   
  });
}
