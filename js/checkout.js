
$('.makePayment').on('click', function() {
  var $this = $(this);
$this.button('loading');
  setTimeout(function() {
     $this.button('reset');
 }, 8000);
});

function refreshCart() 
{ 
    var del_type = $('.btn-toggle .active').data("id");
    $.ajax({
        type: "POST",
        url: "ajaxAddToCart2.php",
        data: {action : 'del_type', qty: del_type},
        success: function(res) 
        {
           if(res==1) {
              window.location.href = 'menu.php';
           } else {
              $('#cartItems_section').html(res);
           }
        }
    });
}
refreshCart();
getCartPrice();

$('.btn-toggle').click(function() {
 
 $(this).find('.btn').toggleClass('active');  
 
 $(this).find('.btn').toggleClass('btn-default');
 var del_type = $('.btn-toggle .active').data("id");
 if(del_type=='collection') {  
    $('#choose_address').hide(); 
    $('.personal_detail, .osahan-payment').show();
 } else {  
    if(dellivery_addr_id===0) {
       $('.personal_detail, .osahan-payment').hide();
    } else {
       $('.personal_detail, .osahan-payment').show();
    }
    $('#choose_address').show(); 
 }
 updateCart(0, del_type, 'del_type');
 //alert($('.btn-toggle .active').data("id"));
    
});
function getCartPrice(){
 $.ajax({
        async: false,
        type: "POST",
        url: "ajaxAddToCart3.php",
        data: {action : 1},
        dataType: "json",
        success: function(res) 
        {
            items = res.items;
            extra_items = res.extra_items;
            ser_fee = res.ser_fee;
            del_fee = res.del_fee;
            disc_fee = res.disc_fee;
            subTot = res.subTot;
            grandTot = res.grandTot;
            grandTot2 = res.grandTot2;
            $('.makePayment').html('Pay Now £'+grandTot);
        }
 });
}
function updateCart(id, qty, act){
    //alert(id+', '+qty+', '+act);
    if(act=='add') {
      qty = qty + 1; act = 'update';
    } else if(act=='remove') {
      qty = qty - 1; act = 'update';
    } else {
       act = act;
    } 
    
    $.ajax({
        type: "POST",
        url: "ajaxAddToCart2.php",
        data: {action : act, qty: qty, id: id},
        success: function(res) 
        {
          if(act!='cart_clear') {
            $('#cartItems_section').html(res);
            getCartPrice();
          }
        }
    });
}
function setInputFilter(textbox, inputFilter) {
["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
  textbox.addEventListener(event, function() {
    if (inputFilter(this.value)) {
      this.oldValue = this.value;
      this.oldSelectionStart = this.selectionStart;
      this.oldSelectionEnd = this.selectionEnd;
    } else if (this.hasOwnProperty("oldValue")) {
      this.value = this.oldValue;
      this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
    } else {
      this.value = "";
    }
  });
});
}
setInputFilter(document.getElementById("p_phone"), function(value) {
return /^-?\d*$/.test(value); 
});
setInputFilter(document.getElementById("cardNumber"), function(value) {
return /^-?\d*$/.test(value); 
});
setInputFilter(document.getElementById("cardCVC"), function(value) {
return /^-?\d*$/.test(value); 
});
function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
function showError(errMsg) {
var x = document.getElementById("snackbar");
x.innerHTML=errMsg;
x.className = "show";
setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
//alert(1);
}
function cardValidation () {
  $('.makePayment').hide();
  $('.pay_loading').show();
 //$(".makePayment").addClass("disable-click");
 //$(".makePayment").css('cursor','not-allowed');
  var valid = true;
  var name = $('#cardname').val();

  var cardNumber = $('#cardNumber').val();
  var month = $('#month').val();
  var year = $('#year').val();
  var cvc = $('#cardCVC').val();

  $("#error-message").html("").hide();

  if (name.trim() == "") {
      valid = false;
  }

  if (cardNumber.trim() == "") {
       valid = false;
  }

  if (month.trim() == "") {
        valid = false;
  }
  if (year.trim() == "") {
      valid = false;
  }
  if (cvc.trim() == "") {
      valid = false;
  }

  if(valid == false) {
      //$("#error-message").html("All Fields are required").show();
     $('.makePayment').show(); 
     $('.pay_loading').hide();
     showError("All Fields are required!");
     $(".makePayment").removeClass("disable-click");
       
  }
var carddet = {
  cardno: cardNumber,
  exp_month: month,
  exp_year: year,
  modified_date: "null",
  name_on_card: name,
  card_type: "null",
  cvv: cvc
};
//var cardetails=JSON.stringify(carddet);
 // sessionStorage.setItem("catdet", cardetails);
  return valid;
}
Stripe.setPublishableKey("pk_test_51HEvMzIfM6gV7KrMQJr4ehPNepKbMQmJ4NgwyQLdNtsprtD1SFNSm3wzvrD5RiDgPaWxnFN6sIJ7r1s8CHaa5Quj00jWsso6VZ");

function stripeResponseHandler(status, response) {
  $('.makePayment').hide();
  $('.pay_loading').show();
  //$(".makePayment").addClass("disable-click");
  //$(".makePayment").css('cursor','not-allowed'); var ser_fee = del_fee = disc_fee = subTot = grandTot = 0;
   var amount = grandTot;
       // console.log(amount.toFixed(2))
       // console.log(JSON.stringify(response))
     //  alert(JSON.stringify(response));
   if (response.error) {
       // alert();
      // alert(response.error.message);
     $('.makePayment').show();
     $('.pay_loading').hide();
      showError("Payment Failed! Error: "+response.error.message);
      $(".makePayment").removeClass("disable-click");
      

        var x = document.getElementById("snackbar"); 
        x.innerHTML=response.error.message;
        document.getElementById('onloader').style.display='none';
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
       //enable the submit button
       // $("#submit-btn").show();
       // $( "#loader" ).css("display", "none");
       //display the errors on the form
       // $("#error-message").html(response.error.message).show();
       document.getElementById('makePayment').style.display="block";
   } else {
       //get token id

       var token = response['id'];
       //insert the token into the form
       $("#frmStripePayment").append("<input type='hidden' name='token' value='" + token + "' />");
        $("#frmStripePayment").append("<input type='hidden' name='stripeToken' value='" + token + "' />");
        $("#frmStripePayment").append("<input type='hidden' name='customer' value='"+userID+"' />");
         $("#frmStripePayment").append("<input type='hidden' name='amount' value='"+grandTot+"' />");
          $("#frmStripePayment").append("<input type='hidden' name='orderID' value='" + userOrderId + "' />");
        
        //alert(JSON.stringify($('#frmStripePayment').serialize()));

        $.ajax({
                    type: 'post',
                    url: 'stripe_charge.php',
                    data: $('#frmStripePayment').serialize(),
                    success: function(json) {
                       //$('.makePayment').show();
                       //showError(JSON.stringify(json));
                       var data = JSON.parse(json);
                       if (data.paymentStatus == 'succeeded') {
                          orderID = userOrderId;
                          alert("Payment Successful! Order ID: "+orderID);
                           // alert(data);
                          // document.getElementById('onloader').style.display='none';
                          placeOrder(orderID, "ECOM");
                       }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        //alert(textStatus);
                       $('.makePayment').show();
                       $('.pay_loading').hide();
                       showError("Payment Failed! "+textStatus);
                       $(".makePayment").removeClass("disable-click");
                       
                    }
                 });
   //$("#theForm").ajaxSubmit({url: 'server.php', type: 'post'})
   }
}
function stripePay(e, ordTy) {
     $('.makePayment').hide();
     $('.pay_loading').show();
     
     ////$(".makePayment").addClass("disable-click");
     //$(".makePayment").css('cursor','not-allowed');
     //alert($(".btn-toggle .active").attr("data-id")); return false;

     var radioValue = addressDetails = dellivery_addr_text;
                          // alert(radioValue); 
     addressDetails = radioValue;
     deliveryorcollection = true;
     
     

     var uname = $("#p_full_name").val();
     var uemail = $("#p_email").val();
     var uphone = $("#p_phone").val();

     if(uname.trim()=='' || uemail.trim()=='' || uphone.trim()=='') {
        $('.makePayment').show();
        $('.pay_loading').hide();
        showError("Please Enter Your Personal Details!");
        return false;
     }
     
     //uphone = document.getElementById("userphone1").value;

     if (uemail == "" || !validateEmail(uemail)) {

        $('.makePayment').show();
        $('.pay_loading').hide();
        showError("Please enter valid email address!");
        return false;
     }

     if (uphone.length <10 || uphone.length>11 ) {
        $('.makePayment').show();
        $('.pay_loading').hide();
        showError("Please enter valid Phone number!");
        return false;
     }
     if(ordTy==1) {
           e.preventDefault();
           var valid = cardValidation();
           if(valid == true) 
           {
              //$("#submit-btn").hide();
           // $( "#loader" ).css("display", "inline-block");
              //alert("Card Details Valid!");
              Stripe.createToken({
                 number: $('#cardNumber').val(),
                 cvc: $('#cardCVC').val(),
                 exp_month: $('#month').val(),
                 exp_year: $('#year').val()
              }, stripeResponseHandler);
              //submit from callback
              return false;
           }
           else{
              
              showError("Please Enter valid Card Details !!!!");
              //alert("Please Enter valid Card Details !!!!");
           }
     } 
     else if(ordTy==2){
        orderID = userOrderId;
        placeOrder(orderID, "COD");
        alert("Your order has been placed! Order ID: "+orderID);
     }
}
$("#checkoutwithcash").click(function() {
  $('#checkoutwithcash').prop('disabled', true);
});
function placeOrder(orderNo, paymentType) {
    //document.getElementById('onloader').style.display='block';
    var delivery_charges_radioradioValue = $(".btn-toggle .active").attr("data-id");
    var del_type = $('.btn-toggle .active').data("id");
    var couponid=document.getElementById('couponid').value;
    var addressDetails =[];
    var deliveryorcollection = false;
    //var delivery_charges_radioradioValue = $("input[name='delivery_charges_radio']:checked").val();
    //var delivery_charges_radioradioValue = "Delivery"

     var radioValue = addressDetails = dellivery_addr_text;
     addressDetails = radioValue;

     if(del_type=='delivery') {
        addressDetails = radioValue.split(",");
        deliveryorcollection = true;
     } else {
        deliveryorcollection = false;
     }

  

  var uname = $("#p_full_name").val();
  var uemail = $("#p_email").val();
  var uphone = $("#p_phone").val();
  
  var jsonData = {
     "websiteFrom": "GR",
     "hotel_name":"Grill Guru",
     "hotel_add":"48-52 Oswald Street, G14pl",
     "hotel_phone":"01412218777",
     "hotel_city":"glasgow",
     "hotel_pin":"G14pl",
     "name": uname,
     "user_add": (deliveryorcollection == false ? '0' : addressDetails[0]),
     "user_email": uemail,
     "number": uphone,
     "email": uemail,
     "card": {
     "card_no": document.getElementById('cardNumber').value,
     "card_type": "null",
     "cvv": document.getElementById('cardCVC').value,
     "exp_month": document.getElementById('month').value,
     "exp_year": document.getElementById('year').value,
     "modified_date": "null",
     "name_on_card": uname,
     "user_id": userID2
     },
     "order": {
        "delivery_charges": del_fee,
        "amount": grandTot2,
        "day": preOrd_dt,
        "delivery_type": (deliveryorcollection == false ? '1' : '0'),
        "discount": disc_fee,
        "driver_id": "0",
        "hotel_id": "47",
        "order_no": orderNo,
        "c_id":couponid,
        "payment_type": paymentType,
        "status": "2",
        "time": preOrd_ti,
        "name": uname,
        "number": uphone,
        "email": uemail,
        "user_address_id":  (deliveryorcollection == false ? '0' : addressDetails[0]),
        "user_id": userID2
     },
     "orderItem": items,
     "extra": extra_items,
     "transcation": {}
  };
 // console.log(JSON.stringify(jsonData)); return false;
  $.ajax({
     type: "POST",
     url: 'https://deliveryguru.co.uk/dg_api/placeOrder',
     dataType: "json",
     crossDomain: true,
     data: jsonData,
     success: function(json) {
       //   console.log(json);
         updateCart(0, 'cart_clear', 'cart_clear');
         sessionStorage.removeItem('cart');
         sessionStorage.removeItem('catdet');
         sessionStorage.removeItem('amount');
         window.location.href = "myaccount.php";
        document.getElementById('onloader').style.display='none';
     },
     error: function(jqXHR, exception,json) {
       document.getElementById('onloader').style.display='none';
       sessionStorage.removeItem('cart');
       sessionStorage.removeItem('catdet');
       sessionStorage.removeItem('amount');
        var msg = '';
        if (jqXHR.status === 0) {
              msg = 'Not connect.\n Verify Network.';
        } else if (jqXHR.status == 404) {
              msg = 'Requested page not found. [404]';
        } else if (jqXHR.status == 500) {
              msg = 'Internal Server Error [500].';
        } else if (exception === 'parsererror') {
              msg = 'Requested JSON parse failed.';
        } else if (exception === 'timeout') {
              msg = 'Time out error.';
        } else if (exception === 'abort') {
              msg = 'Ajax request aborted.';
        } else {
              msg = 'Uncaught Error.\n' + jqXHR.responseText;
        }
        alert('failed to Place order Please Close try Again!');
        window.location.href = "menu.php";
     }
  });
}



function addaddress() {



  // return false;


 if (document.getElementById('street_number').value == "") {
  alert("Door No or Flat No must be filled out");
  return false;
}
if (document.getElementById('addrr').value == "") {
  alert("Street Address must be filled out");
  return false;
}
if (document.getElementById('postal_code').value == "") {
  alert("Postcode must be filled out");
  return false;
}
//     if (document.getElementById('administrative_area_level_1').value == "") {
//       alert("County must be filled out");
//       return false;
//   }
if (document.getElementById('locality').value == "") {
  alert("city must be filled out");
  return false;
}
if (document.getElementById('landmark').value == "") {
  alert("Please Fill Delivery Instructions to help Delivery Person");
  return false;
}
var stadd=document.getElementById('street_number').value;
var stadd1=document.getElementById('street_number1').value;

var routadd=document.getElementById('addrr').value;
var postadd=document.getElementById('postal_code').value;
var cityadd=document.getElementById('locality').value;
//   var stateadd=document.getElementById('administrative_area_level_1').value;
if(stadd1!=''){
var completeadd=stadd+','+stadd1+','+routadd+','+cityadd+','+postadd;

}
else{
var completeadd=stadd+','+routadd+','+cityadd+','+postadd;
}

$.ajax({
  type: "POST",
  url: 'https://deliveryguru.co.uk/dg_api/addAddress',
  // url: 'https://deliveryguru.co.uk/dg_api/placeOrder',
  dataType: "json",
  crossDomain: true,
  data: {
     "u_id": userID,
     "home_address": "Door No.  " + completeadd ,
     "permanent_address": "Door No.  " + completeadd ,
     "pincode": document.getElementById('postal_code').value,
     "city": document.getElementById('locality').value,
     "landmark": document.getElementById('landmark').value,
     "state": 'UK',
     "lat": document.getElementById('lat').value,
     "longt": document.getElementById('long').value
  },
  cache: "false",
  success: function(json) {
     // alert(JSON.stringify(json));
    //  document.getElementById("newaddform").reset();
        var modal = document.getElementById("add-address-modal");
        $("#add-address-modal").modal("hide");
        var x = document.getElementById("snackbar");
        $('#snackbar').css('background-color', 'green');
        x.innerHTML="Address Added Successfully";
         x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
        getaddress();
  },
  error: function(json) {
       var modal = document.getElementById("add-address-modal");
modal.style.display = "none";
     alert(JSON.stringify(json));

  }
});

}

function getprofile(){
$uid=userID;
$token =sessionStorage.getItem('token');
$.ajax({
url: 'https://deliveryguru.co.uk/dg_api/profile/'+$uid,
type: "GET",
crossDomain: true,
dataType: "json",
cache: "false",
headers: {
Authorization: 'Bearer '+$token
},
success: function(result) {
//console.log(result);
$out=JSON.stringify(result);
document.getElementById("profile_name").innerHTML =result.first_name;
document.getElementById("profile_email").innerHTML =result.email;
document.getElementById("profile_phone").innerHTML =result.contact;
document.getElementById("username1").value =result.first_name;
document.getElementById("useremail1").value =result.email;
document.getElementById("userphone1").value =result.contact;
// document.getElementById("usercreated").innerHTML =result.created_at;
}
}); 
}
// getprofile();

function updateprofile(){
var uname = document.getElementById("username1").value;
var uemail = document.getElementById("useremail1").value;
var uphone = document.getElementById("userphone1").value;

$uid=userID;
$token =sessionStorage.getItem('token');
$.ajax({
url: 'https://deliveryguru.co.uk/dg_api/updateUser/'+$uid,
type: "POST",
crossDomain: true,
dataType: "json",
cache: "false",
data:{
first_name: uname,
email: uemail,
contact: uphone

},
headers: {
Authorization: 'Bearer '+$token
},
//  beforeSend: function(x) {
//    if (x && x.overrideMimeType) {
// 	 x.overrideMimeType("application/j-son;charset=UTF-8");
//    }
//  },
success: function(result) {
//Write your code here
if(result!=''){
getprofile();
//alert("updated");
$("#edit-profile-modal").modal("hide");
}
else{
alert("failed");
}
}
}); 
}
function getorders1(){

$uid=userID;
$token =sessionStorage.getItem('token');
var status="";
var obj=[];

$.ajax({
url: 'https://deliveryguru.co.uk/dg_api/getOrders/'+$uid+'/47/7,1,5,2,3,6,12,13',
type: "GET",
crossDomain: true,
dataType: "json",
cache: "false",
headers: {
Authorization: 'Bearer '+$token
},
//  beforeSend: function(x) {
//    if (x && x.overrideMimeType) {
// 	 x.overrideMimeType("application/j-son;charset=UTF-8");
//    }
//  },
success: function(result) {
//Write your code here
if(result.length!=0){
for(var i = 0; i < result.length; i++) {
var preorderdate='';
if(result[i].mainStatus=='7')
{ status="order in kitchen"; }
else if(result[i].mainStatus=='1')
{ status="order accepted and assigned to driver"; }
else if(result[i].mainStatus=='5')
{ status="Order Ready to Deliver";}
else if(result[i].mainStatus=='2')
{ status="Order Placed and in process";}
else if(result[i].mainStatus=='6')
{ status="Rejected By Restaurant";}
else if(result[i].mainStatus=='12')
{ status="Pre Order Placed";
if(result[i].time!=null || result[i].time!='null'){
preorderdate='<i class="icofont-motor-biker"></i> Preorder Delivery Date:'+result[i].day+' | Time: '+result[i].time;
}
}
else if(result[i].mainStatus=='13')
{ status="Pre Order Accepted";
if(result[i].time!=null || result[i].time!='null'){
preorderdate='<i class="icofont-motor-biker"></i> Preorder Delivery Date:'+result[i].day+' | Time: '+result[i].time;
}
}
else if(result[i].mainStatus=='3')
        { status="Order completed";}
            if(result[i].delivery_type=='0'){
            deliverytype="Delivery";
            deliveryaddress=result[i].home_address+','+result[i].pincode;
        }
        else{
            deliverytype="Collection";
            deliveryaddress="Collection Order";
        }
        if(result[i].payment_type=="COD"){
            Paytype="Not Paid";
        }
        else{
            Paytype="Paid";
        }
$orderdet1=result[i];
$orderdet=JSON.stringify($orderdet1);
console.log(result[i]);
var options = { month: 'short', day: 'numeric', year: 'numeric'};
        var today  = new Date(result[i].created_at);
//	obj +="<tr><td>"+result[i].order_no+"</td><td>"+result[i].hotel_name+"</td><td>"+result[i].first_name+"</td><td>"+result[i].amount+"</td><td>"+status+"</td><td><a href='ordertrack?id="+result[i].order_no+"' class='btn btn-primary btn-xs'>Track Order</a></td></tr>"
    //debugger
   obj +='<div class="bg-white card mb-4 order-list shadow-sm">';
   obj +='<div class="gold-members p-4">';
   obj +='<a href="#">';
   obj +='<div class="media">';
   obj +='<img class="mr-4" src="img/3.jpg" alt="Generic placeholder image">';
   obj +='<div class="media-body">';
   obj +='<span class="float-right text-info">'+status+', '+today.toDateString()+','+today.toLocaleTimeString(navigator.language, {hour: '2-digit', minute:'2-digit',hour12: true})+' <i class="icofont-check-circled text-success"></i></span>';
   obj +='<h6 class="mb-2">';
   obj +='<a href="" class="text-black">'+result[i].hotel_name+'</a>';
   obj +='</h6>';
   obj +='<p class="text-gray mb-1"><i class="icofont-location-arrow"></i> '+deliveryaddress+'</p>';
   obj +='<p class="text-gray mb-1"><i class="icofont-list"></i> ORDER #'+result[i].order_no+' <i class="icofont-clock-time ml-2"></i>  '+today.toDateString()+','+today.toLocaleTimeString(navigator.language, {hour: '2-digit', minute:'2-digit',hour12: true})+'</p>';
   obj +='<p class="text-grey mb-1"><i class="icofont-motor-biker"></i> Order Type : '+deliverytype+'</p>';
   obj +='<p class="text-grey mb-1"> '+preorderdate+'</p>';
   obj +='<hr>';
   if(result[i].mainStatus=='3'){
   obj +='<div class="float-right">';
   obj +='<a class="btn btn-sm btn-primary" href="trackorder.php?orderid='+result[i].order_no+'"><i class="icofont-map-pins"></i> VIEW ORDER</a>';
   obj +='</div>';
    }
    else{
   obj +='<div class="float-right">';
   obj +='<a class="btn btn-sm btn-primary" href="trackorder.php?orderid='+result[i].order_no+'"><i class="icofont-map-pins"></i> TRACK ORDER</a>';
   obj +='</div>';
    }
   obj +='<p class="mb-0 text-black text-primary pt-2"><span class="text-black font-weight-bold"> Total :</span> &#163;'+(result[i].amount - result[i].discount).toFixed(2)+'/'+Paytype+'</p>';
   obj +='</div>';
   obj +='</div>';
   obj +='</a>';
   obj +='</div>';
   obj +='</div>';

}
$("#out").html(obj);
}
else{
obj="<tr><td colspan='6'>No Orders Found</td></tr>";
$("#out").html(obj)
}
}
}); 
}
// getorders1();

setInterval(function(){
// 	getorders1();
},12000);
// Address function
function getaddress(){
$uid=userID;
$token =sessionStorage.getItem('token');
var obj=[];
$.ajax({
url: 'https://deliveryguru.co.uk/dg_api/addresses/'+$uid,
type: "GET",
crossDomain: true,
dataType: "json",
cache: "false",
//  beforeSend: function(x) {
//    if (x && x.overrideMimeType) {
// 	 x.overrideMimeType("application/j-son;charset=UTF-8");
//    }
//  },
success: function(result) {
//Write your code here
if(result.address.length==0){
document.getElementById("out").innerHTML = "no recored found";
}
else{
//	console.log(result);
obj+='<div class="row">';
for(var i = 0; i < 2; i++) {
//obj ="<tr><td>"+(i+1)+"</td><td>"+result.address[i].city+"</td><td>"+result.address[i].pincode+"</td><td>"+result.address[i].home_address+"</td><td><a href='address-edit?id="+result.address[i].id+"' class='btn btn-primary'>Edit</a></td><td><a class='btn btn-primary' id="+result.address[i].id+" onclick='deleteaddress(this.id)'>Delete</a></td></tr>"
    
            obj +='<div class="col-md-6">';
              obj +='<div class="bg-white card addresses-item mb-4 border border-success">';
                 obj +='<div class="gold-members p-4">';
                    obj +='<div class="media">';
                       obj +='<div class="mr-3"><i class="icofont-ui-home icofont-3x"></i></div>';
                       obj +='<div class="media-body">';
                          obj +='<h6 class="mb-1 text-black">Home</h6>';
                          obj +='<p class="text-black">'+result.address[i].home_address+'</p>';
                          obj +='<p class="mb-0 text-black font-weight-bold"><a class="btn btn-sm btn-success mr-2" href="#" onclick="getdeliveryfee(this);" id = "'+ result.address[i].id +'" pin="'+result.address[i].pincode+'" addr="'+result.address[i].home_address+'" lat ="'+result.address[i].lat+'" longt = "'+result.address[i].longt+'" value="' + result.address[i].id + "," + result.address[i].home_address + '" name="address_record"> DELIVER HERE</a> ';
                             obj +='<span>40MIN</span>';
                             
                          obj +='</p>';
                           obj +='<p class="mb-0 text-black font-weight-bold"><a class="text-danger" href="#" onclick="deleteaddress1('+result.address[i].id+')"><i class="icofont-ui-delete"></i> DELETE</a></p>';
                       obj +='</div>';
                    obj +='</div>';
                 obj +='</div>';
              obj +='</div>';
           obj +='</div>';
          
        //debugger

}
obj +='<div class="col-md-6">';
              obj +='<div class="bg-white card addresses-item">';
                 obj +='<div class="gold-members p-4">';
                    obj +='<div class="media">';
                       obj +='<div class="mr-3"><i class="icofont-location-pin icofont-3x"></i></div>';
                       obj +='<div class="media-body">';
                         obj +=' <h6 class="mb-1 text-secondary">New Address</h6>';
                          obj +='<p>Deliver only with in 7 miles from Restaurant</p>';
                          obj +='<p class="mb-0 text-black font-weight-bold"><a data-toggle="modal" data-target="#add-address-modal" class="btn btn-sm btn-primary mr-2" href="#"> ADD NEW ADDRESS</a>'; 
                          obj +='</p>';
                       obj +='</div>';
                    obj +='</div>';
                 obj +='</div>';
              obj +='</div>';
           obj +='</div>';
  obj +='<div class="col-md-6">';
              obj +='<div class="bg-white card addresses-item">';
                 obj +='<div class="gold-members p-4">';
                    obj +='<div class="media">';
                       obj +='<div class="mr-3"><i class="icofont-location-pin icofont-3x"></i></div>';
                       obj +='<div class="media-body">';
                         obj +=' <h6 class="mb-1 text-secondary">Show More Address</h6>';
                          obj +='<p>Save Address</p>';
                          obj +='<p class="mb-0 text-black font-weight-bold"><a  class="btn btn-sm btn-primary mr-2" onclick="getaddress1()" style="color:#fff;cursor:pointer;"> Show more</a>'; 
                          obj +='</p>';
                       obj +='</div>';
                    obj +='</div>';
                 obj +='</div>';
              obj +='</div>';
           obj +='</div>';
           obj+='</div>';
     $("#addresslist").html(obj);
}
}
}); 
}
function getaddress1(){
$uid=userID;
$token =sessionStorage.getItem('token');
var obj=[];
$.ajax({
url: 'https://deliveryguru.co.uk/dg_api/addresses/'+$uid,
type: "GET",
crossDomain: true,
dataType: "json",
cache: "false",
headers: {
Authorization: 'Bearer '+$token
},
//  beforeSend: function(x) {
//    if (x && x.overrideMimeType) {
// 	 x.overrideMimeType("application/j-son;charset=UTF-8");
//    }
//  },
success: function(result) {
//Write your code here
if(result.address.length==0){
document.getElementById("out").innerHTML = "no recored found";
}
else{
//	console.log(result);
obj+='<div class="row">';
for(var i = 0; i < result.address.length; i++) {
//obj ="<tr><td>"+(i+1)+"</td><td>"+result.address[i].city+"</td><td>"+result.address[i].pincode+"</td><td>"+result.address[i].home_address+"</td><td><a href='address-edit?id="+result.address[i].id+"' class='btn btn-primary'>Edit</a></td><td><a class='btn btn-primary' id="+result.address[i].id+" onclick='deleteaddress(this.id)'>Delete</a></td></tr>"
    
            obj +='<div class="col-md-6">';
              obj +='<div class="bg-white card addresses-item mb-4 border border-success">';
                 obj +='<div class="gold-members p-4">';
                    obj +='<div class="media">';
                       obj +='<div class="mr-3"><i class="icofont-ui-home icofont-3x"></i></div>';
                       obj +='<div class="media-body">';
                          obj +='<h6 class="mb-1 text-black">Home</h6>';
                          obj +='<p class="text-black">'+result.address[i].home_address+'</p>';
                          obj +='<p class="mb-0 text-black font-weight-bold"><a class="btn btn-sm btn-success mr-2" style="cursor:pointer;color: #fff;" onclick="getdeliveryfee(this);" id = "'+ result.address[i].id +'" pin="'+result.address[i].pincode+'" addr="'+result.address[i].home_address+'" lat ="'+result.address[i].lat+'" longt = "'+result.address[i].longt+'" value="' + result.address[i].id + "," + result.address[i].home_address + '"> DELIVER HERE</a> ';
                             obj +='<span>40MIN</span>';
                             
                          obj +='</p>';
                           obj +='<p class="mb-0 text-black font-weight-bold"><a class="text-danger" href="#" onclick="deleteaddress1('+result.address[i].id+')"><i class="icofont-ui-delete"></i> DELETE</a></p>';
                       obj +='</div>';
                    obj +='</div>';
                 obj +='</div>';
              obj +='</div>';
           obj +='</div>';
          
        //debugger

}
obj +='<div class="col-md-6">';
              obj +='<div class="bg-white card addresses-item">';
                 obj +='<div class="gold-members p-4">';
                    obj +='<div class="media">';
                       obj +='<div class="mr-3"><i class="icofont-location-pin icofont-3x"></i></div>';
                       obj +='<div class="media-body">';
                         obj +=' <h6 class="mb-1 text-secondary">New Address</h6>';
                          obj +='<p>Deliver only with in 7 miles from Restaurant</p>';
                          obj +='<p class="mb-0 text-black font-weight-bold"><a data-toggle="modal" data-target="#add-address-modal" class="btn btn-sm btn-primary mr-2" href="#"> ADD NEW ADDRESS</a>'; 
                          obj +='</p>';
                       obj +='</div>';
                    obj +='</div>';
                 obj +='</div>';
              obj +='</div>';
           obj +='</div>';
           obj+='</div>';
     $("#addresslist").html(obj);
}
}
}); 
}
function changeAddress(){
$('.personal_detail, .osahan-payment').hide();
document.getElementById('addresslist').style.display='block';
document.getElementById('selectedaddresslist').style.display="none";
getaddress();
}
getaddress();
function getdeliveryfee(radio){
var radioValuepin = radio.getAttribute("pin");
var radioValueAddr = radio.getAttribute("addr");
var radioValueId = radio.getAttribute("id");
var post1= radioValuepin.substring(0, 3);
var post=post1.toUpperCase();
// console.log(post);
var dstatus=false;
var dprice='0.00';
// return false;
$.ajax({
type: "GET",
url: 'https://deliveryguru.co.uk/dg_api/getRestaurantsDetails/47',
dataType: "json",
crossDomain: true,
cache: "false",
success: function(json) {
   var fresult=JSON.parse(json[0].deliverydetails);
   var fresult1=fresult.deliverypostcode;
   for(i=0;i<fresult1.length;i++){
       if(post==fresult1[i].postcode){
           dstatus=true;
           dprice=fresult1[i].price;
       }
      
   }
   console.log(dstatus,dprice);
   if(dstatus==true){
                       deliveryfmV = Number(dprice).toFixed(2);
                       document.getElementById("deliveryfm").textContent = deliveryfmV;
                       
                       document.getElementById("sub_total").textContent = (parseFloat(12.00) + parseFloat(dprice) - parseFloat($('#discount').text())+(0.55)).toFixed(2);
                       $('.delivery_address').html(radioValueAddr);
                       dellivery_addr_text = radioValueAddr;
                       dellivery_addr_id = radioValueId;
                       updateCart(0, deliveryfmV, 'del_charge');
                       $('.personal_detail, .osahan-payment').show();
                       document.getElementById('addresslist').style.display='none';
                       document.getElementById('selectedaddresslist').style.display="block";  
   }
   else{
        var x = document.getElementById("snackbar");
           x.innerHTML="NOT DELIVER HERE";
           x.className = "show";
           setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
   }
 
},
error: function(json) {
  alert(JSON.stringify(json));
//   alert('Error occurs in add order!');
}
});

}
function getValue(radio) {
//  document.getElementById('onloader').style.display='block';
var radioValuepin = radio.getAttribute("pin");
var radioValueAddr = radio.getAttribute("addr");
var radioValueId = radio.getAttribute("id");
var post= radioValuepin;
var hotelpost="G14PL";
var distancemiles='';
// console.log("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins="+hotelpost+"&destinations="+post+"&key=AIzaSyDY2j1NE12MzJYS7t-dVay1lXooOpzxZsY");
$.ajax({
  type: "GET",
  url: "getdistace.php?origins="+hotelpost+"&destinations="+post,
   crossDomain: true,
   dataType: 'json',

  success: function(result) {
     if(result.rows[0].elements[0].status=='OK')
     {
        var dis='';
        var distance=result.rows[0].elements[0].distance.text;
        var dis1=distance.split(" ");
        dis=dis1[0];
        
        $.ajax({
        type: "GET",
        url: "https://deliveryguru.co.uk/dg_api/getDeliveryCharge",
        success: function(result) {
              if(dis!=NaN)
              {
                    var deliveryfmTxt = del_sub_total = 0
                    if(dis<=1){
                          deliveryfmTxt = result[0].one;
                    }
                    else if(dis<=2){
                          deliveryfmTxt = result[0].two;
                    }
                    else if(dis<=3){
                          deliveryfmTxt = result[0].three;
                    }
                    else if(dis<=4){
                          deliveryfmTxt = result[0].four;
                    }
                    else if(dis<=5){
                          deliveryfmTxt = result[0].five;
                    }
                    else if(dis<=6){
                          deliveryfmTxt = result[0].six;
                    }
                    else if(dis==7){
                       deliveryfmTxt = result[0].seven;
                    }
                    
                    if(dis<=7) {
                       deliveryfmV = Number(deliveryfmTxt).toFixed(2);
                       document.getElementById("deliveryfm").textContent = deliveryfmV;
                       
                       document.getElementById("sub_total").textContent = (parseFloat(12.00) + parseFloat(deliveryfmTxt) - parseFloat($('#discount').text())+(0.55)).toFixed(2);
                       $('.delivery_address').html(radioValueAddr);
                       dellivery_addr_text = radioValueAddr;
                       dellivery_addr_id = radioValueId;
                       updateCart(0, deliveryfmV, 'del_charge');
                       $('.personal_detail, .osahan-payment').show();
                       document.getElementById('addresslist').style.display='none';
                       document.getElementById('selectedaddresslist').style.display="block";                                       
                    } 
                    else if(dis>7) {
                       document.getElementById("deliveryfm").textContent = "0.00";
                       document.getElementById("sub_total").textContent = (parseFloat(12.00) + 0 - parseFloat($('#discount').text())+(0.55)).toFixed(2);
                       var x = document.getElementById("snackbar");
                       x.innerHTML="NOT DELIVER HERE";
                       x.className = "show";
                       setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
                       return false;
                    }

              }
              else {
                    document.getElementById("deliveryfm").textContent = "7.00";
                    document.getElementById("sub_total").textContent = (parseFloat(12.00) + parseFloat(result[0].seven) - parseFloat($('#discount').text())+(0.55)).toFixed(2);
              }
           }
        });
     }
        else {
           var x = document.getElementById("snackbar");
           x.innerHTML="NOT DELIVER HERE";
           x.className = "show";
           setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
        }
     },
     error: function(XMLHttpRequest, textStatus, errorThrown) {
        console.log(textStatus);
        console.log(errorThrown);
        
              var x = document.getElementById("snackbar");
           x.innerHTML="Try Again";
           x.className = "show";
           setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
           var radList = document.getElementsByName('address_record');

     }
});
}
function deleteaddress(){
var id=document.getElementById('adddeleteid').value;
$token =sessionStorage.getItem('mytoken');
$.ajax({
url: 'https://deliveryguru.co.uk/dg_api/deleteAddress/'+id,
type: "GET",
crossDomain: true,
cache: "false",
headers: {
Authorization: 'Bearer '+$token
},

success: function(result) {

//Write your code here
if(result!=""){
//alert(result);
   $("#delete-address-modal").modal("hide");
  getaddress();
}
else{

alert("failed");
}

}

})
}
function deleteaddress1(id){
document.getElementById('adddeleteid').value=id;
$("#delete-address-modal").modal("show");


}
function addaddress1(){
$("#add-address-modal").modal("show");
}

function couponHandler(){
 document.getElementById('couponSection').style.display="block";
 document.getElementById('couponSelection').style.display="none";
}
function couponValidator(){
var couponval=document.getElementById('couponValue').value;
var userid=userID;
console.log(Number(userid));
if(couponval!='')
{
     $.ajax({ url: 'https://deliveryguru.co.uk/dg_api/verifyCoupon/47/'+couponval+'/'+userid,
        type: "GET",
        crossDomain: true,
        dataType: "json",
        cache: "false",
        success: function(result) {
                 console.log(result);
                 if(result.length!=0 && result.response!="fail") {
                      var discount= result.data.discount;
                       updateCart(couponval, discount, 'coupon');
                     document.getElementById('couponValue').style="display:none";
                   document.getElementById('couponid').value=result.data.id;
                 //if(result.response=="fail" && couponval=='5PK45J') {
                       //var cart = JSON.parse(sessionStorage.cart);
                       
                       //document.getElementById('couponSection').style.display="none";
                       //document.getElementById('afterCoupon').style.display="block";
                       
                       //var total=Number(document.getElementById('sub_total').textContent).toFixed(2);
                       //var finaldiscount=(total)*(discount/100);
                       //var discount=result.data.discount;
                      
                      

                       /*sessionStorage.setItem('coupondiscount',finaldiscount);
                       sessionStorage.setItem('couponid',result.data.id);
                       sessionStorage.setItem('coupondiscountStatus','true');
                       sessionStorage.setItem('coupondiscountper',discount);
                       var getDiscount=sessionStorage.getItem("coupondiscount");
                       sessionStorage.setItem("amount",(total-finaldiscount));
                       document.getElementById('couponDiscount').textContent=Number(getDiscount).toFixed(2);
                       //document.getElementById('discount').textContent=Number(finaldiscount).toFixed(2);
                       //cartUpdate();
                       document.getElementById('sub_total').textContent=Number(total-getDiscount).toFixed(2);*/

                          //document.getElementById('couponAlert').innerHTML="Congrats Coupon Applied SuccesFully!!!"; 
                          
                          var x = document.getElementById("snackbar");
                          x.innerHTML="The <b>"+couponval+"</b> coupon code has been applied and redeemed successfully!";
                          x.className = "show";
                          $('#snackbar').css('background-color', 'green');
                          setTimeout(function(){ x.className = x.className.replace("show", ""); $('#snackbar').css('background-color', 'red'); }, 3000);
                 }
                 else{
                    updateCart(couponval, '', 'coupon');
                    var x = document.getElementById("snackbar");
                          x.innerHTML="Invalid Coupon";
                          x.className = "show";
                          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
                    }
           }
        });
}
else{
        updateCart(couponval, '', 'coupon');
        var x = document.getElementById("snackbar");
        x.innerHTML="Please Enter Coupon";
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}

}
function removeCoupon(){
var couponval='';
updateCart(couponval, '', 'coupon');
}
function GetCardType()
{
var number=document.getElementById('cardNumber').value;
var out='';
// visa
var re = new RegExp("^4");
if (number.match(re) != null)
out="<i class='icofont-visa-alt' style='color: #000;font-size: 39px;'></i>";

// Mastercard 
// Updated for Mastercard 2017 BINs expansion
if (/^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/.test(number)) 
out="<i class='icofont-mastercard-alt' style='color: #000;font-size: 39px;'></i>";

// AMEX
re = new RegExp("^3[47]");
if (number.match(re) != null)
out="<i class='icofont-american-express-alt' style='color: #000;font-size: 39px;'></i>";

// Discover
re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
if (number.match(re) != null)
out="<i class='icofont-discover-alt' style='color: #000;font-size: 39px;'></i>";



// JCB
re = new RegExp("^35(2[89]|[3-8][0-9])");
if (number.match(re) != null)
out="<i class='icofont-jcb-alt' style='color: #000;font-size: 39px;'></i>";

// Visa Electron
re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
if (number.match(re) != null)
out="<i class='icofont-visa-alt' style='color: #000;font-size: 39px;'></i>";
document.getElementById('cardIdname').innerHTML=out;

}
function formats(ele,e){
if(ele.value.length<19){
ele.value= ele.value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
return true;
}else{
return false;
}
}

function numberValidation(e){
e.target.value = e.target.value.replace(/[^\d ]/g,'');
return false;
}