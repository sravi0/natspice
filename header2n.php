<nav class="navbar navbar-expand-sm navbar-light bg-light osahan-nav shadow-sm fixed-top">
         <div class="container">
            <button class="navbar-toggler ml-2 " type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
               <ul class="navbar-nav ml-auto">
               <li class="nav-item active">
                     <a class="nav-link" href="index.php">Home </a>
                  </li>
                  <li class="nav-item active">
                     <a class="nav-link" href="menu.php">Menu </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="deals.php"><i class="icofont-sale-discount"></i> Deals </a>
                  </li>
                 
                  <li class="nav-item">
                     <a class="nav-link" href="contact.php"> Contact Us</a>
                  </li>
                  <?php if(!isset($_SESSION['uid'])){?>
                  <li class="nav-item">
                     <a class="nav-link" href="login.php"> Login </a>
                  </li>
                  <?php } ?>
                   <?php if(isset($_SESSION['uid'])){?>
                  <li class="nav-item">
                     <a class="nav-link" href="myaccount.php"> My Account </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="myaccount.php"> Orders </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="logout.php"> Logout </a>
                  </li>
                  <?php } ?>
                                                    </ul>
            </div>

            <input name="" class="form-control mobileonly col-7 search-mob" placeholder="Search by Item Name..." style="background:#fff;">

            <button class="navbar-toggler mr-2 mobCart" type="button" style="background:red;color:#fff;">
                  <i class="cart icon"></i> <span class="badge-notification">0</span>
            </button>
         </div>
      </nav>