<?php

function mailSend($toName, $toEmail, $subj, $cont)
{
		require_once('class.phpmailer.php');
	  $frmMail = 'info@deliveryguru.org.uk';
	  $frmPass = 'Delivery123';
	  
	  $mail = new PHPMailer(); // create a new object
	  //$mail->IsSMTP(); // enable SMTP
	  $mail->Host = "relay-hosting.secureserver.net";
	  $mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
	  $mail->SMTPAuth = true; // authentication enabled
	  $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
	  $mail->Port = 465; // or 587
	  $mail->IsHTML(true);
	  $mail->Username = $frmMail; // My gmail username
	  $mail->Password = $frmPass; // My Gmail Password
	  $mail->SetFrom($frmMail);
	  $mail->Subject = $subj;
	  $mail->Body = strtr(file_get_contents('email-template.html'), array('{{NAME}}' => $toName, '{{CONTENT}}' => $cont));
	  $mail->AddAddress($toEmail);
	  $mail->Send();
}