<footer>
            <div class="container">
               
               <!-- End row -->
               <div class="row">
                  <div class="col-md-2">
                     <div id="social_footer">
					
                        <ul>
						<h4>Follow us on</h4>
                           <li><a href="https://www.facebook.com/GrillGuruGlasgow/" target="_blank"><img src="img/Facebook.png" alt="" style="width: 30px;"></a></li>
                           <!--<li><a href="#"><i class="icon-twitter"></i></a></li>
                           <li><a href="#"><i class="icon-google"></i></a></li>-->
                           <li><a href="https://www.instagram.com/grillguru_glasgow/" target="_blank"><img src="img/instagram.png" alt="" style="width: 30px;"></a></li>
                           <!--<li><a href="#"><i class="icon-pinterest"></i></a></li>
                           <li><a href="#"><i class="icon-vimeo"></i></a></li>
                           <li><a href="#"><i class="icon-youtube-play"></i></a></li>-->
                        </ul>
                       
                     </div>    
				
                  </div>
				    
				  <div class="col-md-4">
				  	 <div id="social_footer">
					
                           <ul>
						<h4>Download Our App</h4>
                           <li style="margin: 0px"><a href="https://apps.apple.com/us/app/grill-guru/id1305799185" target="_blank"><img src="img/ios.svg" alt="" style="width: 135px;"></a></li>
                           <!--<li><a href="#"><i class="icon-twitter"></i></a></li>
                           <li><a href="#"><i class="icon-google"></i></a></li>-->
                           <li style="margin: 0px 94px 0px 100px;"><a href="https://play.google.com/store/apps/details?id=uk.co.appsforu.android59fb1384c3de6" target="_blank"><img src="img/android.svg" alt="" style="width: 135px;"></a></li>
                           <!--<li><a href="#"><i class="icon-pinterest"></i></a></li>
                           <li><a href="#"><i class="icon-vimeo"></i></a></li>
                           <li><a href="#"><i class="icon-youtube-play"></i></a></li>-->
                        </ul>
                       
                     </div>
                     </div>
                     <div class="col-md-6">
                <div id="subscription_area">
  <div class="">
    <div class="row">
      <div class="col-sm-12">
        <div class="subscribe_now">
          <h4>subscribe</h4>
          <p>Get connected to our insights</p>
          <form class="subscribe_form">
            <div class="input-group">
               <input type="text" class="form-control" name="email" placeholder="Enter your email">
               <span class="input-group-btn">
                    <button class="btn btn-confirm" style="">subscribe</button>
               </span>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
                     </div>
					  
               </div>
               <!-- End row -->
			   <p style="text-align:center;margin-top:30px">© Grillguru</p>
            </div>
            <!-- End container -->
         </footer>