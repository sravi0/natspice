<?php
include 'src/config.php';
if(!isset($_SESSION['uid'])){
    header("Location:login.php");
}
else{
    $uid=$_SESSION['uid'];

  function getHotelAPI() {
  $graph_url= 'https://deliveryguru.co.uk/dg_api/getRestaurantsDetails/200';
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $graph_url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  $output = curl_exec($ch); 
  curl_close($ch);
  return json_decode($output, true); 
}
$getHotelDetails = getHotelAPI();
$_SESSION['discountper']=$getHotelDetails[0]['discount'];
?>
<!doctype html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- Favicons-->
   <title>NATURAL SPICE TANDOORI </title>
      
     <link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
      <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome-->
      <link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
      <!-- Font Awesome-->
      <link href="vendor/icofont/icofont.min.css" rel="stylesheet">
      <!-- Select2 CSS-->
      <link href="vendor/select2/css/select2.min.css" rel="stylesheet">
      <!-- Custom styles for this template-->
      <link href="css1/osahan.css" rel="stylesheet">
      <script src="js/jquery-3.5.1.js"></script>
      <style>
     @media only screen and ((min-width: 576px){
.modal-dialog {
    max-width: 80% !important;
}
}
      </style>
        <style>
#snackbar {
  visibility: hidden;
  min-width: 250px;
  margin-left: -125px;
  background-color: green;
  color: #fff;
  text-align: center;
  border-radius: 2px;
  padding: 16px;
  position: fixed;
  z-index: 1;
  left: 50%;
  bottom: 90px;
  font-size: 17px;
}

#snackbar.show {
  visibility: visible;
  -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
  animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

@-webkit-keyframes fadein {
  from {bottom: 0; opacity: 0;} 
  to {bottom: 90px; opacity: 1;}
}

@keyframes fadein {
  from {bottom: 0; opacity: 0;}
  to {bottom: 90px; opacity: 1;}
}

@-webkit-keyframes fadeout {
  from {bottom: 90px; opacity: 1;} 
  to {bottom: 0; opacity: 0;}
}

@keyframes fadeout {
  from {bottom: 90px; opacity: 1;}
  to {bottom: 0; opacity: 0;}
}
.list-card-body{
    height: 90px !important;
    
}
 @media only screen and (max-width: 600px) {
           #leftarrow{
               display:none;
           }
           #rightarrow{
               display:none;
           }
           .totals {
     position: unset !important; 
    bottom: 0 !important;
     width: 100% !important; 
    height: 186px !important;
    right: 0 !important;
    background: #ae1c2a !important;
}
#sidebar{
    display:block;
    position: unset !important;
}
}
#snackbar1{
    background: green;
    color: #fff;
    width: 100%;
    text-align: center;
    padding: 5px;}
      .unselectable {
        -webkit-user-select: none;
        -webkit-touch-callout: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        /*color: #cc0000;*/
      } 
      .delete_account ul li:hover{
    background: #000;
          color:#fff;
      }
.delete_account ul li p:hover{
    background: #000;
    color:#fff;
}
.delete_account ul li{
       list-style: none;
    line-height: 15px;
    border: 0.5px solid #f1f1f1;
    padding: 17px;
    cursor:pointer;
}
.delete_account ul{
    padding: 0px;
}
.delete_account ul li span{
        color: #b1b1b1;
    font-size: 20px;
}
.delete_account ul li p{
    color:#b1b1b1;
}
</style>
   </head>
   <body class="unselectable">
        <div id="snackbar"></div>
      <!-- Modal -->
      <div class="modal fade" id="edit-profile-modal" tabindex="-1" role="dialog" aria-labelledby="edit-profile" aria-hidden="true">
         <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="edit-profile">Edit profile</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <form onsubmit="return validateemail();">
                     <div class="form-row">
                         <div id="snackbar1" style="display:none;"></div>
                     <div class="form-group col-md-12 mb-0">
                           <label>Name
                           </label>
                           <input type="text" value="" id="username1" class="form-control" placeholder="Enter Name" onpaste="return false;" onCopy="return false" onkeypress="allowAlphaNumericSpace(event)" onCut="return false" onDrag="return false" onDrop="return false" maxlength="17" autocomplete=off>
                        </div>
                        <div class="form-group col-md-12">
                           <label>Phone number
                           </label>
                           <input type="text" value="" id="userphone1" class="form-control" onkeypress="return isNumberKey(event)" maxlength="10" placeholder="Enter Phone number" onpaste="return false;" onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false" autocomplete=off>
                        </div>
                        <div class="form-group col-md-12">
                           <label>Email id
                           </label>
                           <input type="email" value="" id="useremail1" class="form-control" onfocusout="ValidateEmail(this.value)" placeholder="Enter Email" onpaste="return false;" onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false" autocomplete=off>
                        </div>
                        
                     </div>
                  </form>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn d-flex w-50 text-center justify-content-center btn-outline-primary" onclick="updateprofile1()" data-dismiss="modal">CANCEL
                  </button><button type="button" class="btn d-flex w-50 text-center justify-content-center btn-primary" onclick="updateprofile()">UPDATE</button>
               </div>
            </div>
         </div>
      </div>
     
      <!-- Modal -->
      <div class="modal fade" id="add-address-modal" tabindex="-1" role="dialog" aria-labelledby="add-address" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 70%;">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="add-address" style="font-size: 18px;font-weight:bold;">Add Delivery Address (<img src="villa.png" style="width: 20px;"> Drag Icon to Change Address)</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <form id="addressform">
                     <div class="form-row">
                     <div class="col-md-6">
                     <div class="form-group col-md-12">
                     <div id="map_canvas" style="width: 100%; height: 473px;"></div>
                     </div>
                     </div>
                    <div class="col-md-6">
                    <input type="hidden" id="lat" name="lat" val="40.713956" disabled/>
							<input type="hidden" id="long" name="long" val="74.006653" disabled/>
                     <input type="hidden" class="maddress" id="dnum" value="" style="display:none;" placeholder="Door No!">
                        <div class="form-group col-md-12">
                           <label for="inputPassword4">Door</label>
                           <div class="input-group">
                           <input type="text" required id="door" class="form-control" maxlength="10" onkeypress="return myKeyPress(event)" placeholder="Door" autocomplete="off" />
                           </div>
                        </div>
                        <div class="form-group col-md-12">
                           <label for="inputPassword4">Complete Address
                           </label>
                           <input type="text" required id="address" class="form-control" maxlength="45" onkeypress="return myKeyPress(event)" placeholder="Complete Address e.g. Building number, street name, landmark">
                        </div>
                        <div class="form-group col-md-12">
                           <label for="inputPassword4">State
                           </label>
                           <input type="text" required id="state" class="form-control" maxlength="20" onkeypress="return myKeyPress(event)" placeholder="">
                        </div>
                        <div class="form-group col-md-12">
                           <label for="inputPassword4">City
                           </label>
                           <input type="text" required id="city" class="form-control" maxlength="20" onkeypress="return myKeyPress(event)" placeholder="">
                        </div>
                        <div class="form-group col-md-12">
                           <label for="inputPassword4">Post Code
                           </label>
                           <input type="text" required id="pincode" class="form-control" maxlength="10" onkeypress="return myKeyPress(event)" placeholder="">
                        </div>
                        <div class="form-group col-md-12">
                           <label for="inputPassword4">Landmark
                           </label>
                           <input type="text" required id="landmark" class="form-control" maxlength="30" onkeypress="return myKeyPress(event)" placeholder="Delivery Instructions e.g. Opposite Gold Souk Mall">
                           <p id="landmarkerr" style="color:red;display:none;">* Please Fill Landmark!!!</p>
                        </div>
                        </div>
                        <div id="myaddress" style="display: none;"></div>
                        <!-- <div class="form-group col-md-12">
                           <label for="inputPassword4">Delivery Instructions
                           </label>
                           <input type="text" class="form-control" placeholder="Delivery Instructions e.g. Opposite Gold Souk Mall">
                        </div> -->
                        <!-- <div class="form-group mb-0 col-md-12">
                           <label for="inputPassword4">Nickname
                           </label>
                           <div class="btn-group btn-group-toggle d-flex justify-content-center" data-toggle="buttons">
                              <label class="btn btn-info active">
                              <input type="radio" name="options" id="option1" autocomplete="off" checked> Home
                              </label>
                              <label class="btn btn-info">
                              <input type="radio" name="options" id="option2" autocomplete="off"> Work
                              </label>
                              <label class="btn btn-info">
                              <input type="radio" name="options" id="option3" autocomplete="off"> Other
                              </label>
                           </div>
                        </div> -->
                     </div>
                  </form>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn d-flex w-50 text-center justify-content-center btn-outline-primary" data-dismiss="modal" onclick="canceladdress()">CANCEL
                  </button><button type="button" class="btn d-flex w-50 text-center justify-content-center btn-primary" onclick="addaddress()">SUBMIT</button>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="delete-address-modal" tabindex="-1" role="dialog" aria-labelledby="delete-address" aria-hidden="true">
         <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
               <input type="hidden" id="adddeleteid">
                  <h5 class="modal-title" id="delete-address">Delete</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <p class="mb-0 text-black">Are you sure you want to delete?</p>
                
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn d-flex w-50 text-center justify-content-center btn-outline-primary" data-dismiss="modal">NO
                  </button><button type="button" class="btn d-flex w-50 text-center justify-content-center btn-primary" onclick="deleteaddress()">YES</button>
               </div>
            </div>
         </div>
      </div>
           <!-- delete account Modal -->
      <div class="modal fade" id="delete-account-modal" tabindex="-1" role="dialog" aria-labelledby="delete-address" aria-hidden="true">
         <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
               <input type="hidden" id="user_delete_id">
                  <h5 class="modal-title" id="delete-address">Delete Account</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <p class="mb-0 text-black">Are you sure you want to delete?</p>
                    <textarea rows="4" cols="50" style="width: 100%;"> Do you have feedback for us? we would love to hear you(optional)</textarea>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn d-flex w-50 text-center justify-content-center btn-outline-primary" data-dismiss="modal">NO
                  </button><button type="button" class="btn d-flex w-50 text-center justify-content-center btn-primary" >YES</button>
               </div>
            </div>
         </div>
      </div>
     <?php include("headerot.php");?>
      <section class="section pt-4 pb-4 osahan-account-page">
         <div class="container">
            <div class="row">
               <div class="col-md-3">
                  <div class="osahan-account-page-left shadow-sm bg-white h-100">
                     <div class="border-bottom p-4">
                        <div class="osahan-user text-center">
                           <div class="osahan-user-media">
                             
                              <div class="osahan-user-media-body">
                                 <h6 class="mb-2" id="profile_name"></h6>
                                 <p class="mb-1" id="profile_phone"></p><p class="mb-1"><?php echo $_SESSION['user_login_mobile'];?></p>
                                 <p id="profile_email"><a></a></p>
                                 <p class="mb-0 text-black font-weight-bold"><a class="text-primary mr-3" data-toggle="modal" data-target="#edit-profile-modal" href="#"><i class="icofont-ui-edit"></i> EDIT</a></p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <ul class="nav nav-tabs flex-column border-0 pt-4 pl-4 pb-4" id="myTab" role="tablist">
                        <li class="nav-item">
                           <a class="nav-link active" id="orders-tab" data-toggle="tab" href="#orders" role="tab" aria-controls="orders" aria-selected="true"><i class="icofont-food-cart"></i> Orders</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" id="offers-tab" data-toggle="tab" href="#offers" role="tab" aria-controls="offers" aria-selected="false"><i class="icofont-sale-discount"></i> Offers</a>
                        </li>
                        
                        <!-- <li class="nav-item">
                           <a class="nav-link" id="payments-tab" data-toggle="tab" href="#payments" role="tab" aria-controls="payments" aria-selected="false"><i class="icofont-credit-card"></i> Payments</a>
                        </li> -->
                        <li class="nav-item">
                           <a class="nav-link" id="addresses-tab" data-toggle="tab" href="#addresses" role="tab" aria-controls="addresses" aria-selected="false"><i class="icofont-location-pin"></i> Addresses</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" id="account_delete_tab" data-toggle="tab" href="#account_delete" role="tab" aria-controls="account_delete" aria-selected="false"><i class="icofont-ui-delete"></i> Delete Account</a>
                        </li> 
                     </ul>
                  </div>
               </div>
               <div class="col-md-9">
                  <div class="osahan-account-page-right shadow-sm bg-white p-4 h-100">
                     <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="orders" role="tabpanel" aria-labelledby="orders-tab">
                           <h4 class="font-weight-bold mt-0 mb-4">Orders</h4>
                           <div id="out">

                           </div>
                        </div>
                        <div class="tab-pane fade" id="offers" role="tabpanel" aria-labelledby="offers-tab">
                           <h4 class="font-weight-bold mt-0 mb-4"> Latest Offers</h4>
                          <div class="row">
                             <div class="col-md-6" style="padding-bottom: 21px;" id="offer">
                
                             </div>
                              <div class="col-md-6" id="nooofer" style="margin-right:1px;">
                                 <div class="card offer-card shadow-sm">
                                    <div class="card-body">
                                        <h5 class="card-title"><img src="img/favicon.png"> SOON</h5> 
                                       <h6 class="card-subtitle mb-2 text-block">More Offers soon...</h6>
                                       <!-- <p class="card-text">Use code EAT730 &amp; get 50% off on your first osahan order on Website and Mobile site. Maximum discount: $600</p> -->
                                       <!-- <a href="#" class="card-link">COPY CODE</a> -->
                                       <!-- <a href="#" class="card-link">KNOW MORE</a> -->
                                    </div>
                                 </div>
                              </div>
                           </div>
                           
                        </div>
                         <div class="tab-pane fade" id="account_delete" role="tabpanel" aria-labelledby="account_delete">
                           <h4 class="font-weight-bold mt-0 mb-4"> Delete Account</h4>
                          
                          <div class="row">
                             <div class="col-md-10">
                               
                                      <div class="bg-white card addresses-item shadow-sm">
                                    
                                    <div class="gold-members p-4" style="">
                                       <div class="media">
                                          <div class="mr-3"></div>
                                          <div class="media-body delete_account">
                                             <h6 class="mb-1">Why would you like to delete account?</h6>
                                             <ul>
                                                 <li onclick="deleteAccount()"> <p class="mb-0  font-weight-bold">I don't want use Grillguru anymore   <span style="float:right;">&#62;</span></p> </li>
                                                   <li onclick="deleteAccount()"> <p class="mb-0  font-weight-bold">I'm using different account  <span style="float:right;">&#62;</span></p> </li>
                                                     <li onclick="deleteAccount()"> <p class="mb-0 font-weight-bold">I'm worried about my privacy   <span style="float:right;">&#62;</span></p> </li>
                                                       <li onclick="deleteAccount()"> <p class="mb-0  font-weight-bold">You're sending too many emails   <span style="float:right;">&#62;</span></p> </li>
                                                         <li onclick="deleteAccount()"> <p class="mb-0  font-weight-bold">The App not working properly <span style="float:right;">&#62;</span></p> </li>
                                                  <li onclick="deleteAccount()"> <p class="mb-0  font-weight-bold">other Reasons<span style="float:right;">&#62;</span></p> </li>
                                             </ul>
                                             
                                          </div>
                                       </div>
                                    </div>
                                   
                                 </div>
                             </div>
                             
                           </div>
                           
                        </div>
                     
                        <div class="tab-pane fade" id="addresses" role="tabpanel" aria-labelledby="addresses-tab">
                           <h4 class="font-weight-bold mt-0 mb-4">Manage Addresses</h4>
                           
                           <div class="col-md-12">
                               <div class="alert alert-success" id="success-alert" style="display:none;">
  <button type="button" class="close" data-dismiss="alert">x</button>
  <strong>Success! </strong> <p id="alerttxt"></p>.
</div>
                                 <div class="bg-white card addresses-item shadow-sm">
                                    
                                    <div class="gold-members p-4" style="text-align: center;cursor:pointer;" onclick="addaddress1();">
                                       <div class="media">
                                          <div class="mr-3"></div>
                                          <div class="media-body">
                                             <h6 class="mb-1"><i class="icofont-plus"></i> Add New Address</h6>
                                             <p>
                                             </p>
                                             <!-- <p class="mb-0 text-black font-weight-bold"><a class="text-primary mr-3" data-toggle="modal" data-target="#add-address-modal" href="#"><i class="icofont-ui-edit"></i> EDIT</a> <a class="text-danger" data-toggle="modal" data-target="#delete-address-modal" href="#"><i class="icofont-ui-delete"></i> DELETE</a></p> -->
                                          </div>
                                       </div>
                                    </div>
                                   
                                 </div>
                              </div>
                              </br>
                             <div id="outaddress">

                             </div>
                           
                         
                          
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <footer class="pt-4 pb-4 text-center" style="margin-top: 200px;">
         <div class="container">
            <p class="mt-0 mb-0">© Copyright 2022 DeliveryGuru. All Rights Reserved</p>
          
         </div>
      </footer>
      <!-- jQuery -->
     
      <script data-cfasync="false" src="js/email-decode.min.js"></script>
      <!-- <script src="vendor/jquery/jquery-3.3.1.slim.min.js" type="762714e34f17a0f5b9715d6b-text/javascript"></script> -->
      <!-- Bootstrap core JavaScript-->
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js" type="762714e34f17a0f5b9715d6b-text/javascript"></script>
      <!-- Select2 JavaScript-->
      <script src="vendor/select2/js/select2.min.js" type="762714e34f17a0f5b9715d6b-text/javascript"></script>
      <!-- Custom scripts for all pages-->
      <script src="js/custom.js" type="762714e34f17a0f5b9715d6b-text/javascript"></script>
   <script src="js/rocket-loader.min.js" data-cf-settings="762714e34f17a0f5b9715d6b-|49" defer=""></script></body>
  
   <script>
      function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }  

       function validateemail()  
{  
var x=document.myform.email.value;  
var atposition=x.indexOf("@");  
var dotposition=x.lastIndexOf(".");  
if (atposition<1 || dotposition<atposition+2 || dotposition+2>=x.length){  
  alert("Please enter a valid e-mail address \n atpostion:"+atposition+"\n dotposition:"+dotposition);  
  return false;  
  }  
}  

         function ValidateEmail(inputText)
{
var mailformat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
if(inputText.match(mailformat))
{

document.getElementById('useremail1').focus();
return true;
}
else
{
    var x = document.getElementById("snackbar1");
					    x.style.display="block";
                        x.innerHTML="Please Enter Valid Email Address";
                        x.style.background="red";
                        //  x.className = "show";
                        setTimeout(function(){  x.style.display="none"; }, 3000);
return false;
}
}
function getprofile(){
$uid="<?php echo $_SESSION['uid'];?>";
$token =sessionStorage.getItem('token');
$.ajax({
					 url: 'https://deliveryguru.co.uk/dg_api/profile/'+$uid,
					 type: "GET",
					 crossDomain: true,
					 dataType: "json",
					 cache: "false",
				 success: function(result) {
					//console.log(result);
					 $out=JSON.stringify(result);
					 document.getElementById("profile_name").innerHTML =result.first_name;
					 document.getElementById("profile_email").innerHTML =result.email;
					 document.getElementById("profile_phone").innerHTML =result.contact;
					 document.getElementById("username1").value =result.first_name;
					 document.getElementById("useremail1").value =result.email;
					 document.getElementById("userphone1").value =result.contact;
					// document.getElementById("usercreated").innerHTML =result.created_at;
				 }
		   }); 
}
getprofile();
   function gethomedet(){
    
        var offer="<?php echo $_SESSION['discountper'];?>";
        var obj='';
			         if(Number(offer)==0){
			             document.getElementById("nooofer").style.display="block";
			         }
			         else{
			           document.getElementById("nooofer").style.display="none";
			              obj+='<div class="card offer-card border-0 shadow-sm">';
                    obj+='<div class="card-body">';
                       obj+='<h5 class="card-title"><img src="img/favicon.png"> '+offer+'% Off </h5>';
                       obj+='<h6 class="card-subtitle mb-2 text-block">All Orders</h6>';
                       obj+='<p class="card-text">Get '+offer+'% Discount of All online Orders!!!!<br></p>';
                       obj+='<a href="menu.php" class="card-link">ORDER NOW</a>';
                       obj+=' </div>';
                obj+=' </div>';
                       document.getElementById("offer").innerHTML=obj;
			         }
 
}
gethomedet();
function updateprofile1(){
    getprofile();
}
function allowAlphaNumericSpace(e) {
  var code = ('charCode' in e) ? e.charCode : e.keyCode;
  if (!(code == 32) && // space
    !(code > 47 && code < 58) && // numeric (0-9)
    !(code > 64 && code < 91) && // upper alpha (A-Z)
    !(code > 96 && code < 123)) { // lower alpha (a-z)
    e.preventDefault();
  }
}

function updateprofile(){
	var uname = document.getElementById("username1").value;
	var uemail = document.getElementById("useremail1").value;
	var uphone = document.getElementById("userphone1").value;
if(uname!='' && uemail!='' && uphone!=''){
    if(ValidateEmail(uemail)){

    $uid="<?php echo $_SESSION['uid'];?>";
$token =sessionStorage.getItem('token');
$.ajax({
		 url: 'https://deliveryguru.co.uk/dg_api/updateUser/'+$uid,
					 type: "POST",
					 crossDomain: true,
					 dataType: "json",
					 cache: "false",
					 data:{
						first_name: uname,
						 email: uemail,
						 contact: uphone
						 
					 },
					//  beforeSend: function(x) {
					//    if (x && x.overrideMimeType) {
					// 	 x.overrideMimeType("application/j-son;charset=UTF-8");
					//    }
					//  },
					 success: function(result) {
					 //Write your code here
					 if(result!=''){
					 getprofile();
					//alert("updated");
					 var x = document.getElementById("snackbar");
					    x.style.display="block";
                        x.innerHTML="Profile Updated";
                        x.style.background="green";
                        //  x.className = "show";
                        setTimeout(function(){  x.style.display="none";$("#edit-profile-modal").modal("hide"); }, 3000);
              
					 }
					 else{
						 alert("failed");
					 }
				 }
		   }); 
    }
}
else{
     var x = document.getElementById("snackbar1");
					    x.style.display="block";
                        x.innerHTML="Please Fill All Details";
                        x.style.background="red";
                        //  x.className = "show";
                        setTimeout(function(){  x.style.display="none"; }, 3000);
}
}
function getorders1(){

    $uid="<?php echo $_SESSION['uid'];?>";
$token =sessionStorage.getItem('token');
var status="";
var obj=[];
$.ajax({
					 url: 'https://deliveryguru.co.uk/dg_api/getOrders/'+$uid+'/200/7,1,5,2,3,6,12,13',
					 type: "GET",
					 crossDomain: true,
					 dataType: "json",
					 cache: "false",
					//  beforeSend: function(x) {
					//    if (x && x.overrideMimeType) {
					// 	 x.overrideMimeType("application/j-son;charset=UTF-8");
					//    }
					//  },
					 success: function(result) {
					 //Write your code here
					 if(result.length!=0){
					 for(var i = 0; i < result.length; i++) {
					     var preorderdate='';
						if(result[i].mainStatus=='7')
						{ status="order in kitchen"; }
						else if(result[i].mainStatus=='1')
						{ status="order accepted and assigned to driver"; }
						else if(result[i].mainStatus=='5')
						{ status="Order Ready to Deliver";}
						else if(result[i].mainStatus=='2')
						{ status="Order Placed and in process";}
						else if(result[i].mainStatus=='3')
                        { status="Order completed";}
                        	else if(result[i].mainStatus=='12')
						{ status="Pre Order Placed";
						if(result[i].time!=null || result[i].time!='null'){
						    preorderdate='<i class="icofont-motor-biker"></i> Preorder Delivery Date:'+result[i].day+' | Time: '+result[i].time;
						}
						}
						else if(result[i].mainStatus=='13')
						{ status="Pre Order Accepted";
						if(result[i].time!=null || result[i].time!='null'){
						    preorderdate='<i class="icofont-motor-biker"></i> Preorder Delivery Date:'+result[i].day+' | Time: '+result[i].time;
						}
						}
                        else if(result[i].mainStatus==6)
						{ status="Order Rejected by Restaurant";}
                            if(result[i].delivery_type=='0'){
                            deliverytype="Delivery";
                            deliveryaddress=result[i].home_address+','+result[i].pincode;
                        }
                        else{
                            deliverytype="Collection";
                            deliveryaddress="Collection Order";
                        }
                        if(result[i].payment_type=="COD"){
                            Paytype="Not Paid";
                        }
                        else{
                            Paytype="Paid";
                        }
						$orderdet1=result[i];
						$orderdet=JSON.stringify($orderdet1);
				// 	console.log(result[i]);
						var options = { month: 'short', day: 'numeric', year: 'numeric'};
                       var today= new Date(result[i].created_at.replace(/-/g, "/"));
    				//	obj +="<tr><td>"+result[i].order_no+"</td><td>"+result[i].hotel_name+"</td><td>"+result[i].first_name+"</td><td>"+result[i].amount+"</td><td>"+status+"</td><td><a href='ordertrack?id="+result[i].order_no+"' class='btn btn-primary btn-xs'>Track Order</a></td></tr>"
                    //debugger
                   obj +='<div class="bg-white card mb-4 order-list shadow-sm">';
                   obj +='<div class="gold-members p-4">';
                   obj +='<a href="#">';
                   obj +='<div class="media">';
                   obj +='<img class="mr-4" src="img/3.jpg" alt="Generic placeholder image">';
                   obj +='<div class="media-body">';
                   obj +='<span class="float-right text-info">'+status+', '+today.toDateString()+','+today.toLocaleTimeString(navigator.language, {hour: '2-digit', minute:'2-digit',hour12: true})+' <i class="icofont-check-circled text-success"></i></span>';
                   obj +='<h6 class="mb-2">';
                   obj +='<a href="" class="text-black">'+result[i].hotel_name+'</a>';
                   obj +='</h6>';
                   obj +='<p class="text-gray mb-1"><i class="icofont-location-arrow"></i> '+deliveryaddress+'</p>';
                   obj +='<p class="text-gray mb-1"><i class="icofont-list"></i> ORDER #'+result[i].order_no+' <i class="icofont-clock-time ml-2"></i>  '+today.toDateString()+','+today.toLocaleTimeString(navigator.language, {hour: '2-digit', minute:'2-digit',hour12: true})+'</p>';
                   obj +='<p class="text-grey mb-1"><i class="icofont-motor-biker"></i> Order Type : '+deliverytype+'</p>';
                     obj +='<p class="text-grey mb-1"> '+preorderdate+'</p>';
                   obj +='<hr>';
                   if(result[i].mainStatus=='3'){
                   obj +='<div class="float-right">';
                   obj +='<a class="btn btn-sm btn-primary" href="trackorder.php?orderid='+result[i].order_no+'"><i class="icofont-map-pins"></i> VIEW ORDER</a>';
                   obj +='</div>';
                    }
                    else{
                   obj +='<div class="float-right">';
                   obj +='<a class="btn btn-sm btn-primary" href="trackorder.php?orderid='+result[i].order_no+'"><i class="icofont-map-pins"></i> TRACK ORDER</a>';
                   obj +='</div>';
                    }
                   obj +='<p class="mb-0 text-black text-primary pt-2"><span class="text-black font-weight-bold"> Total :</span> &#163;'+(result[i].amount - result[i].discount).toFixed(2)+'/'+Paytype+'</p>';
                   obj +='</div>';
                   obj +='</div>';
                   obj +='</a>';
                   obj +='</div>';
                   obj +='</div>';
				
					 }
					 $("#out").html(obj);
					 }
					 else{
						 obj="<tr><td colspan='6'>No Orders Found</td></tr>";
						 $("#out").html(obj)
					 }
				 }
		   }); 
}
getorders1();

setInterval(function(){
	getorders1();
       },12000);
// Address function
function getaddress(){
 $uid="<?php echo $_SESSION['uid'];?>";
$token =sessionStorage.getItem('token');
var obj=[];
$.ajax({
					 url: 'https://deliveryguru.co.uk/dg_api/addresses/'+$uid,
					 type: "GET",
					 crossDomain: true,
					 dataType: "json",
					 cache: "false",
					//  beforeSend: function(x) {
					//    if (x && x.overrideMimeType) {
					// 	 x.overrideMimeType("application/j-son;charset=UTF-8");
					//    }
					//  },
					 success: function(result) {
					 //Write your code here
					 console.log(result);
					 if(result.address.length==0){
						document.getElementById("outaddress").innerHTML = "no records found";
					 }
					 else{
				//	console.log(result);
					 for(var i = 0; i < result.address.length; i++) {
    					//obj ="<tr><td>"+(i+1)+"</td><td>"+result.address[i].city+"</td><td>"+result.address[i].pincode+"</td><td>"+result.address[i].home_address+"</td><td><a href='address-edit?id="+result.address[i].id+"' class='btn btn-primary'>Edit</a></td><td><a class='btn btn-primary' id="+result.address[i].id+" onclick='deleteaddress(this.id)'>Delete</a></td></tr>"
                        obj +='<div class="col-md-6" style="float: left;">';
                        obj +='<div class="bg-white card addresses-item mb-4 shadow-sm" style="width:375px;height:180px;line-break:anywhere;">';
                        obj +='<div class="gold-members p-4">';
                        obj +='<div class="media">';
                        obj +='<div class="mr-3"><i class="icofont-home icofont-3x"></i></div>';
                        obj +='<div class="media-body">';
                        obj +='<h6 class="mb-1">Address</h6>';
                        obj +='<p>'+result.address[i].home_address+','+result.address[i].city+','+result.address[i].state+','+result.address[i].pincode+' </p>';
                        obj +='<p class="mb-0 text-black font-weight-bold"><a class="text-danger" href="#" onclick="deleteaddress1('+result.address[i].id+')"><i class="icofont-ui-delete"></i> DELETE</a></p>';
                        obj +='</div>';
                        obj +=' </div>';
                        obj +='</div>';
                        obj +=' </div>';
                        obj +='</div>';
                    
                        //debugger
					
					 }
                     $("#outaddress").html(obj);
					 }
				 }
		   }); 
}
getaddress();
function deleteaddress(){
   var id=document.getElementById('adddeleteid').value;
$token =sessionStorage.getItem('mytoken');
$.ajax({
					 url: 'https://deliveryguru.co.uk/dg_api/deleteAddress/'+id,
					 type: "GET",
					 crossDomain: true,
					 cache: "false",
					
					 success: function(result) {
					
					 //Write your code here
					 if(result!=""){
						 //alert(result);
		
                   $("#delete-address-modal").modal("hide");
                     var x = document.getElementById("snackbar");
           x.innerHTML="Address Deleted";
           x.style="backgrond:green";
           x.className = "show";
           setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
                  getaddress();
					 }
					 else{
					
					alert("failed");
					 }
					
					 }
				 
		   })
}
function deleteAccount(){
    $("#delete-account-modal").modal("show");
}
function deleteaddress1(id){
   document.getElementById('adddeleteid').value=id;
   $("#delete-address-modal").modal("show");
  

}
function addaddress1(){
   $("#add-address-modal").modal("show");
}
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyB0Red6O9d3jz72ULoHpGmZ7rBxfPebbfs"></script>
<script type='text/javascript'>
   var myadd=[];
   var twinPeaksArray = new Array("Laura", 2, ["Bob", "Leland", "Dale"]);
         window.onload=function(){
     var newadd=[];	
           var map;
     var dnum=document.getElementById("dnum");
           function initialize() {
               var myLatlng = new google.maps.LatLng(55.8566274, -4.2618615);
              
               var myOptions = {
                   zoom: 15,
                   center: myLatlng,
                   mapTypeId: google.maps.MapTypeId.ROADMAP
               };
               map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
   
               var marker = new google.maps.Marker({
                   draggable: true,
                   position: myLatlng,
   		  icon: {
   				url: "villa.png"
   				},
                   map: map,
                   title: "Delivery Location"
               });
   	 newadd=[];
               google.maps.event.addListener(marker, 'dragend', function (event) {
   		 // console.log("am dragged");
   		  dnum.style="display:block";
   		  if(dnum.value==''){
   		  dnum.style="border:1px solid red";
   		  }
   		  else{
   			   dnum.style="border:1px solid blue";
   		  }
                   document.getElementById("lat").value = event.latLng.lat();
                   document.getElementById("long").value = event.latLng.lng();
   		  var geocoder= new google.maps.Geocoder();
   		   geocoder.geocode({latLng:marker.getPosition()},function(result,status){
   			   //console.log(result);
   			  
   			   document.getElementById("myaddress").innerHTML="";
   			   newadd=result[0].address_components;
   			    // for(i=0;i<=6;i++){
   					// debugger
   				// newadd.push(result[0].address_components[i]);
   				// }
   				// var pname="long_name";
   			    // console.log(newadd);
   				// var tempp="<ul>";
   				// for(let i=0;i<=result[0].address_components.length;i++){
   					// tempp+="<li>"+result[0].address_components[i]+"</li>";
   				// }
   				// tempp+="</ul>";
                //   console.log(newadd);
                   $('#address').val(result[0].formatted_address);
   
                   // alert(result[0].formatted_address);
   
                   var data = result[0].formatted_address.split(",");
                   //console.log(data.length);
   
                   // alert(result[0].formatted_address.split(', '));
   
   var pin_city= data[data.length - 2].trim().split(" ");
   // alert(data[data.length - 2]);
                   $('#city').val(pin_city[0]);
                //   console.log(pin_city.length);
                   if(pin_city.length>2){
                   $('#pincode').val(pin_city[1] + " "+pin_city[2]);
                   }
                   else{
                       $('#pincode').val(pin_city[1]);
                   }
                   $('#state').val(data[data.length - 1]);
                   var address = "";
   			 newadd.forEach(myFunction);
                      function myFunction(item, index) {
                      var y = document.createElement("INPUT");
   					y.setAttribute("type", "text");
   					y.setAttribute("Placeholder", "Please Enter Valid Street");
   					y.setAttribute("id", "add_" + index);
   					y.setAttribute("class", "maddress col-md-6");
                      y.setAttribute("value",item.long_name);
                      
                      address += item.long_name + ' ';
   			       var z="</br>";
   					document.getElementById("myaddress").appendChild(y,z);
   					
   					
   			// document.getElementById("myaddress").innerHTML += "<span type='text' id="+"add_"+index+">"+item.long_name.trim()+"</span><input type='text' data-val="+item.long_name+" value="+item.long_name+"><br>"; 
                      }
   
                      
   			  var stadd=document.getElementById("add_0").value;
              document.getElementById("door").value=newadd[0].long_name;
                  if(stadd=="Unnamed Road"){
                 document.getElementById("add_0").value="";
                 document.getElementById("add_0").style="border:1px solid red";
                 
                     }
   				//document.getElementById("myaddress").innerHTML=tempp;
   				//markerInfo="<div>hello"+result[0].formatted_address+"</div>";
   			   //document.getElementById("nn").value = result[0].formatted_address;
   		   });
   		  // var info = new google.maps.InfoWindow({
   			  // content: markerInfo
   		  // });
                   //InfoWindow.open(map, marker);
               });
           }
           google.maps.event.addDomListener(window, "load", initialize());
         }//]]>
       
    function getadresss(divid){
     //alert(divid);
     if(divid=="getadd1"){
   	  $("#mapadd").addClass("hide");
   	  $("#mmap").addClass("hide");
   	  $("#manualaddress").removeClass("hide");
     }
     else{
   	  $("#mapadd").removeClass("hide");
   	  $("#mmap").removeClass("hide");
   	  $("#manualaddress").addClass("hide");
     }
    }
     function canceladdress(){
        document.getElementById("addressform").reset();
    }
    function addaddress() {
      document.getElementById("landmarkerr").style.display="none";
// alert(JSON.stringify({
//             "u_id": "<?php echo $_SESSION['uid'] ?>",
//             "home_address": "Door No.  " + $('#door').val() + " " + $('#address').val() ,
//             "permanent_address": "Door No.  " + $('#door').val() + " " + $('#address').val() ,
//             "pincode": document.getElementById('pincode').value,
//             "city": document.getElementById('city').value,
//             "landmark": document.getElementById('landmark').value,
//             "state": document.getElementById('state').value,
//             "lat": document.getElementById('lat').value,
//             "longt": document.getElementById('long').value
//          }));

         // return false;
    
         if (document.getElementById('door').value == "") {
         alert("Door no. must be filled out");
         return false;
      }
      if (document.getElementById('address').value == "") {
         alert("Address must be filled out");
         return false;
      }

if (document.getElementById('state').value == "") {
         alert("state must be filled out");
         return false;
      }
      
      if (document.getElementById('pincode').value == "") {
         alert("pincode must be filled out");
         return false;
      }
      if (document.getElementById('city').value == "") {
         alert("city must be filled out");
         return false;
      }
      if (document.getElementById('landmark').value == "") {
        // alert("landmark must be filled out");
         document.getElementById("landmarkerr").style.display="block";
         return false;
      }



      


      $.ajax({
         type: "POST",
         url: 'https://deliveryguru.co.uk/dg_api/addAddress',
         // url: 'http://deliveryguru.co.uk/dg_api/placeOrder',
         dataType: "json",
         crossDomain: true,
         data: {
            "u_id": "<?php echo $_SESSION['uid'] ?>",
            "home_address": "Door No.  " + $('#door').val() + " " + $('#address').val() ,
            "permanent_address": "Door No.  " + $('#door').val() + " " + $('#address').val() ,
            "pincode": document.getElementById('pincode').value,
            "city": document.getElementById('city').value,
            "landmark": document.getElementById('landmark').value,
            "state": document.getElementById('state').value,
            "lat": document.getElementById('lat').value,
            "longt": document.getElementById('long').value
         },
         cache: "false",
         success: function(json) {
            // alert(JSON.stringify(json));
       
            jQuery("#add-address-modal").modal("hide");
          //  userlogin1();
           document.getElementById("addressform").reset();
            var x = document.getElementById("snackbar");
           x.innerHTML="Address Added Successfully";
           x.style="backgrond:green";
           x.className = "show";
           setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
          getaddress();

         },
         error: function(json) {
            alert(JSON.stringify(json));

         }
      });

   }
   
    function myKeyPress(e){
    var keynum;
    if(window.event) { // IE                    
      keynum = e.keyCode;
    } else if(e.which){ // Netscape/Firefox/Opera                   
      keynum = e.which;
    }
    if(keynum==62 || keynum==60)
        e.preventDefault();
  } 
   																																				
</script>
<script>
    $(document).ready(function() { $('body').bind('cut copy', function(e) { e.preventDefault(); }); $("body").on("contextmenu", function(e) { return false; }); window.ondragstart = function() { return false; }  });
</script>
</html>
<?php } ?>