<div class="modal fade" id="popup<?php echo $v1['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle"><?php echo $v1['item_name']; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?php 
                                          if($v1['image'] == 'dummy.jpg') { $itemImg ="No_image.png"; 
                                              
                                          } else { 
                                              $itemImg ="https://deliveryguru.co.uk/admin/images/itemimages/" .  $v1['image']; 
                                              
                                          }
                                          ?>
                                          <img src="<?php echo $itemImg; ?>" class="img-fluid item-img" style="width: 100%;height: 150px; object-fit: contain;" />
                                          
                                          
                                          
                                                <p class="text-gray mb-2" ><?php echo $v1['item_desc']; ?></p>
    
    
    
    
   <?php 
   
    if(sizeof($v1['child']) > 0){
    foreach($v1['child'] as $value) {
        $linkID = date("YmdHis") .  $value['id'];
        ?>
        
       <div class="">
           <div ><label for="<?php echo $value['id']; ?>" extraTitle="<?php echo $linkID;?>" ><?php echo $value['item_name']; ?> </label>
           <span style="float:right;">£<?php echo $value['price']; ?></span></div><p style="font-size: 11px;margin-bottom:5px;">
               <?php echo $value['item_desc']; ?> </p></div>
<?php 
    foreach($value['extra'] as $value1) {
?>
        
<div class="boxes">
<div class="list-card-body">
<h6 class="mb-1"><a href="#" class="text-black"><?php echo $value1['name']; ?></a></h6>
<p class="text-gray mb-2"><?php echo $value1['add_on_desc']; ?></p>
</p>
</div>
<?php 
    foreach($value1['addon'] as $value2) { ?>  
<div class="custom-control custom-checkbox">
<input type="checkbox" class="custom-control-input" id="<?php echo $value2['id']; ?>">
<label class="custom-control-label" parent_id ="<?php echo $value['id']; ?>" parent_name="<?php echo $value['item_name']; ?>" parent_discount="<?php echo $value['discount']; ?>" parent_price = "<?php echo $value['price']; ?>" for="<?php echo $value2['id']; ?>" name="<?php echo $value2['addon_name']; ?>" 
price="<?php echo $value2['addon_price']; ?>"  extraitem="<?php echo $linkID;?>"
onclick="addonClick(this)"><?php echo $value2['addon_name']; ?>
<small class="text-black-50" style="float: right;">£ <?php echo $value2['addon_price']; ?></small>
</label>
</div>
    
    

  <?php } ?>
 
  
  
  
</div>


       
  <?php 
}
?>


  <?php 
}}
else {
?>
   <?php 
    foreach($v1['extra'] as $value) {
?>
        
<div class="boxes">
<div class="list-card-body">
<h6 class="mb-1"><a href="#" class="text-black"><?php echo $value1['name']; ?></a></h6>
<p class="text-gray mb-2"><?php echo $value1['add_on_desc']; ?></p>
</p>
</div>
<?php 
    foreach($value1['addon'] as $value2) { ?>  
    
    
    
<div class="custom-control custom-checkbox">
<input type="checkbox" class="custom-control-input" id="<?php echo $value2['id']; ?>">
<label class="custom-control-label" for="<?php echo $value2['id']; ?>" name="<?php echo $value2['addon_name']; ?>" 
price="<?php echo $value2['addon_price']; ?>" 
onclick="addonClick(this)"><?php echo $value2['addon_name']; ?>
<small class="text-black-50" style="float: right;">£ <?php echo $value2['addon_price']; ?></small>
</label>
</div>
    
    

  <?php } ?>
 
  
  
  
</div>


       
  <?php 
}
}
?>
      </div>
      <div class="modal-footer">

<span class="count-number float-right">
                           <button class="btn btn-outline-secondary  btn-sm left dec" onclick="minusValue(<?php echo $v1['id']; ?>)"> <i class="icofont-minus"></i> </button>
                           <input id="<?php echo $v1['id']; ?>" class="count-number-input" type="number" value="1" readonly="">
                           <button class="btn btn-outline-secondary btn-sm right inc" onclick="plusValue(<?php echo $v1['id']; ?>)"> <i class="icofont-plus"></i> </button>
                           </span>
        <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
       
        <button id="addButton<?php echo $v1['id']; ?>" type="button" class="btn btn-primary" data-dismiss="modal" amount="<?php echo $v1['price']; ?>" item_id="<?php echo $v1['id']; ?>" discount="<?php echo $v1['discount']; ?>" qty="1" status="0" notes="1" item_name = "<?php echo $v1['item_name']; ?>" 
        <?php if(sizeof($v1['child']) == 0 && sizeof($v1['extra']) == 0){ ?>
        onclick="addItem(this)"
        <?php } ?>
        >Add to cart</button>
      </div>
    </div>
  </div>
</div>