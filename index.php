<?php

include 'src/config.php';
function getHotelAPI() {
  $graph_url= 'https://deliveryguru.co.uk/dg_api/getmetadata/200/landing';
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $graph_url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  $output = curl_exec($ch); 
  curl_close($ch);
  return json_decode($output, true); 
}
$getHotelDetails = getHotelAPI();
$keyword=$getHotelDetails['result'][0]['allTag'];
$keyWord=$getHotelDetails['result'][0]['h1_tag'];
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="author" content="">
      <link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
      
<!-- Page Title(Name)-->
<?php echo $keyword;?>

   <!-- Bootstrap core CSS -->
      <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="preconnect" href="https://fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="css/modern-business.css" rel="stylesheet">
      <link href="css/home.css" rel="stylesheet">
      
      <!-- Facebook Pixel Code -->
      <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-222117938-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-222117938-1');
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Restaurant",
  "name": "Natural spice online",
  "image": "https://www.natural-spiceonline.co.uk/img/logo1.png",
  "@id": "",
  "url": "https://www.natural-spiceonline.co.uk",
  "telephone": "01418825400",
  "menu": "https://www.natural-spiceonline.co.uk/",
  "servesCuisine": "Indian | Italian | Turkish",
  "acceptsReservations": "false",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "1791, Paisley Road West, Cardonald",
    "addressLocality": "Glasgow",
    "postalCode": "G52 3SS",
    "addressCountry": "GB"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": 55.8454805,
    "longitude": -4.3420923
  },
  "openingHoursSpecification": [{
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday"
    ],
    "opens": "16:30",
    "closes": "00:20"
  },{
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": [
      "Friday",
      "Saturday"
    ],
    "opens": "16:30",
    "closes": "01:20"
  }] ,
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "4.4",
    "bestRating": "5",
    "worstRating": "0",
    "ratingCount": "97"
  }
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Natural spice online",
  "url": "https://www.natural-spiceonline.co.uk/",
  "logo": "https://www.natural-spiceonline.co.uk/img/logo1.png"
}
</script>


<!-- End Facebook Pixel Code -->
      <style>
         .unselectable {
         -webkit-user-select: none;
         -webkit-touch-callout: none;
         -moz-user-select: none;
         -ms-user-select: none;
         user-select: none;
         /*color: #cc0000;*/
         }
         .button {
         background-color: #004A7F;
         -webkit-border-radius: 10px;
         border-radius: 10px;
         border: none;
         color: #FFFFFF;
         cursor: pointer;
         display: inline-block;
         font-family: Arial;
         font-size: 20px;
         padding: 5px 10px;
         text-align: center;
         text-decoration: none;
         -webkit-animation: glowing 1500ms infinite;
         -moz-animation: glowing 1500ms infinite;
         -o-animation: glowing 1500ms infinite;
         animation: glowing 1500ms infinite;
         }
         @-webkit-keyframes glowing {
         0% { background-color: #B20000; -webkit-box-shadow: 0 0 3px #B20000; }
         50% { background-color: #FF0000; -webkit-box-shadow: 0 0 40px #FF0000; }
         100% { background-color: #B20000; -webkit-box-shadow: 0 0 3px #B20000; }
         }
         @-moz-keyframes glowing {
         0% { background-color: #B20000; -moz-box-shadow: 0 0 3px #B20000; }
         50% { background-color: #FF0000; -moz-box-shadow: 0 0 40px #FF0000; }
         100% { background-color: #B20000; -moz-box-shadow: 0 0 3px #B20000; }
         }
         @-o-keyframes glowing {
         0% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
         50% { background-color: #FF0000; box-shadow: 0 0 40px #FF0000; }
         100% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
         }
         @keyframes glowing {
         0% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
         50% { background-color: #FF0000; box-shadow: 0 0 40px #FF0000; }
         100% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
         }
         .justify-content-center {
         -ms-flex-pack: center!important;
         justify-content: flex-end!important;
         }
         .navbar-nav li {
         padding-left: 0px;
         padding-right: 25px;
         }
         .navbar-dark .navbar-nav .nav-link {
         color: rgb(71 71 71)!important;
         text-align: center;
         font-size: 14px !important;
         font-weight: 500 !important;
         }
         .navbar {
         padding: 0;
         border-bottom: 3px solid #ff0000;
         }
         .banner-text{
         position: absolute;
         text-align: center;
         top: 71%;
         width: 100%;
         }
         .banner-text {
         transform: none;
         }
         .h1_title{
         background-color: #fff;
         display: inline-block;
         padding: 7px 40px;
         border-radius: 71px;
         /*border-bottom-right-radius: 0;*/
         line-height: 1.9;
         color: #000;
         }
         @media only screen and (max-width: 414px) {
             .h1_title{
                 font-size:8px;
             }
             .btn-trans{
                font-size:12px !important;
             }
         }
         @media only screen and (max-width: 375px){
             .h1_title{
                 font-size:12px;
             }
             .btn-trans{
                font-size:12px !important;
             }
         }
         @media only screen and (max-width : 320px){
             .h1_title{
                 font-size:13px;
             }
             .btn-trans{
                font-size:12px !important;
             }
         }
         .btn-trans{
                 color: white;
    padding: 0 17px;
    font-size: 25px;
    border: none !important;
    border-radius: 30px;
    font-weight: bold;
         }
         .navbar-nav li:hover {
 opacity: unset; 
}
     .navbar-nav li a:hover {
 opacity: 0.65; 
 color:#fff;
}
@media only screen and (max-width: 600px) {.banner-text{font-size:25px;top:55%;}} 
      </style>
   </head>
   <style></style>
   <body class="unselectable">
     <!-- <div id="promoModal" class="modal fade" role="dialog" style="margin-top: -7%;">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 style="color:white; margin:0;">Deals Coming Soon.!!</h5>
                  <button type="button" class="close" data-dismiss="modal" style="color: white;font-size: 38px;padding: 9px 14px 1px 1px;">&times;</button>
               </div>
               <div class="modal-body">
                  <div class="page-content page-container" id="page-content">
                     <div class="padding">
                        <div class="row container d-flex justify-content-center">
                           <h6 style="text-align: center;padding-top: 13px;"> Download the App and Get More user friendly Experience!!!</h6>
                           <div class="template-demo mt-2 col-md-12">
                              <img src="img/applinbg.png" style="width:50%;float:left;" alt="grill-guru Ios App"> <img src="img/applinkbg1.png" style="width:50%" alt="grill-guru Android App">
                              <div class="row">
                                 <div class="col-md-6"> <a href="https://apps.apple.com/in/app/grill-guru/id1305799185" target="_blank"> <button class="btn btn-outline-dark btn-icon-text mt-1" style="width: 100%;"> <i class="fa fa-apple btn-icon-prepend mdi-36px"></i> <span class="d-inline-block text-left"> <small class="font-weight-light d-block">Available on the</small> App Store </span> </button> </a></div>
                                 <div class="col-md-6"> <a href="https://play.google.com/store/apps/details?id=uk.co.gg" target="_blank"> <button class="btn btn-outline-dark btn-icon-text mt-1" style="width: 100%;"> <i class="fa fa-android btn-icon-prepend mdi-36px"></i> <span class="d-inline-block text-left"> <small class="font-weight-light d-block">Get it on the</small> Google Play </span> </button> </a></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row mt-2"></div>
               </div>
               <div class="modal-footer text-right"> <a href="#" class="btn btn-sm btn-outline-danger" data-dismiss="modal">Continue with Web</a></div>
            </div>
         </div>
      </div>
      <div id="promoModal" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <h3 style="color:white; margin:0;">Discount offer: Offer Comimg soon.</h3>
                  <button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
               </div>
               <div class="modal-body">
                  <div class="page-content page-container" id="page-content">
                     <div class="padding">
                        <div class="row container d-flex justify-content-center">
                           <div class="template-demo mt-2"> <button class="btn btn-outline-dark btn-icon-text"> <i class="fa fa-apple btn-icon-prepend mdi-36px"></i> <span class="d-inline-block text-left"> <small class="font-weight-light d-block">Available on the</small> App Store </span> </button> <button class="btn btn-outline-dark btn-icon-text"> <i class="fa fa-android btn-icon-prepend mdi-36px"></i> <span class="d-inline-block text-left"> <small class="font-weight-light d-block">Get it on the</small> Google Play </span> </button></div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="modal-footer text-right"> <a class="btn btn-danger btn-sm copypromo" data-id="AGTfvf">Copy the Code<i class="fa fa-gem"></i> </a> <a href="" class="btn btn-sm btn-outline-danger" data-dismiss="modal">No, thanks</a></div>
            </div>
         </div>
      </div>-->
      <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-white fixed-top">
         <div class="container-fluid">
            <a class="navbar-brand" href="index.php">  <img src="img/logo1.png" alt="NATURAL SPICE food delivery logo" style="height: 70px;"/> </a> <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
            <div class="collapse navbar-collapse justify-content-center" id="navbarResponsive">
               <ul class="navbar-nav">
                  <li class="nav-item"> <a class="nav-link" href="menu.php">Menu</a></li>
                  <li class="nav-item"> <a class="nav-link" href="deals.php">Deals</a></li>
                  <li class="nav-item"> <a class="nav-link" href="contact.php">Contact</a></li>
                                   <?php if(!isset($_SESSION[ 'uid'])){?><li class="nav-item"> <a class="nav-link" href="login.php"> Login </a></li> <?php } ?> <?php if(isset($_SESSION[ 'uid'])){?><li class="nav-item"> <a class="nav-link" href="myaccount.php"> My Account </a></li><li class="nav-item"> <a class="nav-link" href="logout.php"> Logout </a></li> <?php } ?>
                                     <li class="nav-item"> <a class="nav-link btn btn-md btn-danger menu-btn" href="menu.php" style="color: #fff!important;">Order Now</a></li>
                  <li class="nav-item">
                 <!-- <a class="nav-link btn btn-md btn-danger menu-btn" href="http://bookinguru.co.uk/index.php?id=" style="color:white !important;">Book A Table</a>-->
                  </li>
                  <li class="nav-item social"> <a href="#" target="_blank"> <span><i class="fa fa-facebook simple_social simple_social" style="color: #1877f2;"></i></span> </a> <a href="#" target="_blank"> <span><i class="fa fa-instagram simple_social simple_social" style="color: #c32aa3;"></i></span> </a> <a href="#" target="_blank"> <span><i class="fa fa-youtube simple_social simple_social" style="color: #ff0000;"></i></span> </a> <a href="#" target="_blank"> <span><i class="fa fa-apple simple_social simple_social" style="color: #a6b1b7;"></i></span> </a> <a href="#" target="_blank"> <span><i class="fa fa-android simple_social simple_social" style="color: #1db954;margin: 0 16px 0 0;font-size: 24px;"></i></span> </a></li>
               </ul>
            </div>
         </div>
      </nav>
      <div class="w-screen mt-4" style="margin-top: 0rem !important;">
         <div class="row justify-content-center pt-5 banner1">
         <img class="img-fluid desktop-banner" src="img/Landing-page-03.jpg" alt="grillgurubanner"> <img class="img-fluid mobile-banner" src="img/Landing-page-03.jpg" alt="grillgurumobilebanner">
            <div class="banner-text bg_shadow">
               <!--<h1 class="h1_title">Home of Italian and Turkish...</h1>-->
              <span class="h1_title" style="background: #fff;color: #000;border-radius: 20px;padding: 5px;">Natural Spice Tandoori is the best choice if you want to choose the best in the town.<h1 style="font-size:7px;text-align:center;"><?php echo $keyWord;?></h1></span>
             
               <p></p>
               <a class="btn btn-md btn-trans" href="menu.php" style="color: white;">Order Now</a>
            </div>
         </div>
      </div>
      <div class="w-screen pt-2" style="background-color: #fff;">
         <div class="row">
            <div class="col-lg-12 portfolio-item" style="margin-top: 4%;padding: 0px !important;margin-bottom: -13px;">
               <div class="bg-white">
                  <div class="row" style="display: flex;align-items: center;">
                     <div class="col-lg-6 bg-white text-center">
                        <h2 class="cl-dar text-center">Our Story</h2>
                        <p class="cl-dar">Natural Spice Tandoori is the best choice if you want to choose the
best in the town. We offers variety of Gluten free options. All meat
served is Halal. Our specialities are Italian, Turkish, Indian and
Tandoori dishes. We offer traditions Indian flavours.
We use the finest ingredients and prepare the food. We have a lot of
options for you to choose from. Our food is great and at reasonable
prices. We are available for collection and delivery! We prepare hot,
healthy and hygienic food which you can order from at the comfort
of your home. Feel free to visit our website or call us! We are located
at 1791, Paisley Road West, Cardonald, Glasgow.<br></p>
                        <p class="cl-dar"> <br></p>
                        <p class="cl-dar"><br></p>
                        <p class="cl-dar"> <br></p>
                        <p class="cl-dar"></p>
                     </div>
                     <div class="col-lg-6" style=" padding: 0px !important;"> <img src="img/chunk.webp" class="img-fluid" alt="Online Food Delivery"></div>
                  </div>
               </div>
            </div>
            <div class="col-lg-12 portfolio-item" style=" padding: 0px !important;">
               <div class="bg-white">
                  <div class="row flex-column-reverse flex-md-row pt-3" style="display: flex;align-items: center;">
                     <div class="col-lg-6 text-center" style="padding: 0px !important;"> <!--<img src="img/table.jpg" class="img-fluid" alt="Food delivery Services glasgow"></div>-->
                     <!--<div class="col-lg-6 bg-white text-center pb-4">
                        <h2 class="cl-dar text-center">Private Dining</h2>
                        <p class="cl-dar">Private Dining enables you to enjoy a group dining experience. Grill Guru offers private space that separates you and your family or friends from the rest of the restaurant which is very much needed during these Covid times. This will enable you to be relaxed and enjoy yourself without being disturbed and disturbing the rest of the restaurant. They are fully catered and meets all your requirements whether it is your choice of food, desserts or just drinks! We also provide you with a private romantic dining space for two for a pleasant evening.</p>
                        <p class="cl-dar">Booking a venue during this pandemic comes with its own benefits of saving time, money and reduces any logistical risks and thereby helps you stay safe and enjoy yourself! It is now incredibly easy to book your private dining with Grill Guru</p>
                        <p class="cl-dar">Grill Guru has a rich decor and a remarkable reputation, this venue is on the top option for private dining in Scotland being among the top 5 takeaways in Scotland for 2018 and also being nominated as the finalist of British Takeaway awards. Therefore, it is perfect for intimate dinners who can enjoy the day or a pleasant evening together. With a warm ambience it creates the perfect atmosphere and helps the guests feel welcome and relaxed when they enter the restaurant. It is located at the heart of Glasgow in the Citi Center, a short distance from the railway station. It is therefore very convenient for everyone to visit.</p>
                        <!--<a class="btn btn-md btn-trans">Book A Table</a>-->
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-12 portfolio-item" style="padding: 0px !important;">
               <h2 class="cl-dar text-center pt-3">Customer Favourites</h2>
               <div class="row">
                  <div class="col-lg-6 portfolio-item" style="padding: 0px !important;">
                     <div class="card h-100">
                        <div class="hero-image bg1">
                           <div class="hero-text bg_shadow ">
                           <h3 class="h1_title">Special Mix Kebab with Nan - King</h3><h6>Shish, Seekh, Chicken, Donner, Mushroom, Onion, Peppers </h6> <a class="btn btn-md btn-trans" href="menu.php#2355" style="color: white;">Order Now</a></div></div></div></div><div class="col-lg-6 portfolio-item" style="padding: 0px !important;"><div class="card h-100"><div class="hero-image bg2"><div class="hero-text bg_shadow "><h2 class="h1_title">Spicy Lamb Calzone</h1><h6></h6> <a class="btn btn-md btn-trans" href="menu.php#2360" style="color: white;">Order Now</a></div></div></div></div><div class="col-lg-6 portfolio-item" style="padding: 0px !important;"><div class="card h-100"><div class="hero-image bg3"><div class="hero-text bg_shadow "><h2 class="h1_title">Chicken Tikka Tandoori</h1><h6></h6> <a class="btn btn-md btn-trans" href="menu.php#2352" style="color: #fff;">Order Now</a></div></div></div></div><div class="col-lg-6 portfolio-item" style="padding: 0px !important;"><div class="card h-100"><div class="hero-image bg4"><div class="hero-text bg_shadow "><h2 class="h1_title">Chicken Biryani</h1><h6></h6> <a class="btn btn-md btn-trans" href="menu.php#2353" style="color: white;">Order Now</a>
                        </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
    <!--  <div class="w-screen pt-4" style="background-color: #ffffff;">
         <div class="container">
            <div class="col-md-12">
               <h2 class="cl-dar text-center">A Recognized Track-Record of Excellence</h2>
               <div class="row text-center">
                  <div class="col-md-3"> <img src="img/f3.png" alt="Restaurants Near me"></div>
                  <div class="col-md-3"> <img src="img/f1.png"></div>
                  <div class="col-md-3"> <img src="img/f2.png"></div>
                  <div class="col-md-3"> <img src="img/f4.png"></div>
               </div>
            </div>
         </div>
      </div>-->
      </div> 
      <footer>
         <div class="container">
            <hr />
            <div class="footerWrap col-md-12 col-md-offset-1">
               <div class="copyright col-md-12">
                  <p style="font-size:15px;">© Copyright 2022 <a href="https://deliveryguru.co.uk/" target="_blank" style="color: #ff296d !important;">DeliveryGuru</a> All rights reserved.</p>
               </div>
            </div>
         </div>
      </footer>
      <!-- Bootstrap core JavaScript -->
      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-formhelpers/2.3.0/js/bootstrap-formhelpers.min.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-formhelpers/2.3.0/css/bootstrap-formhelpers.min.css" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-formhelpers/2.3.0/img/bootstrap-formhelpers-currencies.flags.png" />
      <script src="home.js"></script>
      <script>
         $(document).ready(function() {
           $('body').bind('cut copy', function(e) {
               e.preventDefault();
             });
         $("body").on("contextmenu", function(e) {
               return false;
             });
             window.ondragstart = function() { return false; } 
         });
         var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
         if (isMobile) {  
             promoModal(); 
             
         }

         $(document).ready(function() {
           $('body').bind('cut copy', function(e) {
               e.preventDefault();
             });
         $("body").on("contextmenu", function(e) {
               return false;
             });
             window.ondragstart = function() { return false; } 
         });
      </script>
      <script type="text/javascript" id="cookieinfo"
	src="js/cookieinfo.min.js"
	data-bg="#000"
	data-text="This website uses cookies to ensure you get the best experience on our website. "
	data-fg="#FFFFFF"
	data-link="#00189FF"
	data-cookie="CookieInfoScript"
	data-text-align="left"
       data-close-text="Accept">
</script>
   </body>
</html>