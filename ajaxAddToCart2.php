<?php
session_start();
//$_SESSION['cartItems'];




      if(isset($_GET['unset'])){
         unset($_SESSION['cartItems']);
      }
      if(isset($_GET['print'])){
         d($_SESSION);
      }

      function d($d){
         echo '<pre>';
         print_r($d);
         echo '</pre>';
      }

      //d($_SESSION);
      $addItems = array();
      if(isset($_POST['Items']) && $_POST['Items']!=''){
         $mainItems = json_decode($_POST['Items']);
         if(isset($_POST['addOns']) && count($_POST['addOns'])>0) { $addItems = json_decode($_POST['addOns']); } 

         $addItems = array(0=>array($mainItems, $addItems));


         if(!isset($_SESSION['cartItems'])){
            $_SESSION['cartItems'] = $addItems;
         } else {
            $_SESSION['cartItems'] = array_merge($_SESSION['cartItems'], $addItems);
         }
         $_SESSION['cartItems'] = array_reverse($_SESSION['cartItems']);
      }

      if(isset($_POST['action'])) {
         if($_POST['action']=='update') {
            if($_POST['qty']<=0){
               unset($_SESSION['cartItems'][$_POST['id']]);
            } else {
               $_SESSION['cartItems'][$_POST['id']][0][3] = $_POST['qty'];
            }
         }
         if($_POST['action']=='note') {
            $_SESSION['cartItems'][$_POST['id']][0][4] = $_POST['qty'];
         }
         if($_POST['action']=='del_type') {
            $_SESSION['del_type'] = $_POST['qty'];
         }
         if($_POST['action']=='del_charge') {
            $_SESSION['del_charge'] = $_POST['qty'];
         }
         if($_POST['action']=='pre_order') {	
            $_SESSION['pre_order'] = $_POST['qty'];	
         }
         if($_POST['action']=='coupon') {
            $_SESSION['coupon'] = $_POST['qty'];
            $_SESSION['coupon_code'] = $_POST['id'];
            $_SESSION['coupon_type'] = $_POST['type'];
            if($_POST['qty']=='') {
               unset($_SESSION['coupon']);
            }
         }
         
         if($_POST['action']=='cart_clear') {
             unset($_SESSION['discountper']);
         unset($_SESSION['del_charge']);
         unset($_SESSION['coupon']);
            unset($_SESSION['cartItems']);
            unset($_SESSION['pre_order']);
         }
      } 
$totPrice = 0;
 $disc_fee = 0;
if(isset($_SESSION['cartItems']) && count($_SESSION['cartItems'])>0)
{
      $allItems = array_filter($_SESSION['cartItems']); 
      ?>
       <input type="hidden" id="preorder_status" value="<?php echo $_SESSION['pre_order'];?>">
      <div class="card-body p-2">
      <div class="mb-1 bg-white p-1 clearfix" style="overflow: hidden;">
      <?php
                     if(isset($_SESSION['pre_order']) && $_SESSION['pre_order']!='') {
                        echo '<div class="alert alert-warning" role="alert" id="preorder_value" style="font-size:0.79em !important;"><p><b>Pre Order Date: '.$_SESSION['pre_order'].'</b> <span style="padding-left: 37px;cursor: pointer;font-size:1.1em !important;color:red;;" id="changedate" onclick="changeDate()"><i class="icofont-edit"></i></span><span style="padding-right: 14px;cursor: pointer;font-size:1.1em !important;color:red;float:right;" id="deletedate" onclick="deleteDate()"><i class="icofont-ui-delete"></i></span></p></div>';
  
                     }
                     if(count($allItems)>4){
               ?>
         <ul class="list-group list-group-flush" style="width: 100%;
    height: 50vh;
    overflow-y: scroll;
    padding-right: 17px;
    box-sizing: content-box;">
             <?php
                     }
             else{
             ?>
              <ul class="list-group list-group-flush" style="">
         <?php
                     }
         foreach($allItems as $k => $val) { 
            
            $itemPrice = $val[0][2] * $val[0][4];
           // $totPrice = $totPrice + $itemPrice;

            $listAddons = array();
            $addOnPrice = 0;
            foreach($val[1] as $k2 => $val2) {
               $addOn = explode(',',$val2);  
               $addOnPrice += $addOn[3] * $addOn[4];
              // $totPrice = $totPrice + $addOnPrice;
              $listAddons[] = $addOn[4].'x '.$addOn[2]; 
            }
            $addonList = implode(' &#9679; ', $listAddons);
            if($val[0][6]>0){
                $di=$itemPrice*($val[0][6]/100);
                $item_addon_price = ($itemPrice) + $addOnPrice;
                $disc_fee=$disc_fee+$di;
            }else{
            $item_addon_price = $itemPrice + $addOnPrice;
                
            }
            // $item_addon_price = $itemPrice + $addOnPrice;
            ?>
         
            <li class="list-group-item" style="padding:2px 0px;">
               
                  <div class="row">
                        <span class="col-2">
                           <img src="<?php echo $val[0][3]; ?>"  class="cart_thumb" alt="..." class="pull-left">
                        </span>
                        <span class="col-10 pl-1">
                           <b><?php echo $val[0][4]; ?>x <?php echo $val[0][1]; ?></b>
                           <span  class="cart_item_pr">
                              
                        <b>£ <?php echo number_format($item_addon_price, 2, '.', ''); ?></b>
                           <br>
                           <span class="text-red text-right pt-2"><a onClick="updateCart(<?php echo $k; ?>, 1, 'remove');" ><u><i><b>Remove</b></i></u></a>
                           </span>
                           </span>
                        </span>
                     </div>  
                     <?php 
                     if($addonList!='') { ?>  
                        <div class="row">
                        <div class="col-12">
                            <i class="itemNotes"> <?php echo $addonList; ?></i>
                        </div>
                        </div>
                     <?php   } 
                     if($val[0][5]!='') { ?>  
                     <div class="row">
                        <div class="col-12">
                           <i class="itemNotes"> <i class="icofont-ui-note"> </i> <?php echo $val[0][5]; ?></i>
                        </div>
                     </div>
                     <?php   }   ?>
                     
                     <hr style="color: #acacac;height: .5px;">
            </li>
           
            <?php
           $totPrice = $totPrice + $item_addon_price;
         }  
        
         $ser_fee = 0.50;
         $carry_bag = 0.05;
         $del_fee = 0;
        // $disc_fee=0;
            if(isset($_SESSION['coupon']) && $_SESSION['coupon']!='') {	
             if($_SESSION['coupon_type']==1){		
             $disc_fee = $totPrice * ($_SESSION['coupon']/100);		
               } else{		
                   $disc_fee = $_SESSION['coupon'];			
               }
             }
         else{
            //  $disc_fee = $totPrice*($_SESSION['discountper']/100);
         }
         
         $subTot = $totPrice;
         if($_SESSION['del_type']=='collection') { 
             $del_fee = 0; 
             
         } else if(isset($_SESSION['del_charge'])) { 
             $del_fee = $_SESSION['del_charge']; 
             
         }
       
         $grandTot = $subTot + $ser_fee + $del_fee + $carry_bag - $disc_fee;
         ?>
         
         </ul>
           <?php if(!isset($_SESSION['coupon']) || $_SESSION['coupon']=='' || $_SESSION['coupon']=='0') { ?>
         <div class="bg-white clearfix" style="margin-top: 12px;">
       
                        <div class="input-group input-group-sm mb-2">
                           <input type="text" class="form-control" id="couponValue" placeholder="Enter promo code" onkeypress="allowAlphaNumericSpace(event)" maxlength="10" value="">
                           <div class="input-group-append">
                              <button class="btn btn-primary" type="button" id="button-addon2" onclick="couponValidator();"><i class="icofont-sale-discount"></i> APPLY</button>
                           </div>
                        </div>
                      
         </div>
          <hr>
           <?php } ?>
        
            <div class="mb-2 bg-white  clearfix">
            
                        <p class="mb-1">Sub Total <span class="float-right text-dark">£ <span id="sub_total"><?php echo number_format($subTot, 2, '.', ''); ?></span></span></p>
                        <p class="mb-1">Service Charges <span class="float-right text-dark">£ <?php echo number_format($ser_fee, 2, '.', ''); ?></span></p>
                        <p class="mb-1">Carry Bag Fee <span class="float-right text-dark">£ <?php echo number_format($carry_bag, 2, '.', ''); ?></span></p>
                        <?php if($_SESSION['del_type']!='collection') { ?>
                        <p class="mb-1">Delivery Fee <span class="text-info">
                          
                           </span> <span class="float-right text-dark">£ <span id="deliveryfm"><?php echo number_format($del_fee, 2, '.', ''); ?></span></span>
                        </p>
                        <?php } ?>
                        <p class="mb-1 text-success">Discount
                          <?php  if(!isset($_SESSION['coupon']) || $_SESSION['coupon']=='') { ?>
                        
                        <?php }?>
                        <?php  if(isset($_SESSION['coupon']) && $_SESSION['coupon']!='') { ?>
                        <span id="coupondiscount">(Coupon Applied Get Offer <?php  if($_SESSION['coupon_type']==2){?>£<?php } echo $_SESSION['coupon']; if($_SESSION['coupon_type']==1){?>% <?php } ?><span onclick='removeCoupon()'>) <u style="color:red;"><i><b>Remove</b></i></u><span></span>
                        <?php }?>
                           <span class="float-right text-success">£ <span id="discount"><?php echo number_format($disc_fee,2,'.',''); ?></span></span>
                        </p>
                        <hr>
                        <h6 class="font-weight-bold mb-0"> Total  <span class="float-right">£ <?php echo number_format($grandTot, 2, '.', ''); ?></span></h6>
             </div>
                  </div>
      </div>
      <?php		
      if($grandTot<0){		
          ?>		
           <script>		
       updateCart('0', '', 'coupon');		
                    var x = document.getElementById("snackbar");		
                          x.innerHTML="Total value Below Coupon Value";		
                          x.className = "show";		
                          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);		
   </script>		
          <?php		
      }
}
else {
   
   ?>
   <center style="margin-top:12%;"><img src="https://grill-guru.co.uk/newdev/img/empty-cart.png" width="200" /><br><p>Your cart is empty<br>Please add items</p></center>
   <script>
       var x = document.getElementById("snackbar");
  x.innerHTML="Your cart is empty Please add items";
  x.className = "show";
  setTimeout(function(){ x.className = x.className.replace("show", ""); window.location.href="menu.php"}, 3000);
   </script>
   <?php
 
}
?>