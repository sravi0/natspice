<?php session_start();
$_SESSION['freeItem'] = -1;

      if(isset($_GET['unset'])){
         unset($_SESSION['cartItems']);
      }
      if(isset($_GET['print'])){
         d($_SESSION);
      }

      function d($d){
         echo '<pre>';
         print_r($d);
         echo '</pre>';
      }

      //d($_SESSION);
      $addItems = array();
      if(isset($_POST['Items']) && $_POST['Items']!=''){
         $mainItems = json_decode($_POST['Items']);
         if(isset($_POST['addOns']) && count($_POST['addOns'])>0) { $addItems = json_decode($_POST['addOns']); } 

         $addItems = array(0=>array($mainItems, $addItems));


         if(!isset($_SESSION['cartItems'])){
            $_SESSION['cartItems'] = $addItems;
         } else {
            $_SESSION['cartItems'] = array_merge($addItems, $_SESSION['cartItems']);
         }
        // $_SESSION['cartItems'] = array_reve£e($_SESSION['cartItems']);
      }

      if(isset($_POST['action'])) {
         if($_POST['action']=='update') {
            if($_POST['qty']<=0){
               unset($_SESSION['cartItems'][$_POST['id']]);
            } else {
               $_SESSION['cartItems'][$_POST['id']][0][3] = $_POST['qty'];
            }
         }
         if($_POST['action']=='note') {
            $_SESSION['cartItems'][$_POST['id']][0][4] = $_POST['qty'];
         }
         if($_POST['action']=='del_type') {
            $_SESSION['del_type'] = $_POST['qty'];
         }
         if($_POST['action']=='pre_order') {
            $_SESSION['pre_order'] = $_POST['qty'];
         }
      } 
$totPrice = 0;
$disc_fee = 0;
if(isset($_SESSION['cartItems']) && count($_SESSION['cartItems'])>0)
{
      //d(array_filter($_SESSION['cartItems']));
      $allItems = array_filter($_SESSION['cartItems']); 
      //d($allItems);
      ?>
      
      <div class="card-body p-2">
      <div class="mb-1 bg-white p-1 clearfix">
         <?php
               if(isset($_SESSION['pre_order']) && $_SESSION['pre_order']!='') {
                  echo '<div class="alert alert-warning" role="alert"><b style="font-size: 16px;">Pre Order : '.$_SESSION['pre_order'].'</b></div>';
               }
         ?>
         <ul class="list-group list-group-flush" id="cartItems_section1" style="    height: 48vh;">
             
                                 <?php 
          //   echo $_SESSION['del_type'];
  if($_SESSION['del_type']=='collection'){
      ?>
             <div id="collectiondiv" style="display: block;">
    
              <p style="text-align: center;">Visit <b>1791 Paisley Rd W, Cardonald, Glasgow, G52 3SS</b> to collect this order.</p>
             
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2239.9903104951263!2d-4.344273684065866!3d55.84548278057824!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x488848a6563af0ab%3A0x59f22cd465465796!2sNatural%20Spice%20Tandoori!5e0!3m2!1sen!2sin!4v1644571177982!5m2!1sen!2sin"  style="border: 1px solid #00000038;width: 100%;height: 180px;"  allowfullscreen="" loading="lazy"></iframe>  
    </p></div>
      <?php
  }
  ?>
         <?php
         foreach($allItems as $k => $val) { 
            
            $itemPrice = $val[0][2] * $val[0][4];
           // $totPrice = $totPrice + $itemPrice;

            $listAddons = array();
            $addOnPrice = 0;
            foreach($val[1] as $k2 => $val2) {
               $addOn = explode(',',$val2);  
               $addOnPrice += $addOn[3] * $addOn[4];
              // $totPrice = $totPrice + $addOnPrice;
               $listAddons[] = $addOn[4].'x '.$addOn[2]; 
            }
            $addonList = implode(' &#9679; ', $listAddons);
            if($val[0][6]>0){
               $di=$itemPrice*($val[0][6]/100);
               $item_addon_price = ($itemPrice) + $addOnPrice;	
                 $disc_fee=$disc_fee+$di;	
            }else{	
            $item_addon_price = $itemPrice + $addOnPrice;	
                	
            }
            ?>
           
           
            <li class="list-group-item" style="padding:0px 0px;">
       
         
                  <div class="row pl-3">
                        <span class="col-8 p-1 itemTit">
                           <b><?php echo $val[0][4]; ?>x <?php echo $val[0][1]; ?></b>
                        </span>
                        <span class="col-4 pt-1">
                        <span  class="cart_item_pr">
                        <b>£ <?php echo number_format($item_addon_price, 2, '.', ''); ?></b>
                        </span>
                     </div>  
                     <?php 
                     if($addonList!='') { ?>     
                        <div class="row pl-1">
                        <div class="col-12">
                            <i class="itemNotes p-1"> <?php echo $addonList; ?></i>
                        </div>
                        </div>
                     <?php   } 
                     if($val[0][5]!='') { ?>  
                     <div class="row">
                        <div class="col-12">
                           <i class="itemNotes p-1"> <i class="icofont-ui-note"> </i> <?php echo $val[0][5]; ?></i>
                        </div>
                     </div>
                     <?php   }   ?>
                     <div class="row text-red float-right pr-4"><a onClick="updateCart(<?php echo $k; ?>, 1, 'remove');"><u><i><b>Remove</b></i></u></a></div>
                     <hr style="color: #acacac;height: .5px;margin-top: 25px;">
            </li>
            <?php
               $totPrice = $totPrice + $item_addon_price;
         }  
         $ser_fee = 0;
         $del_fee = 0;
         //$disc_fee = 0;
         $subTot = $totPrice;
         $grandTot = $subTot + $del_fee - $disc_fee;
         ?>
         
         </ul>
         <h6 class="font-weight-bold mb-0 pt-2" style="font-size: 14px;"> Discount  <span class="float-right">- £ <?php echo number_format($disc_fee, 2, '.', ''); ?></span></h6>
                        <a href="checkout.php" class="btn btn-success btn-block btn-lg redbg">Checkout Now  <b>£<?php echo number_format($grandTot, 2, '.', ''); ?></b></a>
                     
                     </div>

                     
         
      </div>
      <?php
}
else {
   ?>
   <center style="margin-top:12%;"><img src="https://grill-guru.co.uk/newdev/img/empty-cart.png" width="200" /><br><p>Your cart is empty<br>Please add items</p></center>
   <?php
}
?>