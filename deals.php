<?php
include 'src/config.php';
function getHotelAPI() {
  $graph_url= 'https://deliveryguru.co.uk/dg_api/getRestaurantsDetails/200';
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $graph_url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  $output = curl_exec($ch); 
  curl_close($ch);
  return json_decode($output, true); 
}
$getHotelDetails = getHotelAPI();
$_SESSION['discountper']=$getHotelDetails[0]['discount'];

function getmetaHotelAPI() {
  $graph_url= 'https://deliveryguru.co.uk/dg_api/getmetadata/200/deals';
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $graph_url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  $output = curl_exec($ch); 
  curl_close($ch);
  return json_decode($output, true); 
}
$getmetaHotelDetails = getmetaHotelAPI();
$keyword=$getmetaHotelDetails['result'][0]['allTag'];
$keyWord=$getmetaHotelDetails['result'][0]['h1_tag'];
?>
<!doctype html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- Favicons-->
   <?php echo $keyword;?>
  
       <link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
      <!-- Bootstrap core CSS-->
      <link href="vendor1/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome-->
      <link href="vendor1/fontawesome/css/all.min.css" rel="stylesheet">
      <!-- Font Awesome-->
      <link href="vendor1/icofont/icofont.min.css" rel="stylesheet">
      <!-- Select2 CSS-->
      <link href="vendor1/select2/css/select2.min.css" rel="stylesheet">
      <!-- Custom styles for this template-->
      <link href="css1/osahan.css" rel="stylesheet">
      <script src="js/jquery-3.5.1.js"></script>
     
      
    
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-222117938-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-222117938-1');
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Restaurant",
  "name": "Natural spice online",
  "image": "https://www.natural-spiceonline.co.uk/img/logo1.png",
  "@id": "",
  "url": "https://www.natural-spiceonline.co.uk",
  "telephone": "01418825400",
  "menu": "https://www.natural-spiceonline.co.uk/",
  "servesCuisine": "Indian | Italian | Turkish",
  "acceptsReservations": "false",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "1791, Paisley Road West, Cardonald",
    "addressLocality": "Glasgow",
    "postalCode": "G52 3SS",
    "addressCountry": "GB"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": 55.8454805,
    "longitude": -4.3420923
  },
  "openingHoursSpecification": [{
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday"
    ],
    "opens": "16:30",
    "closes": "00:20"
  },{
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": [
      "Friday",
      "Saturday"
    ],
    "opens": "16:30",
    "closes": "01:20"
  }] ,
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "4.4",
    "bestRating": "5",
    "worstRating": "0",
    "ratingCount": "97"
  }
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Natural spice online",
  "url": "https://www.natural-spiceonline.co.uk/",
  "logo": "https://www.natural-spiceonline.co.uk/img/logo1.png"
}
</script>


<!-- End Facebook Pixel Code -->
      <style>
              .unselectable {
        -webkit-user-select: none;
        -webkit-touch-callout: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        /*color: #cc0000;*/
      }
      </style>
   </head>
   <body class="unselectable">
      
     <?php include("headerot.php");?>
     
      <section class="section pt-5 pb-5">
         <div class="container">
            <div class="row">
               <div class="col-md-12" id="nooofer" style="display:none;">
               <h1 class="font-weight-bold mt-0 mb-3" style="font-size:20px;"><?php echo $keyWord;?></h1>
                   <h2 style="text-align: center;"> <p style="margin-bottom:20px;">Sorry No offers Available</p> <img src="img/nooffer.png" style="width:300px;">
             
              </h2>
               </div>
              <!-- <h1 class="font-weight-bold mt-0 mb-3" style="font-size:20px;"><?php echo $keyWord;?></h1>-->
               <div class="col-md-6" style="padding-bottom: 21px;" id="offer">
                
              </div>
            
            
            
         </div>
      </section>
      
    <?php
  include('Mainfooter.php');
  ?>
       <script>
          function gethomedet(){
    
$.ajax({
					 url: 'https://deliveryguru.co.uk/dg_api/getRestaurantsDetails/200',
					 type: "GET",
					 crossDomain: true,
					 dataType: "json",
					 cache: "false",
					 success: function(result) {
			         console.log(result);
			         var obj='';
			         if(result[0].discount!=0){
			            
			             	              document.getElementById("nooofer").style.display="none";
			              obj+='<div class="card offer-card border-0 shadow-sm">';
                    obj+='<div class="card-body">';
                       obj+='<h5 class="card-title"><img src="img/favicon.png"> '+result[0].discount+'% Off </h5>';
                       obj+='<h6 class="card-subtitle mb-2 text-block">All Orders</h6>';
                       obj+='<p class="card-text">Get '+result[0].discount+'% Discount of All online Orders!!!!<br></p>';
                       obj+='<a href="menu.php" class="card-link">ORDER NOW</a>';
                       obj+=' </div>';
                obj+=' </div>';
                       document.getElementById("offer").innerHTML=obj;
			         }
			         else{
		 document.getElementById("nooofer").style.display="block";
			         }
                 

				 }
		   });
}
gethomedet();
      </script>
      <!-- jQuery -->
      <!-- <script src="vendor1/jquery/jquery-3.3.1.slim.min.js" type="588203e22900fa56a6292e20-text/javascript"></script> -->
      <!-- Bootstrap core JavaScript-->
      <script src="vendor1/bootstrap/js/bootstrap.bundle.min.js" type="588203e22900fa56a6292e20-text/javascript"></script>
      <!-- Select2 JavaScript-->
      <script src="vendor1/select2/js/select2.min.js" type="588203e22900fa56a6292e20-text/javascript"></script>
      <!-- Custom scripts for all pages-->
      <script src="js/custom.js" type="588203e22900fa56a6292e20-text/javascript"></script>
   <script src="js/rocket-loader.min.js" data-cf-settings="588203e22900fa56a6292e20-|49" defer=""></script>
   <script defer src="js/beacon.min.js" data-cf-beacon='{"rayId":"5d3af9b9cb941016","version":"2020.9.0","si":10}'></script>
     <script>
       $(document).ready(function() {
          $('body').bind('cut copy', function(e) {
              e.preventDefault();
            });
        $("body").on("contextmenu", function(e) {
              return false;
            });
        });
  </script>
</body>
</html>
