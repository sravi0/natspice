<?php 
include 'src/config.php';
if(!isset($_SESSION["uid"])){
	header("Location: login.php"); 
}
else{
?>
<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie ie9">
   <![endif]-->
   <html lang="en-US">
      
      <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
         
         <meta http-equiv="X-UA-Compatible" content="IE=edge">
         <meta name="viewport" content="width=device-width, initial-scale=1">
         <!-- Favicons-->
         <title>Grill guru</title>
         <link rel="apple-touch-icon" sizes="57x57" href="wp-content/themes/quickfood/img/apple-icon-57x57.png">
		 <link rel="apple-touch-icon" sizes="60x60" href="wp-content/themes/quickfood/img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="wp-content/themes/quickfood/img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="wp-content/themes/quickfood/img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="wp-content/themes/quickfood/img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="wp-content/themes/quickfood/img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="wp-content/themes/quickfood/img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="wp-content/themes/quickfood/img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="wp-content/themes/quickfood/img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="wp-content/themes/quickfood/img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="wp-content/themes/quickfood/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="wp-content/themes/quickfood/img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="wp-content/themes/quickfood/img/favicon-16x16.png">
<link rel="manifest" href="wp-content/themes/quickfood/img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="wp-content/themes/quickfood/img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<meta name="description" content="We are so honoured to introduce Grill Guru&#039;s updated website to deliver the best meals prepared daily fresh with the freshest ingredients..."/>
<link rel="canonical" href="https://grill-guru.co.uk/" />
<meta property="og:locale" content="en_GB" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Grill Guru Gril Store | Based In Glasgow" />
<meta property="og:description" content="We are so honoured to introduce Grill Guru&#039;s updated website to deliver the best meals prepared daily fresh with the freshest ingredients..." />
<meta property="og:url" content="https://grill-guru.co.uk/" />
<meta property="og:site_name" content="Grill Guru" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="We are so honoured to introduce Grill Guru&#039;s updated website to deliver the best meals prepared daily fresh with the freshest ingredients..." />
<meta name="twitter:title" content="Grill Guru Gril Store | Based In Glasgow" />
         

         <link rel='stylesheet' href="css/styles.pure.css">
   <link rel="stylesheet" href="css/icofont/icofont.min.css">
         <script type='text/javascript' src='wp-content/plugins/LayerSlider/static/js/greensockcd11.js?ver=1.11.8'></script>
         <script type='text/javascript' src='wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
         <script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1'></script>
         <script type='text/javascript' src='wp-content/plugins/LayerSlider/static/js/layerslider.kreaturamedia.jquery76f9.js?ver=5.6.8'></script>
         <script type='text/javascript' src='wp-content/plugins/LayerSlider/static/js/layerslider.transitions76f9.js?ver=5.6.8'></script>
         <script type='text/javascript' src='wp-content/themes/quickfood/js/ajax2e67.js?ver=2016-08-06'></script>
		 
<body class="page-template page-template-template_part page-template-payment-success page-template-template_partpayment-success-php page page-id-279 wpb-js-composer js-comp-ver-4.11.2 vc_responsive">

    <!--[if lte IE 8]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a>.</p>
    <![endif]-->
    
    <div id="preloader">
        <div class="sk-spinner sk-spinner-wave" id="status">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
    </div><!-- End Preload -->
    
    <!-- Header ================================================== -->
    <?php include "header.php";?>
<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="" data-natural-width="1400" data-natural-height="350" style="background: #000000fa;height: 51px;">
  
</section><!-- End section -->
<!-- End SubHeader ============================================ -->

<div id="position">
    <div class="container">
        <ul class="ec-breadcrumb"><li><a rel="v:url" property="v:title" href="index.php">Home</a></li><li class="active">Profile</li></ul>    </div>
</div><!-- Position -->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<!-- Content ================================================== -->
<style>

/*  bhoechie tab */
div.bhoechie-tab-container{
  z-index: 10;
  background-color: #ffffff;
  padding: 0 !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  border:1px solid #ddd;
  margin-top: 20px;
  margin-left: 50px;
  -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  box-shadow: 0 6px 12px rgba(0,0,0,.175);
  -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  background-clip: padding-box;
  opacity: 0.97;
  filter: alpha(opacity=97);
}
div.bhoechie-tab-menu{
  padding-right: 0;
  padding-left: 0;
  padding-bottom: 0;
}
div.bhoechie-tab-menu div.list-group{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a .glyphicon,
div.bhoechie-tab-menu div.list-group>a .fa {
  color: #db8418;
}
div.bhoechie-tab-menu div.list-group>a:first-child{
  border-top-right-radius: 0;
  -moz-border-top-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a:last-child{
  border-bottom-right-radius: 0;
  -moz-border-bottom-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a.active,
div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
div.bhoechie-tab-menu div.list-group>a.active .fa{
  background-color: #da8010;
  background-color: #da8010;
  color: #ffffff;
}
div.bhoechie-tab-menu div.list-group>a.active:after{
  content: '';
  position: absolute;
  left: 100%;
  top: 50%;
  margin-top: -13px;
  border-left: 0;
  border-bottom: 13px solid transparent;
  border-top: 13px solid transparent;
  border-left: 10px solid #db8418;
}

div.bhoechie-tab-content{
  background-color: #ffffff;
  /* border: 1px solid #eeeeee; */
  padding-left: 20px;
  padding-top: 10px;
}

div.bhoechie-tab div.bhoechie-tab-content:not(.active){
  display: none;
}
table{
	border: 1px solid #ccc;
}
a.list-group-item.active, a.list-group-item.active:hover, a.list-group-item.active:focus{
	border-color: #db8418;
}
</style>
<?php 
$uid=$_SESSION["uid"];
$userdetails="select * from user_details where user_id='$uid'";
$getuser = mysqli_query($connection,$userdetails);
$userres = $getuser->fetch_array();
$orderdet="select * from orders where person_id='$uid' and status!=3 and status!='0' order by created DESC";
$getorder = mysqli_query($connection,$orderdet);
$orderhistory="select * from orders where person_id='$uid' and (status=3 or status='0') order by created DESC";
$getorderhistory = mysqli_query($connection,$orderhistory);
?>
<div class="container" style="margin-bottom: 20%;margin-top: 5%;">
	<div class="row">
        <div class="col-lg-12 col-md-7 col-sm-8 col-xs-9 bhoechie-tab-container">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
              <div class="list-group">
                <a href="#" class="list-group-item active text-center">
                  <h4 class="glyphicon glyphicon-user"></h4><br/>User Details
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="glyphicon glyphicon-bell"></h4><br/>Current Orders
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="glyphicon glyphicon-shopping-cart"></h4><br/>Order History
                </a>
                <a href="logout.php" class="list-group-item text-center">
                  <h4 class="glyphicon glyphicon-log-out"></h4><br/>Logout
                </a>
               
              </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                <!-- flight section -->
                <div class="bhoechie-tab-content active">
                    <center>
					<h2>My Profile</h2>
                     <table class="table table-striped jambo_table bulk_action" style="border: 1px solid #ccc;box-shadow: 4px 5px 11px #ccc;">               
					<tbody>					  	
								   
						<tr><td>Name: </td><td><?php echo $userres['name']; ?></td></tr>
						<tr><td>Email: </td><td><?php echo $userres['email']; ?></td></tr>
						<tr><td>Phone: </td><td><?php echo $userres['mobile']; ?></td></tr>
						<tr><td>Password: </td><td>*********</td></tr>
						<tr><td>Account Created: </td><td><?php echo $userres['created']; ?>.</td></tr>
					</tbody>
				</table>
				<button class="btn btn-success">Edit</button>
                    </center>
                </div>
                <!-- train section -->
                <div class="bhoechie-tab-content">
                    <center>
                     <h2 class="inner">My Current Orders</h2>
					 <table class="table table-striped jambo_table bulk_action">
                      <thead>
                        <tr class="headings">
						  <th>Order. ID #</th>
                          <th>Name</th>                          						  
						  <th>Total Amount</th>
						  <th>Delivery Date & Time</th>
                          <th>Status</th>
                          <th>Action</th>                          
                        </tr>
                      </thead>
                      <tbody>
					   <?php 
					   while($orderres = $getorder->fetch_array()){
					   ?>
							<tr>
							  <td><?php echo $orderres['orderno'];?></td>
							  <td><?php echo $userres['name'];?></td>
							  <td>&pound;<?php echo $orderres['total_amount'];?></td>	
							  <td><?php echo $orderres['created'];?> - <?php echo $orderres['delivery_time'];?></td>
							  <td>
							      <?php 
							      if($orderres['status']=='7'){
							          echo "Order is Accepted By Hotel";
							      }
							      else if($orderres['status']=='2'){
							          echo "Order is Processing";
							      }
							       else if($orderres['status']=='1'){
							          echo "Order is Assigned to the driver";
							      }
							      else if($orderres['status']=='5'){
							          echo "Order is prepared";
							      }
							       else if($orderres['status']=='4'){
							          echo "Driver Picked Your Order";
							      }
							      ?>
							  </td>
							  <td><a href="ordertrack?id=890" class="btn btn-primary btn-xs">Track Order</a></td>
							</tr>
							<?php } ?>
						 
							
												</tbody>
					</table>
                    </center>
                </div>
    
                <!-- hotel search -->
                <div class="bhoechie-tab-content">
                    <center>
                      <h2 class="inner">Order History</h2>
					<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
        <thead>
            <tr>
                <th>Order. ID #</th>
                <th>Name</th>                          						  
				<th>Total Amount</th>
			    <th>Delivery Date & Time</th>
               <th>Status</th>
            </tr>
        </thead>
        <tbody>
            
            <?php 
			  while($orderhistoryres = $getorderhistory->fetch_array()){
					   ?>
            <tr>
                <td><?php echo $orderhistoryres['orderno']; ?></td>
                <td><?php echo $userres['name'];?></td>
                <td><?php echo $orderhistoryres['total_amount']; ?></td>
                <td><?php echo $orderhistoryres['created'];?> - <?php echo $orderhistoryres['delivery_time'];?></td>
                <td>
                    <?php 
							      if($orderhistoryres['status']=='3'){
							          echo "Order Delivered";
							      }
							      else if($orderres['status']=='0'){
							          echo "Order Rejecterd";
							      }
							       
							      ?>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
                    </center>
                </div>
              
             
            </div>
        </div>
  </div>
</div>
<!-- End container -->
<!-- End Content =============================================== -->
   <?php 
		 include("footer.php");
		 ?>

    <!-- End Footer =============================================== -->
<div class="layer"></div><!-- Mobile menu overlay mask -->
<!-- COMMON SCRIPTS -->

<script type='text/javascript' src='wp-content/themes/quickfood/js/common_scripts_min66f2.js?ver=4.7.5'></script>
<script type='text/javascript' src='wp-content/themes/quickfood/js/functions66f2.js?ver=4.7.5'></script>
<script type='text/javascript' src='wp-content/themes/quickfood/assets/validate66f2.js?ver=4.7.5'></script>
<script type='text/javascript' src='http://maps.googleapis.com/maps/api/js?key=AIzaSyDEm44WXMpUR3J4k9SjXmYmv-vQ97YRkZc&amp;ver=4.7.5'></script>
<script type='text/javascript' src='wp-content/themes/quickfood/js/infobox66f2.js?ver=4.7.5'></script>
<script type='text/javascript' src='wp-content/themes/quickfood/js/jquery.sliderPro.min66f2.js?ver=4.7.5'></script>
<script type='text/javascript' src='wp-content/themes/quickfood/js/cat_nav_mobile66f2.js?ver=4.7.5'></script>
<script type='text/javascript' src='wp-content/themes/quickfood/js/ion.rangeSlider66f2.js?ver=4.7.5'></script>
<script type='text/javascript' src='wp-content/themes/quickfood/js/theia-sticky-sidebar66f2.js?ver=4.7.5'></script>
<script type='text/javascript' src='wp-content/themes/quickfood/js/custom-main66f2.js?ver=4.7.5'></script>
<script type='text/javascript' src='wp-includes/js/wp-embed.min66f2.js?ver=4.7.5'></script>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});
</script>
<script>
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</body>
</html>
<?php } ?>