<?php
session_start();
//$_SESSION['cartItems'];




      if(isset($_GET['unset'])){
         unset($_SESSION['cartItems']);
        
      }
      if(isset($_GET['print'])){
         d($_SESSION);
      }

      function d($d){
         echo '<pre>';
         print_r($d);
         echo '</pre>';
      }

      //d($_SESSION);
      $addItems = array();
      if(isset($_POST['Items']) && $_POST['Items']!=''){
         $mainItems = json_decode($_POST['Items']);
         if(isset($_POST['addOns']) && count($_POST['addOns'])>0) { $addItems = json_decode($_POST['addOns']); } 

         $addItems = array(0=>array($mainItems, $addItems));


         if(!isset($_SESSION['cartItems'])){
            $_SESSION['cartItems'] = $addItems;
         } else {
            $_SESSION['cartItems'] = array_merge($_SESSION['cartItems'], $addItems);
         }
         $_SESSION['cartItems'] = array_reverse($_SESSION['cartItems']);
      }

      if(isset($_POST['action'])) {
         if($_POST['action']=='update') {
            if($_POST['qty']<=0){
               unset($_SESSION['cartItems'][$_POST['id']]);
            } else {
               $_SESSION['cartItems'][$_POST['id']][0][3] = $_POST['qty'];
            }
         }
         if($_POST['action']=='note') {
            $_SESSION['cartItems'][$_POST['id']][0][4] = $_POST['qty'];
         }
         if($_POST['action']=='del_type') {
            $_SESSION['del_type'] = $_POST['qty'];
         }
         if($_POST['action']=='del_charge') {
            $_SESSION['del_charge'] = $_POST['qty'];
         }
         if($_POST['action']=='coupon') {
            $_SESSION['coupon'] = $_POST['qty'];
            $_SESSION['coupon_code'] = $_POST['id'];
            if($_POST['qty']=='') {
               unset($_SESSION['coupon']);
            }
         }
         
         if($_POST['action']=='cart_clear') {
            unset($_SESSION['cartItems']);
             unset($_SESSION['discountper']);
         unset($_SESSION['del_charge']);
         unset($_SESSION['coupon']);
         }
      } 
$totPrice = 0;

if(isset($_SESSION['cartItems']) && count($_SESSION['cartItems'])>0)
{
      $allItems = array_filter($_SESSION['cartItems']); 
      ?>
      
      <div class="card-body p-2">
      <div class="mb-1 bg-white p-1 clearfix">
         <ul class="list-group list-group-flush">
         <?php
         foreach($allItems as $k => $val) { 
            
            $itemPrice = $val[0][2] * $val[0][4];
           // $totPrice = $totPrice + $itemPrice;

            $listAddons = array();
            $addOnPrice = 0;
            foreach($val[1] as $k2 => $val2) {
               $addOn = explode(',',$val2);  
               $addOnPrice += $addOn[3] * $addOn[4];
              // $totPrice = $totPrice + $addOnPrice;
              $listAddons[] = $addOn[4].'x '.$addOn[2]; 
            }
            $addonList = implode(' &#9679; ', $listAddons);
            $item_addon_price = $itemPrice + $addOnPrice;
            ?>
         
            <li class="list-group-item" style="padding:2px 0px;">
                  
                  <div class="row">
                        <span class="col-2">
                           <img src="<?php echo $val[0][3]; ?>"  class="cart_thumb" alt="..." class="pull-left">
                        </span>
                        <span class="col-10 pl-1">
                           <b><?php echo $val[0][4]; ?>x <?php echo $val[0][1]; ?></b>
                           <span  class="cart_item_pr"><b>£ <?php echo number_format($item_addon_price, 2, '.', ''); ?></b>
                           <br>
                           <span class="text-red text-right pt-2""><a onClick="updateCart(<?php echo $k; ?>, 1, 'remove');" ><u><i><b>Remove</b></i></u></a>
                           </span>
                           </span>
                        </span>
                     </div>  
                     <?php 
                     if($addonList!='') { ?>  
                        <div class="row">
                        <div class="col-12">
                            <i class="itemNotes"> <?php echo $addonList; ?></i>
                        </div>
                        </div>
                     <?php   } 
                     if($val[0][5]!='') { ?>  
                     <div class="row">
                        <div class="col-12">
                           <i class="itemNotes"> <?php echo $val[0][5]; ?></i>
                        </div>
                     </div>
                     <?php   }   ?>
                     
                     <hr style="color: #acacac;height: .5px;">
            </li>
           
            <?php
           $totPrice = $totPrice + $item_addon_price;
         }  
        
         $ser_fee = 0.50;
         $carry_bag = 0.05;
         $del_fee = 0;
         if(isset($_SESSION['coupon']) && $_SESSION['coupon']!='') {
             $disc_fee = $totPrice * ($_SESSION['coupon']/100);
             }
         else{
             $disc_fee = $totPrice*($_SESSION['discountper']/100);
         }
         
         $subTot = $totPrice-$disc_fee;
         if($_SESSION['del_type']=='collection') { 
             $del_fee = 0; 
             
         } else if(isset($_SESSION['del_charge'])) { 
             $del_fee = $_SESSION['del_charge']; 
             
         }
         
         $grandTot = $subTot + $ser_fee + $del_fee + $carry_bag;
         ?>
         
         </ul>
           <?php if(!isset($_SESSION['coupon']) || $_SESSION['coupon']=='' || $_SESSION['coupon']=='0') { ?>
         <div class="bg-white clearfix">
       
                        <div class="input-group input-group-sm mb-2">
                           <input type="text" class="form-control" id="couponValue" placeholder="Enter promo code" value="<?php if(isset($_SESSION['coupon_code']) && $_SESSION['coupon_code']!='') { echo $_SESSION['coupon_code']; } ?>">
                           <div class="input-group-append">
                              <button class="btn btn-primary" type="button" id="button-addon2" onclick="couponValidator();"><i class="icofont-sale-discount"></i> APPLY</button>
                           </div>
                        </div>
                      
         </div>
          <hr>
           <?php } ?>
        
            <div class="mb-2 bg-white  clearfix">
            
                        <p class="mb-1">Sub Total <span class="float-right text-dark">£ <span id="sub_total"><?php echo number_format($totPrice, 2, '.', ''); ?></span></span></p>
                        <p class="mb-1">Service Charges <span class="float-right text-dark">£ <?php echo number_format($ser_fee, 2, '.', ''); ?></span></p>
                        <p class="mb-1">Carry Bag Fee<i class="icofont-info-circle"></i> <span class="text-info" data-toggle="tooltip" data-placement="top" title="Total discount breakup"> <span class="float-right text-dark">£ <?php echo number_format($carry_bag, 2, '.', ''); ?></span></p>
                        <?php if($_SESSION['del_type']!='collection') { ?>
                        <p class="mb-1">Delivery Fee 
                           
                           </span> <span class="float-right text-dark">£ <span id="deliveryfm"><?php echo number_format($del_fee, 2, '.', ''); ?></span></span>
                        </p>
                        <?php } ?>
                        <p class="mb-1 text-success">Discount
                         <?php  if(!isset($_SESSION['coupon']) || $_SESSION['coupon']=='') { ?>
                        (<?php echo $_SESSION['discountper'];?>%)
                        <?php }?>
                        <?php  if(isset($_SESSION['coupon']) && $_SESSION['coupon']!='') { ?>
                        
                        <span id="coupondiscount">(Coupon Apllied Get Offer <?php echo $_SESSION['coupon'];?>% <span onclick='removeCoupon()'>&nbsp;&nbsp;<a style='cursor:pointer;color:red;'><i class='fa fa-trash' aria-hidden='true'></i>remove</a><span>)</span>
                        <?php }?>
                           <span class="float-right text-success">£ <span id="discount"><?php echo number_format($disc_fee,2,'.',''); ?></span></span>
                        </p>
                        <hr>
                        <h6 class="font-weight-bold mb-0"> Total  <span class="float-right">£ <?php echo number_format($grandTot, 2, '.', ''); ?></span></h6>
             </div>
                  </div>
      </div>
      <?php
}
else {
   ?>
   <center style="margin-top:12%;"><img src="https://grill-guru.co.uk/newdev/img/empty-cart.png" width="200" /><br><p>Your cart is empty<br>Please add items</p></center>
   <?
}
?>