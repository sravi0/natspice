
function getCartPrice(){
  $.ajax({
         async: false,
         type: "POST",
         url: "ajaxAddToCart3.php",
         data: {action : 1},
         dataType: "json",
         success: function(res) 
         {
             totQty = res.totQty;
             //alert(totQty);
             $('.badge-notification').html(totQty);
         }
  });
 }
 
 function getordertype(data){
     // alert(data.id);
     if(data.id=="collection"){
         document.getElementById('collectiondiv').style="display:block";
     }
     else{
         document.getElementById('collectiondiv').style="display:none";
     }
 }
 
 function showError(errMsg) {
   var x = document.getElementById("snackbar");
   $('#snackbar').css('background-color', 'green');
   x.innerHTML=errMsg;
   x.className = "show";
   setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
   //alert(1);
 }
 
 
 function gridSearch(value) {
   $('div[data-role="recipe"]').filter(function() {
               $(this).toggle($(this).find('h6 a').text().toLowerCase().indexOf(value) > -1);
               //alert($('.item-section').children(':visible').length);
    });
    $('.item-section').each( function() { 
               var len = $('div[data-role="recipe"]', this).children(':visible').length;
               if(len==0)
               {
                 $('h3',this).hide();
               } else {
                 $('h3',this).show();
               }
            //   console.log('data-recipe: '+$('h3',this).text()+', '+len);
     });
 }
 
 $("#searchclear").click(function(){
   $("#searchinput").val('');
   //gridSearch('');
 });
 $(".mobCart").click(function() {
     $('html,body').animate({
         scrollTop: $(".component").offset().top - 100 },
         'slow');
 });
 
 $(document).ready(function() {
     $("#searchinput, .search-mob").on("keyup change", function() {
          $("#searchinput, .search-mob").val($(this).val());
          var value = $(this).val().toLowerCase();
          $('div[data-role="recipe"]').filter(function() {
                       $(this).toggle($(this).find('h6 a').text().toLowerCase().indexOf(value) > -1);
                       //alert($('.item-section').children(':visible').length);
           });
           $('.item-section').each( function() { 
                       var len = $('div[data-role="recipe"]', this).children(':visible').length;
                       if(len==0)
                       {
                         $('h3',this).hide();
                       } else {
                         $('h3',this).show();
                       }
                   //   console.log('data-recipe: '+$('h3',this).text()+', '+len);
             });
     });
 });
 
 localStorage.clear();
 function refreshCart() {
 var del_type = $('.btn-toggle .active').data("id");
 $.ajax({
           type: "POST",
           url: "ajaxAddToCart.php",
           data: {action : 'del_type', qty: del_type},
           success: function(res) 
           {
               $('#cartItems_section').html(res);
               getCartPrice();
           }
       });
 }
 refreshCart();
 
 $('.close').on('click', function () {
             $('#addonModal').modal('hide'); //$('.modal-backdrop').hide();
 })
 
 
 
 var itemMinMax = {};
 function btnAddToCart() {
       //alert($('#addInst').val());
       //alert($('input[name="addOnCheck'+selId+'[]"]:checked').length);
        $('.alert-danger').hide();
        var errorMsg='';
        
        var MainSel = $('input[name="subMenuList"]:checked').val();
 
        if($('input[name="subMenuList"]:checked').length == 0) {
          errorMsg = 'Please select at least one item!';
        }  else 
        {
           
           var temp = selItem = new Array();
           selItem = MainSel.split(",");
           selId = selItem[0];
           //alert(itemMinMax[selId]+','+selId);
           if(itemMinMax[selId]){
             temp = itemMinMax[selId].split("},{");
             var addOnItems = [];
             for(i=0;i<temp.length;i++){
                 var adOnQty   = 0;
                 var splitTxt = new Array();
                 splitTxt = temp[i].replace('{','');
                 splitTxt = splitTxt.replace('}','')
                 splitTxt =  splitTxt.split(',');
                 var chkId = $('input[name="addOnCheck'+selId+'_'+splitTxt[0]+'[]"]:checked').length;
                 //alert(chkId);
                 //alert('addOnSel'+selId+'_'+splitTxt[0]+'[]');
                 if(splitTxt[1]==1 && splitTxt[2]==1) {
                     adOnQty = $('input[name="addOnCheck'+selId+'_'+splitTxt[0]+'[]"]:checked').length;
                 } else {
                   $('input[name="addOnSel'+selId+'_'+splitTxt[0]+'[]"]').each( function() { 
                     if($(this).is(":visible")) {
                       adOnQty += parseInt($(this).val());
                     }
                   });
                 }
                 //alert(adOnQty);
 
                 if((adOnQty < splitTxt[1] || adOnQty > splitTxt[2]) && errorMsg==''){
                   errorMsg = 'Please select Min '+splitTxt[1]+' and Max '+splitTxt[2]+' Add on from '+splitTxt[3];
                   //alert('#box_'+selId+'_'+splitTxt[0]);
                   window.location.hash = "";
                   window.location.hash = '#box_'+selId+'_'+splitTxt[0];
                   
                   //$('#box_'+selId+'_'+splitTxt[0]+' .list-card-body').css('color','red !important');
                   $('#box_'+selId+'_'+splitTxt[0]+' .list-card-body .mb-1 a').attr('style', 'color: red !important');
                   $('#box_'+selId+'_'+splitTxt[0]+' .list-card-body .mb-1 a').fadeIn(100).fadeOut(300).fadeIn(300).fadeOut(300).fadeIn(300);
                   //alert('#box_'+selId+'_'+splitTxt[0]+' .list-card-body');
                   /*
                   //$('#addonModal').on('shown.bs.modal', function(event) {
                       $('.modal-body').scrollTop(0);
                       //var section = $(event.relatedTarget).data('section');
                       var sectionOffset = $('#box_'+selId+'_'+splitTxt[0]).offset().top;
                       var position = $('#box_'+selId+'_'+splitTxt[0]).position().top;
                       console.log('sectionOffset: '+sectionOffset+', '+position+', '+$('#box_'+selId+'_'+splitTxt[0]).scrollTop());
                       //scroll the container
                       $('.modal-body').animate({ scrollTop: sectionOffset-30 }, "slow");
                       
                     //var position = $('.box_'+selId+'_'+splitTxt[0]).position();
                     $('#addonModal').scrollTop(position);
                     //scrollTo = $('#AddOns');
                  // }); */
                 }
                 $('input[name="addOnCheck'+selId+'_'+splitTxt[0]+'[]"]:checked').map(function(){
                     if(splitTxt[1]==1 && splitTxt[2]==1) { 
                         var addOnQty = 1; 
                     } else {
                         var addOnQty = $("~ span.addOnSel input", this).val();
                     }
                     addOnItems.push($(this).val()+','+addOnQty);
                 });
                 console.log('Values: '+selId+','+splitTxt[1]+','+splitTxt[2]+', '+chkId);
             }
           }
        }
 
        if(errorMsg!=''){
         $('.alert-danger').html(errorMsg);
         $('.alert-danger').show();
         setTimeout(
             function() {
               $('.alert-danger').hide();
               $('.list-card-body .mb-1 a').attr('style', 'color: #000000 !important');
             }, 5000);
         return false;
        } else {
          var totItem = parseInt($('input[name="totItem"]').val());
          selItem.push(totItem);
          selItem.push($('#addInst').val());
          var ItemsStr = JSON.stringify(selItem);
          var addonItemsStr = JSON.stringify(addOnItems);
         // alert(ItemsStr);
          
          AajxUpdateCart(ItemsStr, addonItemsStr, 'add');
          //alert(MainSel+', '+totItem);
        }
 }
 function AajxUpdateCart(items, addONitem, add){
       //var jsonString = items;
       $('#addonModal').modal('hide'); 
       $.ajax({
           type: "POST",
           url: "ajaxAddToCart.php",
           data: {Items : items, addOns: addONitem},
           success: function(res) 
           {
               getCartPrice();
               $('#cartItems_section').html(res);
               showError('Successfully added!');
           }
       });
 }
 
 function updateCart(id, qty, act){
      
       if(act=='add') {
         qty = qty + 1; action = 'update';
       } else if(act=='remove') {
         qty = qty - 1; action = 'update';
       } else if(act=='note') {
          action = 'note';
       } else if(act=='del_type') {
          action = 'del_type';
       } else if(act=='pre_order') {
          action = 'pre_order';
       }
       //alert(id+', '+qty+', '+act);
       //var jsonString = items;
       /*$('#addonModal').modal('hide');  */
       $.ajax({
           type: "POST",
           url: "ajaxAddToCart.php",
           data: {action : action, qty: qty, id: id},
           success: function(res) 
           {   //alert(res);
               $('#cartItems_section').html(res);
               getCartPrice();
               
           },
           error: function(json) {
                   alert(json);
           }
       });
      
 }
 
 function updateCartItems(items,add){
   var getItem = localStorage.getItem("cartItems");
   alert(getItem);
   if (localStorage.getItem("cartItems") === null) {
     localStorage.setItem('cartItems', items);
   } else {
     getItem = localStorage.getItem("cartItems");
     getItemArr = JSON.parse(getItem);
     getItemArr.push(items);
     localStorage.setItem('cartItems', JSON.stringify(getItemArr));
 
     //getItem = localStorage.getItem("cartItems");
     
   }
   alert(JSON.stringify(JSON.parse(localStorage.getItem("cartItems"))));
 }
 
 function addInst() {
   $('.addInst').show();
 }
 
 function updateNote(id) {
   //alert($('#editNote_'+id+'').val());
   $('.editInst_'+id+'').hide();
   $('.viewInst_'+id+'').css("display", "flex");
   var notes = $('#editNote_'+id+'').val();
   updateCart(id, notes, 'note');
 }
 
 function editNote(id) {
   //alert($('#editNote_'+id+'').val());
   $('.editInst_'+id+'').css("display", "flex");
   $('.viewInst_'+id+'').hide();
 }
 
 
 $(document).on('click', '.btn-number', function(e){
   //var thePrice = $(this).closest('input').val();
     
     e.preventDefault();
     fieldName = $(this).attr('data-field');
     type      = $(this).attr('data-type');
     cls      = $(this).attr('data-id');
     var input = $('.'+cls);
     //alert(input.val());
     var currentVal = parseInt(input.val());
     if (!isNaN(currentVal)) {
         if(type == 'minus') {
             
             if(currentVal > input.attr('min')) {
                 input.val(currentVal - 1).change();
             } 
             if(parseInt(input.val()) == input.attr('min')) {
                 $(this).attr('disabled', true);
             }
 
         } else if(type == 'plus') {
 
             if(currentVal < input.attr('max')) {
                 input.val(currentVal + 1).change();
             }
             if(parseInt(input.val()) == input.attr('max')) {
                 $(this).attr('disabled', true);
             }
 
         }
     } else {
         input.val(1);
     }
 });
 /*
 $(document).on('click', '.input-number', function(e){
    $(this).data('oldValue', $(this).val());
 });
 
 $(document).on('click', '.input-number', function(e){
     minValue =  parseInt($(this).attr('min'));
     maxValue =  parseInt($(this).attr('max'));
     valueCurrent = parseInt($(this).val());
     
     name = $(this).attr('name');
     if(valueCurrent >= minValue) {
         $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
     } else {
         alert('Sorry, the minimum value was reached');
         $(this).val($(this).data('oldValue'));
     }
     if(valueCurrent <= maxValue) {
         $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
     } else {
         alert('Sorry, the maximum value was reached');
         $(this).val($(this).data('oldValue'));
     }
 });
 
 $(".input-number").keydown(function (e) {
         // Allow: backspace, delete, tab, escape, enter and .
         if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
              // Allow: Ctrl+A
             (e.keyCode == 65 && e.ctrlKey === true) || 
              // Allow: home, end, left, right
             (e.keyCode >= 35 && e.keyCode <= 39)) {
                  // let it happen, don't do anything
                  return;
         }
         // Ensure that it is a number and stop the keypress
         if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
             e.preventDefault();
         }
 });
 */
 
 
 
 function showAddOns(mId, arId, iId)
 {
     //var restStatus = getHotelStatus(mId, arId, iId);
     //alert(restStatus);
   //if(getHotelStatus()===true || pre_order_status==1) {
 
       $('.alert-danger').hide();
       var itemData = getItems[mId]['sub_menu'][arId];
       //alert(itemData.image);
       $('#addonModalLabel').html(itemData.item_name);
       if(itemData.image=='dummy.jpg'){ itemImg ='https://www.turquoiseglasgow.co.uk/newdev/img/noimage.jpg'; } else { itemImg ='https://deliveryguru.co.uk/admin/images/itemimages/'+itemData.image; }
       var modelBody = modelFooter = '';
       modelBody += '<p class="text-gray popup-desc">'+itemData.item_desc+'</p>';
       
       modelBody += '<img src="'+itemImg+'" class="img-fluid item-img" style="width: 100%;height: 300px; object-fit: cover;box-shadow: 2px 1px 3px #dddddd;border-radius: 5px;" />';
       
 
       var itemChild = itemData.child;
       if(itemChild.length==0) {
         $('.modal-body').css('height', '67vh');
         modelBody += '<input type="radio" style="display:none" checked onchange="addcatpop(this)" name="subMenuList" data-id="'+itemData.categories_id+'" data-name="'+itemData.item_name+'" data-amount="'+itemData.item_name+'" data-subid="'+itemData.id+'"  item_name="" item_id="" addon_price="'+itemData.price+'" onclick="validate(this,1);" value="'+itemData.id+', '+itemData.item_name+', '+itemData.price+', '+itemImg+'">';
       } 
       else 
       {
         $('.modal-body').css('height', '75vh');
           for (i = 0; i < itemChild.length; i++) 
           {
             //modelBody += '<details><summary>'+itemChild[i].item_name+'<span="price">£'+itemChild[i].price+'</span></summary><p><input type="radio" name="trg'+itemChild[i].id+'" id="acc'+itemChild[i].id+'""><label class="accord" for="acc'+itemChild[i].id+'">'+itemChild[i].item_name+'</label><span style="float:right;">£'+itemChild[i].price+'</span><p style="font-size: 11px;margin-bottom:5px;">'+itemChild[i].item_desc+' </p>';
             modelBody += '<div class="detail det_'+itemChild[i].id+'"><span class="summary" data-id="'+itemChild[i].id+'"><input type="radio" style="display:none" id="'+itemChild[i].id+'" onchange="addcatpop(this)" name="subMenuList" data-id="'+itemChild[i].categories_id+'" data-name="'+itemChild[i].item_name.replace(/,/g," ")+'" data-amount="'+itemChild[i].item_name+'" data-subid="'+itemChild[i].id+'"  item_name="" item_id="" addon_price="'+itemChild[i].price+'" onclick="validate(this,1);" value="'+itemChild[i].id+', '+itemChild[i].item_name.replace(/,/g," ")+', '+itemChild[i].price+', '+itemImg+'"><span class="row col-12"><span class="col-10">'+itemChild[i].item_name+'</span><span  class="col-2 text-right" style="float:right;">£'+itemChild[i].price+'</span></span><div style="font-size: 11px;margin-bottom:5px;" class="addOn_'+itemChild[i].id+' addonDiv">';
 
             //modelBody += '<div class="content"><div class="inner">';
                 
                 var itemExtra = itemChild[i]['extra'];
                 var childID = itemChild[i].id;
                 var txtJson = '';
                 for (var j = 0; j < itemExtra.length; j++) 
                 {
                   
                   var childMin = itemExtra[j].count;
                   var childMax = itemExtra[j].maxcount;
 
                   
 
                     if(itemExtra[j].count) {  txtJson += '{'+j+', '+childMin+', '+childMax+', '+itemExtra[j].name+'}'; }
                     
                     //itemMinMax[childID] = array(1,2);
                       modelBody += '<div class="boxes" id="box_'+childID+'_'+j+'"><div class="list-card-body" style="padding-top: 0px !important;margin-left: -5px;"><h6 class="mb-1"><a href="#" class="text-black">'+itemExtra[j].name+'</a></h6><p class="text-gray mb-2" style="font-size: 10px;">'+itemExtra[j].add_on_desc+'</p></div>';
 
                       var itemAddon = itemExtra[j]['addon'];
                       for (var k = 0; k < itemAddon.length; k++) 
                       {
                         SelQty='';
                         if(childMin==1 && childMax==1) { Chk_Rad = 'radio'; SelQty=''; } else { 
                             Chk_Rad = 'checkbox'; 
                             SelQty += '<span class="addOnSel"><div class="btn-number" disabled="disabled" data-type="minus" data-field="addOnSel'+childID+'_'+j+'[1]"data-id="'+childID+'_'+j+'_'+k+'"><i class="icofont-minus inc_btn"></i></div><input type="text" class="form-control form-control-sm input-number '+childID+'_'+j+'_'+k+'" name="addOnSel'+childID+'_'+j+'[]" value="1" min="1" readonly max="'+childMax+'" style="background: #fff;border: none;text-align: center;"><div class="btn-number" data-type="plus" data-field="addOnSel'+childID+'_'+j+'[1]" data-id="'+childID+'_'+j+'_'+k+'"><i class="icofont-plus inc_btn"></i></div></span>';
                             //SelQty += '<select name="addOnSel'+childID+'_'+j+'[]" class="addOnSel">';
                             //for(var q=1; q<=childMax; q++) {
                               //SelQty += '<option value="'+q+'">'+q+'</option>';
                             //}
                             //SelQty += '</select>';
                           }
                         modelBody += '<div class="custom-control custom-'+Chk_Rad+'"><input type="'+Chk_Rad+'" class="custom-control-input  addonCls_'+childID+'" id="'+itemAddon[k].addon_id+'" name="addOnCheck'+childID+'_'+j+'[]" value="'+itemChild[i].id+','+itemAddon[k].addon_id+','+itemAddon[k].addon_name.replace(/,/g," ")+','+itemAddon[k].addon_price+'"><label class="custom-control-label col-5 col-sm-6" for="'+itemAddon[k].addon_id+'" style="font-weight: 500;">'+itemAddon[k].addon_name.replace(/,/g," ")+'</label>'+SelQty+'';
 
                         modelBody += '<small class="text-black-50 text-right" style="float: right; color: #34373b !important;font-weight: 500;"> £ '+itemAddon[k].addon_price+'</small>';
                         modelBody += '</div>';
                       }
                       modelBody += '</div>';
                       if(itemExtra.length==(j+1)){
                         
                         itemMinMax[childID] = txtJson.replaceAll("}{", "},{");
                       }
                 }
               modelBody += '</div></div>';
               $('.addInst').hide();
               $('#addInst').val('');
 
               } 
         //modelBody += '</div>';
       }
       modelBody += '<span class="row"><b style="padding: 15px 0;font-size: 16px;">Special request to suit your taste buds</b><br><textarea type="text" placeholder="Please leave any special request e.g. Extra spicy, no sauce, well done  etc. Please do not leave any allergen info here." row="4" name="instr" id="addInst" class="col-12 form-control" style="font-size: 14px;"></textarea></span>';
       $('#AddOns').html(modelBody);
             modelFooter += '<span class="count-number float-left"><button class="btn btn-outline-secondary  btn-sm left dec" onclick="minusValue('+iId+')"><i class="icofont-minus"></i></button><input id="'+iId+'" class="count-number-input totItem" type="number" name="totItem" value="1" readonly=""><button class="btn btn-outline-secondary btn-sm right inc" onclick="plusValue('+iId+')"><i class="icofont-plus"></i></button></span><button id="addButton'+iId+'" type="button" class="btn btn-primary addbtn float-right" data-dismiss="modal" amount="'+itemData.price+'" item_id="'+iId+'" discount="'+itemData.discount+'" qty="1" status="0" notes="1" item_name = "'+itemData.item_name+'" '; 
           
           if(itemData.discount.length == 0 && itemData.discount.length == 0){
               modelFooter += 'onclick="addItem(this)"';
           }
           modelFooter += ' onclick="btnAddToCart()">Add to cart</button>';
       $('.modal-footer').html(modelFooter);
       //$('.modal-footer').html('<span class="count-number float-right"><button class="btn btn-outline-secondary  btn-sm left dec"> <i class="icofont-minus"></i> </button><input class="count-number-input" type="text" value="1" readonly=""><button class="btn btn-outline-secondary btn-sm right inc"> <i class="icofont-plus"></i> </button></span><button type="button" class="btn btn-primary" data-dismiss="modal">Add to cart</button>');
       
       $('#addonModal').modal('show');
   //}
 }
 
 
 
 $(document).ready(function() {
 
   
   $(document).on('click', 'details', function(){
      var id = $(this).attr('class');
     //$("details").on("click", function () {
     //$("details").not(this).removeAttr("open");
     //var attr = $(this).attr('open');
     //$('details').removeAttr('open');
     //$(this).addAttr('open');
     //$('details[open]').not(this).removeAttr("open");  
     //$(this).attr('open','');
     $(this).siblings('details').removeAttr('open');
     $(this).attr('open','');
     alert(id);
     
   });
   
   $(document).on('click', '.summary', function(){
       $('.addonDiv').hide();
       $('input[name="subMenuList"]:checked').attr('checked', false);
       //$('.addonDiv').hide();
 
       var id = $(this).data('id');
       //$('.addOn_'+id+'').show();
       $('.summary #'+id+'').attr('checked', true); 
       $('.addOn_'+id+'').show();
       
       //var radio = $(this).children('input[type="radio"]');
       //radio.prop('checked', !radio.prop('checked'));
       //alert(id);
      // $(this).parent().css('background-color', '#fffbec');
       //$(this).addClass("toggled");
     });
 });
 var hidWidth;
 var scrollBarWidths = 40;
 
 var widthOfList = function(){
   var itemsWidth = 0;
   $('.list li').each(function(){
     var itemWidth = $(this).outerWidth();
     itemsWidth+=itemWidth;
   });
   return itemsWidth;
 };
 
 var widthOfHidden = function(){
   var t = (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
   console.log('widthOfHidden: '+(($('.wrapper').outerWidth())+','+widthOfList()+','+getLeftPosi())+','+scrollBarWidths+','+t);
   return (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
 };
 
 var getLeftPosi = function(){
   return $('.list').position().left;
 };
 
 var reAdjust = function(){
   if (($('.wrapper').outerWidth()) < widthOfList()) {
     $('.scroller-right').show();
   }
   else {
     $('.scroller-right').show();
   }
   
   if (getLeftPosi()<0) {
     $('.scroller-left').show();
   }
   else {
     //$('.item').animate({ left:"-="+getLeftPosi()+"px"},'slow');
     $('.scroller-left').hide();
   }
   if(getLeftPosi()==0){
    $('.scroller-left').fadeOut('slow');
   }
 }
 
 reAdjust();
 
 $(window).on('resize',function(e){  
     reAdjust();
 });
 
 $('.scroller-right').click(function() {
   
   $('.scroller-left, .scroller-right').hide();
   $('.scroller-left').fadeIn('slow');
    $('.list').animate({left:"-="+($('.wrapper').outerWidth()   )+"px"},'slow',function() {
       var t = (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
       if(($('.wrapper').outerWidth()) >= (widthOfList()+getLeftPosi())){
          $('.scroller-right').fadeOut('slow');
       } else {
         $('.scroller-right').fadeIn('slow');
       }
    });
 });
 
 $('.scroller-left').click(function() {
    $('.scroller-left, .scroller-right').hide();
    $('.scroller-right').fadeIn('slow');
    $('.list').animate({left:"+="+($('.wrapper').outerWidth())+"px"},'slow',function(){
       var t = (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
       if(getLeftPosi()==0){
          $('.scroller-left').fadeOut('slow');
       } else {
          $('.scroller-left').fadeIn('slow');
       }
     });
 });   
 
 
 
 
 $('.cart-menu a').on('click', function() {
     var scrollAnchor = $(this).attr('data-id');
     scrollPoint = $('#panel_groups #' + scrollAnchor).offset().top - 70 - 50;
     $('body,html').animate({
         scrollTop: scrollPoint
     }, 500);
     return false;
 })
 
 $(window).scroll(function() {
     var windscroll = $(window).scrollTop();
     if (windscroll >= 100) {
         $('#panel_groups .item-section').each(function(i) {
             if ($(this).position().top <= windscroll + 150) {
                 $('.cart-menu a.active').removeClass('active');
                 $('.cart-menu a').eq(i).addClass('active');
             }
         });
 
     } else {
         $('.cart-menu a.active').removeClass('active');
         $('.cart-menu a:first').addClass('active');
     }
 
     var leftOffset = $('.cart-menu a.active').offset().left - $('.cart-menu').offset().left +   $('.cart-menu').scrollLeft();
     var scrollPos = leftOffset / (($('.wrapper').outerWidth())-40);
 
     console.log('abcd: '+$('.wrapper').outerWidth()+','+Math.round(widthOfList())+','+Math.round(getLeftPosi())+','+Math.round(scrollBarWidths)+','+parseInt(scrollPos)+','+leftOffset);
     if(parseInt(scrollPos)>0) 
     {
         setPos = parseInt(scrollPos) * ($('.wrapper').outerWidth());
         $('.list').animate({left: "-"+setPos+"px"}, 0); 
         if(($('.wrapper').outerWidth()) >= (widthOfList()+getLeftPosi())) {
             $('.scroller-left').fadeIn('slow');
             $('.scroller-right').fadeOut('slow');
         }
     } else {
         setPos = parseInt(scrollPos) * ($('.wrapper').outerWidth());
         $('.list').animate({left: "+"+setPos+"px"}, 0); 
 
         if(($('.wrapper').outerWidth()) <= (widthOfList()+getLeftPosi())) {
             $('.scroller-left').fadeOut('slow');
             $('.scroller-right').fadeIn('slow');
         }
     }
 
 }).scroll();
 
 /*
 $(document).scroll(function() {
   console.log('Event Fired');
    var windscroll = $(this).scrollTop();
       console.log('s0:'+windscroll);
      
       $(".item-section").each(function(){
          //console.log('widthOfHidden 33: '+Math.round($('.wrapper').outerWidth())+','+Math.round(widthOfList())+','+Math.round(getLeftPosi())+','+Math.round(scrollBarWidths));
          if ($(this).isInViewport()) {
             //  Use .blogcard instead of this
             //$('.blogcard').addClass('test');
             //anchorTag607
             $('.nav-item').removeClass(' active');
             $('#anchorTag'+$(this).attr("id")+'').addClass('active');
             var posLeft = $('#anchorTag'+$(this).attr("id")+'').offset().left;
             var poslist = $('.list').position().left;
             //var eTop = $('#anchorTag'+$(this).attr("id")+'').offset().left;
             //alert(eTop);
             console.log('posLeft:'+posLeft);
             //$('.list').animate({scrollLeft: posLeft}, 500); 
             //$('.list').animate({scrollLeft: $('#anchorTag'+$(this).attr("id")+'').position().left}, 500); 
             
             var leftOffset = $('#anchorTag'+$(this).attr("id")+'').offset().left - $('.cart-menu').offset().left +   $('.cart-menu').scrollLeft();
             var scrollPos = leftOffset / (($('.wrapper').outerWidth())-40);
 
             console.log('widthOfHidden 3: '+$('.wrapper').outerWidth()+','+Math.round(widthOfList())+','+Math.round(getLeftPosi())+','+Math.round(scrollBarWidths)+','+parseInt(scrollPos)+','+Math.round(posLeft)+','+
             leftOffset);
             
             //if(leftOffset > ($('.wrapper').outerWidth())) {
             if(parseInt(scrollPos)>0) 
             {
                setPos = parseInt(scrollPos) * ($('.wrapper').outerWidth());
                $('.list').animate({left: "-"+setPos+"px"}, 0); 
                if(($('.wrapper').outerWidth()) >= (widthOfList()+getLeftPosi())) {
                   $('.scroller-left').fadeIn('slow');
                   $('.scroller-right').fadeOut('slow');
                }
             } else {
                //$('.list').animate({left: "-"+posLeft+"px"}, 500); 
                setPos = parseInt(scrollPos) * ($('.wrapper').outerWidth());
                $('.list').animate({left: "+"+setPos+"px"}, 0); 
                if(($('.wrapper').outerWidth()) <= (widthOfList()+getLeftPosi())) {
                   $('.scroller-left').fadeOut('slow');
                   $('.scroller-right').fadeIn('slow');
                }
             }
          } 
       });
 }).scroll();
 */
 $('.btn-toggle').click(function() {
    
    $(this).find('.btn').toggleClass('active');  
    
    $(this).find('.btn').toggleClass('btn-default');
    var del_type = $('.btn-toggle .active').data("id");
    updateCart(0, del_type, 'del_type');
    //alert($('.btn-toggle .active').data("id"));
       
 });
 
 
 function addonClick(data)
 {
     
     
 if($("#"+data.getAttribute("for")).is(':checked')){
 // alert('removed');
 }
 else
 {
     
     
 // Check browser support
 if (typeof(Storage) !== "undefined") {
     
 if (sessionStorage.getItem("extra") === null) {
   // Store
 //   alert('null');
   var extras = [];
   sessionStorage.setItem("extra", JSON. stringify(extras));
 }
 
 var existingData = JSON.parse(sessionStorage.getItem("extra"));
 // alert('null'  + existingData);
 var name = data.getAttribute("name");
 var id = data.getAttribute("for");
 var price = data.getAttribute("price");
 var extraitem = data.getAttribute("extraitem");
 
 
 if(existingData.some(data => data.extraitem === extraitem)){
     // alert("Object found inside the array.");
 } else{
         if (typeof(Storage) !== "undefined") {
     
 if (sessionStorage.getItem("orderItem") === null) {
   // Store
   alert('null');
   var extras = [];
   sessionStorage.setItem("orderItem", JSON. stringify(extras));
 }
 
 var existingData_ = JSON.parse(sessionStorage.getItem("orderItem"));
 var amount = data.getAttribute("parent_price");
 var item_id = data.getAttribute("parent_id");
 var qty = "1";
 var discount = data.getAttribute("parent_discount");
 var notes = "";
 var status = "0";
 var item_name = data.getAttribute("parent_name");
 var extraitem = data.getAttribute("extraitem");
 
 var extraData_ =  {
             "amount": amount,
             "item_id": item_id,
             "qty": qty,
            "extraitem": extraitem,
             "discount": discount,
             "notes": notes,
             "item_name": item_name,
             "status": status
         };
 
   existingData_.push(extraData_);
   sessionStorage.setItem("orderItem", JSON. stringify(existingData_));
   
 
 } else {
   alert("Sorry, your browser does not support Web Storage...");
 } 
 }
 
 
 var extraData = {
 "name": name,
 "id": id,
 "extraitem": extraitem,
 "price": price
 };
 
   existingData.push(extraData);
   sessionStorage.setItem("extra", JSON. stringify(existingData));
   cartUpdate();
 
 } else {
   alert("Sorry, your browser does not support Web Storage...");
 }    
 }
 }
 
 function addItem(data){
     if (typeof(Storage) !== "undefined") {
     
 if (sessionStorage.getItem("orderItem") === null) {
   // Store
   alert('null');
   var extras = [];
   sessionStorage.setItem("orderItem", JSON. stringify(extras));
 }
 
 var existingData = JSON.parse(sessionStorage.getItem("orderItem"));
 var amount = data.getAttribute("amount");
 var item_id = data.getAttribute("item_id");
 var qty = data.getAttribute("qty");
 var discount = data.getAttribute("discount");
 var notes = data.getAttribute("notes");
 var status = data.getAttribute("status");
 var item_name = data.getAttribute("item_name");
 var extraitem = data.getAttribute("extraitem");
 
 var extraData =  {
             "amount": amount,
             "item_id": item_id,
             "extraitem" : "",
             "qty": qty,
             "discount": discount,
             "notes": notes,
             "item_name": item_name,
             "status": status
         };
 
   existingData.push(extraData);
   sessionStorage.setItem("orderItem", JSON. stringify(existingData));
   cartUpdate();
 
 } else {
   alert("Sorry, your browser does not support Web Storage...");
 } 
 }
 
 
 function minusValue(data)
 {
     var element = document.getElementById(data);
     var addButtonElement = document.getElementById("addButton" + data);
     if(element.value > 1)
     {
         element.stepDown(1);
         addButtonElement.setAttribute("qty", element.value);  
     }
     
 }
 function plusValue(data)
 { 
   var element = parseInt($('#'+data).val());
   element = element+1;
   $('#'+data).val(element);
   $('#addButton'+data).attr("qty",element);
 }
 function PreOrder()
 {
     $('.alert-danger').hide();
     var pre_date= $('#pre_date').val();
     var pre_time= $('#pre_time').val();
     //alert(pre_date+','+pre_time);
     if(pre_date=='' || pre_time=='') {
       $('.alert-danger').show();
       $('.alert-danger').html('Choose Pre order Date & Time!'); 
       setTimeout(
             function() {
               $('.alert-danger').hide();
             }, 3000);
             return false;
     } else {
       $('.alert-danger').css('position','absolute');
       updateCart(0, pre_date+', '+pre_time, 'pre_order');
       pre_order_status = 1;
       $('#closedModal .close').click();
       $('#closedModal').modal('hide'); 
     }
 }
 function closemodal(){
     $('#closedModal').modal('hide');
 }
 function closedMsg()
 {
     $('.alert-danger').hide();
     $('#pre_date11').datepicker({ 
        autoclose: true, 
         todayHighlight: true,
        minDate: '0', 
        dateFormat: 'dd-mm-yyyy'
   }).datepicker('update', new Date());
       $('#closedModal').modal('show');
     //$('#addonModal').modal({backdrop: 'static', keyboard: false });
 }
 //closedMsg();
 sessionStorage.setItem("restatus",0);