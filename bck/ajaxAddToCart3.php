<?php
session_start();


      function d($d){
         echo '<pre>';
         print_r($d);
         echo '</pre>';
      }

$addItems = array();
      
$totPrice = 0;
$jsonItems = $jsonAddOns = array();
$totQty = 0;
if(isset($_SESSION['cartItems']) && count($_SESSION['cartItems'])>0)
{
      $allItems = array_filter($_SESSION['cartItems']); 
      
         foreach($allItems as $k => $val) { 
            
            $itemPrice = $val[0][2] * $val[0][4];
            $totQty = $totQty + $val[0][4];
           // $totPrice = $totPrice + $itemPrice;

            $listAddons = array();
            $addOnPrice = 0;
            foreach($val[1] as $k2 => $val2) {
               $addOn = explode(',',$val2);  
               $addOnPrice += $addOn[3] * $addOn[4];
               $listAddons[] = $addOn[2]; 
               $jsonAddOns[] = array(
                  "add_on_id" => trim($addOn[1]),
                  "item_id" => trim($addOn[0]),
                  "for" => '0',
                  "amount" => trim($addOn[3]),
                  "qty" => (int) (trim($addOn[4])),
               );

            }
            $addonList = implode(' &#9679; ', $listAddons);
            $item_addon_price = $itemPrice + $addOnPrice;
            $totPrice = $totPrice + $item_addon_price;

            //JSON HERE
            $jsonItems[] = array(
               "amount" => trim($val[0][2]),
               "item_id" => trim($val[0][0]),
               "qty" => (int) (trim($val[0][4])),
               "discount" => '0',
               "extraFor" => '0',
               "notes" => trim($val[0][5]),
               "status" => '0'
            );
         }  
         $ser_fee = 0.50;
         $carry_bag = 0.05;
         $del_fee = 0;
         $disc_fee = 0;
         $subTot = $totPrice;
          if(isset($_SESSION['coupon']) && $_SESSION['coupon']!='') {
             $disc_fee = $totPrice * ($_SESSION['coupon']/100);
             }
         else{
             $disc_fee = $totPrice * ($_SESSION['discountper']/100);
         
         }
        $subTot1 = $totPrice-$disc_fee;
        
         $subTot=$subTot1;
         if($_SESSION['del_type']=='collection') { $del_fee = 0; } else if(isset($_SESSION['del_charge'])) { $del_fee = $_SESSION['del_charge']; }
        //  if(isset($_SESSION['coupon']) && $_SESSION['coupon']!='') { $disc_fee = $subTot * ($_SESSION['coupon']/100); }
         $grandTot = $subTot + $ser_fee + $del_fee + $carry_bag;
         $grandTot_WithoutDiscount = $subTot + $ser_fee + $del_fee + $carry_bag +$disc_fee;

  // d($jsonItems);
  $jsonAll = array('items'=>$jsonItems, 'extra_items'=>$jsonAddOns, 'ser_fee'=>number_format($ser_fee, 2, '.', ''), 'del_fee'=>number_format($del_fee, 2, '.', ''), 'disc_fee'=>number_format($disc_fee, 2, '.', ''), 'subTot'=>$subTot, 'grandTot'=>number_format($grandTot, 2, '.', ''), 'grandTot2'=>number_format($grandTot_WithoutDiscount, 2, '.', ''), 'totQty'=>$totQty);
} else {
   $jsonAll = array('items'=> array(), 'extra_items'=> array(), 'ser_fee'=>0, 'del_fee'=>0, 'disc_fee'=>0, 'subTot'=>0, 'grandTot'=>0, 'grandTot2'=>0, 'totQty'=>0);
}

header('Content-type: application/json');
echo json_encode($jsonAll);