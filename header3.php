<nav class="navbar navbar-expand-sm navbar-light osahan-nav shadow-sm fixed-top bg-white">
         <div class="container">
            <button class="navbar-toggler ml-2 " type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown" style="margin-left:-2px;">
            
            <div class="steven-and-leah desktoponly">
               <div class="team-bx pad_lr10 desktoponly"><h3 style="display: flex; min-height: 50px; margin-left:-70px !important; align-items: center;" id="headerText"> NATURAL SPICE </h3></div>
            </div>

            <input class="col-4 search form-control float-right ml-3 ml-auto desktoponly" type="search" id="searchinput"  placeholder="Search by Item Name" id="example-search-input">
               <ul class="navbar-nav ml-auto">
               <li class="nav-item active">
                     <a class="nav-link" href="index.php">Home </a>
                  </li>
                  <li class="nav-item active">
                     <a class="nav-link" href="menu.php">Menu </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="deals.php"><i class="icofont-sale-discount"></i> Deals </a>
                  </li>
                 
                  <li class="nav-item">
                     <a class="nav-link" href="contact.php"> Contact Us</a>
                  </li>
                  <?php if(!isset($_SESSION['uid'])){?>
                  <li class="nav-item">
                     <a class="nav-link" href="login.php"> Login </a>
                  </li>
                  <?php } ?>
                   <?php if(isset($_SESSION['uid'])){?>
                  <li class="nav-item">
                     <a class="nav-link" href="myaccount.php"> My Account </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="myaccount.php"> Orders </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="logout.php"> Logout </a>
                  </li>
                  <?php } ?>
                                                    </ul>
            </div>
            
            <input type="search" name="" class="form-control mobileonly col-7 search-mob" placeholder="Search">

            <button class="navbar-toggler mr-2 mobCart dropdown-toggle mobileonly redbg" type="button" id="mobCartBody" data-bs-toggle="dropdown" aria-expanded="false">
                  <i class="cart icon"></i> <span class="badge-notification">0</span>
            </button>
           <span class="dropdown-menu" aria-labelledby="mobCartBody">
               <p onclick="allergy()" style="text-align: center;
    font-size: 13px;    margin: 5px;"><i class="bi bi-info-circle-fill" style="color:red;font-size: 17px;"></i>    <a href="#" style="text-decoration: underline;"> Info on Food Allergy and FHIS Rating</a></p>
               <div class="btn-group btn-toggle" style="width:95%;">
               <?php if($_SESSION['del_type_status']==0 || $_SESSION['del_type_status']==2){?> <button class="btn btn-lg btn-default <?php if(!isset($_SESSION['del_type']) || (isset($_SESSION['del_type']) && $_SESSION['del_type']=='delivery')) { ?> active<?php } ?>" id="delivery" data-id="delivery" >Delivery <small id="mdel_sm">In 40 min</small></button><?php }?>
                    <?php if($_SESSION['del_type_status']==0 || $_SESSION['del_type_status']==1){?>  <button class="btn btn-lg btn-default <?php if(isset($_SESSION['del_type']) && $_SESSION['del_type']=='collection') { ?>active<?php } ?>" id="collection" data-id="collection" >Collection <small id="mcol_sm">In 15 min</small></button><?php } ?>
                  </div>
                  
                  <div class="footer2 p-2">
                     <div class="mobCartBody" id="cartItems_section" style="max-height: 75vh; overflow-x: hidden;"></div>
                  </div>
            </span>
         </div>
      </nav>