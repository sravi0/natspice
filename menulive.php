<?php session_start();
//$_SESSION['cartItems'];
$linkID = '';
if(!isset($_SESSION['del_type'])) { $_SESSION['del_type']='delivery'; }
?>

<?php
function getAPI() {
  $graph_url= 'https://deliveryguru.co.uk/dg_api/menu_submenu/47';
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $graph_url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  $output = curl_exec($ch); 
  curl_close($ch);
  return json_decode($output, true); 
}
$getItems = getAPI();


function getHotelAPI() {
  $graph_url= 'https://deliveryguru.co.uk/dg_api/getRestaurantsDetails/47';
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $graph_url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  $output = curl_exec($ch); 
  curl_close($ch);
  return json_decode($output, true); 
}
$getHotelDetails = getHotelAPI();
$_SESSION['discountper']=$getHotelDetails[0]['discount'];
function getmetaHotelAPI() {
  $graph_url= 'https://deliveryguru.co.uk/dg_api/getmetadata/47/Menu';
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $graph_url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  $output = curl_exec($ch); 
  curl_close($ch);
  return json_decode($output, true); 
}
$getMetaHotelDetails = getmetaHotelAPI();
$title=$getMetaHotelDetails['result'][0]['meta_title'];
$keyword=$getMetaHotelDetails['result'][0]['meta_keyword'];
$desc=$getMetaHotelDetails['result'][0]['meta_description'];
?>  
<!DOCTYPE html>
<html>
  <head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
           <title><?php echo $title;?></title>
        <meta name="description" content="<?php echo $desc;?>"/>
<meta name="keywords" content="<?php echo $keyword;?>"/>

    <meta property="og:title" content="Food Delivery Services|Online Food Delivery|Grill Guru"/>
	<meta property="og:type" content="website"/>
	<meta property="og:url" content="https://www.grill-guru.co.uk/menu.php">
	<meta property="og:image" content="https://www.grill-guru.co.uk/img/grill-guru-full-landingLogo.png"/>
	<meta property="og:site_name" content="grill-guru Menu"/>
	<meta property="og:description" content="Order a food online in Grill Guru and Get it delivered to your door step .Here is a best food delivery services Get a offer ,discount for the weekend and occasions"/>
       <link rel="canonical" href="https://www.grill-guru.co.uk/menu.php" />
         <link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
     <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
         <!--<link href="vendor/select2/css/select2.min.css" rel="stylesheet">-->
         <!-- Custom styles for this template-->
         <link href="css/osahan.css" rel="stylesheet">
         <link href="vendor/icofont/icofont.min.css" rel="stylesheet">
         <script src="js/jquery-3.5.1.js"></script>
      
         <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap" rel="stylesheet">
      <link href="https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b598ebab05857545f242caf2b4b48c/dist/semantic.min.css" rel="stylesheet" type="text/css" />
<!--<script src="https://code.jquery.com/jquery-2.1.4.js"></script>-->
<script src="https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b598ebab05857545f242caf2b4b48c/dist/semantic.min.js"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="menu-src/menu.css" rel="stylesheet">
    <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '388919832591600');
  fbq('track', 'PageView');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-181746269-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-181746269-1');
</script>

<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=388919832591600&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<style>
.custom-control-label::after {
    position: absolute;
    top: .25rem;
    left: -1.54rem;
    display: block;
    width: 1rem;
    height: 1rem;
    content: "";
    background-repeat: no-repeat;
    background-position: center center;
    background-size: 50% 50%;
}
@media screen 
    and (min-device-width: 200px) 
    and (max-device-width: 500px) 
    and (-webkit-min-device-pixel-ratio: 1) { 
      .mobileonly { display:block !important; }
      .desktoponly { display:none !important; }
      .navbar-nav { background: #ccc9c9; }
      .osahan-nav .nav-link { padding: 15px 0px !important;     margin: 0; padding-left: 10px !important; text-align: left; }
      #navbarNavDropdown {     margin-top: -10px; }
      .search-mob { background: #f8f9fa;  }
      .navbar-collapse {
          flex-basis: 100%;
          flex-grow: 1;
          align-items: center;
          position: absolute;
          top: 50px;
          width: 100%;
          margin-top: 0px !important;
      }
      .bg-light {
          background-color: #f8f9fa !important;
      }
}
@media screen 
    and (min-device-width: 500px) 
    and (max-device-width: 3500px) 
    and (-webkit-min-device-pixel-ratio: 1) { 
      .mobileonly { display:none; }
      .desktoponly { display:block; }
      .navbar-brand, .float { display: none; }
      
      .bg-light {
          background-color: #000 !important;
      }
      
}
@media screen 
    and (min-device-width: 200px) 
    and (max-device-width: 575px) 
    and (-webkit-min-device-pixel-ratio: 1) { 
      .navbar-collapse {
          flex-basis: 100%;
          flex-grow: 1;
          align-items: center;
          position: absolute;
          top: 50px;
          width: 100%;
          margin-top: 0px !important;
      }
      .bg-light {
          background-color: #ffffff !important;
      }
      .mobilediv{
          overflow: auto;
    height: 500px;
      }
}
.footerdiv{
          position: fixed;
    height: 50px;
    bottom: 0px;
    left: 0px;
    right: 0px;
    margin-bottom: 0px;
      }
.dropdown-menu { width: 100%; }
 .unselectable {
        -webkit-user-select: none;
        -webkit-touch-callout: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        /*color: #cc0000;*/
}
@keyframes placeHolderShimmer{
    0%{
        background-position: -468px 0
    }
    100%{
        background-position: 468px 0
    }
}
.linear-background {
    animation-duration: 1s;
    animation-fill-mode: forwards;
    animation-iteration-count: infinite;
    animation-name: placeHolderShimmer;
    animation-timing-function: linear;
    background: #f6f7f8;
    background: linear-gradient(to right, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
    background-size: 1000px 104px;
    height: 30px;
    margin:15px;
    position: relative;
    overflow: hidden;
}

.menusWrapper figure{
        margin:0px;
        padding:0px;
    }
    .menusWrapper{
       display:grid;
       grid-template-columns: 33% 33% 33%;
       
    }
    .menuInner {
        display: inline-block;
        border: 1px solid #d2cbcb;
        break-inside: avoid;
        page-break-inside: avoid;
        margin: .5rem;
        background: white;
        box-shadow: 0 4px 11px -6px #b3aeae;
    }
    .menuInner p{
        -webkit-line-clamp: 2;
        overflow: hidden;
        -webkit-box-orient: vertical;
        font-size:1rem;
        line-height:1.5rem;
        display: -webkit-box;
    }
    .menuInner h4{
        text-overflow:ellipsis;
        overflow: hidden;
        white-space: nowrap;
        
    }
    .menuInner.withImage{
        grid-row: span 2;
    }
    @media all and (max-width:767px){
        .menusWrapper{
       grid-template-columns: 100%;
    }
    }
    .menusWrapper img{
        max-width:100%;
    }
    
    
    .modal-body { padding: 0px; }
    .itemDataModal h5, .itemDataModal button, .itemDataModal div, .popup-desc, .Addons, #addonModalLabel { padding:10px; }
    #addonModalLabel { padding: 0 10px; }
    .sidebar { top: 65px !important; }
    .modal-footer>* { margin: 0; }
    .txtDiv {
    padding: 1rem;
}
.btn-group-lg>.btn, .btn-lg {padding: .5rem; }
.steven-and-leah {
  display: flex; min-height: 50px; align-items: center;
   
}
.steven-and-leah > * {
    display: inline-block;
   
}
.border-left {     border-left: 1px solid #ccc; font-size: 12px;
    line-height: 20px; }
.pad_lr10 { padding: 0 10px; }

.stickyLogo {
  position: fixed;
  top: 0;
  width: 100%;
}

.stickyLogo + .unselectable {
  padding-top: 102px;
}
img.stickyLogo {
    height: 49px;
    width: 53px !important;
    z-index: 100;
}
.restaurant-detailed-header-left img { z-index: 9999; }
.sidebar {   margin: 10px; }
    main { max-width: 1400px; }
    .component, #cartItems_section { border: none; }

.sticky-div {
    background-color: #fff;
    position: sticky;
    top: 49px;
    padding: 0px 0px 3px 0px;
    z-index: 1;
    border: 1px solid #eae8e8;
    -webkit-box-shadow: 0 8px 6px -6px #dad9d9;
    -moz-box-shadow: 0 8px 6px -6px black;
    box-shadow: 0 4px 11px -6px #c4c4c4;
    border-radius: 5px;
}
#searchinput {
    border: none;
    border: 1px solid #e4e3e3;
}
 #cartItems_section {
    border: none;
    max-height: 60vh;
    overflow-x: hidden;
}
.navbar-light .navbar-toggler {
    color: #fff;
    border-color: rgba(0,0,0,.1);
}
</style>
<script>
<?php
$ids = array_column($getItems, 'id');
//print_r($ids);

?>
//const catIDs = [174, 147, 1997, 182];
const catIDs = [<?php echo implode(',', $ids); ?>];
//const catIDs = [];
let fLen = catIDs.length;

var allItems = [];

window.onload = function(){
  
  var f = (function(){
      var xhr = [], i;
      for (let i = 0; i < fLen; i++) {
          (function(i){
              var j = catIDs[i];
            
              const xhr = new XMLHttpRequest();
              xhr.open('GET', 'api-test.php?catId='+j);
              xhr.responseType = 'json';
              xhr.onload = function(e) {
                //console.log(this.status);
                if (this.status == 200) {
                  //console.log('response', this.response); // JSON response  
                  allItems[j] = this.response;
                }
              };
              xhr.send();
          })(i);
      }
  })();

};
// PHP [closure.php]
//echo "Hello Kitty -> " . $_GET["data"];

</script>
</head>


  <body class="unselectable">
  <div id="snackbar"></div>

<!-- Menu addon Modal -->
<div class="modal fade" id="addonModal" tabindex="-1" aria-labelledby="addonModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addonModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="font-size: 36px;font-weight: 400;">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="itemDataModal" id="itemDataModal"></div>
        <div class="linear-background"></div>
          <div class="linear-background"></div>
          <div class="linear-background"></div>
        <div class="Addons accordionMenu" id="AddOns">
        </div>
      </div>
      <div class="alert alert-danger" role="alert"></div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<!-- Closed hotel Modal -->
<div class="modal fade" id="closedModal" tabindex="-1" role="dialog" aria-labelledby="closedModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
      <div class="modal-header">
              <h5 class="modal-title" style="text-align:center;">CLOSED NOW!</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closemodal()">
                <span aria-hidden="true">&times;</span>
              </button>
      </div>
      <div class="row no-gutters">
        <div class="col-md-6 ">
          <div class="modal-body1 p-0 text-center d-flex1">
              <img src="img/closepopimg.jpg" style="width:100%;">
          </div>
        </div>
        <div class="col-md-6 ">
          <div class="modal-body1 p-2 d-flex align-items-center">
            <div class="text w-100 text-center">
                
              <h2 class="mb-0">Pre Order</h2>  <br>
              <form action="#" class="code-form">
                <div class="form-group p2">
             
                <span class="col-4 p-1">
                Date:<br>
                                    <div class="ui calendar" id="preorderdateformat1" style="">
                            
    <div class="ui input left icon" style="width: 100%;">
      <i class="calendar icon"></i>
      <input type="text" id="pre_date" class="form-control"   placeholder="Schedule Date and Time" style="width: 100% !important; background:white; " readonly/>
    </div>
  </div>
               
                Time:<br> <select id="pre_time" class="form-control">     
                <option value="">Please Select Time</option>
                <option value="17:00">17:00</option>
                <option value="18:00">18:00</option>
                <option value="19:00">19:00</option>
                <option value="20:00">20:00</option>
                <option value="21:00">21:00</option>
                <option value="22:00">22:00</option>
                <option value="23:00">23:00</option>
                <option value="24:00">24:00</option>
                <option value="01:00">01:00</option>
                <option value="02:00">02:00</option>
                <option value="03:00">03:00</option>
                <option value="04:00">04:00</option>
                <option value="05:00">05:00</option></select>
                </div>
                <div class="alert alert-danger" role="alert"></div>
              </form>
              
              <a class="btn btn-primary d-block py-3" onClick="PreOrder();" style="color:#fff;">Pre Order</a>
            </div>
          </div>
         </div>
        </div>
      </div>
    </div>
</div>
<?php include "header3.php"; ?>
<section class="restaurant-detailed-banner mobileonly" style="margin-top: -80px;">
         <div class="text-center">
            <img class="img-fluid cover" src="img/Banner.jpg">
         </div>
         <div class="restaurant-detailed-header">
            <div class="container" style="max-width:1600px;">
               <div class="row d-flex align-items-end">
                  <div class="col-md-8">
                     <div class="restaurant-detailed-header-left">
                        <img class="img-fluid mr-3 float-left" alt="osahan" src="https://grill-guru.co.uk/img/grill-guru-full-landingLogo.png">
                        
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="restaurant-detailed-header-right text-right">
                     
                        
                      
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>  
      
    <main>
    <!--<span class="float">
      <i class="cart icon"></i>
      <span class="badge rounded-pill badge-notification bg-danger">0</span>
    </span>  -->
   
        <article class="article">
          <div class="banner-new desktoponly" style="margin-top: 55px; padding: 10px;">
            <img src="img/Banner.jpg" style="width:100%; border-radius:15px;"  />
              <div class="row d-flex" style="position: relative;">
                      <div class="col-md-12" style="margin-top: -140px; ">
                        <div class="">
                            <img class="img-fluid mr-3 pl-2" id="bannerLogo" alt="osahan" src="https://grill-guru.co.uk/img/grill-guru-full-landingLogo.png">
                        </div>
                      </div>
                  </div>
          </div>
         <div class="sticky-div"> 
                  <div class="scroller scroller-left"><i class="arrowPrev"></i></div>
               <div class="scroller scroller-right"><i class="arrowNext"></i></div>
               <div class="wrapper">
               <ul class="nav nav-tabs list cart-menu" id="myTab">
                <?php
                foreach($getItems as $k => $val) {
                    echo '<li><a data-id="'.$val['id'].'" id="anchorTag'.$val['id'].'" class="nav-item">'.$val['name'].'</a></li>';
                }
                ?>
                </ul>
               </div>
               </div>
      
      <div id="panel_groups" class="">
        
      <div class="discount-banner">
          <!--<img src="img/banner.jpeg" style="width:100%" />-->
         
          </div>
           <div id="noitem-section">
                                
                                </div>

                           <?php
                           foreach($getItems as $k => $val) {
                              //echo '<li><a href="#'.$val['id'].'" id="anchorTag'.$val['id'].'" class="nav-item">'.$val['name'].'</a></li>';
                              //print_r($val); exit;
                           ?>
                           <div class="item-section" id="<?php echo $val['id']; ?>">
                                <h3 class="mb-4 mt-3 col-md-12" style="text-transform: capitalize;"><?php echo $val['name']; ?> <small class="h6 text-black-50"></small></h3>
                                 <div class="menusWrapper">
                                 <?php foreach($val['value'] as $k1 => $v1) { ?>
                                    <div class="menuInner "  data-role="recipe">
                                    <div class="card" style="cursor:pointer;" onclick="getHotelStatus(<?php echo $k; ?>, <?php echo $k1; ?>, <?php echo $v1['id']; ?>, <?php echo $val['id']; ?>);">
                                    <?php 
                                          if($v1['image'] == 'dummy.jpg') { 
                                              $itemImg ="menu-src/noimage.jpg"; 
                                              $divClass = 'w-100';
                                          } else { 
                                              $itemImg ="https://deliveryguru.co.uk/admin/images/itemimages/" .  $v1['image']; 
                                              $divClass = 'w-50'; $imgClass = 'w-50';
                                          }
                                          if(isset($_GET['img'])) {
                                            $itemImg ="menu-src/noimage.jpg"; 
                                          } 
                                         // $divClass =  $imgClass = 'w-50';
                                          ?>
                                        <div class="card-body d-flex p-0">
                                            <div class="<?php echo $divClass; ?> txtDiv">
                                            <h6 class="mb-1 item-tit"><a class="text-black" style="text-transform: capitalize;"><?php echo $v1['item_name']; ?></a></h6>
                                            <p class="text-gray mb-2 item-desc" style="text-transform: capitalize;"><?php echo $v1['item_desc']; ?></p>
                                            <p class="text-gray time">
                                                 <?php 
                                                 if($v1['price']!=0){
                                                 ?>
                                                <span style="font-weight: 700; font-size: 16px;" class="text-dark rounded-sm pl-0 pb-1 pt-1 pr-2">£<?php echo $v1['price']; ?></span> 
                                            <?php } else {?>
                                            <span style="font-weight: 900;" class="text-dark rounded-sm pl-0 pb-1 pt-1 pr-2"></span> 
                                            <?php }?>
                                             </p>
                                              <?php 
                                          if($v1['discount'] != '0' || $v1['discount'] != 0 ) {  ?>
                                              <div class="list-card-badge">
                                                <span class="badge badge-info">OFFER</span> 
                                                <small><?php echo $v1['discount']; ?> off </small> 
                                             </div> 
                                             <?php  }
                                          ?>
                                             
                                            </div>
                                            <?php if($v1['image'] != 'dummy.jpg') { ?>
                                            <div class="<?php echo $imgClass; ?> text-right">
                                            <img class="" src="<?php echo $itemImg; ?>" alt="sans" width="150px" height="120">
                                                
                                            </div>
                                            
                                            <?php } ?>
                                        </div>
                                    </div>
                                       <!---->
                                 </div>
                              <?php } ?>
                              </div>
                              </div>
                           <?php } ?>
                           
                        </div>

      </article>
     
      <aside class="sidebar desktoponly">
        
        <div class="cartItems shadow component">
          <div class="card-header">
                <h3 class="text-center">Your Order</h3>
          </div>
              <div class="btn-group btn-toggle">
                <button class="btn btn-lg btn-default <?php if(!isset($_SESSION['del_type']) || (isset($_SESSION['del_type']) && $_SESSION['del_type']=='delivery')) { ?> active<?php } ?>" id="delivery" data-id="delivery" >Delivery <small id="del_sm">In 40 min</small></button>
                <button class="btn btn-lg btn-default <?php if(isset($_SESSION['del_type']) && $_SESSION['del_type']=='collection') { ?>active<?php } ?>" id="collection" data-id="collection" >Collection <small  id="col_sm">In 15 min</small></button>
              </div>

           <div class="footer2 row p-2">
              <div id="cartItems_section"></div>
          </div>
        </div>
      </aside>


    </main>
   <footer>
          
          <div class="container">
              <div class="footerWrap col-md-12 col-md-offset-1">
                  <div class="copyright col-md-12">
                      <p style="color: #fff;font-size: 14px;">Copyright © 2021 <a href="https://deliveryguru.co.uk/" target="_blank" style="color:#e81818;">DeliveryGuru</a> All rights reserved.</p>
                  </div>
              </div>
          </div>
      </footer>
                          
  </body>
 
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  
<script>
//$(document).on('click', '.component .btn-toggle, .dropdown-menu .btn-toggle', function(){


window.onscroll = function() { fixedLogo() };

var header = document.getElementById("bannerLogo");
//var headerText = document.getElementById("headerText");
var sticky = header.offsetTop;

function fixedLogo() {
  //alert(window.pageYOffset+', '+sticky)
  if (window.pageYOffset > 100) {
    header.classList.add("stickyLogo");
    document.getElementById("headerText").style.marginLeft = '20px';
  } else {
    header.classList.remove("stickyLogo");
    document.getElementById("headerText").style.marginLeft = '-30px';
  }
}
</script>

<script>
/*function loadJson(catId){
  alert(catId);
  const xhr = new XMLHttpRequest();
    xhr.open('GET', 'api-test.php?id='+catId);
    xhr.responseType = 'json';
    xhr.onload = function(e) {
      if (this.status == 200) {
        alert('loaded');
        //console.log('response', this.response); // JSON response  
        //document.getElementById('showData').innerHTML = this.response;
      }
    };
    xhr.send();
}
loadJson(174);*/

$(document).ready(function() {
 
  var openhours='17:00';
  var closehour='05:00';
  var midtime="00:00";
  var event = new Date().toLocaleTimeString('en-GB', { timeZone: 'Europe/London' ,hour: 'numeric',minute: 'numeric', hour12: false});
  if(event<openhours && event>closehour){
    $('#del_sm').html('Open at 17:00');
    $('#col_sm').html('Open at 17:00');
  }

 $(document).on('click', 'details', function(){
    var id = $(this).attr('class');
   $(this).siblings('details').removeAttr('open');
   $(this).attr('open','');
   //alert(id);
   
 });
 
 $(document).on('click', '.summary', function(){
     $('.addonDiv').hide();
     $('input[name="subMenuList"]:checked').attr('checked', false);
     var id = $(this).data('id');
     $('.summary #'+id+'').attr('checked', true); 
     $('.addOn_'+id+'').show();
   });
});
var hidWidth;
var scrollBarWidths = 40;

var widthOfList = function(){
 var itemsWidth = 0;
 $('.list li').each(function(){
   var itemWidth = $(this).outerWidth();
   itemsWidth+=itemWidth;
 });
 return itemsWidth;
};

var widthOfHidden = function(){
 var t = (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
 //console.log('widthOfHidden: '+(($('.wrapper').outerWidth())+','+widthOfList()+','+getLeftPosi())+','+scrollBarWidths+','+t);
 return (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
};

var getLeftPosi = function(){
 return $('.list').position().left;
};

var reAdjust = function(){
 if (($('.wrapper').outerWidth()) < widthOfList()) {
   $('.scroller-right').show();
 }
 else {
   $('.scroller-right').show();
 }
 
 if (getLeftPosi()<0) {
   $('.scroller-left').show();
 }
 else {
   $('.scroller-left').hide();
 }
 if(getLeftPosi()==0){
  $('.scroller-left').fadeOut('slow');
 }
}

reAdjust();

$(window).on('resize',function(e){  
   reAdjust();
});

$('.scroller-right').click(function() {
    $('.scroller-left, .scroller-right').hide();
    $('.scroller-left').fadeIn('slow');
    //var t = (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
    var t1 = -1 * (getLeftPosi()-($('.wrapper').outerWidth())) + ($('.wrapper').outerWidth());
    if(t1>Math.round(widthOfList())) {
      $('.list').animate({left: "-"+((Math.round(widthOfList())+20)-$('.wrapper').outerWidth())+"px"}, 'slow'); 
      $('.scroller-right').fadeOut('slow');
    } else {
      $('.list').animate({left:"-="+($('.wrapper').outerWidth()   )+"px"},'slow',function() {
            if(($('.wrapper').outerWidth()) >= (widthOfList()+getLeftPosi())){
                $('.scroller-right').fadeOut('slow');
            } else {
              $('.scroller-right').fadeIn('slow');
            }
      });
    }
});

$('.scroller-left').click(function() {
  $('.scroller-left, .scroller-right').hide();
  $('.scroller-right').fadeIn('slow');
  var t1 =  getLeftPosi() + $('.wrapper').outerWidth() - 40;
  //alert(t1);
  if(t1==0 || t1>0) {
    $('.list').animate({left: "+0px"}, 'slow'); 
    $('.scroller-left').fadeOut('slow');
  } else {
    $('.list').animate({left:"+="+($('.wrapper').outerWidth())+"px"},'slow',function(){
      var t = (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
      //alert(getLeftPosi());
      if(getLeftPosi()==0){
          $('.scroller-left').fadeOut('slow');
      } else {
          $('.scroller-left').fadeIn('slow');
      }
    });
  }
});   

$(document).on('click', "input:radio", function(){
    var parent = $(this).closest(".boxes").nextAll(".minY").eq(0).attr('id');
    if(typeof(parent) !== "undefined") {
      window.location.hash = "";
      window.location.hash = '#'+parent;
    }
});

$('.cart-menu a').on('click', function() {
   var scrollAnchor = $(this).attr('data-id');
   scrollPoint = $('#panel_groups #' + scrollAnchor).offset().top - 70 - 50;
   $('body,html').animate({
       scrollTop: scrollPoint
   }, 500);
   return false;
})

$(window).scroll(function() {
   var windscroll = $(window).scrollTop();
   if (windscroll >= 100) {
       $('#panel_groups .item-section').each(function(i) {
           if ($(this).position().top <= windscroll + 150) {
               $('.cart-menu a.active').removeClass('active');
               $('.cart-menu a').eq(i).addClass('active');
           }
       });

   } else {
       $('.cart-menu a.active').removeClass('active');
       $('.cart-menu a:first').addClass('active');
   }

   var leftOffset = $('.cart-menu a.active').offset().left - $('.cart-menu').offset().left +   $('.cart-menu').scrollLeft();
   var scrollPos = leftOffset / (($('.wrapper').outerWidth())-40);
   var calWid = Math.round(widthOfList()) - $('.wrapper').outerWidth();

  //console.log(''+$('.wrapper').outerWidth()+'_'+Math.round(widthOfList())+'_'+Math.round(getLeftPosi())+'_'+Math.round(scrollBarWidths)+'_'+parseInt(scrollPos)+'_'+leftOffset+'_'+parseInt(calWid));

  
    if(parseInt(scrollPos)>0) 
    {
      if((parseInt(leftOffset)+parseInt(calWid)+40)>Math.round(widthOfList())) 
      {
        $('.list').animate({left: "-"+((Math.round(widthOfList())+20)-$('.wrapper').outerWidth())+"px"}, 0); 
        $('.scroller-left').fadeIn('slow');
        $('.scroller-right').fadeOut('slow');
      } else {
        setPos = (parseInt(scrollPos) * ($('.wrapper').outerWidth()));
        $('.list').animate({left: "-"+(setPos)+"px"}, 0); 
        if(($('.wrapper').outerWidth()) >= (widthOfList()+getLeftPosi())) {
            $('.scroller-left').fadeIn('slow');
            $('.scroller-right').fadeOut('slow');
        }
      }
    } else {
        setPos = parseInt(scrollPos) * ($('.wrapper').outerWidth());
        $('.list').animate({left: "+"+setPos+"px"}, 0); 
        if(($('.wrapper').outerWidth()) <= (widthOfList()+getLeftPosi())) {
            $('.scroller-left').fadeOut('slow');
            $('.scroller-right').fadeIn('slow');
        }
    }
  


}).scroll();


var getItems = <?php echo json_encode($getItems); ?>;
var pre_order_status = 0;
var date = new Date();
date.setDate(date.getDate());
</script>
<script src="menu.js"></script>
<script src="https://cdn.jsdelivr.net/npm/pikaday/pikaday.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/pikaday/css/pikaday.css">

<script>
// You can get and set dates with moment objects
var today = new Date();

const picker = new Pikaday({
    field: document.getElementById('pre_date'),
    firstDay: 1,
    minDate: new Date(),
    maxDate: new Date(today.getFullYear(), today.getMonth(), today.getDate() + 5),
    yearRange: [2021,2021],
    onSelect: date => {
      const year = date.getFullYear()  ,month = date.getMonth() + 1 ,day = date.getDate(),formattedDate = [ day < 10 ? '0' + day : day, month < 10 ? '0' + month : month, year].join('/');
      document.getElementById('pre_date').value = formattedDate
  }
});

function closedMsg()
 {
  $('.alert-danger').hide();
       $('#closedModal').modal('show');
     //$('#addonModal').modal({backdrop: 'static', keyboard: false });
 }
function closePopup() { 
       $('#addonModal').modal('hide'); 
}
var itemMinMax = {};
 function btnAddToCart() {
        $('.alert-danger').hide();
        var errorMsg='';
         $(".addbtn").prop('disabled', true);
        var MainSel = $('input[name="subMenuList"]:checked');

        if($('input[name="subMenuList"]:checked').length == 0) {
          errorMsg = 'Please select at least one item!';
           $(".addbtn").prop('disabled', false);
        } else {
           
           var temp = selItem = new Array();

           selItem = [MainSel.attr("data-subid"), MainSel.attr("data-name"), MainSel.attr("addon_price"),  MainSel.attr("data-image")];
           
           selId = selItem[0];
           //alert(itemMinMax[selId]);
           if(itemMinMax[selId]){
             temp = itemMinMax[selId].split("},{");
             var addOnItems = [];
             for(i=0;i<temp.length;i++){
                 var adOnQty   = 0;
                 var splitTxt = new Array();
                 splitTxt = temp[i].replace('{','');
                 splitTxt = splitTxt.replace('}','');
                 splitTxt =  splitTxt.split(',');
                 var chkId = $('input[name="addOnCheck'+selId+'_'+splitTxt[0]+'[]"]:checked').length;
                 if(splitTxt[1]==1 && splitTxt[2]==1) {
                     adOnQty = $('input[name="addOnCheck'+selId+'_'+splitTxt[0]+'[]"]:checked').length;
                 } else {
                   $('input[name="addOnSel'+selId+'_'+splitTxt[0]+'[]"]').each( function() { 
                     if($(this).is(":visible")) {
                       adOnQty += parseInt($(this).val());
                     }
                   });
                 }
                 //alert(adOnQty+','+splitTxt[1]+','+splitTxt[2]);
                 if((adOnQty < splitTxt[1] || adOnQty > splitTxt[2]) && errorMsg==''){
                   errorMsg = 'Please select Min '+splitTxt[1]+' and Max '+splitTxt[2]+' Add on from '+splitTxt[3];
                   window.location.hash = "";
                   window.location.hash = '#box_'+selId+'_'+splitTxt[0];
                   $('#box_'+selId+'_'+splitTxt[0]+' .list-card-body .mb-1 a').attr('style', 'color: red !important');
                   $('#box_'+selId+'_'+splitTxt[0]+' .list-card-body .mb-1 a').fadeIn(100).fadeOut(300).fadeIn(300).fadeOut(300).fadeIn(300);
                 }
                 $('input[name="addOnCheck'+selId+'_'+splitTxt[0]+'[]"]:checked').map(function(){
                     if(splitTxt[1]==1 && splitTxt[2]==1) { 
                         var addOnQty = 1; 
                     } else {
                         var addOnQty = $("~ span.addOnSel input", this).val();
                     }
                     addOnItems.push($(this).val()+','+addOnQty);
                 });
                 //console.log('Values: '+selId+','+splitTxt[1]+','+splitTxt[2]+', '+chkId);
             }
           }
        }
 
        if(errorMsg!=''){
            $('.alert-danger').html(errorMsg);
            $('.alert-danger').show();
             $(".addbtn").prop('disabled', false);
            setTimeout(
                function() {
                  $('.alert-danger').hide();
                  $('.list-card-body .mb-1 a').attr('style', 'color: #000000 !important');
                }, 5000);
            return false;
        } else {
             $(".addbtn").prop('disabled', true);
            var totItem = parseInt($('input[name="totItem"]').val());
            selItem.push(totItem);
             var instrction = $('#addInst').val();
            if(instrction.trim().length>0){
            selItem.push(instrction);
            }
            //selItem.push($('#addInst').val());
            var ItemsStr = JSON.stringify(selItem);
            var addonItemsStr = JSON.stringify(addOnItems);
            AajxUpdateCart(ItemsStr, addonItemsStr, 'add');
        }
 }
function showAddOnsAjax(mId, arId, iId, cId)
{
  var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
  if (isMobile) {  modalHeight = 43; } else { modalHeight = 47; }
      $('.linear-background').show();
      var itemData = getItems[mId]['value'][arId];
      $('.alert-danger').hide();      
      var modelBody = modelFooter = '';
      $('#AddOns').html(modelBody);
      $('#addonModal').modal('show');
      $('#addonModalLabel').html(itemData.item_name);
      if(itemData.image=='dummy.jpg'){ itemImg ='menu-src/noimage.jpg'; } else { itemImg ='https://deliveryguru.co.uk/admin/images/itemimages/'+itemData.image; }
      var modelBody = modelFooter = modelItem = '';
      
      if(itemData.image!='dummy.jpg'){
        modelItem += '<img src="'+itemImg+'" class="img-fluid item-img" style="width: 100%;height: 300px; object-fit: cover;" />';
        modalHeight = 20 +modalHeight;  
      }
      modelItem += '<p class="text-gray popup-desc">'+itemData.item_desc+'</p>';
      $('.itemDataModal').html(modelItem);
      $.ajax({
               type: "GET",
               url: "https://deliveryguru.co.uk/dg_api/basedOnCatId_addon_list/"+iId+"/47",
               dataType: "json",
               crossDomain: true,
               cache: "false",
               success: function(response) {
                      
                        var itemChild = response;
                        if(itemChild.length==1 && itemChild[0]['child']!=true) { //alert(itemData.id);
                            $('.modal-body').css('height', modalHeight+'vh');
                              modelBody += '<input type="radio" style="display:none" checked onchange="addcatpop(this)" name="subMenuList" data-id="'+itemData.categories_id+'" data-name="'+(itemData.item_name).replace(/\"/g,"inch")+'" data-amount="'+itemData.item_name+'" data-subid="'+itemData.id+'"  item_name="" item_id="" addon_price="'+itemData.price+'" data-image="'+itemImg+'" onclick="validate(this,1);" value="'+itemData.id+', '+(itemData.item_name).replace(/\"/g,"inch")+', '+itemData.price+', '+itemImg+'">';
                              if(itemChild[0]['menu_addon'].length>0) {
                                    var itemExtra = itemChild[0]['menu_addon'];
                                    var childID = itemExtra[0]['menuid'];
                                    
                                    var txtJson = '';
                                    for (var j = 0; j < itemExtra.length; j++) 
                                    {
                                      var childMin = itemExtra[j].count;
                                      var childMax = itemExtra[j].maxcount;
                                        txtJson += '{'+j+', '+childMin+', '+childMax+', '+itemExtra[j].title+'}';
                                        var minCls='';
                                        if(childMin==1 && childMax==1) { minCls = 'minY'; } 
                                          modelBody += '<div class="boxes '+minCls+'" id="box_'+childID+'_'+j+'" style="margin-top: 5%;"><div class="list-card-body" style="padding-top: 0px !important;margin-left: -5px;"><h6 class="mb-1"><a class="text-black">'+itemExtra[j].title+'</a></h6><p class="text-gray mb-2" style="font-size: 10px;">'+itemExtra[j].add_on_desc+'</p></div>';
                                          var itemAddon = itemExtra[j]['extra'];
                                          for (var k = 0; k < itemAddon.length; k++) 
                                          {
                                            SelQty='';
                                            if(childMin==1 && childMax==1) { Chk_Rad = 'radio'; SelQty=''; } else { 
                                                Chk_Rad = 'checkbox'; 
                                                SelQty += '<span class="addOnSel"><div class="btn-number" disabled="disabled" data-type="minus" data-field="addOnSel'+childID+'_'+j+'[1]"data-id="'+childID+'_'+j+'_'+k+'"><i class="icofont-minus inc_btn"></i></div><input type="text" class="form-control form-control-sm input-number '+childID+'_'+j+'_'+k+'" name="addOnSel'+childID+'_'+j+'[]" value="1" min="1" readonly max="'+childMax+'"><div class="btn-number" data-type="plus" data-field="addOnSel'+childID+'_'+j+'[1]" data-id="'+childID+'_'+j+'_'+k+'"><i class="icofont-plus inc_btn"></i></div></span>';
                                              }
                                            modelBody += '<div class="custom-control custom-'+Chk_Rad+'"><input type="'+Chk_Rad+'" class="custom-control-input  addonCls_'+childID+'" id="'+childID+'_'+itemAddon[k].addon_id+'" name="addOnCheck'+childID+'_'+j+'[]" value="'+childID+','+itemAddon[k].addon_id+','+itemAddon[k].addon_name.replace(/\"/g,"inch").replace(/,/g," ")+','+itemAddon[k].addon_price+'"><label class="custom-control-label col-5 col-sm-6" for="'+childID+'_'+itemAddon[k].addon_id+'" style="font-weight: 500;">'+itemAddon[k].addon_name.replace(/,/g," ")+'</label>'+SelQty+'';
                                            modelBody += '<small class="text-black-50 text-right" style="float: right; color: #34373b !important;font-weight: 500;"> £ '+itemAddon[k].addon_price+'</small>';
                                            modelBody += '</div>';
                                          }
                                          modelBody += '</div>';
                                          if(itemExtra.length==(j+1)){
                                            itemMinMax[childID] = txtJson.replaceAll("}{", "},{");
                                          }
                                        }
                                    }
                          }
                          else 
                          { 
                            $('.modal-body').css('height', (modalHeight+10)+'vh');
                            for (i = 0; i < itemChild.length; i++) 
                            {
                              
                              modelBody += '<div class="detail det_'+itemChild[i]['menu'].id+'"><span class="summary" data-id="'+itemChild[i]['menu'].id+'"><input type="radio" style="display:none" id="'+itemChild[i]['menu'].id+'" onchange="addcatpop(this)" name="subMenuList" data-id="'+itemChild[i]['menu'].categories_id+'" data-name="'+(itemChild[i]['menu'].item_name.replace(/,/g," ")).replace(/\"/g,"inch")+'" data-amount="'+itemChild[i]['menu'].item_name+'" data-subid="'+itemChild[i]['menu'].id+'"  item_name="" item_id="" addon_price="'+itemChild[i]['menu'].price+'" onclick="validate(this,1);" data-image="'+itemImg+'"  value="'+itemChild[i]['menu'].id+', '+(itemChild[i]['menu'].item_name.replace(/,/g," ")).replace(/\"/g,"inch")+', '+itemChild[i]['menu'].price+', '+itemImg+'"><span class="row col-12"><span class="col-10">'+itemChild[i]['menu'].item_name+'</span><span  class="col-2 text-right" style="float:right;">£'+itemChild[i]['menu'].price+'</span></span><div style="font-size: 11px;margin-bottom:5px;" class="addOn_'+itemChild[i]['menu'].id+' addonDiv">';
                                  var itemExtra = itemChild[i]['menu_addon'];
                                  var childID = itemChild[i]['menu'].id;
                                  var txtJson = '';
                                  for (var j = 0; j < itemExtra.length; j++) 
                                  {
                                    var childMin = itemExtra[j].count;
                                    var childMax = itemExtra[j].maxcount;
                                      txtJson += '{'+j+', '+childMin+', '+childMax+', '+itemExtra[j].title+'}';
                                        var minCls='';
                                        if(childMin==1 && childMax==1) { minCls = 'minY'; } 
                                        modelBody += '<div class="boxes '+minCls+'" id="box_'+childID+'_'+j+'"><div class="list-card-body" style="padding-top: 0px !important;margin-left: -5px;"><h6 class="mb-1"><a class="text-black">'+itemExtra[j].title+'</a></h6><p class="text-gray mb-2" style="font-size: 10px;">'+itemExtra[j].add_on_desc+'</p></div>';
                                        var itemAddon = itemExtra[j]['extra'];
                                        for (var k = 0; k < itemAddon.length; k++) 
                                        {
                                          SelQty='';
                                          if(childMin==1 && childMax==1) { Chk_Rad = 'radio'; SelQty=''; } else { 
                                              Chk_Rad = 'checkbox'; 
                                              SelQty += '<span class="addOnSel"><div class="btn-number" disabled="disabled" data-type="minus" data-field="addOnSel'+childID+'_'+j+'[1]"data-id="'+childID+'_'+j+'_'+k+'"><i class="icofont-minus inc_btn"></i></div><input type="text" class="form-control form-control-sm input-number '+childID+'_'+j+'_'+k+'" name="addOnSel'+childID+'_'+j+'[]" value="1" min="1" readonly max="'+childMax+'"><div class="btn-number" data-type="plus" data-field="addOnSel'+childID+'_'+j+'[1]" data-id="'+childID+'_'+j+'_'+k+'"><i class="icofont-plus inc_btn"></i></div></span>';
                                            }
                                          modelBody += '<div class="custom-control custom-'+Chk_Rad+'"><input type="'+Chk_Rad+'" class="custom-control-input  addonCls_'+childID+'" id="'+itemChild[i]['menu'].id+'_'+itemAddon[k].addon_id+'" name="addOnCheck'+childID+'_'+j+'[]" value="'+itemChild[i]['menu'].id+','+itemAddon[k].addon_id+','+itemAddon[k].addon_name.replace(/\"/g,"inch").replace(/,/g," ")+','+itemAddon[k].addon_price+'"><label class="custom-control-label col-5 col-sm-6" for="'+itemChild[i]['menu'].id+'_'+itemAddon[k].addon_id+'" style="font-weight: 500;">'+itemAddon[k].addon_name.replace(/,/g," ")+'</label>'+SelQty+'';
                                          modelBody += '<small class="text-black-50 text-right" style="float: right; color: #34373b !important;font-weight: 500;"> £ '+Number(itemAddon[k].addon_price).toFixed(2)+'</small>';
                                          modelBody += '</div>';
                                        }
                                        modelBody += '</div>';
                                        if(itemExtra.length==(j+1)){
                                          itemMinMax[childID] = txtJson.replaceAll("}{", "},{");
                                        }
                                  }
                                modelBody += '</div></div>';
                                $('.addInst').hide();
                                $('#addInst').val('');
                                } 
                      }
                    modelBody += '<span class="row"><b style="padding: 15px 0;font-size: 16px;">Special request to suit your taste buds</b><br><textarea type="text" placeholder="Please leave any special request e.g. Extra spicy, no sauce, well done  etc. Please do not leave any allergen info here." row="4" name="instr" id="addInst" class="col-12 form-control" style="font-size: 14px;maxlength="100" onpaste="return false;" onCopy="return false" onkeypress="allowAlphaNumericSpace(event)" onCut="return false" onDrag="return false" onDrop="return false"></textarea><p style="font-size:12px;text-align:right;">(Max 100 characters)</p></span>';
                    $('.linear-background').hide();
                    $('#AddOns').html(modelBody);
                          modelFooter += '<span class="count-number float-left"><button class="btn btn-outline-secondary  btn-sm left dec" onclick="minusValue('+iId+')"><i class="icofont-minus"></i></button><input id="'+iId+'" class="count-number-input totItem" type="number" name="totItem" value="1" readonly=""><button class="btn btn-outline-secondary btn-sm right inc" onclick="plusValue('+iId+')"><i class="icofont-plus"></i></button></span><button id="addButton'+iId+'" type="button" class="btn btn-primary addbtn float-right" data-dismiss="modal" amount="'+itemData.price+'" item_id="'+iId+'" discount="'+itemData.discount+'" qty="1" status="0" notes="1" item_name = "'+(itemData.item_name).replace(/\"/g,"inch")+'" '; 
                        if(itemData.discount.length == 0 && itemData.discount.length == 0){
                            modelFooter += 'onclick="addItem(this)"';
                        }
                        modelFooter += ' onclick="btnAddToCart()">Add to cart</button>';
                    $('.modal-footer').html(modelFooter);
                    
               },
               error: function(json) {
                  alert(JSON.stringify(json));
               }
      });

}
function showAddOns(mId, arId, iId, cId)
{
  var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
  if (isMobile) {  modalHeight = 43; } else { modalHeight = 47; }
      //alert(mId+', '+arId+', '+iId+', '+cId+', '+JSON.stringify(allItems[cId]['sub_menu'][arId]));
      //alert(allItems[cId]['sub_menu']);
     
      if(allItems[cId]) {
        $('.linear-background').hide();
        var itemData = getItems[mId]['value'][arId];
        // alert(itemData.image);
        $('.alert-danger').hide();      
        var modelBody = modelFooter = '';
        $('#AddOns').html(modelBody);
        $('#addonModal').modal('show');
        $('#addonModalLabel').html(itemData.item_name);
        if(itemData.image=='dummy.jpg'){ itemImg ='menu-src/noimage.jpg'; } else { itemImg ='https://deliveryguru.co.uk/admin/images/itemimages/'+itemData.image; }
        var modelBody = modelFooter = modelItem = '';
        
        if(itemData.image!='dummy.jpg'){
          modelItem += '<img src="'+itemImg+'" class="img-fluid item-img" style="width: 100%;height: 300px; object-fit: cover;" />';
          modalHeight = 20 +modalHeight;  
        }
        modelItem += '<p class="text-gray popup-desc">'+itemData.item_desc+'</p>';
        $('.itemDataModal').html(modelItem);

        var itemChild = allItems[cId]['sub_menu'][arId]['child'];
        
                        if(itemChild.length==0) { //alert(itemData.id);
                            $('.modal-body').css('height', modalHeight+'vh');
                              modelBody += '<input type="radio" style="display:none" checked onchange="addcatpop(this)" name="subMenuList" data-id="'+itemData.categories_id+'" data-name="'+(itemData.item_name).replace(/\"/g,"inch")+'" data-amount="'+itemData.item_name+'" data-subid="'+itemData.id+'"  item_name="" item_id="" addon_price="'+itemData.price+'" data-image="'+itemImg+'" onclick="validate(this,1);" value="'+itemData.id+', '+(itemData.item_name).replace(/\"/g,"inch")+', '+itemData.price+', '+itemImg+'">';
                              var itemChild = allItems[cId]['sub_menu'][arId];
                              
                              if(itemChild['extra'].length>0) {
                                    var itemExtra = itemChild['extra'];
                                    var childID = itemChild['id'];
                                    //alert(itemExtra+'_'+childID+'_'+itemExtra.length);
                                    var txtJson = '';
                                    for (var j = 0; j < itemExtra.length; j++) 
                                    {
                                      
                                      var childMin = itemExtra[j].count;
                                      var childMax = itemExtra[j].maxcount;
                                        txtJson += '{'+j+', '+childMin+', '+childMax+', '+itemExtra[j].name+'}';
                                        var minCls='';
                                       // alert(childID);
                                        if(childMin==1 && childMax==1) { minCls = 'minY'; } 
                                          modelBody += '<div class="boxes '+minCls+'" id="box_'+childID+'_'+j+'" style="margin-top: 5%;"><div class="list-card-body" style="padding-top: 0px !important;margin-left: -5px;"><h6 class="mb-1"><a class="text-black">'+itemExtra[j].name+'</a></h6><p class="text-gray mb-2" style="font-size: 10px;">'+itemExtra[j].add_on_desc+'</p></div>';
                                          var itemAddon = itemExtra[j]['addon'];
                                          for (var k = 0; k < itemAddon.length; k++) 
                                          {
                                            SelQty='';
                                            if(childMin==1 && childMax==1) { Chk_Rad = 'radio'; SelQty=''; } else { 
                                                Chk_Rad = 'checkbox'; 
                                                SelQty += '<span class="addOnSel"><div class="btn-number" disabled="disabled" data-type="minus" data-field="addOnSel'+childID+'_'+j+'[1]"data-id="'+childID+'_'+j+'_'+k+'"><i class="icofont-minus inc_btn"></i></div><input type="text" class="form-control form-control-sm input-number '+childID+'_'+j+'_'+k+'" name="addOnSel'+childID+'_'+j+'[]" value="1" min="1" readonly max="'+childMax+'"><div class="btn-number" data-type="plus" data-field="addOnSel'+childID+'_'+j+'[1]" data-id="'+childID+'_'+j+'_'+k+'"><i class="icofont-plus inc_btn"></i></div></span>';
                                              }
                                            modelBody += '<div class="custom-control custom-'+Chk_Rad+'"><input type="'+Chk_Rad+'" class="custom-control-input  addonCls_'+childID+'" id="'+childID+'_'+itemAddon[k].addon_id+'" name="addOnCheck'+childID+'_'+j+'[]" value="'+childID+','+itemAddon[k].addon_id+','+itemAddon[k].addon_name.replace(/\"/g,"inch").replace(/,/g," ")+','+itemAddon[k].addon_price+'"><label class="custom-control-label col-5 col-sm-6" for="'+childID+'_'+itemAddon[k].addon_id+'" style="font-weight: 500;">'+itemAddon[k].addon_name.replace(/,/g," ")+'</label>'+SelQty+'';
                                            modelBody += '<small class="text-black-50 text-right" style="float: right; color: #34373b !important;font-weight: 500;"> £ '+Number(itemAddon[k].addon_price).toFixed(2)+'</small>';
                                            modelBody += '</div>';
                                          }
                                          modelBody += '</div>';
                                          if(itemExtra.length==(j+1)){
                                            //alert(txtJson);
                                            itemMinMax[childID] = txtJson.replaceAll("}{", "},{");
                                          }
                                        }
                                    }
                          }
                          else 
                          { 
                            $('.modal-body').css('height', (modalHeight+10)+'vh');
                            for (i = 0; i < itemChild.length; i++) 
                            {
                              
                              modelBody += '<div class="detail det_'+itemChild[i].id+'"><span class="summary" data-id="'+itemChild[i].id+'"><input type="radio" style="display:none" id="'+itemChild[i].id+'" onchange="addcatpop(this)" name="subMenuList" data-id="'+itemChild[i].categories_id+'" data-name="'+(itemChild[i].item_name.replace(/,/g," ")).replace(/\"/g,"inch")+'" data-amount="'+itemChild[i].item_name+'" data-subid="'+itemChild[i].id+'"  item_name="" item_id="" addon_price="'+itemChild[i].price+'" onclick="validate(this,1);" data-image="'+itemImg+'"  value="'+itemChild[i].id+', '+(itemChild[i].item_name.replace(/,/g," ")).replace(/\"/g,"inch")+', '+itemChild[i].price+', '+itemImg+'"><span class="row col-12"><span class="col-10">'+itemChild[i].item_name+'</span><span  class="col-2 text-right" style="float:right;">£'+itemChild[i].price+'</span></span><div style="font-size: 11px;margin-bottom:5px;" class="addOn_'+itemChild[i].id+' addonDiv">';
                                  var itemExtra = itemChild[i]['extra'];
                                  var childID = itemChild[i].id;
                                  var txtJson = '';
                                  //alert(JSON.stringify(itemExtra));
                                  for (var j = 0; j < itemExtra.length; j++) 
                                  {
                                    var childMin = itemExtra[j].count;
                                    var childMax = itemExtra[j].maxcount;
                                      txtJson += '{'+j+', '+childMin+', '+childMax+', '+itemExtra[j].name+'}';
                                      //alert(itemExtra+'_'+childID+'_'+itemExtra.length);
                                        var minCls='';
                                        if(childMin==1 && childMax==1) { minCls = 'minY'; } 
                                        modelBody += '<div class="boxes '+minCls+'" id="box_'+childID+'_'+j+'"><div class="list-card-body" style="padding-top: 0px !important;margin-left: -5px;"><h6 class="mb-1"><a class="text-black">'+itemExtra[j].name+'</a></h6><p class="text-gray mb-2" style="font-size: 10px;">'+itemExtra[j].add_on_desc+'</p></div>';
                                        var itemAddon = itemExtra[j]['addon'];
                                        for (var k = 0; k < itemAddon.length; k++) 
                                        {
                                          SelQty='';
                                          if(childMin==1 && childMax==1) { Chk_Rad = 'radio'; SelQty=''; } else { 
                                              Chk_Rad = 'checkbox'; 
                                              SelQty += '<span class="addOnSel"><div class="btn-number" disabled="disabled" data-type="minus" data-field="addOnSel'+childID+'_'+j+'[1]"data-id="'+childID+'_'+j+'_'+k+'"><i class="icofont-minus inc_btn"></i></div><input type="text" class="form-control form-control-sm input-number '+childID+'_'+j+'_'+k+'" name="addOnSel'+childID+'_'+j+'[]" value="1" min="1" readonly max="'+childMax+'"><div class="btn-number" data-type="plus" data-field="addOnSel'+childID+'_'+j+'[1]" data-id="'+childID+'_'+j+'_'+k+'"><i class="icofont-plus inc_btn"></i></div></span>';
                                            }
                                          modelBody += '<div class="custom-control custom-'+Chk_Rad+'"><input type="'+Chk_Rad+'" class="custom-control-input  addonCls_'+childID+'" id="'+itemChild[i].id+'_'+itemAddon[k].addon_id+'" name="addOnCheck'+childID+'_'+j+'[]" value="'+itemChild[i].id+','+itemAddon[k].addon_id+','+itemAddon[k].addon_name.replace(/,/g," ")+','+itemAddon[k].addon_price+'"><label class="custom-control-label col-5 col-sm-6" for="'+itemChild[i].id+'_'+itemAddon[k].addon_id+'" style="font-weight: 500;">'+itemAddon[k].addon_name.replace(/,/g," ")+'</label>'+SelQty+'';
                                          modelBody += '<small class="text-black-50 text-right" style="float: right; color: #34373b !important;font-weight: 500;"> £ '+Number(itemAddon[k].addon_price).toFixed(2)+'</small>';
                                          modelBody += '</div>';
                                        }
                                        modelBody += '</div>';
                                        if(itemExtra.length==(j+1)){
                                          itemMinMax[childID] = txtJson.replaceAll("}{", "},{");
                                        }
                                  }
                                modelBody += '</div></div>';
                                $('.addInst').hide();
                                $('#addInst').val('');
                                } 
                      }
                    modelBody += '<span class="row"><b style="padding: 15px 0;font-size: 16px;">Special request to suit your taste buds</b><br><textarea type="text" placeholder="Please leave any special request e.g. Extra spicy, no sauce, well done  etc. Please do not leave any allergen info here." row="4" name="instr" id="addInst" class="col-12 form-control" style="font-size: 14px;maxlength="100" onpaste="return false;" onCopy="return false" onkeypress="allowAlphaNumericSpace(event)" onCut="return false" onDrag="return false" onDrop="return false"></textarea><p style="font-size:12px;text-align:right;">(Max 100 characters)</p></span>';
                    $('.linear-background').hide();
                    $('#AddOns').html(modelBody);
                          modelFooter += '<span class="count-number float-left"><button class="btn btn-outline-secondary  btn-sm left dec" onclick="minusValue('+iId+')"><i class="icofont-minus"></i></button><input id="'+iId+'" class="count-number-input totItem" type="number" name="totItem" value="1" readonly=""><button class="btn btn-outline-secondary btn-sm right inc" onclick="plusValue('+iId+')"><i class="icofont-plus"></i></button></span><button id="addButton'+iId+'" type="button" class="btn btn-primary addbtn float-right" data-dismiss="modal" amount="'+itemData.price+'" item_id="'+iId+'" discount="'+itemData.discount+'" qty="1" status="0" notes="1" item_name = "'+(itemData.item_name).replace(/\"/g,"inch")+'" '; 
                        if(itemData.discount.length == 0 && itemData.discount.length == 0){
                            modelFooter += 'onclick="addItem(this)"';
                        }
                        modelFooter += ' onclick="btnAddToCart()">Add to cart</button>';
                    $('.modal-footer').html(modelFooter);
      
      
      } else {
        showAddOnsAjax(mId, arId, iId, cId);
      }
}
// getsta();

function getHotelStatus(mId, arId, iId, cId) {

   if(pre_order_status!==0) {
      showAddOns(mId, arId, iId, cId);
   } else {
      var d = new Date();
      var n = d.getDay();
      var openhours='17:00';
      var closehour='05:00';
      var midtime="00:00";
      var event = new Date().toLocaleTimeString('en-GB', { timeZone: 'Europe/London' ,hour: 'numeric',minute: 'numeric', hour12: false});
   //alert(event);
  if(event<openhours && event>closehour){
        <?php if(!isset($_SESSION['pre_order'])) { ?>closedMsg();  <?php } else { ?>  pre_order_status=1; showAddOns(mId, arId, iId, cId); <?php }  ?>
     }
    else{
        showAddOns(mId, arId, iId, cId);
    }
    }

}
  //var dd = new Date();
  // var nn = dd.getDay();
  
/*
var today = new Date();
$('#preorderdateformat1').calendar({
  monthFirst: false,
 type: 'date',
  
  formatter: {
    date: function (date, settings) {
      if (!date) return '';
      var day = date.getDate();
      var month = date.getMonth() + 1;
      var year = date.getFullYear();
      return day + '/' + month + '/' + year;
    }
  },
  minDate: new Date(),
  maxDate: new Date(today.getFullYear(), today.getMonth(), today.getDate() + 5)
});*/
$(".dropdown-menu, .mobCartBody").click(function(e){
   e.stopPropagation();
})
//getHotelStatus();
    $(document).ready(function() {
          $('body').bind('cut copy', function(e) {
              e.preventDefault();
            });
        $("body").on("contextmenu", function(e) {
              return false;
            });
            window.ondragstart = function() { return false; } 
        });
          function allowAlphaNumericSpace(e) {
  var code = ('charCode' in e) ? e.charCode : e.keyCode;
  if (!(code == 32) && // space
    !(code > 47 && code < 58) && // numeric (0-9)
    !(code > 64 && code < 91) && // upper alpha (A-Z)
    !(code > 96 && code < 123)) { // lower alpha (a-z)
    e.preventDefault();
  }
}
</script>


</html>