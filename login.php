<?php
include 'src/config.php';
if(isset($_SESSION['uid'])){
    header("Location:menu.php");
}
else{
    if(isset($_GET['pg'])) { $retURL = $_GET['pg'].'.php'; } else { $retURL = 'menu.php'; }
    function getHotelAPI() {
      $graph_url= 'https://deliveryguru.co.uk/dg_api/getmetadata/200/login';
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $graph_url);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      $output = curl_exec($ch); 
      curl_close($ch);
      return json_decode($output, true); 
    }
    $getHotelDetails = getHotelAPI();
    $keyword=$getHotelDetails['result'][0]['allTag'];
    $keyWord=$getHotelDetails['result'][0]['h1_tag'];
?>
<!doctype html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- Favicons-->
   <?php echo $keyword;?>
  
      <!-- Required meta tags -->
      <link rel="icon" type="image/png" href="img/favicon.png">
      <!-- Bootstrap core CSS-->
      <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome-->
      <link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
      <!-- Font Awesome-->
      <link href="vendor/icofont/icofont.min.css" rel="stylesheet">
      <!-- Select2 CSS-->
      <link href="vendor/select2/css/select2.min.css" rel="stylesheet">
      <!-- Custom styles for this template-->
      <link href="css1/osahan.css" rel="stylesheet">
      <script src="js/jquery-3.5.1.js"></script>
        
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-222117938-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-222117938-1');
</script>

       <!-- Facebook Pixel Code -->
       


<!-- End Facebook Pixel Code -->
      <style>
      .bg-image {
    background-image: url(img/login-bg.jpg) !important;
    background-size: cover;
    background-position: center;
}
      </style>
      <style>
#snackbar {
  visibility: hidden;
  min-width: 250px;
  margin-left: -125px;
  background-color: #333;
  color: #fff;
  text-align: center;
  border-radius: 2px;
  padding: 16px;
  position: fixed;
  z-index: 1;
  left: 50%;
  top: 90px;
  font-size: 17px;
}

#snackbar.show {
  visibility: visible;
  -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
  animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

@-webkit-keyframes fadein {
  from {top: 0; opacity: 0;} 
  to {top: 90px; opacity: 1;}
}

@keyframes fadein {
  from {top: 0; opacity: 0;}
  to {top: 90px; opacity: 1;}
}

@-webkit-keyframes fadeout {
  from {top: 90px; opacity: 1;} 
  to {top: 0; opacity: 0;}
}

@keyframes fadeout {
  from {top: 90px; opacity: 1;}
  to {top: 0; opacity: 0;}
}
.loginlogo{
   width: 90px;
    text-align: center;
   margin-left: 39%;
    
    /*border: 1px solid #ccc;
    box-shadow: 1px 4px 8px #cccc;*/
}


#otp input{
    padding-top: 0px !important;
    padding-bottom: 0px !important;
}
.form-text{
	position:relative;
}
.mobile input{
	/*padding:0px 36px 50px 60px;
  font-weight:bold;
	font-size:1em;*/
    padding-top: 11px !important;
    padding-left: 10px !important;
    /* padding-right: 27px !important; */
    padding-bottom: 10px !important;
   
   font-size:22px;
   }

.static-value{
	position:absolute;
	left:32px;
   margin-bottom:20px;
   
	color:#444;
	top:10px;
}

.inputs input {
    width: 60px;
    height: 50px
}

input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin: 0
}



.base-timer {
  position: relative;
  width: 1px;
  height: 1px;
  margin-left:50px;
  margin-bottom:30px;
}

.base-timer__svg {
  transform: scaleX(-1);
}

.base-timer__circle {
  fill: none;
  stroke: none;
}

.base-timer__path-elapsed {
  stroke-width: 7px;
  stroke: grey;
}

.base-timer__path-remaining {
  stroke-width: 7px;
  stroke-linecap: round;
  transform: rotate(90deg);
  transform-origin: center;
  transition: 1s linear all;
  fill-rule: nonzero;
  stroke: currentColor;
}

.base-timer__path-remaining.green {
  color: rgb(65, 184, 131);
}

.base-timer__path-remaining.orange {
  color: orange;
}

.base-timer__path-remaining.red {
  color: red;
}

.base-timer__label {
  position: absolute;
  width: 40px;
  height: 40px;
  margin-left:85px;
  top: 0;
  
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 17px;
  }

  :root {
    --input-padding-x: 1.5rem !important;
    --input-padding-y: 0.5rem !important;
}
.login-heading {
    font-weight: 300;
    margin: 0px 0px 20px -1px !important;
    left:2% im !important;
}
.form-label-group {
    left: 1%;
    position: relative;
    margin-bottom: 1rem !important;
} 
.form-label-group>label {
    position: absolute;
    top: 60px !important;
    left: 7%;
    display: block;
    width: 100%;
    margin-bottom: 0;
    /* Override default `<label>` margin */
    line-height: 1.5;
    color: #495057;
    cursor: text;
    /* Match the input under the label */
    border: 1px solid transparent;
    border-radius: .25rem;
    transition: all .1s ease-in-out;
}
.login, .image {
    min-height: 65vh;
}

.red_link {
    font-size: 13px;
    text-decoration: underline;
    vertical-align: middle;
    color: red;
    cursor: pointer;
}
h5#loginview2 {
    line-height: 30px;
}
@media (min-width:768px){
    .maincnt{
    left:26%;
}
}
@media (max-width:767px){
      .maincnt{
    left:0px;
} 
}

 .unselectable {
        -webkit-user-select: none;
        -webkit-touch-callout: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        /*color: #cc0000;*/
      }
</style>
   </head>
   <div class="loader" style="display:none" id="onloader">
	<div class="loader_inner"></div>
</div>
   <body class="bg-white unselectable">
       <?php include("headerot.php");?>
      <div class="container-fluid">
         <div class="row no-gutter" id="login">
            <!--<div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>-->
            <div class="col-md-8 col-lg-6 maincnt" style="">
               <div class="login d-flex align-items-center">
                  <div class="container">
                      <div id="snackbar"></div>
                     <div class="row">
                        <div class="col-md-12 col-lg-12 mx-auto pl-5 pr-5" id="login">
                        
                          <!--<img src="img/dglogo.png" class="loginlogo">-->
                           <form>
                           <div id="logoimgDiv" class="loginlogo"></div>
                           <div id="logoimageDiv" class="loginlogo"></div>
                           <h4 style="text-align: center;" id="mobilenumlogin">Login With Your Mobile Number!<h1 class="font-weight-bold mt-0 mb-3" style="font-size:15px;text-align:center;"><?php echo $keyWord;?></h1></h4>
                           <div class="mb-4" id="loginview2" style="text-align:center; font-size: 16px;"></div>
                              <div class="form-label-group" id="mobilediv">
                                  <input type="hidden" id="mobilenumber">
                                  <div class="form-text mobile" >
      <input  type="text" style="margin-left:-1% !important;width: 100%;" id="mobile" onpaste="return false;" ondrop="return false;" maxlength=11 autocomplete="off" onkeypress="return isNumber(event)">
     
    </div>
                              <!--<input type="text" id="mobile" maxlength=10 onkeypress="javascript:return isNumber(event)" onfocusout="phonenumber(this.value)" placeholder="Mobile Number"  class="form-control" autocomplete="off">
                                 !--<label for="mobile"><Span>+44</span></label>-->
                                 <p id='email_error'></p>
                              </div>
                              
                              <div class="form-label-group" id="otpdiv" style="display:none">
                              <div id="otp" class="inputs d-flex flex-row justify-content-center mt-2"  autocomplete="off" > 
                              <input type="text" class="m-2 text-center form-control rounded" autocomplete="off" onkeypress="return isNumber(event)"  id="first" maxlength="1" style="font-size:19px;border: 1px solid #cbcbcb;"/> 
                              <input class="m-2 text-center form-control rounded" type="text" onpaste="return false;" ondrop="return false;" id="second" maxlength="1" style="font-size:19px;border: 1px solid #cbcbcb;"  onkeypress="return isNumber(event)" autocomplete="off"/> 
                              <input class="m-2 text-center form-control rounded" type="text" onpaste="return false;" ondrop="return false;" onpaste="return false;" ondrop="return false;" id="third" maxlength="1" style="font-size:19px;border: 1px solid #cbcbcb;"  onkeypress="return isNumber(event)" autocomplete="off"/> 
                              
                              <input class="m-2 text-center form-control rounded" type="text" id="fourth" ondrop="return false;" onpaste="return false;" maxlength="1" style="font-size:19px;border: 1px solid #cbcbcb;" onkeypress="return isNumber(event)" autocomplete="off" />
                              <!--<input class="m-2 text-center form-control rounded" type="text" id="fifth" maxlength="1" /> <input class="m-2 text-center form-control rounded" type="text" id="sixth" maxlength="1" />--> </div>
                              
                             <!-- <input type="text"   id="otp" placeholder="OTP" class="form-control" autocomplete="off">
                                 <label for="otp">OTP</label>      class="inputs d-flex flex-row justify-content-center mt-2"-->
                                 <p id="password_error"></p>
                              </div>
                               <h4 class="login-heading mb-4" id="loginview" style="text-align:center;"></h4>
                              <button class="btn btn-lg btn-outline-primary btn-block btn-login text-uppercase font-weight-bold mb-2" id="loginbutton" type="button" onclick="loginsubmit();" style="margin-top: 30px;">LogIn</button>
				<button class="btn btn-lg btn-outline-primary btn-block btn-login text-uppercase font-weight-bold mb-2" id="otpbutton" type="button" onclick="verifyotp();" style="display:none;margin-top:43px;" >Verify</button>
                    <p style="text-transform: capitalize;" class="unselectable" id="onlymobile">By entering your number, you confirm that you accept our <a href="#" style="color;red;">Terms & conditions</a>,
Deliveryguru <a href="#" style="color;red;">Terms & Conditions</a>, and acknowledge our <a href="#" style="color;red;">Privacy Policy</a></p>
                            <div class="mt-4" id="resend" style="text-align:center; font-size: 16px;"></div>
                           </form>
                          
                        </div>
                     </div>
                  </div>
               </div>
			   
            </div>
            
         </div>
       
      </div>
      <!-- jQuery -->
      <!-- <script src="vendor/jquery/jquery-3.3.1.slim.min.js" type="c4b1ce5c15dceccf6e832422-text/javascript"></script> -->
      <!-- Bootstrap core JavaScript-->
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js" type="c4b1ce5c15dceccf6e832422-text/javascript"></script>
      <!-- Select2 JavaScript-->
      <!-- <script src="vendor/select2/js/select2.min.js" type="c4b1ce5c15dceccf6e832422-text/javascript"></script> -->
      <!-- Custom scripts for all pages-->
      <!-- <script src="js/custom.js" type="c4b1ce5c15dceccf6e832422-text/javascript"></script> -->
   <script src="js/rocket-loader.min.js" data-cf-settings="c4b1ce5c15dceccf6e832422-|49" defer=""></script>
   <!-- <script defer src="js/beacon.min.js" data-cf-beacon='{"rayId":"5d3af9c9e8941016","version":"2020.9.0","si":10}'></script> -->
</body>
<script>

function onloadMobile(){
    
    document.getElementById('mobile').value='07';
}
$("#mobile").keydown(function(e) {
    var oldvalue=$(this).val();
    var field=this;
    setTimeout(function () {
        if(field.value.indexOf('07') !== 0) {
            $(field).val(oldvalue);
        } 
    }, 1);
});
onloadMobile();
function onTimesUp() {
  clearInterval(timerInterval);
}

function startTimer() {
  timerInterval = setInterval(() => {
    timePassed = timePassed += 1;
    timeLeft = TIME_LIMIT - timePassed;
    document.getElementById("base-timer-label").innerHTML = formatTime(
      timeLeft
    );
    setCircleDasharray();
    setRemainingPathColor(timeLeft);

    if (timeLeft === 0) {
      onTimesUp();
    }
  }, 1000);
}

function formatTime(time) {
  const minutes = Math.floor(time / 60);
  let seconds = time % 60;

  if (seconds < 10) {
    seconds = `0${seconds}`;
  }

  return `${minutes}:${seconds}`;
}

function setRemainingPathColor(timeLeft) {
  const { alert, warning, info } = COLOR_CODES;
  if (timeLeft <= alert.threshold) {
    document
      .getElementById("base-timer-path-remaining")
      .classList.remove(warning.color);
    document
      .getElementById("base-timer-path-remaining")
      .classList.add(alert.color);
  } else if (timeLeft <= warning.threshold) {
    document
      .getElementById("base-timer-path-remaining")
      .classList.remove(info.color);
    document
      .getElementById("base-timer-path-remaining")
      .classList.add(warning.color);
  }
}

function calculateTimeFraction() {
  const rawTimeFraction = timeLeft / TIME_LIMIT;
  return rawTimeFraction - (1 / TIME_LIMIT) * (1 - rawTimeFraction);
}

function setCircleDasharray() {
  const circleDasharray = `${(
    calculateTimeFraction() * FULL_DASH_ARRAY
  ).toFixed(0)} 283`;
  document
    .getElementById("base-timer-path-remaining")
    .setAttribute("stroke-dasharray", circleDasharray);
}



</script>



  <script>
  document.addEventListener("DOMContentLoaded", function(event) {

function OTPInput() {
const inputs = document.querySelectorAll('#otp > *[id]');
for (let i = 0; i < inputs.length; i++) { inputs[i].addEventListener('keydown', function(event) { if (event.key==="Backspace" ) { inputs[i].value='' ; if (i !==0) inputs[i - 1].focus(); } else { if (i===inputs.length - 1 && inputs[i].value !=='' ) { return true; } else if (event.keyCode> 47 && event.keyCode < 58) { inputs[i].value=event.key; if (i !==inputs.length - 1) inputs[i + 1].focus(); event.preventDefault(); } else if (event.keyCode> 64 && event.keyCode < 91) { inputs[i].value=String.fromCharCode(event.keyCode); if (i !==inputs.length - 1) inputs[i + 1].focus(); event.preventDefault(); } } }); } } OTPInput(); });

$(document).ready(function(){
    document.getElementById("loginview").innerHTML="<p style='color:#000;font-size:14px;font-weight:400;'>Enter Your Mobile Number We Will Sent You Text with 4 Digit Security Pin to Verify Your Mobile Number...</p>";  
    document.getElementById("logoimgDiv").innerHTML= '<img src="img/1.jpg" class="form-label-group" style="height: auto;width: 100%;">';
       
});

         // WRITE THE VALIDATION SCRIPT.
         function isNumber(evt) {
            
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
               return false;

            return true;
         }

         function phonenumber(inputtxt) {

            var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
            if (inputtxt.match(phoneno)) {
               return true;
            } else {
               //alert("Enter Valid Phone Number");
               return false;

            }
         }
      </script>
      <script>
        
         var x = document.getElementById("snackbar");
         function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    timerInterval = setInterval(() => {
    //setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;        
        if (--timer < 0) {
            timer = duration;
        }
        if(minutes==0 && seconds==0) {
          document.getElementById("timer").innerHTML= "<i class='red_link' onClick='loginsubmit()'>Resend</i>";
          clearInterval(timerInterval);
        }
    }, 1000);
}
 function loginsubmit() {
     //document.getElementById('onloader').style.display='block';
     var mb=document.getElementById("mobile").value;
     var res = mb.substring(1);
    if(mb.length=!11){
         document.getElementById('onloader').style.display='none';
                  x.className = "show";
                  x.innerHTML="Please Enter Valid Mobile Number";
                setTimeout(function(){ x.className = x.className.replace("show", "");
               
                }, 3000);
                return false;
    }
            $mobile="44"+res;
          console.log($mobile);
    //  return false;
            if (document.getElementById("mobile").value != "") {
               var hid = sessionStorage.getItem("hotelId");
          var dataa={
      	   to:$mobile,
           name:"NATURAL SPICE TANDOORI",
           hotel_id:"200"
      	};
        //alert(mb);
        
        
             $.ajax({
		type:"POST",
		url:"otp.php",
      crossDomain: true,
		dataType: 'text',
     	data:{
         "to" : $mobile,
         "hotel_id":"200",
         "name": "NATURAL SPICE TANDOORI",
         edit:"mobilenumberlogin"
        },
		success:function(result){
			if(result=='PENDING'){
			     document.getElementById("logoimgDiv").innerHTML='';
        document.getElementById("loginview").innerHTML="<h4 style='text-align:center;'>Verify With 4 Digit Security Pin!<h4>";  
           document.getElementById("loginview2").innerHTML="Enter The Pin We Just Send on Your Mobile Phone "+mb+" <small class='red_link'><i><a href='login.php'>Change</a></i></small>"; 
           document.getElementById("resend").innerHTML="Didn't receive the code? <small id='timer'></small>";  
           document.getElementById("logoimageDiv").innerHTML= '<img src="img/1.jpg" style="height: auto;width: 78px;">';
            document.getElementById("mobilediv").style.display="none";
                    document.getElementById("otpdiv").style.display="block";
                    document.getElementById("loginbutton").style.display="none";
                    document.getElementById("otpbutton").style.display="block";
        document.getElementById('mobilenumlogin').style.display="none";
        document.getElementById('onlymobile').style.display="none";
          var fiveMinutes = 60 * .5,
          display = document.querySelector('#timer');
          startTimer(fiveMinutes, display);
			    
			    document.getElementById("mobilenumber").value=$mobile;
			    document.getElementById('onloader').style.display='none';
                  x.className = "show";
                   x.style.background="green";
                  x.innerHTML="OTP send to your "+$mobile+" Number";
                setTimeout(function(){ x.className = x.className.replace("show", ""); 
                  
                }, 3000);
                
                  }
                  else{
                      document.getElementById('onloader').style.display='none';
                        x.className = "show";
                        x.style.background="red";
                  x.innerHTML="Failed To Send otp Please Enter Valid Mobile Number";
                setTimeout(function(){ x.className = x.className.replace("show", "");
                
                   
                }, 3000);
                }
                  

               },
               error: function(jqXHR, textStatus, errorThrown) {
                   
                   document.getElementById('onloader').style.display='none';
                    x.className = "show";
                        x.style.background="red";
                  x.innerHTML="Failed To Send otp Please Enter Valid Mobile Number";
                setTimeout(function(){ x.className = x.className.replace("show", "");
                
                   
                }, 3000);
                  console.log(textStatus, errorThrown);
               }
            });
            }
            else{
                document.getElementById('onloader').style.display='none';
                  x.className = "show";
                  x.innerHTML="Please Enter Mobile Number";
                setTimeout(function(){ x.className = x.className.replace("show", "");
               
                }, 3000);
            }
         }
         function verifyotp(){
             document.getElementById('onloader').style.display='block';
            $otp=document.getElementById("otp").value;
            $mobile=document.getElementById("mobilenumber").value;
            $otp1=document.getElementById("first").value;
            $otp2=document.getElementById("second").value;
            $otp3=document.getElementById("third").value;
            $otp4=document.getElementById("fourth").value;
            console.log($otp1+$otp2+$otp3+$otp4);
            var finalotp=$otp1+$otp2+$otp3+$otp4;
                  if ($otp != "") {
                $.ajax({
		type:"GET",
		url:"https://deliveryguru.co.uk/dg_api/verifyotp/"+$mobile,
	   success:function(result){
	       console.log(result);
			if(result[0].otp==finalotp){
            sessionStorage.setItem("userid", result[0].id);
			         var dataPost = {

                     uid: result[0].id,
                  };
                  var dataString = JSON.stringify(dataPost);
                  $.ajax({
                     type: 'POST',
                     url: 'sess.php',
                     dataType: "json",
                     crossDomain: true,
                     cache: "false",
                     data: {
                        myData: dataString
                     },
                     success: function(data) {
                     document.getElementById('onloader').style.display='none';
                          var x = document.getElementById("snackbar");
                     x.innerHTML="Successfully login!!!";
                     x.style.background="green";
                     x.style.color="#fff";
  x.className = "show";
  setTimeout(function(){ x.className = x.className.replace("show", ""); window.location.href = "<?php echo $retURL; ?>";}, 3000);
                        

                     },
                     error: function(XMLHttpRequest, textStatus, errorThrown) {
document.getElementById('onloader').style.display='none';
                         var x = document.getElementById("snackbar");
                     x.innerHTML="Successfully login!!!";
                     x.style.background="green";
                     x.style.color="#fff";
  x.className = "show";
  setTimeout(function(){ x.className = x.className.replace("show", ""); window.location.href = "<?php echo $retURL; ?>";}, 3000);

                     }
                  });

			    
			    
			}
			else{
			    document.getElementById('onloader').style.display='none';
			        x.className = "show";
                        x.style.background="red";
                  x.innerHTML="Please Enter Valid OTP";
                setTimeout(function(){ x.className = x.className.replace("show", "");
              
                   
                }, 3000);
			    
			}
                     }
                  });
                  }
                  else{
                      document.getElementById('onloader').style.display='none';
                        x.className = "show";
                  x.innerHTML="Please Enter OTP";
                setTimeout(function(){ x.className = x.className.replace("show", "");
               
                }, 3000);
                  }
         }
 $(document).ready(function() {
          $('body').bind('cut copy', function(e) {
              e.preventDefault();
            });
        $("body").on("contextmenu", function(e) {
              return false;
            });
        });
      </script>
	  </html>
      <?php } ?>