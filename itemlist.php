
                           <?php
                           foreach($getItems as $k => $val) {
                              //echo '<li><a href="#'.$val['id'].'" id="anchorTag'.$val['id'].'" class="nav-item">'.$val['name'].'</a></li>';
                              //print_r($val); exit;
                           ?>
                           <div class="item-section" id="<?php echo $val['id']; ?>">
                                <h3 class="mb-4 mt-3 col-md-12"><?php echo $val['name']; ?> <small class="h6 text-black-50"></small></h3>
                                 <div class="row">
                                 <?php foreach($val['sub_menu'] as $k1 => $v1) { ?>
                                    <div class="col-md-3 mb-4 ">
                                       <div class="list-card bg-white rounded overflow-hidden position-relative shadow-sm" data-toggle="modal" data-target="#popup<?php echo $v1['id']; ?>" style="cursor:pointer;box-shadow: 1px 2px 7px 1px #b6b6b6 !important;"> 
                                          <div class="list-card-image">
                                          <!--<div class="star position-absolute">
                                             <span class="badge badge-success"><i class="icofont-star"></i> 3.1 (300+)</span>
                                          </div>-->
                                          <div class="favourite-heart text-danger position-absolute">
                                             <i class="icofont-heart"></i>
                                          </div>
                                         
                                          <?php 
                                          if($v1['image'] == 'dummy.jpg') { $itemImg = 'No_image.png'; } else { $itemImg =$v1['image']; }
                                          ?>
                                          <img src="https://deliveryguru.co.uk/admin/images/itemimages/<?php echo $itemImg; ?>" class="img-fluid item-img" style="width: 100%;height: 150px; object-fit: cover;" />
                                       </div>
                                       <div class="p-3 position-relative">
                                          <div class="list-card-body">
                                              <h6 class="mb-1" style="overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    height: 17px;
    white-space: normal;" ><a href="#" class="text-black" ><?php echo $v1['item_name']; ?></a></h6>
                                             
                                             <!--<p class="text-gray">North Indian • Indian • Pure veg</p>-->
                                             
                                             <p class="text-gray mb-2" style="    
   
    letter-spacing: 0ch;
    text-transform: none;
   font-size: 10px;
  
    padding: 0px 0px 0px 0px;
    font-variant-ligatures: no-common-ligatures;
    max-width: 100%;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    height: 28px;
    white-space: normal;"><?php echo $v1['item_desc']; ?></p>
                                             <p class="text-gray mb-3 time">
                                                 <?php 
                                                 if($v1['price']!=0){
                                                 ?>
                                                <span style="font-weight: 900;" class="text-dark rounded-sm pl-2 pb-1 pt-1 pr-2">£<?php echo $v1['price']; ?></span> 
                  <?php } else {?>
                  <span style="font-weight: 900;" class="text-dark rounded-sm pl-2 pb-1 pt-1 pr-2"></span> 
                  <?php }?>
                                             </p>
                                              <?php 
                                          if($v1['discount'] != '0' || $v1['discount'] != 0 ) {  ?>
                                              <div class="list-card-badge">
                                                <span class="badge badge-info">OFFER</span> 
                                                <small><?php echo $v1['discount']; ?> off </small> 
                                             </div> 
                                             <?php  }
                                          ?>
                                             
                                             
                                          </div>
                                       </div>
                                    </div>
                                    
                                    
                                    
                                    <!---->
                                    
                                    
                                      <div class="modal fade" id="popup<?php echo $v1['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle"><?php echo $v1['item_name']; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?php 
                                          if($v1['image'] == 'dummy.jpg') { $itemImg = 'No_image.png'; } else { $itemImg =$v1['image']; }
                                          ?>
                                          <img src="https://deliveryguru.co.uk/admin/images/itemimages/<?php echo $itemImg; ?>" class="img-fluid item-img" style="width: 100%;
    height: 180px;
    object-fit: cover;
    border-radius: 10px;" />
                                          
                                          
                                          
                                                <p class="text-gray" style="font-size: 12px;
   
    letter-spacing: 0ch;
    text-transform: none;
    color: rgb(118, 118, 118);
    padding: 10px 0px 10px 0px;
    font-variant-ligatures: no-common-ligatures;
    max-width: 100%;
    overflow: hidden;
   
    display: -webkit-box;
   
    -webkit-box-orient: vertical;
    white-space: normal;"><?php echo $v1['item_desc']; ?></p>
    
     <?php 
    foreach($v1['extra'] as $value) {
        ?>
          <div id="textbox"><p class="alignleft extitle" style="float: left;color: #000;
    font-size: 15px;
    font-weight: 500;"><?php echo $value['name']; ?>
          </p><p class="alignright" style="margin-bottom: 0px !important;padding:5px;float: right;" 
          id="'+result.addon[i].title.replaceAll(' ','_')+'">Required.</p></div><div style="clear: both;"></div>
                             
                             
         <div id= "extramenutable" ><p class="subextitle" style="font-size: 12px;
    color: #727272;"><?php echo $value['add_on_desc']; ?></p></div>
         
         
         
         <div class="boxes">
   <?php 
    foreach($value['addon'] as $value2) { ?>  
    
    
    
<div class="custom-control custom-checkbox">
<input type="checkbox" class="custom-control-input" id="<?php echo $value2['id']; ?>">
<label class="custom-control-label" for="<?php echo $value2['id']; ?>" name="<?php echo $value2['addon_name']; ?>" 
price="<?php echo $value2['addon_price']; ?>" 
onclick="addonClick(this)"><?php echo $value2['addon_name']; ?>
<small class="text-black-50" style="float: right;">£ <?php echo $value2['addon_price']; ?></small>
</label>
</div>
    
    

  <?php } ?>
</div>


       
  <?php 
}
?>
    
    
   <?php 
    foreach($v1['child'] as $value) {
        ?>
 <div class="" style="padding:10px;">
           <div ><label for="<?php echo $value['id']; ?>"><?php echo $value['item_name']; ?> </label>
           <span style="float:right;">£<?php echo $value['price']; ?></span></div><p style="font-size: 11px;margin-bottom:5px;">
               <?php echo $value['item_desc']; ?> </p></div>
               
               
               
               
                <?php 
    foreach($value['extra'] as $value1) {
        ?>
        
          <div class="boxes">
<div class="list-card-body">
<h6 class="mb-1"><a href="#" class="text-black"><?php echo $value1['name']; ?></a></h6>
<p class="text-gray mb-2"><?php echo $value1['add_on_desc']; ?></p>
</p>
</div>
<?php 
    foreach($value1['addon'] as $value2) { ?>  
<div class="custom-control custom-checkbox">
<input type="checkbox" class="custom-control-input" id="<?php echo $value2['id']; ?>">
<label class="custom-control-label" for="<?php echo $value2['id']; ?>" name="<?php echo $value2['addon_name']; ?>" 
price="<?php echo $value2['addon_price']; ?>" 
onclick="addonClick(this)"><?php echo $value2['addon_name']; ?>
<small class="text-black-50" style="float: right;">£ <?php echo $value2['addon_price']; ?></small>
</label>
</div>
<?php } ?>
</div>
  <?php 
}
?>


  <?php 
}
?>
<div class="Dinstrcution" stye="padding: 10px;">
     <p>Drop instructions <span id="<?php echo "charNum".$v1['id']; ?>" style="float:right;"></span></p>
         <textarea id="<?php echo "itminsid".$v1['id']; ?>" onkeyup="countChar(this)" style="width:100%;border-radius: 7px;border: 1px solid #000;height: 90px;"></textarea>
    <div ></div>
 </div>
 </div>
 
      <div class="modal-footer">

<span class="count-number float-right">
                           <button class="btn btn-outline-secondary  btn-sm left dec"> -</button>
                           <input class="count-number-input" type="text" value="1" readonly="">
                           <button class="btn btn-outline-secondary btn-sm right inc"> +</button>
                           </span>
        <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
        <button type="button" class="btn btn-primary" id="<?php echo $v1['id']; ?>" data-itemname="<?php echo $v1['item_name']; ?>" data-itemprice="<?php echo $v1['price']; ?>" data-itemid="<?php echo $v1['id']; ?>" onclick="addItem(this)" data-dismiss="modal">Add to cart</button>
      </div>
    </div>
  </div>
</div>
                                   
                                    
                                    
                                 </div>
                              <?php } ?>
                              </div>
                              </div>
                           <?php } ?>

