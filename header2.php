<nav class="navbar navbar-expand-sm navbar-light bg-light osahan-nav shadow-sm fixed-top container-fluid" style="max-width:1600px;">
         <div class="container">
            <button class="navbar-toggler ml-2 " type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
               <ul class="navbar-nav ml-auto">
               <li class="nav-item active">
                     <a class="nav-link" href="index.php">Home </a>
                  </li>
                  <li class="nav-item active">
                     <a class="nav-link" href="menu.php">Menu </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="deals.php"><i class="icofont-sale-discount"></i> Deals </a>
                  </li>
                 
                  <li class="nav-item">
                     <a class="nav-link" href="contact.php"> Contact Us</a>
                  </li>
                  <?php if(!isset($_SESSION['uid'])){?>
                  <li class="nav-item">
                     <a class="nav-link" href="login.php"> Login </a>
                  </li>
                  <?php } ?>
                   <?php if(isset($_SESSION['uid'])){?>
                  <li class="nav-item">
                     <a class="nav-link" href="myaccount.php"> My Account </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="myaccount.php"> Orders </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="logout.php"> Logout </a>
                  </li>
                  <?php } ?>
                                                    </ul>
            </div>

            <input name="" class="form-control mobileonly col-7 search-mob" placeholder="Search by Item Name...">

            <button class="navbar-toggler mr-2 mobCart dropdown-toggle mobileonly" type="button" id="mobCartBody" data-bs-toggle="dropdown" aria-expanded="false" style="color: #fff;">
                  <i class="cart icon"></i> <span class="badge-notification">0</span>
            </button>
            <span class="dropdown-menu" aria-labelledby="mobCartBody" style="background: #fff;
    border: 1px solid #000;
    width: 100%;">
               <div class="btn-group btn-toggle mobtoggle" style="width:95%;">
                  <button class="btn btn-lg btn-default <?php if(!isset($_SESSION['del_type']) || (isset($_SESSION['del_type']) && $_SESSION['del_type']=='delivery')) { ?> active<?php } ?>" id="delivery" data-id="delivery" >Delivery <small>In 40 min</small></button>
                     <button class="btn btn-lg btn-default <?php if(isset($_SESSION['del_type']) && $_SESSION['del_type']=='collection') { ?>active<?php } ?>" id="collection" data-id="collection" >Collection <small>In 15 min</small></button>
                  </div>
                  <span class="mobCartBody"></span>
            </span>
         </div>
      </nav>