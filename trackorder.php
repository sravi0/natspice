<?php
include 'src/config.php';
if(!isset($_SESSION['uid'])){
    header("Location:login.php");
}
else{
?>
<!doctype html>
<html lang="en">
 <head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- Favicons-->
   <title>NATURAL SPICE TANDOORI</title>
      
      <link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
      <!-- Bootstrap core CSS-->
      <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome-->
      <link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
      <!-- Font Awesome-->
      <link href="vendor/icofont/icofont.min.css" rel="stylesheet">
      <!-- Select2 CSS-->
      <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

      <link href="vendor/select2/css/select2.min.css" rel="stylesheet">
      <!-- Custom styles for this template-->
      <link href="css1/osahan.css" rel="stylesheet">
      <script src="js/jquery-3.5.1.js"></script>
   </head>
   <style>
   #snackbar {
  visibility: hidden;
  min-width: 250px;
  margin-left: -125px;
  background-color: green;
  color: #fff;
  text-align: center;
  border-radius: 2px;
  padding: 16px;
  position: fixed;
  z-index: 1;
  left: 50%;
  bottom: 90px;
  font-size: 17px;
}

#snackbar.show {
  visibility: visible;
  -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
  animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

@-webkit-keyframes fadein {
  from {bottom: 0; opacity: 0;} 
  to {bottom: 90px; opacity: 1;}
}

@keyframes fadein {
  from {bottom: 0; opacity: 0;}
  to {bottom: 90px; opacity: 1;}
}

@-webkit-keyframes fadeout {
  from {bottom: 90px; opacity: 1;} 
  to {bottom: 0; opacity: 0;}
}

@keyframes fadeout {
  from {bottom: 90px; opacity: 1;}
  to {bottom: 0; opacity: 0;}
}
         .unselectable {
        -webkit-user-select: none;
        -webkit-touch-callout: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        /*color: #cc0000;*/
      }
      fieldset, label { margin: 0; padding: 0; }
      .rating { 
  border: none;
  float: left;
}

.rating > input { display: none; } 
.rating > label:before { 
  margin: 5px;
  font-size: 1.25em;
  font-family: FontAwesome;
  display: inline-block;
  content: "\f005";
}

.rating > .half:before { 
  content: "\f089";
  position: absolute;
}

.rating > label { 
  color: #ddd; 
 float: right; 
}

/***** CSS Magic to Highlight Stars on Hover *****/

.rating > input:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

.rating > input:checked + label:hover, /* hover current star when changing rating */
.rating > input:checked ~ label:hover,
.rating > label:hover ~ input:checked ~ label, /* lighten current selection */
.rating > input:checked ~ label:hover ~ label { color: #FFED85;  } 
@media (min-width: 576px){
.modal-sm {
    max-width: 600px;
}
}
.rating-stars ul {
  list-style-type:none;
  padding:0;
  
  -moz-user-select:none;
  -webkit-user-select:none;
}
.rating-stars ul > li.star {
  display:inline-block;
  
}

/* Idle State of the stars */
.rating-stars ul > li.star > i.fa {
  font-size:2.5em; /* Change the size of the stars */
  color:#ccc; /* Color on idle state */
}

/* Hover state of the stars */
.rating-stars ul > li.star.hover > i.fa {
  color:#FFCC36;
}

/* Selected state of the stars */
.rating-stars ul > li.star.selected > i.fa {
  color:#FF912C;
}

   </style>
   <body class="unselectable">
        <div id="snackbar"></div>
  <?php 
  include("headerot.php");
  ?>
  <div class="modal fade" id="edit-profile-modal" tabindex="-1" role="dialog" aria-labelledby="edit-profile" aria-hidden="true">
         <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="edit-profile">Order Feedback</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <form id="form1">
                     <div class="form-row">
                         <div id="snackbar1" style="display:none;"></div>
                         <p style="width: 100%;" id="order_num"></p><br>
                         <form id="myform">
                          <p id="reviewnameErr" style="color:red"></p>
                        <div class="col-md-12 comments" style="padding: 0px 0px 0px 27px;">
                         <div class="form-group col-md-12 mb-0">
                           <label>Name </label>
                           <input type="text" value="" id="reviewName" class="form-control" placeholder="Enter Name" onpaste="return false;" onCopy="return false" onkeypress="allowAlphaNumericSpace(event)" onCut="return false" onDrag="return false" onDrop="return false" maxlength="17" autocomplete=off>
                           
                        </div>
                        <div class="form-group col-md-12">
                           <label>Comments
                           </label>
                           <textarea id="reviewComment" class="form-control" placeholder="Enter your Comments" onpaste="return false;" onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false" autocomplete=off >
                            </textarea>
                        </div>
                        <div class="form-group col-md-12 rating-stars">
                           <label>Rating </label></br>
                           <input id="ratingCount" type="hidden">
                         <ul id='stars'>
      <li class='star' title='Poor' data-value='1'>
        <i class='fa fa-star fa-fw'></i>
      </li>
      <li class='star' title='Fair' data-value='2'>
        <i class='fa fa-star fa-fw'></i>
      </li>
      <li class='star' title='Good' data-value='3'>
        <i class='fa fa-star fa-fw'></i>
      </li>
      <li class='star' title='Excellent' data-value='4'>
        <i class='fa fa-star fa-fw'></i>
      </li>
      <li class='star' title='WOW!!!' data-value='5'>
        <i class='fa fa-star fa-fw'></i>
      </li>
    </ul>
                       </br></br> </div>
                         <div class="col-md-12 commentsMain" style="display:none;">
                             <div class="nameDiv">
                                 <h6><b>Test</b></h6>
                                 <p>20/12/2021 | 02:00 PM</p>
                                 <hr>
                                 <p>Food is Awesome, Fast Delivery, Good response</p>
                                 <p>
                                       <fieldset class="rating">
    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
    <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
    <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
    <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
    <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
    <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
    <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
</fieldset>
                                </br> </p>
                             </div>
                         </div>
                        
                          <div class="col-md-12 commentsMain" style="display:none;">
                             <div class="nameDiv">
                                 <h6><b>Test</b></h6>
                                 <p>20/12/2021 | 02:00 PM</p>
                                 <hr>
                                 <p>Food is Awesome, Fast Delivery, Good response</p>
                                 <p>
                                       <fieldset class="rating">
    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
    <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
    <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
    <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
    <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
    <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
    <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
</fieldset>
                               </br>  </p>
                             </div>
                         </div>
                       
                          <div class="col-md-12 commentsMain" style="display:none;">
                             <div class="nameDiv">
                                 <h6><b>Test</b></h6>
                                 <p>20/12/2021 | 02:00 PM</p>
                                 <hr>
                                 <p>Food is Awesome, Fast Delivery, Good response</p>
                                 <p>
                                       <fieldset class="rating">
    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
    <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
    <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
    <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
    <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
    <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
    <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
</fieldset>
                                 </br></p>
                                 
                             </div>
                         </div>
                       
                         </div>
                       </form>
                    
                        
                     </div>
                  </form>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn d-flex w-50 text-center justify-content-center btn-outline-primary" onclick="cancelreview()">CANCEL
                  </button><button type="button" class="btn d-flex w-50 text-center justify-content-center btn-primary" onclick="addReview()">Submit</button>
               </div>
            </div>
         </div>
      </div>
      <section class="section bg-white osahan-track-order-page position-relative">
          <input type="hidden" id="output">
<input type="hidden" id="output1">
<input type="hidden" id="getorderno" value="<?php echo $_GET['orderid'];?>">
							<input type="hidden" id="getuserlat">
							<input type="hidden" id="getuserlong">
							<input type="hidden" id="getdriverlat">
							<input type="hidden" id="getdriverlong">
         <!--<iframe class="position-absolute" src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d13696.650704896498!2d75.82434255!3d30.8821099!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v15551842020550!5m2!1sen!2sin" width="100%" height="676" frameborder="0" style="border:0" allowfullscreen></iframe>-->
         <!--<div class="position-absolute" id="map" width="100%" height="800px" frameborder="0" style="border:0" allowfullscreen></div>-->
         <!--<iframe src="" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>-->
         <iframe class="position-absolute" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2239.9903104951263!2d-4.344273684065866!3d55.84548278057824!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x488848a6563af0ab%3A0x59f22cd465465796!2sNatural%20Spice%20Tandoori!5e0!3m2!1sen!2sin!4v1644571177982!5m2!1sen!2sin"  width="100%" height="800px" frameborder="0" style="border:0" allowfullscreen></iframe>
        <div class="container pt-5 pb-5">
            <div class="row d-flex align-items-center">
               <div class="col-md-6 text-center pb-4">
                  <div class="osahan-point mx-auto"></div>
               </div>
               <div class="col-md-6">
                  <div id="out">
                  </div>
                 
               </div>
            </div>
         </div>
      </section>
      
      <footer class="pt-4 pb-4 text-center">
         <div class="container">
            <p class="mt-0 mb-0">© Copyright 2022 DeliveryGuru. All Rights Reserved</p>
           
         </div>
      </footer>
      <!-- jQuery -->
      <!-- <script src="vendor/jquery/jquery-3.3.1.slim.min.js" type="788aa149b043702ff5068d84-text/javascript"></script> -->
      <!-- Bootstrap core JavaScript-->
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js" type="788aa149b043702ff5068d84-text/javascript"></script>
      <!-- Select2 JavaScript-->
      <script src="vendor/select2/js/select2.min.js" type="788aa149b043702ff5068d84-text/javascript"></script>
      <!-- Custom scripts for all pages-->
      <script src="js/custom.js" type="788aa149b043702ff5068d84-text/javascript"></script>
   <script src="js/rocket-loader.min.js" data-cf-settings="788aa149b043702ff5068d84-|49" defer=""></script></body>
</html>
<script>

function addReview(){
    var orderno = "<?php echo $_GET['orderid']; ?>";
    var hotelId='200';
    var uid="<?php echo $_SESSION['uid'];?>";
    var frate=document.getElementById('ratingCount').value;
    var comment=document.getElementById('reviewComment').value;
    var name=document.getElementById('reviewName').value;
    var pr_rate ='' ;
    var p_rate = '';
    var c_rate = '';
   
      document.getElementById('reviewnameErr').innerHTML='';
    if(name.trim()==''){
        document.getElementById('reviewnameErr').innerHTML='Please Enter Name';
        return false;
    }
    if(comment.trim()==''){
         document.getElementById('reviewnameErr').innerHTML='Please Enter Comment';
        return false;
    }
   
    // console.log(frate);
      $.ajax({
         type: "POST",
         url: 'https://deliveryguru.co.uk/dg_api/addreview',
         // url: 'http://deliveryguru.co.uk/dg_api/placeOrder',
         dataType: "json",
         crossDomain: true,
         data: {
           'hotel_id':'200',
           'orderno':orderno,
           'uid':uid,
           'f_rate':frate,
           'p_rate':'0',
           'pr_rate':'0',
           'c_rate':'0',
           'review':comment.trim(),
           'status':'0'
         },
         cache: "false",
         success: function(json) {
            // alert(JSON.stringify(json));
    // document.getElementById("myform").reset();
          document.getElementById('reviewComment').value='';
    document.getElementById('reviewName').value='';
            jQuery("#edit-profile-modal").modal("hide");
      
          //  userlogin1();
        //   document.getElementById("addressform").reset();
            var x = document.getElementById("snackbar");
           x.innerHTML="Review Added Successfully";
           x.style="backgrond:green";
           x.className = "show";
           setTimeout(function(){ x.className = x.className.replace("show", ""); window.location.reload();}, 3000);
        //   getaddress();

         },
         error: function(json) {
            alert(JSON.stringify(json));

         }
      });
}

$(document).ready(function(){
  
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
    var msg = "";
    if (ratingValue > 1) {
        msg = "Thanks! You rated this " + ratingValue + " stars.";
    }
    else {
        msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
    }
    responseMessage(msg,ratingValue);
    
  });
  
  
});


function responseMessage(msg,count) {
//  alert(msg);
 document.getElementById('ratingCount').value=count;
}
function getorders2(){
$orderno="<?php echo $_GET['orderid']; ?>";
$uid="<?php echo $_SESSION['uid'];?>";
// $token =sessionStorage.getItem('token');
var status="";
var deliverytype="";
var obj=[];
$.ajax({
					 url: 'https://deliveryguru.co.uk/dg_api/orderItems/'+$orderno,
					 type: "GET",
					 crossDomain: true,
					 dataType: "json",
					 cache: "false",
					 success: function(result) {
                      console.log(result);
					 //Write your code here
					 var preorder='';
					 if(result.length!=0){
						if(result[0].orderStatus=='5')
						{ status="order in kitchen"; }
						else if(result[0].orderStatus=='1')
						{ status="order accepted and assigned to driver"; }
						else if(result[0].orderStatus=='7')
						{ status="food is Ready to Deliver";}
						else if(result[0].orderStatus=='2')
						{ status="Order Accepted and in process";}
						else if(result[0].orderStatus=='3')
						{ status="Order completed";}
						else if(result[0].orderStatus=='6')
                        { status="Order Rejected by Restaurant";}
                        	else if(result[0].orderStatus=='12')
                        { 
                            status="Preorder Placed";
                            preorder='<p>Pre order';
                            if(result[0].delivery_type=='0'){
                            preorder+=' Delivery '; 
                            }
                            else{
                                preorder+=' Collection '; 
                            }
                            preorder+='Date and Time: '+result[0].day+' | '+result[0].time+'<p>';
                        }
                         	else if(result[0].orderStatus=='13')
                        { 
                            status="Preorder Accepted";
                            preorder='<p>Pre order';
                             if(result[0].delivery_type=='0'){
                            preorder+=' Delivery '; 
                            }
                            else{
                                preorder+=' Collection '; 
                            }
                            preorder+='Date and Time: '+result[0].day+' | '+result[0].time+'<p>';
                        }
						if(result[0].delivery_type=='0'){
						    var dfee='<p class="mb-0 font-weight-bold text-black">Delivery Fee  <span class="float-right text-secondary">(+)&#163;'+Number(result[0].delivery_charges).toFixed(2)+'</span></p>';
						     var dfee1='<p class="mb-0 font-weight-bold text-black">Admin and Carrybag Fee  <span class="float-right text-secondary">(+)&#163;0.55</span></p>';
							deliverytype="Delivery";
							var deliverytext="DELIVER TO";
							var deliveradd='<h6 class="mb-1 mt-1"><span class="text-black"><i class="icofont-map-pins"></i> '+result[0].username+' </span>';
               deliveradd +='        </h6>';
               deliveradd +='        <p class="text-gray mb-0">'+result[0].home_address+'</p>';
               deliveradd +='        <p>'+result[0].email+',</p>';
               deliveradd +='<p>'+result[0].contact+'</p>';
               if(result[0].landmark!=null || result[0].landmark!=''){
                deliveradd+='<p>Delivery Instruction: '+result[0].landmark+'</p>';
               }
						}
						else{
							deliverytype="Collection";
							var dfee='';
							var dfee1='<p class="mb-0 font-weight-bold text-black">Admin and Carrybag Fee  <span class="float-right text-secondary">(+)&#163;0.55</span></p>';
							var deliverytext="COLLECT BY";
								var deliveradd='<h6 class="mb-1 mt-1"><span class="text-black"><i class="icofont-map-pins"></i> '+result[0].username+' </span>';
               deliveradd +='        </h6>';
               deliveradd +='        <p>'+result[0].email+',</p>';
                deliveradd +='<p>'+result[0].contact+'</p>';
               
						}
                  if(result[0].payment_type=="COD"){
                     paytxt="NOT PAID";
                  }
                  else{
                     paytxt="PAID";
                  }
				var today  = new Date(result[0].orderTime);
					//debugger
               obj +='<div class="bg-white p-4 shadow-lg mb-2">';
               obj +='<div class="mb-2"><small>Order #'+result[0].order_no+'<a class="float-right font-weight-bold" href="myaccount.php"><i class="icofont-arrow-left"></i>Back</a>  <a class="float-right font-weight-bold" href="#"> </a> </small></div>';
               obj +='<h6 class="mb-1 mt-1"><a class="text-black">'+result[0].hotel_name+'';
               obj +='</a>';
               obj +='</h6>';
               obj +='<p class="text-gray mb-0"><i class="icofont-clock-time"></i> '+today.toLocaleTimeString(navigator.language, {hour: '2-digit', minute:'2-digit',hour12: true})+'| '+result[0].order_details.length+' Items | &#163;'+(result[0].amount)+' | Order Type: '+deliverytype+'</p>';
              obj+=preorder;
              obj+='<p onClick="review()" style="color:red;">Click here to review order</p>';
               obj +='</div>';
               obj +='<div class="bg-white p-4 shadow-lg">';
               obj +='<div class="osahan-track-order-detail po">';
               obj +='   <h5 class="mt-0 mb-3">Order Details</h5>';
               obj +='   <div class="row">';
               obj +='     <div class="col-md-5">';
               obj +='        <small>FROM</small>';
               obj +='        <h6 class="mb-1 mt-1"><a class="text-black"><i class="icofont-food-cart"></i> '+result[0].hotel_name+'';
               obj +='           </a>';
               obj +='        </h6>';
               obj +='        <p class="text-gray mb-5">'+result[0].hotel_address+'</p>';
               obj +='        <small>'+deliverytext+'</small>';
               obj +=deliveradd;
               obj +='     </div>';
               obj +='     <div class="col-md-7">';
               obj +='        <div class="mb-2"><small><i class="icofont-list"></i> '+result[0].order_details.length+' ITEMS</small></div>';
               for(let i=0;i<result[0].order_details.length;i++){
               obj +='        <p class="mb-2"><i class="icofont-ui-press text-danger food-item"></i> '+result[0].order_details[i].item_name+' x '+result[0].order_details[i].qty+'   <span class="float-right text-secondary">&#163;'+Number(result[0].order_details[i].amount*result[0].order_details[i].qty).toFixed(2)+'</span></p>';
               if(result[0].order_details[i].notes!=''){
               obj+='<span style=""font-size:10px;>'+result[0].order_details[i].notes+'<span>';
               }
               console.log(result[0].order_details[i].extra);
               for(let j=0;j<result[0].order_details[i].extra.length;j++){
               // obj +='<p class="mb-2" style="font-size: 10px;"><i class="icofont-ui-add text-danger food-item"></i> '+result[0].order_details[i].extra[j].addon_name+'<span class="float-right text-secondary">&#163;'+Number(result[0].order_details[i].extra[j].addon_price).toFixed(2)+'</span></p>'; 
               obj +='<p class="mb-2" style="font-size: 10px;"><i class="icofont-ui-add text-danger food-item"></i> '+result[0].order_details[i].extra[j].qty+' X '+result[0].order_details[i].extra[j].addon_name+'<span class="float-right text-secondary">&#163;'+Number(result[0].order_details[i].extra[j].qty*result[0].order_details[i].extra[j].addon_price).toFixed(2)+'</span></p>';
                   
               }
               }
               obj +='        <hr>';
               obj+='<p class="mb-0 font-weight-bold text-black">Discount';
             
                  obj+='<span class="float-right text-secondary">(-)&#163;'+Number(result[0].discount).toFixed(2)+'</span></p>';
                 if(result[0].cDiscount!='' && result[0].discount>=20){
               obj+='<span style="font-size: 9px;color: green;">You have applied coupon of  &#163; '+result[0].discount+' Discount</span>'; 
               }
               obj +=dfee;
               obj +=dfee1;
               obj +='        <p class="mb-0 font-weight-bold text-black">TOTAL  <span class="float-right text-secondary">&#163;'+(result[0].amount)+'</span></p>';
               obj +='        <p class="mb-0 text-info"><small>'+paytxt+'';
            //   obj +='           <span class="float-right text-danger">&#163;'++' OFF</span></small>';
               obj +='        </p>';
               obj +='     </div>';
               obj +='  </div>';
               obj +='</div>';
               obj +='</div>';
               obj +='<div class="bg-white p-4 shadow-lg mt-2">';
               obj +='<div class="row text-center">';
               if(result[0].orderStatus=='3')
						{
               obj +='  <div class="col" style="">';
               obj +='     <i class="icofont-tasks icofont-3x text-info"></i>';
               obj +='     <p class="mt-1 font-weight-bold text-dark mb-0">Completed</p>';
               obj +='     <small class="text-info mb-0">NOW</small>';
               obj +='  </div>';
                  }
                  else if(result[0].orderStatus=='5')
						{
               obj +='  <div class="col">';
               obj +='     <i class="icofont-check-circled icofont-3x text-success"></i>';
               obj +='     <p class="mt-1 font-weight-bold text-dark mb-0">Order Ready to Deliver</p>';
               obj +='     <small class="text-success mb-0"></small>';
               obj +='  </div>';
                  }
                  else if(result[0].orderStatus=='4')
						{
               obj +='  <div class="col">';
               obj +='     <i class="icofont-delivery-time icofont-3x text-primary"></i>';
               obj +='     <p class="mt-1 font-weight-bold text-dark mb-0">Order Picked Up</p>';
               obj +='     <small class="text-primary mb-0">LATER (ET : 30min)</small>';
               obj +='  </div>';
                  }
                  else if(result[0].orderStatus=='2')
						{
               obj +='  <div class="col">';
               obj +='     <i class="icofont-check-circled icofont-3x text-success"></i>';
               obj +='     <p class="mt-1 font-weight-bold text-dark mb-0">Order Placed Waiting For hotel Response</p>';
               obj +='     <small class="text-success mb-0"></small>';
               obj +='  </div>';
                  }
                    else if(result[0].orderStatus=='12')
						{
               obj +='  <div class="col">';
               obj +='     <i class="icofont-check-circled icofont-3x text-success"></i>';
               obj +='     <p class="mt-1 font-weight-bold text-dark mb-0">Pre Order Placed Waiting For Restaurant Response</p>';
               obj +='     <small class="text-success mb-0"></small>';
               obj +='  </div>';
                  }
                  else if(result[0].orderStatus=='6')
						{
               obj +='  <div class="col">';
               obj +='     <i class="icofont-close-circled icofont-3x text-success"></i>';
               obj +='     <p class="mt-1 font-weight-bold text-dark mb-0">Order Rejected By Hotel</p>';
            //   obj +='     <small class="text-danger mb-0">Reason: '+result[0].note+'</small>';
               obj +='     <small class="text-danger mb-0">More info please call 01418825400</small>';
               obj +='  </div>';
                  }
                     else if(result[0].orderStatus=='7')
						{
               obj +='  <div class="col">';
               obj +='     <i class="icofont-food-cart icofont-3x text-success"></i>';
               obj +='     <p class="mt-1 font-weight-bold text-dark mb-0">Order in Kitchen</p>';
               obj +='     <small class="text-success mb-0"></small>';
               obj +='  </div>';
                  }
                     else if(result[0].orderStatus=='13')
						{
               obj +='  <div class="col">';
               obj +='     <i class="icofont-food-cart icofont-3x text-success"></i>';
               obj +='     <p class="mt-1 font-weight-bold text-dark mb-0">Pre Order Accpted </p>';
               obj +='     <small class="text-success mb-0"></small>';
               obj +='  </div>';
                  }
               obj +='</div>';
               obj +='</div>';
					
					 
					 }


					 else{
						 obj="<tr><td colspan='6'>No Orders Found</td></tr>";
						
					 }
					 $("#out").html(obj)
				 }
		   }); 
}
getorders2();

setInterval(function(){
	getorders2();
	   },12000);
</script>
<!--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0Red6O9d3jz202ULoHpGmZ7rBxfPebbfs&callback=initiazemap&libraries=places"></script>	-->
   
<script>
 function getlocation(){
	debugger
	$orderno=$("#getorderno").val();
$.ajax({
					 url: 'https://deliveryguru.co.uk/dg_api/showByIdorderStatus/'+$orderno,
					 type: "POST",
					 crossDomain: true,
					 dataType: "json",
					 cache: "false",
					 success: function(result) {
					 //Write your code here
					 if(result.length==0){
				// 		alert("empty");
					 }
					 else{
				// 		alert(result[0].lat);
						document.getElementById("getuserlat").value = result[0].lat; 
						document.getElementById("getuserlong").value = result[0].longt; 
						document.getElementById("getdriverlat").value = result[0].olat; 
						document.getElementById("getdriverlong").value = result[0].olongt; 
				// 		getpath();
					 }
					
					 }
				 
		   }); 
}
getlocation();
 
    </script>
    <script>
    function cancelreview(){
    
  var modal = document.getElementById("edit-profile-modal");
  $("#edit-profile-modal").modal("hide");
 document.getElementById("form1").reset();
  
  
}
    function review(){
        var orderno = "<?php echo $_GET['orderid']; ?>";
         $("#edit-profile-modal").modal("show");
          document.getElementById("order_num").innerHTML = 'Order No: '+orderno; 
    }
     $(document).ready(function() {
          $('body').bind('cut copy', function(e) {
              e.preventDefault();
            });
        $("body").on("contextmenu", function(e) {
              return false;
            });
        });
    function allowAlphaNumericSpace(e) {
  var code = ('charCode' in e) ? e.charCode : e.keyCode;
  if (!(code == 32) && // space
    !(code > 47 && code < 58) && // numeric (0-9)
    !(code > 64 && code < 91) && // upper alpha (A-Z)
    !(code > 96 && code < 123)) { // lower alpha (a-z)
    e.preventDefault();
  }
}
</script>
   <?php } ?>