<?php
session_start();
//$_SESSION['cartItems'];




      if(isset($_GET['unset'])){
         unset($_SESSION['cartItems']);
      }
      if(isset($_GET['print'])){
         d($_SESSION);
      }

      function d($d){
         echo '<pre>';
         print_r($d);
         echo '</pre>';
      }

      //d($_SESSION);
      $addItems = array();
      if(isset($_POST['Items']) && $_POST['Items']!=''){
         $mainItems = json_decode($_POST['Items']);
         if(isset($_POST['addOns']) && count($_POST['addOns'])>0) { $addItems = json_decode($_POST['addOns']); } 

         $addItems = array(0=>array($mainItems, $addItems));


         if(!isset($_SESSION['cartItems'])){
            $_SESSION['cartItems'] = $addItems;
         } else {
            $_SESSION['cartItems'] = array_merge($addItems, $_SESSION['cartItems']);
         }
        // $_SESSION['cartItems'] = array_reverse($_SESSION['cartItems']);
      }

      if(isset($_POST['action'])) {
         if($_POST['action']=='update') {
            if($_POST['qty']<=0){
               unset($_SESSION['cartItems'][$_POST['id']]);
            } else {
               $_SESSION['cartItems'][$_POST['id']][0][3] = $_POST['qty'];
            }
         }
         if($_POST['action']=='note') {
            $_SESSION['cartItems'][$_POST['id']][0][4] = $_POST['qty'];
         }
         if($_POST['action']=='del_type') {
            $_SESSION['del_type'] = $_POST['qty'];
         }
      } 
$totPrice = 0;

if(isset($_SESSION['cartItems']) && count($_SESSION['cartItems'])>0)
{
      //d(array_filter($_SESSION['cartItems']));
      $allItems = array_filter($_SESSION['cartItems']); 
      //d($allItems);
      ?>
      
      <div class="card-body p-2">
      <div class="mb-1 bg-white p-1 clearfix">
         <ul class="list-group list-group-flush">
         <?php
         foreach($allItems as $k => $val) { 
            
            $itemPrice = $val[0][2] * $val[0][4];
           // $totPrice = $totPrice + $itemPrice;

            $listAddons = array();
            $addOnPrice = 0;
            foreach($val[1] as $k2 => $val2) {
               $addOn = explode(',',$val2);  
               $addOnPrice += $addOn[3] * $addOn[4];
              // $totPrice = $totPrice + $addOnPrice;
               $listAddons[] = $addOn[2]; 
            }
            $addonList = implode(' &#9679; ', $listAddons);
            $item_addon_price = $itemPrice + $addOnPrice;
            ?>
         
            <li class="list-group-item" style="padding:0px 0px;">
                  
                  <div class="row">
                        <span class="col-10 p-1 itemTit">
                           <b><?php echo $val[0][4]; ?>x <?php echo $val[0][1]; ?></b>
                        </span>
                        <span class="col-2 pt-1">
                        <span  class="cart_item_pr">
                        <b>£ <?php echo number_format($item_addon_price, 2, '.', ''); ?></b>
                        </span>
                     </div>  
                     <?php 
                     if($addonList!='') { ?>     
                        <div class="row">
                        <div class="col-12">
                           <i class="itemNotes"> <?php echo $addonList; ?></i>
                        </div>
                        </div>
                     <?php   } 
                     if($val[0][5]!='') { ?>  
                     <div class="row">
                        <div class="col-12">
                           <i class="icofont-ui-edit"></i><i class="itemNotes"> <?php echo $val[0][5]; ?></i>
                        </div>
                     </div>
                     <?php   }   ?>
                     <div class="row text-red text-right"><a onClick="updateCart(<?php echo $k; ?>, 1, 'remove');"><u><i><b>Remove</b></i></u></a></div>
                     <hr style="color: #acacac;height: .5px;">
            </li>
            <?php
               $totPrice = $totPrice + $item_addon_price;
         }  
         $ser_fee = 0;
         $del_fee = 0;
         $disc_fee = 0;
         $subTot = $totPrice;
         $grandTot = $subTot + $del_fee - $disc_fee;
         ?>
         
         </ul>
         
            
                       
                        <h6 class="font-weight-bold mb-0 pt-2" style="font-size: 18px;"> Total  <span class="float-right">£ <?php echo number_format($subTot, 2, '.', ''); ?></span></h6>
                     <br>

                     <a href="checkout.php" class="btn btn-success btn-block btn-lg">Checkout <i class="icofont-long-arrow-right"></i></a>
                     
                     </div>

                     
         
      </div>
      <?php
}
else {
   ?>
   <center style="margin-top:12%;"><img src="https://grill-guru.co.uk/newdev/img/empty-cart.png" width="200" /><br><p>Your cart is empty<br>Please add items</p></center>
   <?
}
?>