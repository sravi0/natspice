<?php
include 'src/config.php';
function getHotelAPI() {
  $graph_url= 'https://deliveryguru.co.uk/dg_api/getmetadata/200/contact';
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $graph_url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  $output = curl_exec($ch); 
  curl_close($ch);
  return json_decode($output, true); 
}
$getHotelDetails = getHotelAPI();
$keyword=$getHotelDetails['result'][0]['allTag'];
$keyWord=$getHotelDetails['result'][0]['h1_tag'];
?>
<!doctype html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- Favicons-->
   <?php echo $keyword;?>
  
    
     <link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
     
      <!-- Bootstrap core CSS-->
      <link href="vendor1/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome-->
      <link href="vendor1/fontawesome/css/all.min.css" rel="stylesheet">
      <!-- Font Awesome-->
      <link href="vendor1/icofont/icofont.min.css" rel="stylesheet">
      <!-- Select2 CSS-->
      <link href="vendor1/select2/css/select2.min.css" rel="stylesheet">
      <!-- Custom styles for this template-->
      <link href="css1/osahan.css" rel="stylesheet">
      <script src="js/jquery-3.5.1.js"></script>
      <!-- End Facebook Pixel Code -->
   </head>
   <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-222117938-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-222117938-1');
</script>
   <style>
            .unselectable {
        -webkit-user-select: none;
        -webkit-touch-callout: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        /*color: #cc0000;*/
      }
      table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
 
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

   </style>
   <body class="unselectable">
      
     <?php include("headerot.php");?>
      
      <div class="loader" style="display:none" id="onloader">
	<div class="loader_inner"></div>
</div>
      <section class="section pt-5 pb-5">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
               <h1 class="font-weight-bold mt-0 mb-3" style="font-size:20px;"><?php echo $keyWord;?></h1>
               </div>
               <div id="restaurant-info" style="margin: auto;width: -webkit-fill-available;" class="bg-white rounded shadow-sm p-4 mb-4">
                              <div class="address-map float-right ml-5">
                                 <div class="mapouter">
                                    <div class="gmap_canvas">
                                    <iframe width="600" height="500" id="gmap_canvas" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2239.9903104951263!2d-4.344273684065866!3d55.84548278057824!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x488848a6563af0ab%3A0x59f22cd465465796!2sNatural%20Spice%20Tandoori!5e0!3m2!1sen!2sin!4v1644571177982!5m2!1sen!2sin" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                     </div>
                                 </div>
                              </div>
                              <h5 class="mb-4" id="hotel_name"></h5>
                             <p class="mb-3" id="hotel_add">  
                              </p>
                              <p class="mb-2 text-black" id="hotel_phone"></p>
                              <p class="mb-2 text-black" id="hotel_email"></p>
                              <p class="mb-2 text-black" id="hotel_status">
                              </p>
                              <div id="openingHours">
                                  
                              </div>
                              <hr class="clearfix">
                              <p class="text-black mb-0"><i class="icofont-info-circle text-primary mr-2"></i>If you suffer from any allergies then please inquire with a member of staff for full details<a class="text-info font-weight-bold" > Call us : 01418825400 </a></p>
                              <hr class="clearfix">
                              <h5 class="mt-4 mb-4" id="hotel_name1"></h5>
                              
                              <div class="border-btn-main mb-4">
                              <a class="border-btn text-success mr-2" href="#"><i class="icofont-check-circled"></i> Indian</a>
                                 <a class="border-btn text-success mr-2" href="#"><i class="icofont-check-circled"></i> Italian</a>
                                 <a class="border-btn text-success mr-2" href="#"><i class="icofont-check-circled"></i>Turkish</a>
                                
                              </div>
                           </div>
                    </div>
      </section>
      
  <?php
  include('Mainfooter.php');
  ?>
      <script>
          function gethomedet(){
    document.getElementById('onloader').style.display='block';

var obj=[];
$.ajax({
					 url: 'https://deliveryguru.co.uk/dg_api/getRestaurantsDetails/200',
					 type: "GET",
					 crossDomain: true,
					 dataType: "json",
					 cache: "false",
					 success: function(result) {
			         //console.log(result);
			         document.getElementById("hotel_name").innerHTML=result[0].hotel_name;
			         document.getElementById("hotel_name1").innerHTML=result[0].hotel_name+' Info';
			         document.getElementById("hotel_add").innerHTML='<i class="icofont-pin text-primary mr-2"></i>'+result[0].address;
			         document.getElementById("hotel_email").innerHTML='<i class="icofont-email text-primary mr-2"></i> order@deliveryguru.co.uk';
			         document.getElementById("hotel_phone").innerHTML='<i class="icofont-phone-circle text-primary mr-2"></i>'+result[0].hotel_mob;
			         if(result[0].rest_status==0){
			             var sts='<span class="badge badge-success" style="background:green !important;"> OPEN NOW </span>';
			         }	
			         else{
			             var sts='<span class="badge badge-danger" style="background:red !important;"> CLOSED NOW </span>';
			         }
			         //document.getElementById("hotel_status").innerHTML='<i class="icofont-clock-time text-primary mr-2"></i> Monday-Sunday 5pm-5am'+sts;
			         
                                 
             document.getElementById('onloader').style.display='none';

				 }
		   });
}
gethomedet();
function getopeningHours(){
    var obj=[];
$.ajax({
					 url: 'https://deliveryguru.co.uk/dg_api/getopeninghoursByHotelAndType/200/delivery',
					 type: "GET",
					 crossDomain: true,
					 dataType: "json",
					 cache: "false",
					 success: function(result) {
					       var d = new Date();
					     var n = d.getDay();
					      var openhours='';
                          var closehour='';
					       var event = new Date().toLocaleTimeString('en-GB', { timeZone: 'Europe/London' ,hour: 'numeric',minute: 'numeric', hour12: false});
					    var obj='';
					     var data=JSON.parse(result[0].openingtime);
					      console.log(n);
					    obj+='<table>';
    obj+='<tr>';
      obj+='<th>Day</th>';
    obj+='<th>Opening Hours</th>';
    obj+='<th>Closing Hours</th>';
    obj+='</tr>';
    if(n==1){
    obj+='<tr style="background: #135e00;color: #fff;">';
      openhours=data.monlunst;
    closehour=data.mondend;
    }else{
     obj+='<tr>';   
    }
    obj+='<td>Monday</td>';
    if(data.monlclose==1){
    obj+='<td><p style="color:red">Closed</p></td>';
    obj+='<td><p style="color:red">Closed</p></td>';
    }else {
    obj+='<td>'+data.monlunst+'</td>';
    obj+='<td>'+data.mondend+'</td>';
  
    }
  obj+='</tr>';
     if(n==2){
    obj+='<tr style="background: #135e00;color: #fff;">';
    openhours=data.tulst;
    closehour=data.tudend;
    }else{
     obj+='<tr>';   
    }
    obj+='<td>Tuesday</td>';
     if(data.tulclose==1){
    obj+='<td><p style="color:red">Closed</p></td>';
    obj+='<td><p style="color:red">Closed</p></td>';
    }else {
    obj+='<td>'+data.tulst+'</td>';
    obj+='<td>'+data.tudend+'</td>';
    
    }
  obj+='</tr>';
     if(n==3){
    obj+='<tr style="background: #135e00;color: #fff;">';
     openhours=data.wedlst;
    closehour=data.weddend;
    }else{
     obj+='<tr>';   
    }
    obj+='<td>Wednesday</td>';
     if(data.wedlclose==1){
    obj+='<td><p style="color:red">Closed</p></td>';
    obj+='<td><p style="color:red">Closed</p></td>';
    }else {
    obj+='<td>'+data.wedlst+'</td>';
    obj+='<td>'+data.weddend+'</td>';
   
    }
  obj+='</tr>';
     if(n==4){
    obj+='<tr style="background: #135e00;color: #fff;">';
     openhours=data.thlst;
    closehour=data.thdend;
    }else{
     obj+='<tr>';   
    }
    obj+='<td>Thursday</td>';
    if(data.thlclose==1){
    obj+='<td><p style="color:red">Closed</p></td>';
    obj+='<td><p style="color:red">Closed</p></td>';
    }else {
    obj+='<td>'+data.thlst+'</td>';
    obj+='<td>'+data.thdend+'</td>';
    }
  obj+='</tr>';
     if(n==5){
    obj+='<tr style="background: #135e00;color: #fff;">';
    openhours=data.frilst;
    closehour=data.fridend;
    }else{
     obj+='<tr>';   
    }
    obj+='<td>Friday</td>';
    if(data.frilclose==1){
    obj+='<td><p style="color:red">Closed</p></td>';
    obj+='<td><p style="color:red">Closed</p></td>';
    }else {
    obj+='<td>'+data.frilst+'</td>';
    obj+='<td>'+data.fridend+'</td>';
    }
  obj+='</tr>';
     if(n==6){
    obj+='<tr style="background: #135e00;color: #fff;">';
     openhours=data.satlst;
    closehour=data.satdend;
    }else{
     obj+='<tr>';   
    }
    obj+='<td>Saturday</td>';
     if(data.satlclose==1){
    obj+='<td><p style="color:red">Closed</p></td>';
    obj+='<td><p style="color:red">Closed</p></td>';
    }else {
    obj+='<td>'+data.satlst+'</td>';
    obj+='<td>'+data.satdend+'</td>';
    }
  obj+='</tr>';
     if(n==0){
    obj+='<tr style="background: #135e00;color: #fff;">';
      openhours=data.sunlst;
     closehour=data.sundend;
    }else{
     obj+='<tr>';   
    }
    obj+='<td>Sunday</td>';
    if(data.sunlclose==1){
    obj+='<td><p style="color:red">Closed</p></td>';
    obj+='<td><p style="color:red">Closed</p></td>';
    }else {
    obj+='<td>'+data.sunlst+'</td>';
    obj+='<td>'+data.sundend+'</td>';
   
    }
  obj+='</tr>';
obj+='</table>';
// openhours="09:00";
// event = "09:45";
if(event<openhours && event>closehour){
 
           var sts='<span class="badge badge-danger" style="background:red !important;"> CLOSED NOW </span> NATURAL SPICE Accepting Pre-Order <a href="menu.php">Order Now</a>';
         }
    else{
        var sts='<span class="badge badge-success" style="background:green !important;"> OPEN NOW </span>';
    }

 document.getElementById("hotel_status").innerHTML=sts;
document.getElementById('openingHours').innerHTML=obj;
					 }
});
}
getopeningHours();
      </script>
      <!-- jQuery -->
      <!-- <script src="vendor1/jquery/jquery-3.3.1.slim.min.js" type="588203e22900fa56a6292e20-text/javascript"></script> -->
      <!-- Bootstrap core JavaScript-->
      <script src="vendor1/bootstrap/js/bootstrap.bundle.min.js" type="588203e22900fa56a6292e20-text/javascript"></script>
      <!-- Select2 JavaScript-->
      <script src="vendor1/select2/js/select2.min.js" type="588203e22900fa56a6292e20-text/javascript"></script>
      <!-- Custom scripts for all pages-->
      <script src="js/custom.js" type="588203e22900fa56a6292e20-text/javascript"></script>
   <script src="js/rocket-loader.min.js" data-cf-settings="588203e22900fa56a6292e20-|49" defer=""></script>
   <script defer src="js/beacon.min.js" data-cf-beacon='{"rayId":"5d3af9b9cb941016","version":"2020.9.0","si":10}'></script>
     <script>
       $(document).ready(function() {
          $('body').bind('cut copy', function(e) {
              e.preventDefault();
            });
        $("body").on("contextmenu", function(e) {
              return false;
            });
        });
  </script>
</body>
</html>
