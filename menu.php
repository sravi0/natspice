<?php session_start();
//$_SESSION['cartItems'];
$linkID = '';
if(!isset($_SESSION['del_type'])) { $_SESSION['del_type']='delivery'; }
?>

<?php
function getAPI() {
  $graph_url= 'https://deliveryguru.co.uk/dg_api/menu_submenu/200';
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $graph_url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  $output = curl_exec($ch); 
  curl_close($ch);
  return json_decode($output, true); 
}
$getItems = getAPI();


function getHotelAPI() {
  $graph_url= 'https://deliveryguru.co.uk/dg_api/getRestaurantsDetails/200';
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $graph_url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  $output = curl_exec($ch); 
  curl_close($ch);
  return json_decode($output, true); 
}
$getHotelDetails = getHotelAPI();
$_SESSION['discountper']=$getHotelDetails[0]['discount'];
 $_SESSION['del_type_status']=$getHotelDetails[0]['delivery_status'];
//  $_SESSION['del_type_status']=0;
if($_SESSION['del_type_status']==1){
    $_SESSION['del_type']='collection';
}
else if($_SESSION['del_type_status']==2){
     $_SESSION['del_type']='delivery';
}
else if($_SESSION['del_type_status']==0){
   $_SESSION['del_type']='delivery'; 
}
function getmetaHotelAPI() {
  $graph_url= 'https://deliveryguru.co.uk/dg_api/getmetadata/200/Menu';
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $graph_url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  $output = curl_exec($ch); 
  curl_close($ch);
  return json_decode($output, true); 
}
$getmetaHotelDetails = getmetaHotelAPI();
$keyword=$getmetaHotelDetails['result'][0]['allTag'];
$keyWord=$getmetaHotelDetails['result'][0]['h1_tag'];
?>  
<!DOCTYPE html>
<html lang="en">
  <head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <?php echo $keyword;?>
          
         <link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
     <link href="https://grill-guru.co.uk/devV3/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
     
       <!--  <link href="vendor/select2/css/select2.min.css" rel="stylesheet">-->
         <!-- Custom styles for this template
         <link href="css/osahan.css" rel="stylesheet">
         <link href="vendor/icofont/icofont.min.css" rel="stylesheet">-->
         <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
         <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.1/font/bootstrap-icons.css">
         <!-- CSS only -->
      <!--  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    <!--<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap" rel="stylesheet">
      <link href="https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b2028ebab05857545f242caf2b4b48c/dist/semantic.min.css" rel="stylesheet" type="text/css" />-->
<!--<script src="https://code.jquery.com/jquery-2.1.4.js"></script>
<script src="https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b2028ebab05857545f242caf2b4b48c/dist/semantic.min.js"></script>-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://grill-guru.co.uk/devV3/menu-src/menu.min.css" rel="stylesheet">
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-222117938-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-222117938-1');
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Restaurant",
  "name": "Natural spice online",
  "image": "https://www.natural-spiceonline.co.uk/img/logo1.png",
  "@id": "",
  "url": "https://www.natural-spiceonline.co.uk",
  "telephone": "01418825400",
  "menu": "https://www.natural-spiceonline.co.uk/",
  "servesCuisine": "Indian | Italian | Turkish",
  "acceptsReservations": "false",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "1791, Paisley Road West, Cardonald",
    "addressLocality": "Glasgow",
    "postalCode": "G52 3SS",
    "addressCountry": "GB"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": 55.8454805,
    "longitude": -4.3420923
  },
  "openingHoursSpecification": [{
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday"
    ],
    "opens": "16:30",
    "closes": "00:20"
  },{
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": [
      "Friday",
      "Saturday"
    ],
    "opens": "16:30",
    "closes": "01:20"
  }] ,
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "4.4",
    "bestRating": "5",
    "worstRating": "0",
    "ratingCount": "97"
  }
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Natural spice online",
  "url": "https://www.natural-spiceonline.co.uk/",
  "logo": "https://www.natural-spiceonline.co.uk/img/logo1.png"
}
</script>


  <style>
    .card-header { padding: 0.75rem 1.25rem 0 1.25rem; }
    .count-number .btn { border: none; }
    .count-number-input { border: none; margin-left: 10px; }
    .count-number .bi { font-size: 30px;}
    .btn-number { border: none !important; }
    .inc_btn {
    font-size: 20px; color: #757070;}
.btn-outline-secondary:hover { background: none !important }
.form-control:disabled, .form-control[readonly] {  margin-top: 2px; font-size: 15px; }
.btn.focus, .btn:focus { box-shadow: none; }
.txtDiv { padding: 1rem 1rem 0 1rem; width: 75%; }
.restaurant-detailed-header  { position: absolute;  left: 50%;
    transform: translate(-50%, -120%);  }
  .restaurant-detailed-header img { height: 80px;  }
  .redbg,.redbg:hover { background: red !important; color: white; border: none !important; }
  .navbar-light .navbar-nav .nav-link { color: #000; } 
  .osahan-nav .nav-link {
    font-weight: 600;
    font-size: 12px;
}
@media (min-width: 1400px) {
.container, .container-lg, .container-md, .container-sm, .container-xl, .container-xxl {
    max-width: 1320px;
}}
h1, h2, h3, h4, h5 {font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif; line-height: 1.2857em;font-weight: 700; padding: 0; }
h5 { font-size: 1rem; }
.h2, h2 { font-size: 1.714rem;}
input[type=number] {
  -moz-appearance: textfield;
}
@media screen 
    and (min-device-width: 500px) 
    and (max-device-width: 3500px) 
    and (-webkit-min-device-pixel-ratio: 1) { 
      .menusWrapper {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    justify-content: space-around;
    grid-gap: 10px;
margin:0px;}
.menuInner {
    display: inline-grid;
    border: 1px solid #d2cbcb;
    break-inside: avoid;
    page-break-inside: avoid;
    margin: 0rem;
    background: #fff;
    box-shadow: 0 4px 11px -6px #b3aeae;
}
.sticky-div{width:auto;}
}
.rating > input { display: none; } 
.rating > label:before { 
  margin: 5px;
  font-size: 1.25em;
  font-family: FontAwesome;
  display: inline-block;
  content: "\f005";
}

.rating > .half:before { 
  content: "\f089";
  position: absolute;
}

.rating > label { 
  color: #ddd; 
 float: right; 
}
img.stickyLogo {
    height: 63px !important;
    width: 84px !important;
    
    z-index: 100;
    margin-left:-110px !important;
}
/***** CSS Magic to Highlight Stars on Hover *****/

.rating > input:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

.rating > input:checked + label:hover, /* hover current star when changing rating */
.rating > input:checked ~ label:hover,
.rating > label:hover ~ input:checked ~ label, /* lighten current selection */
.rating > input:checked ~ label:hover ~ label { color: #FFED85;  } 
@media (min-width: 600px){
.review-modal {
    max-width: 900px;
}
}
@media (max-width: 600px){
.review-modal {
    max-width: 93%;
}
}
@media (min-width: 576px){
.modal-sm {
    max-width: 600px;
}
}
.rating-stars ul {
  list-style-type:none;
  padding:0;
  
  -moz-user-select:none;
  -webkit-user-select:none;
}
.rating-stars ul > li.star {
  display:inline-block;
  
}

/* Idle State of the stars */
.rating-stars ul > li.star > i.fa {
  font-size:2.5em; /* Change the size of the stars */
  color:#ccc; /* Color on idle state */
}

/* Hover state of the stars */
.rating-stars ul > li.star.hover > i.fa {
  color:#FFCC36;
}

/* Selected state of the stars */
.rating-stars ul > li.star.selected > i.fa {
  color:#FF912C;
}
.star i{
    color: green;
}
.nameDiv{
    border: 1px solid #ccc; 
    padding: 11px;
    border-radius: 7px;
    box-shadow: 0px 1px 3px #ccc;
        height: 140px;
  margin: 0px 0px 20px 0px;
}
.reviewC{
    line-height: 1.5em;
    height: 3em;
    overflow: hidden;
}
.btn-toggle {
    border: 1px solid #e2e0e0;
    border-radius: 10px;
    margin: 5px 10px 0px 10px;
    padding: 5px;
    background: #e2e0e0;
}
.btn-toggle .active {
    background: #379ed0;
    font-weight: 400;
    color: #fff;
    width: 65%;
   
    border-radius: 10px !important;
}
  </style>
<script>
<?php
$ids = array_column($getItems, 'id');
//print_r($ids);

?>
//const catIDs = [174, 147, 1997, 202];
const catIDs = [<?php echo implode(',', $ids); ?>];
//const catIDs = [];
let fLen = catIDs.length;

var allItems = [];

window.onload = function(){
  
  var f = (function(){
      var xhr = [], i;
      for (let i = 0; i < fLen; i++) {
          (function(i){
              var j = catIDs[i];
            
              const xhr = new XMLHttpRequest();
              xhr.open('GET', 'api-test.php?catId='+j);
              xhr.responseType = 'json';
              xhr.onload = function(e) {
                console.log(this.status);
                if (this.status == 200) {
                  //console.log('response', this.response); // JSON response  
                  allItems[j] = this.response;
                }
              };
              xhr.send();
          })(i);
      }
  })();

};

// PHP [closure.php]
//echo "Hello Kitty -> " . $_GET["data"];

</script>
</head>
 <style>
    @media only screen and (min-width: 1193px) {
           .discount-banner p{
               background: red;
    padding: 16px;
    color: #fff;
    width: 97%;
    font-size: 14px;
    font-weight: 600;
    border-radius: 5px;
           }
           .preorderDivSection{
  margin-top: 0%;text-align: left;  
}
.title{
    margin-top: 75px;padding: 10px;
}
    }
    @media only screen and (max-width: 1193px) {
           .discount-banner p {
    background: red;
    padding: 8px;
    color: #fff;
    width: 100%;
    font-size: 10px;
    font-weight: 600;
    border-radius: 5px;
}
   .preorderDivSection{
 text-align: left;  
}
.title{
   padding: 10px;
}

}

.preorderDivSection button{
    width:80%;
    padding:14px;
    font-size:15px;
    font-weight:600;
    border:none;
    text-transform:uppercase;
}
.title p{
    font-size: 1rem;
    font-weight: 500;
}
/* width */
::-webkit-scrollbar {
  width: 5px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
}
.card-header {
    padding: 0.75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgb(255 255 255);
    border-bottom: 1px solid rgba(0,0,0,.125);
}
#restOrder_status{
cursor:default;
}
      .alert-warning {
    color: #0b81ff;
    background-color: #ffffff;
    border-color: #00000038;
}
       </style>

  <body class="unselectable">
  <div id="snackbar"></div>

<!-- Menu addon Modal -->
<div class="modal fade" id="addonModal" tabindex="-1" aria-labelledby="addonModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addonModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="font-size: 36px;font-weight: 400;">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="itemDataModal" id="itemDataModal"></div>
        <div class="linear-background"></div>
          <div class="linear-background"></div>
          <div class="linear-background"></div>
        <div class="Addons accordionMenu" id="AddOns">
        </div>
      </div>
      <div class="alert alert-danger" role="alert"></div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<!-- Closed hotel Modal -->
<div class="modal fade" id="closedModal" tabindex="-1" role="dialog" aria-labelledby="closedModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
      <div class="modal-header">
              <h5 class="modal-title" style="text-align:center;">CLOSED NOW!</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closemodal()" id="closemessagebtn">
                <span aria-hidden="true">&times;</span>
              </button>
      </div>
      <div class="row no-gutters">
        <div class="col-md-6 ">
          <div class="modal-body1 p-0 text-center d-flex1">
              <img src="img/closepopimg.jpg" style="width:100%;">
          </div>
        </div>
        <div class="col-md-6 ">
          <div class="modal-body1 p-2 d-flex align-items-center">
            <div class="text w-100 text-center">
                <div id="prepopupcont">
                    
                </div>
                
            <div id="prepopupcont1">
              <h2 class="mb-0">Pre Order</h2>  <br>
              <form action="#" class="code-form">
                <div class="form-group p2">
             
                <span class="col-4 p-1">
                Date:<br>
                                    <div class="ui calendar" id="preorderdateformat1" style="">
                            
    <div class="ui input left icon" style="width: 100%;">
      <i class="calendar icon"></i>
      <input type="text" id="pre_date" class="form-control"   placeholder="Schedule Date and Time" style="width: 100% !important; background:white; " readonly/>
    </div>
  </div>
               
                Time:<br> <select id="pre_time" class="form-control">     
                <option value="">Please Select Time</option>
               
               
                <option value="17:00">17:00</option>
                <option value="18:00">18:00</option>
                <option value="19:00">19:00</option>
                <option value="20:00">20:00</option>
                <option value="21:00">21:00</option>
                <option value="22:00">22:00</option>
                <option value="23:00">23:00</option>
               </select>
                </div>
                <div class="alert alert-danger" role="alert"></div>
              </form>
              </div>
              <a class="btn btn-primary d-block py-3 redbg" onClick="PreOrder();" style="color:#fff;cursor:pointer;font-size:16px;padding:8px !important;">PRE-ORDER </a>
            </div>
          </div>
         </div>
        </div>
      </div>
    </div>
</div>
<div class="modal fade" id="allergyModal" tabindex="-1" role="dialog" aria-labelledby="closedModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
      <div class="modal-header">
              <h5 class="modal-title" style="text-align:center;font-size:16px;">Food Allergy and FHIS Rating</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closemodal1()" id="closemessagebtn">
                <span aria-hidden="true">&times;</span>
              </button>
      </div>
      <div class="row no-gutters" style="padding: 28px;">
          <h6><b>Do you have a food allergy?</b></h6>
       <p style="
    text-align: justify;">If you have a food allergy or intolerance (or someone you're ordering for has), 
phone the restaurant on 01418825400 before placing your order and read what this 
restaurant has to say about allergies.
Please Do not order if you cannot get the allergy information you need.</p>
<h6 style="width: 100%;"><b>FHIS Rating : </b></h6></br>
<p style="text-align: center;"><img src="img/pass.jpeg" style="width: 106px;margin-left: 161px;"></p>
<p style="text-align: justify;">NATURAL SPICE TANDOORI has a FHIS rating of Pass. This information was updated FSS Website. The current rating is on their page on the FSS Website. Allergies, intolerances and dietary requirements: Before ordering, please contact the restaurant directly and ask to speak to a member of staff who can assist if you require information about ingredients and help cater for your needs.</p>
        </div>
      </div>
    </div>
</div>
<div class="modal fade" id="reviewModal" tabindex="-1" role="dialog" aria-labelledby="closedModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered review-modal" role="document">
      <div class="modal-content">
      <div class="modal-header">
              <h5 class="modal-title" style="text-align:left;font-size:16px;" id="reviewTile">Reviews</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closemodal2()" id="closemessagebtn">
                <span aria-hidden="true">&times;</span>
              </button>
      </div>
      <div class="row no-gutters" style="padding: 28px;height: 75vh;
    overflow-y: scroll;" id="reviewList">
       
       
                             </div>
      </div>
    </div>
</div>
<?php include "header3.php"; ?>
<section class="restaurant-detailed-banner mobileonly" style="margin-top: -80px;">
         <div class="text-center">
            <img class="img-fluid cover" src="img/Banner.jpg">
         </div>
         <div class="restaurant-detailed-header">
            <div class="container" style="max-width:1600px;">
               <div class="row d-flex align-items-end">
                   <div class="col-md-4">
                <img class="img-fluid mr-3 float-left" alt="NATURAL SPICE TANDOORI food delivery Logo" src="img/logo1.png" style="width:105px;">        
                   </div>
                   <div class="col-md-8">
                       
                   </div>
               
               </div>
               
            </div>
         </div>
      </section>  
      
    <main>
    <!--<span class="float">
      <i class="cart icon"></i>
      <span class="badge rounded-pill badge-notification bg-danger">0</span>
    </span>  -->
   
        <article class="article" style="margin: 0px;
    padding: 0px;">
          <div>
           <divclass="col-md-12">
             <div class="row" style="margin-left: 0px;">
                 
                 <div class="col-md-6" style="padding:0px">
                      <div class="banner-new desktoponly" style="margin-top: 57px; padding: 10px;">
                      <img src="img/Banner.jpg" style="width:100%; border-radius:15px;height: 197px;"  />
              <div class="row d-flex" style="position: relative;">
                      <div class="col-md-12" id="logoDiv" style="margin-top: -140px;
    text-align: center;">
                        <div class="">
                            <img class="img-fluid mr-3 pl-2" id="bannerLogo" alt="NATURAL SPICE TANDOORI food delivery Logo" src="img/logo1.png" style="width:100px;">
                        </div>
                       
                      </div>
                  </div>
                  </div>
                 </div>
                 <div class="col-md-6" style="padding:0px">
                     <div class="title" style="">
                         <div class="row">
                              <div class="col-md-8">
                          <h4><b>NATURAL SPICE TANDOORI</b> </h4>
                         <p> Indian | Italian | Turkish </p>
                         <!--<p><i class="bi bi-pin-map"></i> 48-52 Oswald Street,Glasgow, G1 4PL, <i class="bi bi-telephone-fill"></i> 0141 221 20277</p>-->
                         <!--<p><i class="bi bi-clock"></i> Open At - 17:00 - 05:00 </p>-->
                         <p>Minimum Delivery Fee - &#163;2 | Minimum Order - &#163;10</p>
                         <p>Delivery 40 - 60min  | Collection 15 - 20min</p>
                     </div>
                        
                          <div class="preorderDivSection col-md-4">
                         <p id="star_review" class="rating-stars" style="margin-bottom: 0px;
    font-size: 23px;">            <!-- <i class="bi bi-star-fill" style="color:green;"></i>-->
    <!--                     <i class="bi bi-star-fill" style="color:green;"></i>-->
    <!--                     <i class="bi bi-star-fill" style="color:green;"></i>-->
    <!--                     <i class="bi bi-star-fill" style="color:green;"></i>-->
    <!--                     <i class="bi bi-star" style="color:green;"></i></p>-->
                         <p onclick="openReview()" style="margin-left: 24px;"><b><a style="text-decoration: underline;cursor: pointer;">View <span id="reviewCountt">0</span> Reviews</a></b></p>
                        <p>
                            <input type="hidden" id="restOrder_status">
                             <!--<button class="btn btn-primary" style="color:#fff;background:green;" id="restOrder_status">Open Now</button>-->
                             </p> 
                   <p id="sheduleOrder">
                       <!--<button class="btn btn-primary" style="color:blue;background:#fff;border:1px solid #ccc;" onclick="closedMsg()" > Shedule Order</button>-->
                       </p> 
                   <p id="preOrderButton">
                       <!--<button class="btn btn-primary" style="color:#0b81ff;background:#fff;border:1px solid #ccc;" onclick="closedMsg()" > Pre -order <i class="bi bi-caret-down-fill"></i></button>-->
                       </p> 
                    <!--<p><button class="btn btn-primary" style="color:#fff;background:green;">Book Table</button></p> -->
                    </div> 
                         </div>
                   
                        
                        
      <div class="discount-banner">
          <!--<img src="img/banner.jpeg" style="width:100%" />-->
         <div class="">
            <?php if($_SESSION['discountper']>0){?>
             <div>
                 <p style="font-size: 15px;"><i class="bi bi-tag"></i><b> Get <?php echo $_SESSION['discountper'];?>% Discount</b> on All Online Orders. T&Cs apply !</p>
             </div>
             <?php } ?>
         </div>
          </div>
                     </div>
                       
                 </div>
                
             </div>  
           </div>
           
          </div>
          <h1 class="font-weight-bold mt-0 mb-3" style="font-size:15px;text-align:center;"><?php echo $keyWord;?></h1>
         <div class="sticky-div"> 
                  <div class="scroller scroller-left"><i class="arrowPrev"></i></div>
               <div class="scroller scroller-right"><i class="arrowNext"></i></div>
               <div class="wrapper">
               <ul class="nav nav-tabs list cart-menu" id="myTab">
                <?php
                foreach($getItems as $k => $val) {
                    echo '<li><a data-id="'.$val['id'].'" id="anchorTag'.$val['id'].'" class="nav-item">'.$val['name'].'</a></li>';
                }
                ?>
                </ul>
               </div>
               </div>
      
      <div id="panel_groups" class="">
        
      <div class="discount-banner">
          <!--<img src="img/banner.jpeg" style="width:100%" />-->
         
          </div>
<div id="noitem-section">
                                
                                </div>
                           <?php
                           foreach($getItems as $k => $val) {
                              //echo '<li><a href="#'.$val['id'].'" id="anchorTag'.$val['id'].'" class="nav-item">'.$val['name'].'</a></li>';
                              //print_r($val); exit;
                           ?>
                           <div class="item-section" id="<?php echo $val['id']; ?>">
                                 <h3 class="mb-4 mt-3 col-md-12" style="text-transform: capitalize;"><?php echo $val['name']; ?> <?php if($val['discount']>0){?>(<?php echo $val['discount']; ?>% off) <?php } ?><small class="h6 text-black-50"></small></h3>
                                 <div class="menusWrapper">
                                 <?php foreach($val['value'] as $k1 => $v1) { ?>
                                    <div class="menuInner "  data-role="recipe">
                                    <div class="card" style="cursor:pointer;" onclick="getHotelStatus(<?php echo $k; ?>, <?php echo $k1; ?>, <?php echo $v1['id']; ?>, <?php echo $val['id']; ?>,<?php echo $val['discount']; ?>);">
                                    <?php 
                                          if($v1['image'] == 'dummy.jpg') { 
                                              $itemImg ="menu-src/noimage.jpg"; 
                                              $divClass = 'w-100';
                                          } else { 
                                              $itemImg ="https://deliveryguru.co.uk/admin/images/itemimages/" .  $v1['image']; 
                                              $divClass = ''; $imgClass = 'w-50';
                                          }
                                          if(isset($_GET['img'])) {
                                            $itemImg ="menu-src/noimage.jpg"; 
                                          } 
                                         // $divClass =  $imgClass = 'w-50';
                                          ?>
                                        <div class="card-body d-flex p-0">
                                            <div class="txtDiv <?php echo $divClass; ?>">
                                            <h6 class="mb-1 item-tit" data-ref="<?php echo $v1['item_name']; ?> <?php echo $val['name']; ?>"><a class="text-black" style="text-transform: capitalize;"><?php echo $v1['item_name']; ?></a></h6>
                                            <p class="text-gray mb-2 item-desc" style="text-transform: capitalize;"><?php echo $v1['item_desc']; ?></p>
                                            <p class="text-gray time">
                                                 <?php 
                                                 if($v1['price']!=0){
                                                 ?>
                                                <span style="font-weight: 700; font-size: 16px;" class="text-dark rounded-sm pl-0 pb-1 pt-1 pr-2">£<?php echo $v1['price']; ?></span> 
                                            <?php } else {?>
                                            <span style="font-weight: 900;" class="text-dark rounded-sm pl-0 pb-1 pt-1 pr-2"></span> 
                                            <?php }?>
                                             </p>
                                              <?php 
                                          if($v1['discount'] != '0' || $v1['discount'] != 0 ) {  ?>
                                              <div class="list-card-badge">
                                                <span class="badge badge-info">OFFER</span> 
                                                <small><?php echo $v1['discount']; ?> off </small> 
                                             </div> 
                                             <?php  }
                                          ?>
                                             
                                            </div>
                                            <?php if($v1['image'] != 'dummy.jpg') { ?>
                                            <div class="text-right">
                                              <img class="" src="<?php echo $itemImg; ?>" alt="sans" width="150px" height="120">
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                       <!---->
                                 </div>
                              <?php } ?>
                              </div>
                              </div>
                           <?php } ?>
                           
                        </div>

      </article>
     
     <aside class="sidebar desktoponly" style="    margin: 0px;
    padding: 0px;">
        
        <div class="cartItems shadow component mt-1" style="    border: 1px solid #dcdcdc;">
          <div class="card-header" style="padding: 8px 0px 0px 0px;">
                <h5 class="text-center" style="font-size: 16px;
    text-transform: uppercase;">Your Order</h5>
    <p onclick="allergy()" style="text-align: center;
    font-size: 13px;    margin: 5px;"><i class="bi bi-info-circle-fill" style="color:red;font-size: 17px;"></i>    <a href="#" style="text-decoration: underline;"> Info on Food Allergy and FHIS Rating</a></p>
          </div>
              <div class="btn-group btn-toggle">
                  <input type="hidden" id="del_statuuu" value="<?php echo $_SESSION['del_type_status'];?>">
               <?php if($_SESSION['del_type_status']==0 || $_SESSION['del_type_status']==2){?> <button class="btn btn-lg btn-default <?php if(!isset($_SESSION['del_type']) || (isset($_SESSION['del_type']) && $_SESSION['del_type']=='delivery')) { ?> active<?php } ?>" id="delivery" data-id="delivery" >Delivery <small id="del_sm">In 40 min</small></button><?php } ?>
                 <?php if($_SESSION['del_type_status']==0 || $_SESSION['del_type_status']==1){?><button class="btn btn-lg btn-default <?php if(isset($_SESSION['del_type']) && $_SESSION['del_type']=='collection') { ?>active<?php } ?>" id="collection" data-id="collection" <?php if($_SESSION['del_type_status']==1){?>disabled<?php }?> >Collection <small  id="col_sm">In 15 min</small></button><?php } ?>
              </div>

           <div class="footer2 p-2" style="    padding: 0px !important;">
              <div id="cartItems_section" ></div>
          </div>
        </div>
      </aside>


    </main>
   <footer>
          
          <div class="container">
              <div class="footerWrap col-md-12 col-md-offset-1">
                  <div class="copyright col-md-12">
                      <p style="color: #fff;font-size: 14px;">Copyright © 2022 <a href="https://deliveryguru.co.uk/" target="_blank" style="color:#e81818;">DeliveryGuru</a> All rights reserved.</p>
                  </div>
              </div>
          </div>
      </footer>
                          
  </body>
 
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/pikaday/pikaday.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/pikaday/css/pikaday.css">
<script src="menu-src/menu.min.js"></script>
<script>
  //$(this).toggle($(this).find("h6 a").text().toLowerCase().indexOf(t)>-1)
  //$(this).toggle($(this).find("h6").data("ref").toLowerCase().indexOf(t)>-1)

    
$('input[type=search]').on('search', function () {
  $('#searchinput, .search-mob').val('').change();
});
window.onscroll = function() { fixedLogo() };
  
var header = document.getElementById("bannerLogo");
var sticky = header.offsetTop;
function fixedLogo() {
    if (window.pageYOffset > 100) {
      header.classList.add("stickyLogo");
    document.getElementById("headerText").style.marginLeft = '20px';
    document.getElementById("headerText").style = 'color: #fff;';
    
  } else {
    header.classList.remove("stickyLogo");
    document.getElementById("headerText").style.marginLeft = '-30px';
    document.getElementById("headerText").style = 'color: #000;';
  }
}
$(document).on('click', "input:radio", function(){
    var parent = $(this).closest(".boxes").nextAll(".minY").eq(0).attr('id');
    if(typeof(parent) !== "undefined") {
      window.location.hash = "";
      window.location.hash = '#'+parent;
    }
});
var today = new Date();

const picker = new Pikaday({
    field: document.getElementById('pre_date'),
    firstDay: 1,
    minDate: new Date(),
    maxDate: new Date(today.getFullYear(), today.getMonth(), today.getDate() + 5),
    yearRange: [2021,2021],
    onSelect: date => {
      const year = date.getFullYear()  ,month = date.getMonth() + 1 ,day = date.getDate(),formattedDate = [ day < 10 ? '0' + day : day, month < 10 ? '0' + month : month, year].join('/');
      document.getElementById('pre_date').value = formattedDate
  }
});

var getItems = <?php echo json_encode($getItems); ?>;
var pre_order_status = 0;
var date = new Date();
date.setDate(date.getDate());
</script>
<script>
function getsta(){
      
      $.ajax({
               type: "GET",
               url: 'https://deliveryguru.co.uk/dg_api/restaurentdelandorder_status/200',
               dataType: "json",
               crossDomain: true,
               cache: "false",
               success: function(json) {
                   console.log();
                  if(json[0].rest_status==0){
                      sessionStorage.setItem("preorderstatus",false);
                      sessionStorage.setItem("restatus",'0');
                    //   document.getElementById('resbadgestatus').style="background:green;color:white;";
                    //   document.getElementById('resbadgestatus').innerHTML='OPEN NOW';
                      // $("#modal").show();
                       
                  }
                  else{
                      sessionStorage.setItem("restatus",'1');
                    //   document.getElementById('resbadgestatus').style="background:red;color:white;";
                    //   document.getElementById('resbadgestatus').innerHTML='CLOSED NOW';
                      // $("#modal").hide();
                  }
                 
                 
               },
               error: function(json) {
                  //alert(JSON.stringify(json));
                //   alert('Error occurs in add order!');
               }
            });
 
}
getsta();


function getHotelStatus(mId, arId, iId, cId, discount) {
var getitem=sessionStorage.getItem("restatus");
if(getitem==0 || getitem=='0'){
document.getElementById('prepopupcont1').style="display:block";
document.getElementById('prepopupcont').style="display:none";

   if(pre_order_status!==0) {
      showAddOns(mId, arId, iId, cId, discount);
   } else {
      var d = new Date();
      var n = d.getDay();
      var openhours='';
      var closehour='';
      var midtime="00:00";
      var event = new Date().toLocaleTimeString('en-GB', { timeZone: 'Europe/London' ,hour: 'numeric',minute: 'numeric', hour12: false});
      
   //alert(event);
   $.ajax({
                     type: 'get',
                     url: 'https://deliveryguru.co.uk/dg_api/getopeninghoursByHotelAndType/200/delivery',
                     success: function(result) {
                         if(result!=''){
                            //  console.log(result);
                              var data=JSON.parse(result[0].openingtime);
                                  if(n==0){
                                    if(data.sunlclose==0){
                                        openhours=data.sunlst;
                                        closehour=data.sundend;
                                    }
                                    else{
                                       closedMsg();
                                    document.getElementById('prepopupcont').style="display:block";
    document.getElementById('prepopupcont1').style="display:none";
      var obj='<h3 style="margin-top:40%;">We are Closed Now</h3>';
                      obj+='<p style="font-weight: 600;">Restaurant Busy Not Taking Any Orders<br> Sunday-Thursday 4.30pm-12.20am,Friday and Saturday 4.30pm-1.20am    </p>';
                      $("#prepopupcont").html(obj);
                      document.getElementById('closemessagebtn').style="display:none";
                    closedMsg();       return false;
                                    }
                                 }
                            else if(n==1){
                                    if(data.monlclose==0){
                                        openhours=data.monlunst;
                                        closehour=data.mondend;
                                    }
                                     else{
                                      document.getElementById('prepopupcont').style="display:block";
    document.getElementById('prepopupcont1').style="display:none";
      var obj='<h3 style="margin-top:40%;">We are Closed Now</h3>';
                       obj+='<p style="font-weight: 600;">Restaurant Busy Not Taking Any Orders<br> Sunday-Thursday 4.30pm-12.20am,Friday and Saturday 4.30pm-1.20am    </p>';
                      $("#prepopupcont").html(obj);
                      document.getElementById('closemessagebtn').style="display:none";
                    closedMsg();     return false;
                                    }
                                 }
                              else if(n==2){
                                    if(data.tulclose==0){
                                        openhours=data.tulst;
                                        closehour=data.tudend;
                                    }
                                     else{
                                   document.getElementById('prepopupcont').style="display:block";
    document.getElementById('prepopupcont1').style="display:none";
      var obj='<h3 style="margin-top:40%;">We are Closed Now</h3>';
                      obj+='<p style="font-weight: 600;">Restaurant Busy Not Taking Any Orders<br> Sunday-Thursday 4.30pm-12.20am,Friday and Saturday 4.30pm-1.20am    </p>';
                      $("#prepopupcont").html(obj);
                      document.getElementById('closemessagebtn').style="display:none";
                    closedMsg();      return false;
                                    }
                                 }
                          else if(n==3){
                                    if(data.wedlclose==0){
                                        openhours=data.wedlst;
                                        closehour=data.weddend;
                                    }
                                     else{
                                     document.getElementById('prepopupcont').style="display:block";
    document.getElementById('prepopupcont1').style="display:none";
      var obj='<h3 style="margin-top:40%;">We are Closed Now</h3>';
                      obj+='<p style="font-weight: 600;">Restaurant Busy Not Taking Any Orders<br> Sunday-Thursday 4.30pm-12.20am,Friday and Saturday 4.30pm-1.20am    </p>';
                      $("#prepopupcont").html(obj);
                      document.getElementById('closemessagebtn').style="display:none";
                    closedMsg();     return false;
                                    }
                                 }
                          else if(n==4){
                                    if(data.thlclose==0){
                                        openhours=data.thlst;
                                        closehour=data.thdend;
                                    }
                                    else{
                                       document.getElementById('prepopupcont').style="display:block";
    document.getElementById('prepopupcont1').style="display:none";
      var obj='<h3 style="margin-top:40%;">We are Closed Now</h3>';
                     obj+='<p style="font-weight: 600;">Restaurant Busy Not Taking Any Orders<br> Sunday-Thursday 4.30pm-12.20am,Friday and Saturday 4.30pm-1.20am    </p>';
                      $("#prepopupcont").html(obj);
                      document.getElementById('closemessagebtn').style="display:none";
                    closedMsg();   return false;
                                    }
                                 }
                         else if(n==5){
                                    if(data.frilclose==0){
                                        openhours=data.frilst;
                                        closehour=data.fridend;
                                    }
                                     else{
                                     document.getElementById('prepopupcont').style="display:block";
    document.getElementById('prepopupcont1').style="display:none";
      var obj='<h3 style="margin-top:40%;">We are Closed Now</h3>';
                      obj+='<p style="font-weight: 600;">Restaurant Busy Not Taking Any Orders<br> Sunday-Thursday 4.30pm-12.20am,Friday and Saturday 4.30pm-1.20am    </p>';
                      $("#prepopupcont").html(obj);
                      document.getElementById('closemessagebtn').style="display:none";
                    closedMsg();    return false;
                                    }
                                 }
                         else if(n==6){
                                    if(data.satlclose==0){
                                        openhours=data.satlst;
                                        closehour=data.satdend;
                                    }
                                     else{
                                      document.getElementById('prepopupcont').style="display:block";
    document.getElementById('prepopupcont1').style="display:none";
      var obj='<h3 style="margin-top:40%;">We are Closed Now</h3>';
                     obj+='<p style="font-weight: 600;">Restaurant Busy Not Taking Any Orders<br> Sunday-Thursday 4.30pm-12.20am,Friday and Saturday 4.30pm-1.20am    </p>';
                      $("#prepopupcont").html(obj);
                      document.getElementById('closemessagebtn').style="display:none";
                    closedMsg();   return false;
                                    }
                                 }
  if(event<openhours && event>closehour){
    sessionStorage.setItem("preorder_time",'1');
         <?php if(!isset($_SESSION['pre_order'])) { ?>closedMsg();  <?php } else { ?>  pre_order_status=1; showAddOns(mId, arId, iId, cId, discount); <?php }  ?>
        document.getElementById('restOrder_status').innerHTML='CLOSED NOW';
     }
    else{
        
         sessionStorage.setItem("preorder_time",'0');
         showAddOns(mId, arId, iId, cId, discount);
         document.getElementById('restOrder_status').innerHTML='OPEN NOW';
    }

    }

}
  });
  
    }
}
else{
    document.getElementById('prepopupcont').style="display:block";
    document.getElementById('prepopupcont1').style="display:none";
      var obj='<h3 style="margin-top:40%;">We are Closed Now</h3>';
                      obj+='<p style="font-weight: 600;">Restaurant Busy Not Taking Any Orders</p>';
                      $("#prepopupcont").html(obj);
                      document.getElementById('closemessagebtn').style="display:none";
                    closedMsg(); 
}

}
function getHotelStatus1() {

      var d = new Date();
      var n = d.getDay();
      var openhours='';
      var closehour='';
      var midtime="00:00";
      var event = new Date().toLocaleTimeString('en-GB', { timeZone: 'Europe/London' ,hour: 'numeric',minute: 'numeric', hour12: false});
   //alert(event);
   $.ajax({
                     type: 'get',
                     url: 'https://deliveryguru.co.uk/dg_api/getopeninghoursByHotelAndType/200/delivery',
                     success: function(result) {
                         if(result!=''){
                            //  console.log(result);
                              var data=JSON.parse(result[0].openingtime);
                                  if(n==0){
                                    if(data.sunlclose==0){
                                        openhours=data.sunlst;
                                        closehour=data.sundend;
                                    }
                                    else{
                                      document.getElementById('restOrder_status').style="background:#fff;color:red;border:1px solid #ccc;";
       document.getElementById('sheduleOrder').style="display:none";
      document.getElementById('preOrderButton').style="display:none";
        document.getElementById('restOrder_status').innerHTML='CLOSED NOW';
                                     return false;
                                    }
                                 }
                            else if(n==1){
                                    if(data.monlclose==0){
                                        openhours=data.monlunst;
                                        closehour=data.mondend;
                                    }
                                     else{
                                          document.getElementById('restOrder_status').style="background:#fff;color:red;border:1px solid #ccc;";
       document.getElementById('sheduleOrder').style="display:none";
      document.getElementById('preOrderButton').style="display:none";
        document.getElementById('restOrder_status').innerHTML='CLOSED NOW';
                                     return false;
                                    }
                                 }
                              else if(n==2){
                                    if(data.tulclose==0){
                                        openhours=data.tulst;
                                        closehour=data.tudend;
                                    }
                                     else{
                                          document.getElementById('restOrder_status').style="background:#fff;color:red;border:1px solid #ccc;";
       document.getElementById('sheduleOrder').style="display:none";
      document.getElementById('preOrderButton').style="display:none";
        document.getElementById('restOrder_status').innerHTML='CLOSED NOW';
                                     return false;
                                    }
                                 }
                          else if(n==3){
                                    if(data.wedlclose==0){
                                        openhours=data.wedlst;
                                        closehour=data.weddend;
                                    }
                                     else{
                                          document.getElementById('restOrder_status').style="background:#fff;color:red;border:1px solid #ccc;";
       document.getElementById('sheduleOrder').style="display:none";
      document.getElementById('preOrderButton').style="display:none";
        document.getElementById('restOrder_status').innerHTML='CLOSED NOW';
                                  return false;
                                    }
                                 }
                          else if(n==4){
                                    if(data.thlclose==0){
                                        openhours=data.thlst;
                                        closehour=data.thdend;
                                    }
                                    else{
                                         document.getElementById('restOrder_status').style="background:#fff;color:red;border:1px solid #ccc;";
       document.getElementById('sheduleOrder').style="display:none";
      document.getElementById('preOrderButton').style="display:none";
        document.getElementById('restOrder_status').innerHTML='CLOSED NOW';
                                return false;
                                    }
                                 }
                         else if(n==5){
                                    if(data.frilclose==0){
                                        openhours=data.frilst;
                                        closehour=data.fridend;
                                    }
                                     else{
                                 document.getElementById('restOrder_status').style="background:#fff;color:red;border:1px solid #ccc;";
       document.getElementById('sheduleOrder').style="display:none";
      document.getElementById('preOrderButton').style="display:none";
        document.getElementById('restOrder_status').innerHTML='CLOSED NOW';
                      return false;
                                    }
                                 }
                         else if(n==6){
                                    if(data.satlclose==0){
                                        openhours=data.satlst;
                                        closehour=data.satdend;
                                    }
                                     else{
                                    document.getElementById('restOrder_status').style="background:#fff;color:red;border:1px solid #ccc;";
      document.getElementById('sheduleOrder').style="display:none";
      document.getElementById('preOrderButton').style="display:none";
        document.getElementById('restOrder_status').innerHTML='CLOSED NOW';
                     return false;
                                    }
                                 }
  if(event<openhours && event>closehour){
    
          document.getElementById('restOrder_status').style="background:#fff;color:red;border:1px solid #ccc;";
      document.getElementById('sheduleOrder').style="display:none";
      document.getElementById('preOrderButton').style="display:block";
        document.getElementById('restOrder_status').innerHTML='CLOSED NOW';
    
     }
    else{
         document.getElementById('sheduleOrder').style="display:block";
         document.getElementById('preOrderButton').style="display:none";
         document.getElementById('restOrder_status').innerHTML='OPEN NOW';
         
        
    }    

    }

}
  });



}
getHotelStatus1();
setInterval(function(){ getsta();getHotelStatus1(); }, 5000);
$(".dropdown-menu, .mobCartBody").click(function(e){
   e.stopPropagation();
})
//getHotelStatus();
    $(document).ready(function() {
      
        var openhours='16:30';
        var closehour='01:00';
        var midtime="00:00";
        var event = new Date().toLocaleTimeString('en-GB', { timeZone: 'Europe/London' ,hour: 'numeric',minute: 'numeric', hour12: false});
        if(event<openhours && event>closehour){
          $('#del_sm').html('Open at 16:30');
          $('#col_sm').html('Open at 16:30');
          $('#mdel_sm').html('Open at 16:30');
          $('#mcol_sm').html('Open at 16:30');
        }
          $('body').bind('cut copy', function(e) {
              e.preventDefault();
            });
        $("body").on("contextmenu", function(e) {
              return false;
            });
            window.ondragstart = function() { return false; } 
        });
  refreshCart();    
  var hidWidth;
var scrollBarWidths = 40;

var widthOfList = function(){
 var itemsWidth = 0;
 $('.list li').each(function(){
   var itemWidth = $(this).outerWidth();
   itemsWidth+=itemWidth;
 });
 return itemsWidth;
};

var widthOfHidden = function(){
 var t = (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
 console.log('widthOfHidden: '+(($('.wrapper').outerWidth())+','+widthOfList()+','+getLeftPosi())+','+scrollBarWidths+','+t);
 return (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
};

var getLeftPosi = function(){
 return $('.list').position().left;
};

var reAdjust = function(){
 if (($('.wrapper').outerWidth()) < widthOfList()) {
   $('.scroller-right').show();
 }
 else {
   $('.scroller-right').show();
 }
 
 if (getLeftPosi()<0) {
   $('.scroller-left').show();
 }
 else {
   $('.scroller-left').hide();
 }
 if(getLeftPosi()==0){
  $('.scroller-left').fadeOut('slow');
 }
}

reAdjust();

$(window).on('resize',function(e){  
   reAdjust();
});

$('.scroller-right').click(function() {
    $('.scroller-left, .scroller-right').hide();
    $('.scroller-left').fadeIn('slow');
    //var t = (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
    var t1 = -1 * (getLeftPosi()-($('.wrapper').outerWidth())) + ($('.wrapper').outerWidth());
    if(t1>Math.round(widthOfList())) {
      $('.list').animate({left: "-"+((Math.round(widthOfList())+20)-$('.wrapper').outerWidth())+"px"}, 'slow'); 
      $('.scroller-right').fadeOut('slow');
    } else {
      $('.list').animate({left:"-="+($('.wrapper').outerWidth()   )+"px"},'slow',function() {
            if(($('.wrapper').outerWidth()) >= (widthOfList()+getLeftPosi())){
                $('.scroller-right').fadeOut('slow');
            } else {
              $('.scroller-right').fadeIn('slow');
            }
      });
    }
});

$('.scroller-left').click(function() {
  $('.scroller-left, .scroller-right').hide();
  $('.scroller-right').fadeIn('slow');
  var t1 =  getLeftPosi() + $('.wrapper').outerWidth() - 40;
  //alert(t1);
  if(t1==0 || t1>0) {
    $('.list').animate({left: "+0px"}, 'slow'); 
    $('.scroller-left').fadeOut('slow');
  } else {
    $('.list').animate({left:"+="+($('.wrapper').outerWidth())+"px"},'slow',function(){
      var t = (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
      //alert(getLeftPosi());
      if(getLeftPosi()==0){
          $('.scroller-left').fadeOut('slow');
      } else {
          $('.scroller-left').fadeIn('slow');
      }
    });
  }
});   


$('.cart-menu a').on('click', function() {
   var scrollAnchor = $(this).attr('data-id');
   scrollPoint = $('#panel_groups #' + scrollAnchor).offset().top - 70 - 50;
   $('body,html').animate({
       scrollTop: scrollPoint
   }, 500);
   return false;
})

$(window).scroll(function() {
   var windscroll = $(window).scrollTop();
   if (windscroll >= 100) {
       $('#panel_groups .item-section').each(function(i) {
           if ($(this).position().top <= windscroll + 150) {
               $('.cart-menu a.active').removeClass('active');
               $('.cart-menu a').eq(i).addClass('active');
           }
       });

   } else {
       $('.cart-menu a.active').removeClass('active');
       $('.cart-menu a:first').addClass('active');
   }

   var leftOffset = $('.cart-menu a.active').offset().left - $('.cart-menu').offset().left +   $('.cart-menu').scrollLeft();
   var scrollPos = leftOffset / (($('.wrapper').outerWidth())-40);
   var calWid = Math.round(widthOfList()) - $('.wrapper').outerWidth();

  console.log(''+$('.wrapper').outerWidth()+'_'+Math.round(widthOfList())+'_'+Math.round(getLeftPosi())+'_'+Math.round(scrollBarWidths)+'_'+parseInt(scrollPos)+'_'+leftOffset+'_'+parseInt(calWid));

  
    if(parseInt(scrollPos)>0) 
    {
      if((parseInt(leftOffset)+parseInt(calWid)+40)>Math.round(widthOfList())) 
      {
        $('.list').animate({left: "-"+((Math.round(widthOfList())+20)-$('.wrapper').outerWidth())+"px"}, 0); 
        $('.scroller-left').fadeIn('slow');
        $('.scroller-right').fadeOut('slow');
      } else {
        setPos = (parseInt(scrollPos) * ($('.wrapper').outerWidth()));
        $('.list').animate({left: "-"+(setPos)+"px"}, 0); 
        if(($('.wrapper').outerWidth()) >= (widthOfList()+getLeftPosi())) {
            $('.scroller-left').fadeIn('slow');
            $('.scroller-right').fadeOut('slow');
        }
      }
    } else {
        setPos = parseInt(scrollPos) * ($('.wrapper').outerWidth());
        $('.list').animate({left: "+"+setPos+"px"}, 0); 
        if(($('.wrapper').outerWidth()) <= (widthOfList()+getLeftPosi())) {
            $('.scroller-left').fadeOut('slow');
            $('.scroller-right').fadeIn('slow');
        }
    }
  


}).scroll();
function validateDelivery(){
     var del_type = "<?php echo $_SESSION['del_type_status'];?>";
    
    
      $.ajax({
               type: "GET",
               url: 'https://deliveryguru.co.uk/dg_api/restaurentdelandorder_status/200',
               dataType: "json",
               crossDomain: true,
               cache: "false",
               success: function(json) {
                  if(json[0].delivery_status==del_type){
                    // alert(del_type);
                  }
                  else{
                       window.location.href="menu.php";
                //   showError("delivey Orders Not Accepted");
                  }
               },
               error: function(json) {
                  //alert(JSON.stringify(json));
                //   alert('Error occurs in add order!');
               }
            });
     if(del_type=='1'){
          $("#collection").off("click");
     }
}
setInterval(function(){ 
  validateDelivery(); 
    
}, 5000);
// validateDelivery(); 
function getlistNoitem(tr){
      if(tr==0){
                 var divv="<p style='text-align:center;font-size: 24px;margin-bottom: 600px;'>No items Available!....</p>";
                 $('#noitem-section').html(divv);
                 return false;
             }else{var divv="";$('#noitem-section').html(divv);return false;}}
     
     function allowAlphaNumericSpace(e) {
  var code = ('charCode' in e) ? e.charCode : e.keyCode;
  if (!(code == 32) && // space
    !(code > 47 && code < 58) && // numeric (0-9)
    !(code > 64 && code < 91) && // upper alpha (A-Z)
    !(code > 96 && code < 123)) { // lower alpha (a-z)
    e.preventDefault();
  }
}
function allergy(){
      jQuery("#allergyModal").modal("show");
}
function closemodal1(){
     jQuery("#allergyModal").modal("hide");
}
$(document).ready(function() {
     $("#searchinput, .search-mob").on("keyup change", function() {
          $("#searchinput, .search-mob").val($(this).val());
          var value = $(this).val().toLowerCase();
          $('div[data-role="recipe"]').filter(function() {
                       $(this).toggle($(this).find('h6 a').text().toLowerCase().indexOf(value) > -1);
           });
           var tr=0;
           $('.item-section').each( function() {
                       var len = $('div[data-role="recipe"]', this).children(':visible').length;
                       if(len==0)
                       {
                       $('h3',this).hide();
                       } else {
                            tr++;
                         $('h3',this).show();
                       }
             });
            getlistNoitem(tr);
             console.log(tr);
     });
 });
function getReviews(){
     var del_type = "<?php echo $_SESSION['del_type_status'];?>";
   var average=0;
    
      $.ajax({
               type: "GET",
               url: 'https://deliveryguru.co.uk/dg_api/review/hotel/200',
               dataType: "json",
               crossDomain: true,
               cache: "false",
               success: function(json) {
                 console.log(json);
                 document.getElementById('reviewCountt').innerHTML=json.length;
                 
                 if(json.length>0){
                     var obj='';
                   
                obj+='<div class="col-md-12">';
           obj+='<div class="row">';
                     for(i=0;i< json.length;i++){
                          console.log(json[i]);
                          average=average+Number(json[i].f_rate);
                          
                          var today  = new Date(json[i].created_at);
                       obj+='<div class="col-md-6"><div class="nameDiv rating-stars">';
                       obj+='<h6>'+json[i].first_name;
                          obj+='<span style="float: right;">';
                        obj+="<ul id='stars'>";
                        for(j=0;j<json[i].f_rate;j++){
      obj+="<li class='star' title='Poor' data-value='1'>";
       obj+='<i class="bi bi-star-fill"></i>';
      obj+='</li>';
                        }
                for(k=0;k<(5-json[i].f_rate);k++){
      obj+="<li class='star' title='WOW!!!' data-value='5'>";
       obj+=' <i class="bi bi-star"></i>';
    obj+='  </li>';
                }
   obj+=' </ul></span>';
                       obj+='</h6>';
                       obj+='<p>'+json[i].created_at+'</p>';
                        obj+='<hr>';
                        obj+='<p class="reviewC">'+json[i].review+'</p>';
                     
                         obj+='</div></div>';
                         
          
                     }
       obj+='</div>';
       obj+='</div>';
                     $('#reviewList').html(obj);
                     
                     var average_rate= Math.round(average/(json.length));
                 
                  var obj2='<h5>Average Customer  Rating <span> ' + average_rate + ' of 5 &nbsp;&nbsp;</span>';
                  var obj3='';
                 obj2+='<span class="rating-stars" style="float: right;">';
                        obj3+="<ul id='stars' style='text-align: left;'>";
    //   obj2+="<li class='star' title='Poor' data-value='1' style='display: inline-block;'>";
    //   obj2+='<i class="bi bi-star-fill"></i>';
    //   obj2+='</li>';
    //  obj2+="<li class='star' title='Fair' data-value='2' style='display: inline-block;'>";
    //   obj2+='  <i class="bi bi-star-fill"></i>';
    //  obj2+=' </li>';
    //   obj2+="<li class='star' title='Good' data-value='3' style='display: inline-block;'>";
    //   obj2+=' <i class="bi bi-star-fill"></i>';
    //  obj2+=' </li>';
    //  obj2+="<li class='star' title='Excellent' data-value='4' style='display: inline-block;'>";
    //  obj2+='  <i class="bi bi-star-fill"></i>';
    //  obj2+=' </li>';
    //   obj2+="<li class='star' title='WOW!!!' data-value='5' style='display: inline-block;'>";
    //   obj2+=' <i class="bi bi-star"></i>';
    // obj2+='  </li>';
   
   for(j=0;j<average_rate;j++){
      obj3+="<li class='star' title='Poor' data-value='1'>";
       obj3+='<i class="bi bi-star-fill"></i>';
      obj3+='</li>';
                        }
                for(k=0;k<(5-average_rate);k++){
      obj3+="<li class='star' title='WOW!!!' data-value='5'>";
       obj3+=' <i class="bi bi-star"></i>';
    obj3+='  </li>';
                }
             
   obj3+=' </ul>';
    obj2+=obj3;  
   obj2+='</span></h5><p>Total Reviews ('+json.length+') </p>';
                 document.getElementById('reviewTile').innerHTML=obj2;
                 document.getElementById('star_review').innerHTML=obj3;
                 }
                 
                 
                 
               },
               error: function(json) {
                  //alert(JSON.stringify(json));
                //   alert('Error occurs in add order!');
               }
            });
   
}
getReviews();
function openReview(){
    if(reviewCountt.innerHTML==0){
      jQuery("#reviewModal").modal("hide");
        var x = document.getElementById("snackbar"); 
        x.innerHTML='No Reviews Available! Please Add a Review for your order.';
        // document.getElementById('onloader').style.display='none';
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }
      else{
          jQuery("#reviewModal").modal("show");
      }
}
function closemodal2(){
     jQuery("#reviewModal").modal("hide");
}
function showAddOnsAjax(mId, arId, iId, cId,discount)
{
  var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
  if (isMobile) {  modalHeight = 43; } else { modalHeight = 47; }
      $('.linear-background').show();
      var itemData = getItems[mId]['value'][arId];
      $('.alert-danger').hide();      
      var modelBody = modelFooter = '';
      $('#AddOns').html(modelBody);
      $('#addonModal').modal('show');
      $('#addonModalLabel').html(itemData.item_name);
      if(itemData.image=='dummy.jpg'){ itemImg ='menu-src/noimage.jpg'; } else { itemImg ='https://deliveryguru.co.uk/admin/images/itemimages/'+itemData.image; }
      var modelBody = modelFooter = modelItem = '';
      
      if(itemData.image!='dummy.jpg'){
        modelItem += '<img src="'+itemImg+'" class="img-fluid item-img" style="width: 100%;height: 300px; object-fit: cover;" />';
        modalHeight = 20 +modalHeight;  
      }
      modelItem += '<p class="text-gray popup-desc">'+itemData.item_desc+'</p>';
      $('.itemDataModal').html(modelItem);
      $.ajax({
               type: "GET",
               url: "https://deliveryguru.co.uk/dg_api/basedOnCatId_addon_list/"+iId+"/200",
               dataType: "json",
               crossDomain: true,
               cache: "false",
               success: function(response) {
                      
                        var itemChild = response;
                        if(itemChild.length==1 && itemChild[0]['child']!=true) { //alert(itemData.id);
                            $('.modal-body').css('height', modalHeight+'vh');
                              modelBody += '<input type="radio" style="display:none" checked onchange="addcatpop(this)" name="subMenuList" data-id="'+itemData.categories_id+'" data-name="'+(itemData.item_name).replace(/\"/g,"inch")+'" data-amount="'+itemData.item_name+'" data-subid="'+itemData.id+'"  item_name="" item_id="" addon_price="'+itemData.price+'" data-image="'+itemImg+'" onclick="validate(this,1);" data-discount="'+discount+'" value="'+itemData.id+', '+(itemData.item_name).replace(/\"/g,"inch")+', '+itemData.price+', '+itemImg+','+discount+'">';
                              if(itemChild[0]['menu_addon'].length>0) {
                                    var itemExtra = itemChild[0]['menu_addon'];
                                    var childID = itemExtra[0]['menuid'];
                                    
                                    var txtJson = '';
                                    for (var j = 0; j < itemExtra.length; j++) 
                                    {
                                      var childMin = itemExtra[j].count;
                                      var childMax = itemExtra[j].maxcount;
                                        txtJson += '{'+j+', '+childMin+', '+childMax+', '+itemExtra[j].title+'}';
                                        var minCls='';
                                        if(childMin==1 && childMax==1) { minCls = 'minY'; } 
                                          modelBody += '<div class="boxes '+minCls+'" id="box_'+childID+'_'+j+'" style="margin-top: 5%;"><div class="list-card-body" style="padding-top: 0px !important;margin-left: -5px;"><h6 class="mb-1"><a class="text-black">'+itemExtra[j].title+'</a></h6><p class="text-gray mb-2" style="font-size: 10px;">'+itemExtra[j].add_on_desc+'</p></div>';
                                          var itemAddon = itemExtra[j]['extra'];
                                          for (var k = 0; k < itemAddon.length; k++) 
                                          {
                                            SelQty='';
                                            if(childMin==1 && childMax==1) { Chk_Rad = 'radio'; SelQty=''; } else { 
                                                Chk_Rad = 'checkbox'; 
                                                SelQty += '<span class="addOnSel"><div class="btn-number" disabled="disabled" data-type="minus" data-field="addOnSel'+childID+'_'+j+'[1]"data-id="'+childID+'_'+j+'_'+k+'"><i class="bi bi-dash-circle inc_btn"></i></div><input type="text" class="form-control form-control-sm input-number '+childID+'_'+j+'_'+k+'" name="addOnSel'+childID+'_'+j+'[]" value="1" min="1" readonly max="'+childMax+'"><div class="btn-number" data-type="plus" data-field="addOnSel'+childID+'_'+j+'[1]" data-id="'+childID+'_'+j+'_'+k+'"><i class="bi bi-plus-circle inc_btn"></i></div></span>';
                                              }
                                            modelBody += '<div class="custom-control custom-'+Chk_Rad+'"><input type="'+Chk_Rad+'" class="custom-control-input  addonCls_'+childID+'" id="'+childID+'_'+itemAddon[k].addon_id+'" name="addOnCheck'+childID+'_'+j+'[]" value="'+childID+','+itemAddon[k].addon_id+','+itemAddon[k].addon_name.replace(/\"/g,"inch").replace(/,/g," ")+','+itemAddon[k].addon_price+'"><label class="custom-control-label col-5 col-sm-6" for="'+childID+'_'+itemAddon[k].addon_id+'" style="font-weight: 500;">'+itemAddon[k].addon_name.replace(/,/g," ")+'</label>'+SelQty+'';
                                            modelBody += '<small class="text-black-50 text-right" style="float: right; color: #34373b !important;font-weight: 500;"> £ '+itemAddon[k].addon_price+'</small>';
                                            modelBody += '</div>';
                                          }
                                          modelBody += '</div>';
                                          if(itemExtra.length==(j+1)){
                                            itemMinMax[childID] = txtJson.replaceAll("}{", "},{");
                                          }
                                        }
                                    }
                          }
                          else 
                          { 
                            $('.modal-body').css('height', (modalHeight+10)+'vh');
                            for (i = 0; i < itemChild.length; i++) 
                            {
                              
                              modelBody += '<div class="detail det_'+itemChild[i]['menu'].id+'"><span class="summary" data-id="'+itemChild[i]['menu'].id+'"><input type="radio" style="display:none" id="'+itemChild[i]['menu'].id+'" onchange="addcatpop(this)" name="subMenuList" data-id="'+itemChild[i]['menu'].categories_id+'" data-name="'+(itemChild[i]['menu'].item_name.replace(/,/g," ")).replace(/\"/g,"inch")+'" data-amount="'+itemChild[i]['menu'].item_name+'" data-subid="'+itemChild[i]['menu'].id+'"  item_name="" item_id="" addon_price="'+itemChild[i]['menu'].price+'" data-discount="'+discount+'"  onclick="validate(this,1);" data-image="'+itemImg+'"  value="'+itemChild[i]['menu'].id+', '+(itemChild[i]['menu'].item_name.replace(/,/g," ")).replace(/\"/g,"inch")+', '+itemChild[i]['menu'].price+', '+itemImg+','+discount+'"><span class="row col-12"><span class="col-10">'+itemChild[i]['menu'].item_name+'</span><span  class="col-2 text-right" style="float:right;">£'+itemChild[i]['menu'].price+'</span></span><div style="font-size: 11px;margin-bottom:5px;" class="addOn_'+itemChild[i]['menu'].id+' addonDiv">';
                                  var itemExtra = itemChild[i]['menu_addon'];
                                  var childID = itemChild[i]['menu'].id;
                                  var txtJson = '';
                                  for (var j = 0; j < itemExtra.length; j++) 
                                  {
                                    var childMin = itemExtra[j].count;
                                    var childMax = itemExtra[j].maxcount;
                                      txtJson += '{'+j+', '+childMin+', '+childMax+', '+itemExtra[j].title+'}';
                                        var minCls='';
                                        if(childMin==1 && childMax==1) { minCls = 'minY'; } 
                                        modelBody += '<div class="boxes '+minCls+'" id="box_'+childID+'_'+j+'"><div class="list-card-body" style="padding-top: 0px !important;margin-left: -5px;"><h6 class="mb-1"><a class="text-black">'+itemExtra[j].title+'</a></h6><p class="text-gray mb-2" style="font-size: 10px;">'+itemExtra[j].add_on_desc+'</p></div>';
                                        var itemAddon = itemExtra[j]['extra'];
                                        for (var k = 0; k < itemAddon.length; k++) 
                                        {
                                          SelQty='';
                                          if(childMin==1 && childMax==1) { Chk_Rad = 'radio'; SelQty=''; } else { 
                                              Chk_Rad = 'checkbox'; 
                                              SelQty += '<span class="addOnSel"><div class="btn-number" disabled="disabled" data-type="minus" data-field="addOnSel'+childID+'_'+j+'[1]"data-id="'+childID+'_'+j+'_'+k+'"><i class="bi bi-dash-circle inc_btn"></i></div><input type="text" class="form-control form-control-sm input-number '+childID+'_'+j+'_'+k+'" name="addOnSel'+childID+'_'+j+'[]" value="1" min="1" readonly max="'+childMax+'"><div class="btn-number" data-type="plus" data-field="addOnSel'+childID+'_'+j+'[1]" data-id="'+childID+'_'+j+'_'+k+'"><i class="bi bi-plus-circle inc_btn"></i></div></span>';
                                            }
                                          modelBody += '<div class="custom-control custom-'+Chk_Rad+'"><input type="'+Chk_Rad+'" class="custom-control-input  addonCls_'+childID+'" id="'+itemChild[i]['menu'].id+'_'+itemAddon[k].addon_id+'" name="addOnCheck'+childID+'_'+j+'[]" value="'+itemChild[i]['menu'].id+','+itemAddon[k].addon_id+','+itemAddon[k].addon_name.replace(/\"/g,"inch").replace(/,/g," ")+','+itemAddon[k].addon_price+'"><label class="custom-control-label col-5 col-sm-6" for="'+itemChild[i]['menu'].id+'_'+itemAddon[k].addon_id+'" style="font-weight: 500;">'+itemAddon[k].addon_name.replace(/,/g," ")+'</label>'+SelQty+'';
                                          modelBody += '<small class="text-black-50 text-right" style="float: right; color: #34373b !important;font-weight: 500;"> £ '+Number(itemAddon[k].addon_price).toFixed(2)+'</small>';
                                          modelBody += '</div>';
                                        }
                                        modelBody += '</div>';
                                        if(itemExtra.length==(j+1)){
                                          itemMinMax[childID] = txtJson.replaceAll("}{", "},{");
                                        }
                                  }
                                modelBody += '</div></div>';
                                $('.addInst').hide();
                                $('#addInst').val('');
                                } 
                      }
                    modelBody += '<span class=""><b style="padding: 15px 0;font-size: 16px;">Special request to suit your taste buds</b><br><textarea type="text" placeholder="Please leave any special request e.g. Extra spicy, no sauce, well done  etc. Please do not leave any allergen info here." row="4" name="instr" id="addInst" class="col-12 form-control" style="font-size: 14px;maxlength="100" onpaste="return false;" onCopy="return false" onkeypress="allowAlphaNumericSpace(event)" onCut="return false" onDrag="return false" onDrop="return false"></textarea><p style="font-size:12px;text-align:right;">(Max 100 characters)</p></span>';
                    $('.linear-background').hide();
                    $('#AddOns').html(modelBody);
                          modelFooter += '<span class="count-number float-left"><button class="btn btn-outline-secondary  btn-sm left dec" onclick="minusValue('+iId+')"><i class="bi bi-dash-circle"></i></button><input id="'+iId+'" class="count-number-input totItem" type="number" name="totItem" value="1" readonly=""><button class="btn btn-outline-secondary btn-sm right inc" onclick="plusValue('+iId+')"><i class="bi bi-plus-circle"></i></button></span><button id="addButton'+iId+'" type="button" class="btn btn-primary addbtn float-right redbg" data-dismiss="modal" amount="'+itemData.price+'" item_id="'+iId+'" discount="'+itemData.discount+'" qty="1" status="0" notes="1" item_name = "'+(itemData.item_name).replace(/\"/g,"inch")+'" '; 
                        if(itemData.discount.length == 0 && itemData.discount.length == 0){
                            modelFooter += 'onclick="addItem(this)"';
                        }
                        modelFooter += ' onclick="btnAddToCart()">Add to cart</button>';
                    $('.modal-footer').html(modelFooter);
                    
               },
               error: function(json) {
                  alert(JSON.stringify(json));
               }
      });

}
var itemMinMax = {};
 function btnAddToCart() {
        $('.alert-danger').hide();
        var errorMsg='';
         $(".addbtn").prop('disabled', true);
        var MainSel = $('input[name="subMenuList"]:checked');

        if($('input[name="subMenuList"]:checked').length == 0) {
          errorMsg = 'Please select at least one item!';
           $(".addbtn").prop('disabled', false);
        } else {
           
           var temp = selItem = new Array();

           selItem = [MainSel.attr("data-subid"), MainSel.attr("data-name"), MainSel.attr("addon_price"),  MainSel.attr("data-image")];
           
           selId = selItem[0];
           //alert(itemMinMax[selId]);
           if(itemMinMax[selId]){
             temp = itemMinMax[selId].split("},{");
             var addOnItems = [];
             for(i=0;i<temp.length;i++){
                 var adOnQty   = 0;
                 var splitTxt = new Array();
                 splitTxt = temp[i].replace('{','');
                 splitTxt = splitTxt.replace('}','');
                 splitTxt =  splitTxt.split(',');
                 var chkId = $('input[name="addOnCheck'+selId+'_'+splitTxt[0]+'[]"]:checked').length;
                 if(splitTxt[1]==1 && splitTxt[2]==1) {
                     adOnQty = $('input[name="addOnCheck'+selId+'_'+splitTxt[0]+'[]"]:checked').length;
                 } else {
                   $('input[name="addOnSel'+selId+'_'+splitTxt[0]+'[]"]').each( function() { 
                     if($(this).is(":visible")) {
                       adOnQty += parseInt($(this).val());
                     }
                   });
                 }
                 //alert(adOnQty+','+splitTxt[1]+','+splitTxt[2]);
                 if((adOnQty < splitTxt[1] || adOnQty > splitTxt[2]) && errorMsg==''){
                   errorMsg = 'Please select Min '+splitTxt[1]+' and Max '+splitTxt[2]+' Add on from '+splitTxt[3];
                   window.location.hash = "";
                   window.location.hash = '#box_'+selId+'_'+splitTxt[0];
                   $('#box_'+selId+'_'+splitTxt[0]+' .list-card-body .mb-1 a').attr('style', 'color: red !important');
                   $('#box_'+selId+'_'+splitTxt[0]+' .list-card-body .mb-1 a').fadeIn(100).fadeOut(300).fadeIn(300).fadeOut(300).fadeIn(300);
                 }
                 $('input[name="addOnCheck'+selId+'_'+splitTxt[0]+'[]"]:checked').map(function(){
                     if(splitTxt[1]==1 && splitTxt[2]==1) { 
                         var addOnQty = 1; 
                     } else {
                         var addOnQty = $("~ span.addOnSel input", this).val();
                     }
                     addOnItems.push($(this).val()+','+addOnQty);
                 });
                 //console.log('Values: '+selId+','+splitTxt[1]+','+splitTxt[2]+', '+chkId);
             }
           }
        }
 
        if(errorMsg!=''){
            $('.alert-danger').html(errorMsg);
            $('.alert-danger').show();
             $(".addbtn").prop('disabled', false);
            setTimeout(
                function() {
                  $('.alert-danger').hide();
                  $('.list-card-body .mb-1 a').attr('style', 'color: #000000 !important');
                }, 5000);
            return false;
        } else {
             $(".addbtn").prop('disabled', true);
            var totItem = parseInt($('input[name="totItem"]').val());
            selItem.push(totItem);
           var disc= MainSel.attr("data-discount");
           
             var instrction = $('#addInst').val();
            // if(instrction.trim().length>0){
            selItem.push(instrction);
            // }
            selItem.push(disc);
            //selItem.push($('#addInst').val());
            var ItemsStr = JSON.stringify(selItem);
            console.log(ItemsStr);
            var addonItemsStr = JSON.stringify(addOnItems);
            AajxUpdateCart(ItemsStr, addonItemsStr, 'add');
        }
 }
function showAddOns(mId, arId, iId, cId,discount)
{
  var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
  if (isMobile) {  modalHeight = 43; } else { modalHeight = 47; }
      //alert(mId+', '+arId+', '+iId+', '+cId+', '+JSON.stringify(allItems[cId]['sub_menu'][arId]));
      //alert(allItems[cId]['sub_menu']);
     console.log(discount);
      if(allItems[cId]) {
        $('.linear-background').hide();
        var itemData = getItems[mId]['value'][arId];
        // alert(itemData.image);
        $('.alert-danger').hide();      
        var modelBody = modelFooter = '';
        $('#AddOns').html(modelBody);
        $('#addonModal').modal('show');
        $('#addonModalLabel').html(itemData.item_name);
        if(itemData.image=='dummy.jpg'){ itemImg ='menu-src/noimage.jpg'; } else { itemImg ='https://deliveryguru.co.uk/admin/images/itemimages/'+itemData.image; }
        var modelBody = modelFooter = modelItem = '';
        
        if(itemData.image!='dummy.jpg'){
          modelItem += '<img src="'+itemImg+'" class="img-fluid item-img" style="width: 100%;height: 300px; object-fit: cover;" />';
          modalHeight = 20 +modalHeight;  
        }
        modelItem += '<p class="text-gray popup-desc">'+itemData.item_desc+'</p>';
        $('.itemDataModal').html(modelItem);

        var itemChild = allItems[cId]['sub_menu'][arId]['child'];
        
                        if(itemChild.length==0) { //alert(itemData.id);
                            $('.modal-body').css('height', modalHeight+'vh');
                              modelBody += '<input type="radio" style="display:none" checked onchange="addcatpop(this)" name="subMenuList" data-id="'+itemData.categories_id+'" data-name="'+(itemData.item_name).replace(/\"/g,"inch")+'" data-amount="'+itemData.item_name+'" data-discount="'+discount+'" data-subid="'+itemData.id+'"  item_name="" item_id="" addon_price="'+itemData.price+'" data-image="'+itemImg+'" onclick="validate(this,1);" value="'+itemData.id+', '+(itemData.item_name).replace(/\"/g,"inch")+', '+itemData.price+', '+itemImg+','+discount+'">';
                              var itemChild = allItems[cId]['sub_menu'][arId];
                              
                              if(itemChild['extra'].length>0) {
                                    var itemExtra = itemChild['extra'];
                                    var childID = itemChild['id'];
                                    //alert(itemExtra+'_'+childID+'_'+itemExtra.length);
                                    var txtJson = '';
                                    for (var j = 0; j < itemExtra.length; j++) 
                                    {
                                      
                                      var childMin = itemExtra[j].count;
                                      var childMax = itemExtra[j].maxcount;
                                        txtJson += '{'+j+', '+childMin+', '+childMax+', '+itemExtra[j].name+'}';
                                        var minCls='';
                                       // alert(childID);
                                        if(childMin==1 && childMax==1) { minCls = 'minY'; } 
                                          modelBody += '<div class="boxes '+minCls+'" id="box_'+childID+'_'+j+'" style="margin-top: 5%;"><div class="list-card-body" style="padding-top: 0px !important;margin-left: -5px;"><h6 class="mb-1"><a class="text-black">'+itemExtra[j].name+'</a></h6><p class="text-gray mb-2" style="font-size: 10px;">'+itemExtra[j].add_on_desc+'</p></div>';
                                          var itemAddon = itemExtra[j]['addon'];
                                          for (var k = 0; k < itemAddon.length; k++) 
                                          {
                                            SelQty='';
                                            if(childMin==1 && childMax==1) { Chk_Rad = 'radio'; SelQty=''; } else { 
                                                Chk_Rad = 'checkbox'; 
                                                SelQty += '<span class="addOnSel"><div class="btn-number" disabled="disabled" data-type="minus" data-field="addOnSel'+childID+'_'+j+'[1]"data-id="'+childID+'_'+j+'_'+k+'"><i class="bi bi-dash-circle inc_btn"></i></div><input type="text" class="form-control form-control-sm input-number '+childID+'_'+j+'_'+k+'" name="addOnSel'+childID+'_'+j+'[]" value="1" min="1" readonly max="'+childMax+'"><div class="btn-number" data-type="plus" data-field="addOnSel'+childID+'_'+j+'[1]" data-id="'+childID+'_'+j+'_'+k+'"><i class="bi bi-plus-circle inc_btn"></i></div></span>';
                                              }
                                            modelBody += '<div class="custom-control custom-'+Chk_Rad+'"><input type="'+Chk_Rad+'" class="custom-control-input  addonCls_'+childID+'" id="'+childID+'_'+itemAddon[k].addon_id+'" name="addOnCheck'+childID+'_'+j+'[]" value="'+childID+','+itemAddon[k].addon_id+','+itemAddon[k].addon_name.replace(/\"/g,"inch").replace(/,/g," ")+','+itemAddon[k].addon_price+'"><label class="custom-control-label col-5 col-sm-6" for="'+childID+'_'+itemAddon[k].addon_id+'" style="font-weight: 500;">'+itemAddon[k].addon_name.replace(/,/g," ")+'</label>'+SelQty+'';
                                            modelBody += '<small class="text-black-50 text-right" style="float: right; color: #34373b !important;font-weight: 500;"> £ '+Number(itemAddon[k].addon_price).toFixed(2)+'</small>';
                                            modelBody += '</div>';
                                          }
                                          modelBody += '</div>';
                                          if(itemExtra.length==(j+1)){
                                            //alert(txtJson);
                                            itemMinMax[childID] = txtJson.replaceAll("}{", "},{");
                                          }
                                        }
                                    }
                          }
                          else 
                          { 
                            $('.modal-body').css('height', (modalHeight+10)+'vh');
                            for (i = 0; i < itemChild.length; i++) 
                            {
                              
                              modelBody += '<div class="detail det_'+itemChild[i].id+'"><span class="summary" data-id="'+itemChild[i].id+'"><input type="radio" style="display:none" id="'+itemChild[i].id+'" onchange="addcatpop(this)" name="subMenuList" data-id="'+itemChild[i].categories_id+'" data-name="'+(itemChild[i].item_name.replace(/,/g," ")).replace(/\"/g,"inch")+'" data-discount="'+discount+'" data-amount="'+itemChild[i].item_name+'" data-subid="'+itemChild[i].id+'"  item_name="" item_id="" addon_price="'+itemChild[i].price+'" onclick="validate(this,1);" data-image="'+itemImg+'"  value="'+itemChild[i].id+', '+(itemChild[i].item_name.replace(/,/g," ")).replace(/\"/g,"inch")+', '+itemChild[i].price+', '+itemImg+','+discount+'"><span class="row col-12"><span class="col-10">'+itemChild[i].item_name+'</span><span  class="col-2 text-right" style="float:right;">£'+itemChild[i].price+'</span></span><div style="font-size: 11px;margin-bottom:5px;" class="addOn_'+itemChild[i].id+' addonDiv">';
                                  var itemExtra = itemChild[i]['extra'];
                                  var childID = itemChild[i].id;
                                  var txtJson = '';
                                  //alert(JSON.stringify(itemExtra));
                                  for (var j = 0; j < itemExtra.length; j++) 
                                  {
                                    var childMin = itemExtra[j].count;
                                    var childMax = itemExtra[j].maxcount;
                                      txtJson += '{'+j+', '+childMin+', '+childMax+', '+itemExtra[j].name+'}';
                                      //alert(itemExtra+'_'+childID+'_'+itemExtra.length);
                                        var minCls='';
                                        if(childMin==1 && childMax==1) { minCls = 'minY'; } 
                                        modelBody += '<div class="boxes '+minCls+'" id="box_'+childID+'_'+j+'"><div class="list-card-body" style="padding-top: 0px !important;margin-left: -5px;"><h6 class="mb-1"><a class="text-black">'+itemExtra[j].name+'</a></h6><p class="text-gray mb-2" style="font-size: 10px;">'+itemExtra[j].add_on_desc+'</p></div>';
                                        var itemAddon = itemExtra[j]['addon'];
                                        for (var k = 0; k < itemAddon.length; k++) 
                                        {
                                          SelQty='';
                                          if(childMin==1 && childMax==1) { Chk_Rad = 'radio'; SelQty=''; } else { 
                                              Chk_Rad = 'checkbox'; 
                                              SelQty += '<span class="addOnSel"><div class="btn-number" disabled="disabled" data-type="minus" data-field="addOnSel'+childID+'_'+j+'[1]"data-id="'+childID+'_'+j+'_'+k+'"><i class="bi bi-dash-circle inc_btn"></i></div><input type="text" class="form-control form-control-sm input-number '+childID+'_'+j+'_'+k+'" name="addOnSel'+childID+'_'+j+'[]" value="1" min="1" readonly max="'+childMax+'"><div class="btn-number" data-type="plus" data-field="addOnSel'+childID+'_'+j+'[1]" data-id="'+childID+'_'+j+'_'+k+'"><i class="bi bi-plus-circle inc_btn"></i></div></span>';
                                            }
                                          modelBody += '<div class="custom-control custom-'+Chk_Rad+'"><input type="'+Chk_Rad+'" class="custom-control-input  addonCls_'+childID+'" id="'+itemChild[i].id+'_'+itemAddon[k].addon_id+'" name="addOnCheck'+childID+'_'+j+'[]" value="'+itemChild[i].id+','+itemAddon[k].addon_id+','+itemAddon[k].addon_name.replace(/,/g," ")+','+itemAddon[k].addon_price+'"><label class="custom-control-label col-5 col-sm-6" for="'+itemChild[i].id+'_'+itemAddon[k].addon_id+'" style="font-weight: 500;">'+itemAddon[k].addon_name.replace(/,/g," ")+'</label>'+SelQty+'';
                                          modelBody += '<small class="text-black-50 text-right" style="float: right; color: #34373b !important;font-weight: 500;"> £ '+Number(itemAddon[k].addon_price).toFixed(2)+'</small>';
                                          modelBody += '</div>';
                                        }
                                        modelBody += '</div>';
                                        if(itemExtra.length==(j+1)){
                                          itemMinMax[childID] = txtJson.replaceAll("}{", "},{");
                                        }
                                  }
                                modelBody += '</div></div>';
                                $('.addInst').hide();
                                $('#addInst').val('');
                                } 
                      }
                    modelBody += '<span class=""><b style="padding: 15px 0;font-size: 16px;">Special request to suit your taste buds</b><br><textarea type="text" placeholder="Please leave any special request e.g. Extra spicy, no sauce, well done  etc. Please do not leave any allergen info here." row="4" name="instr" id="addInst" class="col-12 form-control" style="font-size: 14px;maxlength="100" onpaste="return false;" onCopy="return false" onkeypress="allowAlphaNumericSpace(event)" onCut="return false" onDrag="return false" onDrop="return false"></textarea><p style="font-size:12px;text-align:right;">(Max 100 characters)</p></span>';
                    $('.linear-background').hide();
                    $('#AddOns').html(modelBody);
                          modelFooter += '<span class="count-number float-left"><button class="btn btn-outline-secondary  btn-sm left dec" onclick="minusValue('+iId+')"><i class="bi bi-dash-circle"></i></button><input id="'+iId+'" class="count-number-input totItem" type="number" name="totItem" value="1" readonly=""><button class="btn btn-outline-secondary btn-sm right inc" onclick="plusValue('+iId+')"><i class="bi bi-plus-circle"></i></button></span><button id="addButton'+iId+'" type="button" class="btn btn-primary addbtn float-right redbg" data-dismiss="modal" amount="'+itemData.price+'" item_id="'+iId+'" discount="'+itemData.discount+'" qty="1" status="0" notes="1" item_name = "'+(itemData.item_name).replace(/\"/g,"inch")+'" '; 
                        if(itemData.discount.length == 0 && itemData.discount.length == 0){
                            modelFooter += 'onclick="addItem(this)"';
                        }
                        modelFooter += ' onclick="btnAddToCart()">Add to cart</button>';
                    $('.modal-footer').html(modelFooter);
      
      
      } else {
        showAddOnsAjax(mId, arId, iId, cId,discount);
      }
     
}
</script>


</html>