<?php

function getHotelAPI() {
   //fetch lead info from Hotel API
   $graph_url= 'https://deliveryguru.co.uk/dg_api/getRestaurantsDetails/62';
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $graph_url);
   curl_setopt($ch, CURLOPT_HEADER, 0);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
   $output = curl_exec($ch); 
   curl_close($ch);
   return json_decode($output, true); 
}
$getHotelDetails = getHotelAPI();
?>  
  <section class="restaurant-detailed-banner">
         <div class="text-center">
            <img class="img-fluid cover" src="img/mall-dedicated-banner.png">
         </div>
         <div class="restaurant-detailed-header">
            <div class="container-fluid" style="max-width: 1600px;">
               <div class="row d-flex align-items-end">
                  <div class="col-md-8">
                     <div class="restaurant-detailed-header-left">
                        <img class="img-fluid mr-3 float-left" alt="osahan" src="https://deliveryguru.co.uk/admin/images/itemimages/9796Untitled-1.jpg">
                        <h2 class="text-white"><?php echo $getHotelDetails[0]['hotel_name'];?></h2>
                        <p class="text-white mb-1"><i class="icofont-location-pin"></i> <?php echo $getHotelDetails[0]['address'];?>
                      
                        </p>
                        <p class="text-white mb-0"><i class="icofont-food-cart"></i> <?php echo $getHotelDetails[0]['type'];?>
                        </p>
                        <p class="text-white mb-0"><i class="icofont-clock-time"></i>Monday-Sunday 5pm-5am
                          <?php if($getHotelDetails[0]['status']==0){?>
                        <span class="badge badge-success">OPEN</span>
                        <?php } else{?>
                        <span class="badge badge-danger">CLOSED</span>
                        <?php } ?>
                        </p>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="restaurant-detailed-header-right text-right">
                        <button class="btn btn-success" type="button"><i class="icofont-clock-time"></i> OPEN NOW </button>
                        <select class="btn btn-success" onchange="dayorder(this)">
                            <option>ORDER NOW</option>
                            <option>LATER</option>
                        </select>
                              <div class="ui calendar" id="preorderdateformat" style="display:none;">
                            <label>Shedule Date and TIme : </label>
    <div class="ui input left icon">
      <i class="calendar icon"></i>
      <input type="text" id="preorderdate" placeholder="Date/Time">
    </div>
  </div>
                        <!--<button class="btn btn-success" type="button"><i class="icofont-clock-time"></i> </button>-->
                        <!--<h6 class="text-white mb-0 restaurant-detailed-ratings"><span class="generator-bg rounded text-white"><i class="icofont-star"></i> 3.1</span> 23 Ratings  <i class="ml-3 icofont-speech-comments"></i> 91 reviews</h6>-->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section> 