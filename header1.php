<nav class="navbar navbar-expand-lg navbar-light bg-light osahan-nav shadow-sm" style="height: 70px;">
         <div class="container">
            <a class="navbar-brand" href="index.php"><img alt="logo" src="https://grill-guru.co.uk/img/grill-guru-full-landingLogo.png" style="width: 60px;"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
               <ul class="navbar-nav ml-auto">
                  <li class="nav-item active">
                     <a class="nav-link" href="index.php">Home </a>
                  </li>
                  <li class="nav-item active">
                     <a class="nav-link" href="menu.php">Menu </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="deals.php"><i class="icofont-sale-discount"></i> Deals </a>
                  </li>
                 
                  <li class="nav-item">
                     <a class="nav-link" href="contact.php"> Contact Us</a>
                  </li>
                  <?php if(!isset($_SESSION['uid'])){?>
                  <li class="nav-item">
                     <a class="nav-link" href="login.php"> Login </a>
                  </li>
                  <?php } ?>
                   <?php if(isset($_SESSION['uid'])){?>
                  <li class="nav-item">
                     <a class="nav-link" href="myaccount.php"> My Account </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="myaccount.php"> Orders </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="logout.php"> Logout </a>
                  </li>
                  <?php } ?>
               </ul>
            </div>
         </div>
      </nav>