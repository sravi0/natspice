/*$(document).ready(function() {
	'use strict';
   	  HeaderVideo.init({
      container: $('.header-video'),
      header: $('.header-video--media'),
      videoTrigger: $("#video-trigger"),
      autoPlayVideo: true
    });    

});*/
jQuery(function($){
$( document ).ready(function( $ ) {
	$('#cat_nav').mobileMenu();
		$( '#Img_carousel' ).sliderPro({
			width: 960,
			height: 500,
			fade: true,
			arrows: true,
			buttons: false,
			fullScreen: false,
			smallSize: 500,
			startSlide: 0,
			mediumSize: 1000,
			largeSize: 3000,
			thumbnailArrows: true,
			autoplay: false
		});

		$(function() {
	 'use strict';
	  $('a[href*=\\#]:not([href="#"])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top - 70
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});

jQuery('#sidebar').theiaStickySidebar({
      additionalMarginTop: 80
    });


	});


$( document ).ready(function( $ ) {
		$( '#Img_carousel' ).sliderPro({
			width: 960,
			height: 500,
			fade: true,
			arrows: true,
			buttons: false,
			fullScreen: false,
			smallSize: 500,
			startSlide: 0,
			mediumSize: 1000,
			largeSize: 3000,
			thumbnailArrows: true,
			autoplay: false
		});
	});

(function ($){
$(function () {
		 'use strict';
        $("#range").ionRangeSlider({
            hide_min_max: true,
            keyboard: true,
            min: 0,
            max: 15,
            from: 0,
            to:10,
            type: 'double',
            step: 1,
            prefix: "Km ",
            grid: true
        });
    });
})(jQuery);



	$(document).ready(function(){


 $(".filter_type input:checkbox").change(function() {
   var chkArray = [];
   var chk2Array = [];
	
	/* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
	$(".a:checked").each(function() {
		chkArray.push($(this).val());
	});
	$(".b:checked").each(function() {
		chk2Array.push($(this).val());
	});
	
	/* we join the array separated by the comma */
	var selected, option;

	selected = chkArray.join(',');
	option = chk2Array.join(',');
	
	/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
	if(selected.length >= 0 ||  options.length >= 0){
		 
		 $.ajax({
			    	url: "<?php echo get_template_directory_uri();?>/ajax-filter.php",
			    	type       : "GET",
			    	data       : {types : selected, option : option},

			    	 beforeSend:function() { 

         $("#show").append("<img  src='<?php echo get_template_directory_uri();?>/img/load.gif' id='loading-excel' />");
     },
     complete:function() {
           $("#loading-excel").remove();
     },
			    	success: function(result){
			    		$('#loading-excel').fadeOut('7000');
			            $("#show").html(result);
			        }
			    });
	}else{
		alert("Please at least one of the checkbox");	
	}
 });


$('#cat_nav').mobileMenu();
});


});