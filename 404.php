<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="">

           <title>Natural Spice Tandoori Glasgow | Takeaway & Delivery Near Me</title>
<meta name="description" content="Natural Spice Tandoori in Glasgow Is the best place for Indian, Italian & Turkish food that offers Takeaway, Delivery in Cardonald, Glasgow. Order Now Online" />
<meta name="keywords" content="Natural spice Glasgow, local restaurants near me, burger delivery near me, Natural spice Glasgow menu, Indian restaurant Near  Me, Italian restaurant Near Me, Fast food delivery, Restaurants near me, Online Food delivery service, takeaway restaurant near me, pizza restaurant near me, delivery places near me, Turkish restaurant near me, burger deals near me, indian meals near me, best restaurant near me, Special offers near me, best indian takeaway near me, burger delivery near me, home delivery food near me" />
<meta property="og:title" content="Natural Spice Online Glasgow | Takeaway & Delivery| G52 3SS">
<meta property="og:site_name" content="Natural spice Tandoori ">
<meta property="og:url" content="https://www.natural-spiceonline.co.uk/">
<meta property="og:description" content="Natural Spice Online Glasgow Is A Takeaway Serving Sensational Indian, Italian & Turkish  Food With Takeaway Food In 1791, Paisley Road West, Cardonald, Glasgow">
<meta property="og:type" content="restaurant">
<meta property="og:image" content="https://www.natural-spiceonline.co.uk/img/logo1.png">
         <link rel="icon" type="image/png" sizes="16x16" href="https://grill-guru.co.uk/img/favicon.png">

    <!-- Bootstrap core CSS -->
    <link href="https://grill-guru.co.uk/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="https://grill-guru.co.uk/newdev/css/modern-business.css" rel="stylesheet">
  </head>
<style>
    body html { font-family: 'DM Sans', sans-serif !important; }
    .navbar { padding:0;     border-bottom: 1px solid #efefef; }
    .navbar-dark .navbar-nav .nav-link {
        color: rgb(0 0 0) !important; text-align:center;
    }
   .bg1 { background-image: url("img/food1.jpg"); }
    .bg2 { background-image: url("img/food2.jpg"); }
    .bg3 { background-image: url("img/food3.jpg"); }
    .bg4 { background-image: url("img/food4.jpg"); }
    .bg5 { background-image: url("img/food5.jpg"); }
    .bg6 { background-image: url("img/food6.jpg"); }
    .bg_shadow {     background: rgb(76 70 70 / 50%); }
    .bg_shadow_white {     background: rgb(76 70 70 / 50%); }
    .top_left { top: 20%; left: 35%; }
    .top_right { top: 20%; right: 35%; }
    .bot_left { bottom: -10%; left: 35%; }
    .bot_right { bottom: -14%; right: -24%; }
    .middle { top: 50%; left: 50%; }

    .hero-image {
        
        background-color: #cccccc;
        height: 500px;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        position: relative;
      }
      
      .hero-text {
        position: absolute;
        /* transform: translate(-50%, -50%); */
        color: white;
        padding: 10px;
        text-align: center;
        bottom: 0%;
        background: #0000001a !important;
        width: 100%;
      }
      
     .portfolio-item { margin-bottom:5px; }
     .col-lg-6.portfolio-item {
        padding: 5px;
        margin-bottom: 0px;
     }
     .h1_title { font-size: 30px; }
     .nav-link {
        font-size: 18px !important;
     }
     .btnbook {
        background: #72c0d2;
        color: white !important;
     }
     a.nav-link {
        padding: 5px !important;
    }
   
   
@media only screen and (min-width: 1600px) {
  .banner {
        background-color: #cccccc;
        height: 80vh;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        position: relative;
        background-image: url(img/banner.jpeg);
        /* height: 100%; */
        position: relative;
        margin-top: 80px;
        }
    .mt-5, .my-5 {
        margin-top: -10% !important;
    }
}
@media only screen and (max-width: 1600px) {
 .banner {
        background-color: #cccccc;
        height: 80vh;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        position: relative;
        background-image: url(img/banner.jpeg);
        /* height: 100%; */
        position: relative;
        margin-top:
        margin-top: 80px;
    }
    .mt-5, .my-5 {
    margin-top: 3rem!important;
    }
}

    @media screen 
    and (min-device-width: 0px) 
    and (max-device-width: 991px) {
        .mobile-banner { display: block; }
        .desktop-banner { display: none; }
        .banner-text {
            position: absolute;
            /* transform: translate(-50%, -50%); */
            color: white;
            padding: 10px;
            text-align: center;
            bottom: 10%;
            background: #0000001a !important;
            width: 100%;
          }
    }
    @media screen 
    and (min-device-width: 991px) 
    and (max-device-width: 3600px) 
    and (-webkit-min-device-pixel-ratio: 1) { 
        .mobile-banner { display: none; }
        .desktop-banner { display: block; }
        .banner-text {
            position: absolute;
            
            color: white;
            padding: 10px;
            text-align: center;
            bottom: 0%;
            background: #0000001a !important;
            width: 100%;
          }
    }
    @media screen 
    and (min-device-width: 991px) 
    and (max-device-width: 1600px) 
    and (-webkit-min-device-pixel-ratio: 1) { 
        .banner-text {
            transform: translate(-25%, -30%);
        }
    }
    @media screen 
    and (min-device-width: 1600px) 
    and (max-device-width: 3600px) 
    and (-webkit-min-device-pixel-ratio: 1) { 
        .banner-text {
            transform: translate(-16%, -30%);
        }
    }
    @media (max-width: 991.98px) {
        .navbar-expand-lg>.container, .navbar-expand-lg>.container-fluid {
             padding-right: 10px; 
             padding-left: 10px; 
        }
        .h1_title { font-size: 24px; }
        .menu-btn {
            margin: 10px;
        }
        li.nav-item.social {
            text-align: center;
        }
    }
    .navbar-dark .navbar-toggler-icon {
        background-image: url("img/mobile-menu-8-834368.png");
    }
    .btn-trans {
        background: red;
        cursor: pointer;
        border: 1px solid red;
        margin-top:0%;
        width: 200px;
    box-shadow: 3px 3px 5px #6a6a6a;
}
    .text-black { color: #f9f7f7; border-color: #f5f9f6; }
    .footerWrap {
    color: #fff;
    font-size: 18px;
    text-align: center;
}
footer {
    background-color: #000000;
    font-size: 11px;
    height: auto;
    width: 100%;
}
.card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
     border: none !important; 
    border-radius: .25rem;
}
.navbar-nav li {
    padding-left: 25px;
    padding-right: 25px;
}
.navbar-dark .navbar-nav  .menu-btn { color: white !important;
    padding-left: 15px !important;
    padding-right: 15px !important; 
}
.simple_social {  margin: 0 16px 0 0; font-size:20px; }
.cl-dar, .col-lg-12.portfolio-item, .copyright {
    font-family: 'DM Sans', sans-serif !important;
}
h2.cl-dar {
    font-size: 30px;
}
.banner1 {
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 35px;
    background: #121d2b;
    position: relative;
}

@media only screen and (max-width: 600px) {
#promoModal .modal-dialog {
    height: 50%;
    width: 100%;
    margin-left:2%;
    margin: auto;
    margin-top: 10%;
    font-family: 'DM Sans', sans-serif !important;
}
}
@media only screen and (min-width: 600px) {
#promoModal .modal-dialog {
    height: 50%;
    width: 80%;
    margin: auto;
    margin-top: 10%;
    font-family: 'DM Sans', sans-serif !important;
}
}
#snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color:red;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 1;
    left: 50%;
    bottom: 30px;
    font-size: 17px;
  }
  
  #snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
  }
  
  @-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;} 
    to {bottom: 30px; opacity: 1;}
  }
  
  @keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
  }
  
  @-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;} 
    to {bottom: 0; opacity: 0;}
  }
  
  @keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
  }
#promoModal  .text1 {
    background-color: red;
    color: white;
    width: 50%;
    text-align: center;     padding: 10px;
    margin: 30px auto;
}

#promoModal  .icon2 {
    color: red; font-size: 150px;
}

#promoModal  .modal-header {
    color: white;
    background-color: red
}

#promoModal  .openmodal {
    margin-left: 35%;
    width: 25%;
    margin-top: 25%
}
.copypromo { color: #fff !important; }
@media (min-width:320px)  { 
  
  div#panel_groups {
      border-right: none;
  }
}
.btn-outline-dark {
    color: red;     border-color: red;
}
.btn-outline-danger {
    color: red;
    background-color: transparent;
    background-image: none;
    border-color: red;
}
.btn-outline-danger:hover{
     color: #fff;
    background-color: red;
    border-color: red;
}
.btn-outline-danger:not(:disabled):not(.disabled).active, .btn-outline-danger:not(:disabled):not(.disabled):active, .show>.btn-outline-danger.dropdown-toggle {
    color: #fff;
    background-color: red;
    border-color: red;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}


</style>
  <body>
        <!--Modal Launch Button-->



      <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-white fixed-top">
          <div class="container">
              <a class="navbar-brand" href="https://www.natural-spiceonline.co.uk/index.php"><img src="img/logo1.png" style="height: 70px;" /></a>
              <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse justify-content-center" id="navbarResponsive">
                  <ul class="navbar-nav">

                      <li class="nav-item">
                          <a class="nav-link" href="https://www.natural-spiceonline.co.uk/menu.php">Menu</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="https://www.natural-spiceonline.co.uk/deals.php">Deals</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="https://www.natural-spiceonline.co.uk/contact.php">Contact</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="https://www.natural-spiceonline.co.uk/login.php">Login</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link btn btn-md btn-danger  menu-btn"  href="https://www.natural-spiceonline.co.uk/menu.php">Order Now</a>
                      </li>
                      <!--<li class="nav-item">-->
                      <!--    <a class="nav-link btn btn-md btn-danger  menu-btn" href="#">Book A Table</a>-->
                      <!--</li>-->
                     
                    <li class="nav-item social">
                            <a href="" target="_blank">
                                <span><i class="fa fa-facebook  simple_social  simple_social" style="color: #1877f2;"></i></span>
                            </a>
                            <a href="" target="_blank">
                                <span><i class="fa fa-instagram  simple_social  simple_social" style="color: #c32aa3;"></i></span>
                            </a>
                            <a href="" target="_blank">
                                <span><i class="fa fa-youtube  simple_social  simple_social" style="color: #ff0000;"></i></span>
                            </a>
                            <a href="" target="_blank">
                                <span><i class="fa fa-apple  simple_social  simple_social" style="color: #a6b1b7;"></i></span>
                            </a>
                            <a href="" target="_blank">
                                <span><i class="fa fa-android  simple_social  simple_social" style="color: #1db954;margin: 0 16px 0 0;font-size: 24px;"></i></span>
                            </a>
                    </li>
                    
                  </ul>
              </div>
          </div>
      </nav>
      
     <div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="error-template" style="text-align: center;margin-top: 14px;">
                <img src="img/HTML-404-Page-Animated.png">
                <div class="error-actions">
                    <a href="https://www.natural-spiceonline.co.uk/index.php" class="btn btn-primary btn-lg" style="background:red;margin-top: 2%;"><span class="fa fa-home"></span>
                        Take Me Home </a>
                </div>
            </div>
        </div>
    </div>
</div>
      <div class="w-screen pt-4" style="background-color: #ffffff;">
          <div class="container">
              <div class="col-md-12">
                  <h2 class="cl-dar text-center">A Recognized Track-Record of Excellence</h2>
                 <div class="row text-center">
                     <div class="col-md-3">
                        <img src="img/f3.png"> 
                     </div>
                       <div class="col-md-3">
                         <img src="img/f1.png"> 
                     </div>
                      <div class="col-md-3">
                         <img src="img/f2.png"> 
                     </div>
                      <div class="col-md-3">
                         <img src="img/f4.png"> 
                     </div>
            
              </div>  
              </div>
             
          </div>
      </div>

      </div>



      <footer>
          
          <div class="container">

              <hr />
              <div class="footerWrap col-md-12 col-md-offset-1">
                  <div class="copyright col-md-12">
                      <p style="font-size:15px;">© Copyright 2021 <a href="https://deliveryguru.co.uk/" target="_blank" style="color: #ff296d !important;">DeliveryGuru</a> All rights reserved.</p>
                  </div>
              </div>
          </div>
      </footer>
      <!-- Bootstrap core JavaScript -->
      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-formhelpers/2.3.0/js/bootstrap-formhelpers.min.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-formhelpers/2.3.0/css/bootstrap-formhelpers.min.css" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-formhelpers/2.3.0/img/bootstrap-formhelpers-currencies.flags.png" />
      <script>
        function promoModal()
        {
            $('.alert-danger').hide();
            $('#promoModal').modal('show');
            //$('#addonModal').modal({backdrop: 'static', keyboard: false });
        }
        
        promoModal();
        $('.copypromo').click(function(){
            text  = $(this).attr('data-id');
            //$('#promoModal').modal('hide');
            var input = document.createElement('input');
            input.setAttribute('value', text);
            document.body.appendChild(input);
            input.select();
            var result = document.execCommand('copy');
            document.body.removeChild(input);
            $('#promoModal').modal('hide');
            $('.close').click();
          
            var x = document.getElementById("snackbar");
            $('#snackbar').css('background-color', 'green');
            x.innerHTML="Copied the Promo Code: " + text;
            x.className = "show";
        
            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            return result;
          
        });
    </script>

  </body>

</html>
